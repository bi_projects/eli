# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Admin::AdminUser.all.blank?
	admin_user = Admin::AdminUser.create!(:email => "admin@example.com", :password => "123456", :name => 'admin')
end

if Facturacion::BillingUser.all.blank?
	billing_user = Facturacion::BillingUser.create!(:email => "admin@example.com", :password => "123456", :name => 'admin')
end

ResourceType.find_or_create_by!(:description => "Catalogo")
ResourceType.find_or_create_by!(:description => "Reporte")

=begin
if Admin::Resource.all.blank?
	module_dirs = ['academico', 'facturacion']
	module_dirs.each do |module_dir|
		module_name = "#{module_dir}".camelize
		Dir[Rails.root.join("app/models/#{module_dir}/*.rb").to_s].each do |filename|
		  klass = File.basename(filename, '.rb').camelize
			klass_full_name = "#{module_name}::#{klass}"
			next if !klass_full_name.constantize.ancestors.include?(ActiveRecord::Base) || (['Admin::Resource', 'Academico::AcademicoAbility', 'Admin::Permission'].include?(klass_full_name))
			puts "saving resource class #{klass_full_name}"
			Admin::Resource.create(:description => klass_full_name,
																											:model => klass_full_name,
																											:module => module_name,
																											:resource_type_id => ResourceType.first.id)
		end
	end
end
=end

Admin::Resource.find_or_create_by!(:description => 'Academico::AgeRange', :model => 'Academico::AgeRange', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::AcademicUser', :model => 'Academico::AcademicUser', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::UserRole', :model => 'Academico::UserRole', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::LevelProgramBook', :model => 'Academico::LevelProgramBook', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Student', :model => 'Academico::Student', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::AcademicoRole', :model => 'Academico::AcademicoRole', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Room', :model => 'Academico::Room', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Program', :model => 'Academico::Program', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Preregistration', :model => 'Academico::Preregistration', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Level', :model => 'Academico::Level', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::LevelProgram', :model => 'Academico::LevelProgram', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Course', :model => 'Academico::Course', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Book', :model => 'Facturacion::Book', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Carrier', :model => 'Facturacion::Carrier', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::CompanyType', :model => 'Facturacion::CompanyType', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Tutor', :model => 'Facturacion::Tutor', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::StudentTutor', :model => 'Facturacion::StudentTutor', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::DocumentType', :model => 'Facturacion::DocumentType', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::StudentLevelProgram', :model => 'Facturacion::StudentLevelProgram', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::BillingUser', :model => 'Facturacion::BillingUser', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Preregistration', :model => 'Facturacion::Preregistration', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::CostCenter', :model => 'Facturacion::CostCenter', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::StudentCompany', :model => 'Facturacion::StudentCompany', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Discount', :model => 'Facturacion::Discount', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::StudentDiscount', :model => 'Facturacion::StudentDiscount', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::StudentCarrier', :model => 'Facturacion::StudentCarrier', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Stock', :model => 'Facturacion::Stock', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Company', :model => 'Facturacion::Company', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Warehouse', :model => 'Facturacion::Warehouse', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Course', :model => 'Academico::Course', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Staff', :model => 'Academico::Staff', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::EvaluationScheme', :model => 'Academico::EvaluationScheme', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::EvaluationParameter', :model => 'Academico::EvaluationParameter', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::EvaluationParameterType', :model => 'Academico::EvaluationParameterType', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseSchedulle', :model => 'Academico::CourseSchedulle', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::Temary', :model => 'Academico::Temary', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseEvaluation', :model => 'Academico::CourseEvaluation', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseResult', :model => 'Academico::CourseResult', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::TeacherAttendance', :model => 'Academico::TeacherAttendance', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::AccountingAccount', :model => 'Facturacion::AccountingAccount', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::StudentReportsController', :model => 'Academico::StudentReportsController', :module => 'Academico', :resource_type_id => 2)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseStudentReportsController', :model => 'Academico::CourseStudentReportsController', :module => 'Academico', :resource_type_id => 2)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseQuotaReportsController', :model => 'Academico::CourseQuotaReportsController', :module => 'Academico', :resource_type_id => 2)
Admin::Resource.find_or_create_by!(:description => 'Academico::CourseModality', :model => 'Academico::CourseModality', :module => 'Academico', :resource_type_id => ResourceType.first.id)
Admin::Resource.find_or_create_by!(:description => 'Academico::StudentAgesReportsController', :model => 'Academico::StudentAgesReportsController', :module => 'Academico', :resource_type_id => 2)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Receipt', :model => 'Facturacion::Receipt', :module => 'Facturacion', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Invoice', :model => 'Facturacion::Invoice', :module => 'Facturacion', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::Note', :model => 'Facturacion::Note', :module => 'Facturacion', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::DailyProcess', :model => 'Facturacion::DailyProcess', :module => 'Facturacion', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::PaymentDetailConcept', :model => 'Facturacion::PaymentDetailConcept', :module => 'Facturacion', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Academico::TimeFrame', :model => 'Academico::TimeFrame', :module => 'Academico', :resource_type_id => 1)
Admin::Resource.find_or_create_by!(:description => 'Facturacion::CourseBookRegistration', :model => 'Facturacion::CourseBookRegistration', :module => 'Facturacion', :resource_type_id => ResourceType.first.id)

Action.find_or_create_by!(:id => 1, :description => 'index')
Action.find_or_create_by!(:id => 2, :description => 'show')
Action.find_or_create_by!(:id => 3, :description => 'create')
Action.find_or_create_by!(:id => 4, :description => 'update')
Action.find_or_create_by!(:id => 5, :description => 'new')
Action.find_or_create_by!(:id => 6, :description => 'edit')
Action.find_or_create_by!(:id => 7, :description => 'destroy')
Action.find_or_create_by!(:id => 8, :description => 'destroy_multiple')
Action.find_or_create_by!(:id => 9, :description => 'filtering_params')

Action.all.each do |action|
	Admin::Resource.all.each do |admin_resource|
		Admin::Permission.find_or_create_by!(:action_id => action.id, :admin_resource_id => admin_resource.id)
	end
end

academico_role = Academico::AcademicoRole.find_or_create_by!(:name => 'academico')
academico_permissions = Admin::Permission.joins(:admin_resource).where('admin_resources.module' => 'Academico')
academico_permissions.each do |permission|
	academico_role.academico_role_permissions.find_or_create_by!(admin_resource_id: permission.admin_resource.id, action_id: permission.action.id, academico_academico_role_id: academico_role.id)
end

if Academico::AcademicUser.all.blank?
	academic_user = Academico::AcademicUser.create(:email => "admin@example.com", :password => "123456", :name => "admin")
	academic_user.save!
	academico_role = Academico::AcademicoRole.find_by(:name => 'academico')
	academic_user.academico_user_roles.create(academico_academico_roles_id: academico_role.id)
	academic_user.save!
end

if Academico::AgeRange.all.blank?
	Academico::AgeRange.create(:name => "Niños", :min_age => 8, :max_age => 12, :description => "Niños")
	Academico::AgeRange.create(:name => "Adolescentes", :min_age => 13, :max_age => 17, :description => "Adolescentes")
	Academico::AgeRange.create(:name => "Adultos", :min_age => 18, :max_age => 99, :description => "Adultos")
end

if Academico::Program.all.blank?
	#TODO remplazar IDs de age_range estaticos
	Academico::Program.create(:name => "Niños", :academico_age_range_id => 1, :price => 20.0, :description => "Niños")
	Academico::Program.create(:name => "Adolescentes", :academico_age_range_id => 2, :price => 20.0, :description => "Adolescentes")
	Academico::Program.create(:name => "Adultos", :academico_age_range_id => 3, :price => 20.0, :description => "Adultos")
end

(1..16).each do |level|
	Academico::Level.find_or_create_by!(:name => level.to_s, :level_sequence => level)
end

Academico::Program.all.each do |program|
	Academico::Level.all.each do |level|
		Academico::LevelProgram.find_or_create_by!(academico_level_id: level.id, academico_program_id: program.id)
	end
end

Admin::Module.find_or_create_by!(id: 1, name: 'Admin')
Admin::Module.find_or_create_by!(id: 2, name: 'Academico')
Admin::Module.find_or_create_by!(id: 3, name: 'Facturacion')

Admin::Position.find_or_create_by!(id: 1, name: 'Docente', description: 'Personal Docente', admin_module_id: 2)
Admin::Position.find_or_create_by!(id: 2, name: 'Coordinador', description: 'Coordinador Docente', admin_module_id: 2)
Admin::Position.find_or_create_by!(id: 3, name: 'Supervisor', description: 'Supervisor', admin_module_id: 2)


if Facturacion::DocumentType.all.blank?
	Facturacion::DocumentType.create(:id => 1, :name => 'Factura')
	Facturacion::DocumentType.create(:id => 2, :name => 'Recibo')
	Facturacion::DocumentType.create(:id => 3, :name => 'Nota Crédito')
	Facturacion::DocumentType.create(:id => 4, :name => 'Nota Débito')
	Facturacion::DocumentType.create(:id => 5, :name => 'Comprobante')
end

if Academico::CourseStatus.all.blank?
	Academico::CourseStatus.create(id: 1, description: 'Inscripcion')
	Academico::CourseStatus.create(id: 2, description: 'En curso')
	Academico::CourseStatus.create(id: 3, description: 'Terminado')
end

if Academico::RegistrationStatus.all.blank?
	Academico::RegistrationStatus.create(id: 1, description: 'Pendiente')
	Academico::RegistrationStatus.create(id: 2, description: 'Completa')
end

if Academico::PreregistrationStatus.all.blank?
	Academico::PreregistrationStatus.create(id: 1, description: 'Creado')
	Academico::PreregistrationStatus.create(id: 2, description: 'Matriculado')
end

Academico::StaffStatus.find_or_create_by!(id: 1, description: 'Activo')
Academico::StaffStatus.find_or_create_by!(id: 2, description: 'Inactivo')

if Facturacion::PreregistrationStatus.all.blank?
	Facturacion::PreregistrationStatus.create(id: 1, description: 'Creado')
	Facturacion::PreregistrationStatus.create(id: 2, description: 'Matriculado')
end

Facturacion::InvoiceStatus.find_or_create_by!(id: 1, description: 'Creada')
Facturacion::InvoiceStatus.find_or_create_by!(id: 2, description: 'Anulada')

Facturacion::ReceiptStatus.find_or_create_by!(id: 1, description: 'Creado')
Facturacion::ReceiptStatus.find_or_create_by!(id: 2, description: 'Anulado')

if Facturacion::DocumentType.where(:name => 'Inventario').blank?
	Facturacion::DocumentType.create(:id => 6, :name => 'Inventario')
end


Facturacion::AccountToRecord.find_or_create_by(id: 1, description: 'CUENTAS POR COBRAR')
Facturacion::AccountToRecord.find_or_create_by(id: 2, description: 'CUENTAS POR PAGAR')
Facturacion::AccountToRecord.find_or_create_by(id: 3, description: 'CUENTA BANCARIA')
Facturacion::AccountToRecord.find_or_create_by(id: 4, description: 'TARJETAS DE CREDITO')
Facturacion::AccountToRecord.find_or_create_by(id: 5, description: 'RETENCIONES')
Facturacion::AccountToRecord.find_or_create_by(id: 6, description: 'IMPUESTO IVA')
Facturacion::AccountToRecord.find_or_create_by(id: 7, description: 'OTROS INGRESOS')
Facturacion::AccountToRecord.find_or_create_by(id: 8, description: 'INVENTARIO')
Facturacion::AccountToRecord.find_or_create_by(id: 9, description: 'COSTOS DE INVENTARIO')
Facturacion::AccountToRecord.find_or_create_by(id: 10, description: 'INGRESOS FINANCIEROS (No Facturados)')
Facturacion::AccountToRecord.find_or_create_by(id: 11, description: 'GASTOS NO DEDUCIBLES')
Facturacion::AccountToRecord.find_or_create_by(id: 12, description: 'NINGUNO')
Facturacion::AccountToRecord.find_or_create_by(id: 13, description: 'COSTOS')
Facturacion::AccountToRecord.find_or_create_by(id: 14, description: 'DEPRECIACION')

Facturacion::AccountingAccountCategory.find_or_create_by(id: 1, description: 'ACTIVO')
Facturacion::AccountingAccountCategory.find_or_create_by(id: 2, description: 'PASIVO')
Facturacion::AccountingAccountCategory.find_or_create_by(id: 3, description: 'CAPITAL')
Facturacion::AccountingAccountCategory.find_or_create_by(id: 4, description: 'EGRESO')
Facturacion::AccountingAccountCategory.find_or_create_by(id: 5, description: 'INGRESO')

Facturacion::AccountType.find_or_create_by(id: 1, name: 'DETALLE')
Facturacion::AccountType.find_or_create_by(id: 2, name: 'MAYOR')

Facturacion::InvoiceDetailStatus.find_or_create_by(id: 1, description: 'Activo')
Facturacion::InvoiceDetailStatus.find_or_create_by(id: 2, description: 'Inactivo')

if Academico::TimeFrameStatus.all.blank?
  Academico::TimeFrameStatus.create(id: 1, description: 'Activo')
  Academico::TimeFrameStatus.create(id: 2, description: 'Inactivo')
end