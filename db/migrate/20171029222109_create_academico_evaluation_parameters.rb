class CreateAcademicoEvaluationParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_evaluation_parameters do |t|
      t.string :name

      t.timestamps
    end
  end
end
