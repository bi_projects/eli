class AddCommentToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_preregistrations, :note, :string
  end
end
