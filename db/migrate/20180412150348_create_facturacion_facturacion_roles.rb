class CreateFacturacionFacturacionRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_facturacion_roles do |t|
      t.string :name

      t.timestamps
    end
  end
end
