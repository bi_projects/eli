class CreateAcademicoSessionAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_session_assignments do |t|
      t.references :academico_session_parameters, foreign_key: true, index: {:name => "index_for_academico_session_assignment"}
      t.string :description

      t.timestamps
    end
  end
end
