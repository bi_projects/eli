class AddBillingUsersToFacturacion < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoices, :facturacion_billing_user, foreign_key: true
    add_reference :facturacion_receipts, :facturacion_billing_user, foreign_key: true
    add_reference :facturacion_notes, :facturacion_billing_user, foreign_key: true
  end
end
