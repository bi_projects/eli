class AddUpdateUserToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_courses, :created_by, :integer
    add_column :academico_courses, :updated_by, :integer
    add_foreign_key :academico_courses, :academico_staffs, column: :created_by, foreign_key: true, index: { name: 'idx_course_created_by'}
    add_foreign_key :academico_courses, :academico_staffs, column: :updated_by, foreign_key: true, index: { name: 'idx_course_updated_by'}
  end
end
