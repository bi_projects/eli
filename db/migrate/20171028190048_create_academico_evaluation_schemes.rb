class CreateAcademicoEvaluationSchemes < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_evaluation_schemes do |t|
      t.references :academico_programs, foreign_key: true

      t.timestamps
    end
  end
end
