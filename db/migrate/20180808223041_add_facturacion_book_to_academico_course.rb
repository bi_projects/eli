class AddFacturacionBookToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_courses, :facturacion_book, foreign_key: true
  end
end
