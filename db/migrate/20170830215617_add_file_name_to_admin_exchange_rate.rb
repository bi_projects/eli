class AddFileNameToAdminExchangeRate < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_exchange_rates, :file_name, :string
  end
end
