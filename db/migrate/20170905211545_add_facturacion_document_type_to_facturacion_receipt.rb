class AddFacturacionDocumentTypeToFacturacionReceipt < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_receipts, :facturacion_document_type, foreign_key: true
  end
end
