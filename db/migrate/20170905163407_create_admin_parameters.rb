class CreateAdminParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_parameters do |t|
      t.string :invoice_prefix
      t.integer :invoice_number
      t.string :receipt_prefix
      t.integer :receipt_number

      t.timestamps
    end
  end
end
