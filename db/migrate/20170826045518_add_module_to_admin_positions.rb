class AddModuleToAdminPositions < ActiveRecord::Migration[5.0]
  def change
    add_reference :admin_positions, :admin_module, foreign_key: true
  end
end
