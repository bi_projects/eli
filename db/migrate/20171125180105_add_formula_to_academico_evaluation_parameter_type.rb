class AddFormulaToAcademicoEvaluationParameterType < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_evaluation_parameter_types, :formula, :integer, null: false
  end
end
