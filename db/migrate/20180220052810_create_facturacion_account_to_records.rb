class CreateFacturacionAccountToRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_account_to_records do |t|
      t.string :description

      t.timestamps
    end
  end
end
