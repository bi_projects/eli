class AddSecondTeacherToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_courses, :second_academico_staff_id, :integer
    add_foreign_key :academico_courses, :academico_staffs, column: :second_academico_staff_id
  end
end
