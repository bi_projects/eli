class AddBirthDateIndexToFacturacionPreregistrations < ActiveRecord::Migration[5.0]
  def change
  	add_index :facturacion_preregistrations, :birth_date
  end
end
