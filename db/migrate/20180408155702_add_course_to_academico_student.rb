class AddCourseToAcademicoStudent < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_students, :academico_course, foreign_key: true
  end
end
