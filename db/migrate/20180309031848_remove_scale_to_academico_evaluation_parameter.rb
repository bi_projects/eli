class RemoveScaleToAcademicoEvaluationParameter < ActiveRecord::Migration[5.0]
  def change
    remove_column :academico_evaluation_parameters, :scale
  end
end
