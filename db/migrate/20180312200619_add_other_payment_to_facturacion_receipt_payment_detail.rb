class AddOtherPaymentToFacturacionReceiptPaymentDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_receipt_payment_details, :other_payment, :decimal
  end
end
