class CreateFacturacionAccountingAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_accounting_accounts do |t|
      t.string :name
      t.string :account_number
      t.integer :parent_account
      t.boolean :is_disabled
      t.references :facturacion_account_to_records, foreign_key: true, index: {name: :idx_account_facturacion_account_to_records}
      t.references :facturacion_account_types, foreign_key: true, index: {name: :idx_account_facturacion_account_types}
      t.references :facturacion_accounting_account_categories, foreign_key: true, index: {name: :idx_account_facturacion_accounting_account_categories}

      t.timestamps
    end
  end
end
