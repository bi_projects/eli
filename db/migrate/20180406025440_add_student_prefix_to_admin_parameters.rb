class AddStudentPrefixToAdminParameters < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_parameters, :student_prefix, :string, null: false
  end
end
