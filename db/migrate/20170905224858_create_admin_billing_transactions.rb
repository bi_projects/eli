class CreateAdminBillingTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_billing_transactions do |t|
      t.text :description
      t.date :date
      t.integer :document_id
      t.references :facturacion_document_type, foreign_key: true, index: {:name => "index_transactions_on_facturacion_document_type_id"}
      t.integer :document_number
      t.decimal :transaction_amount

      t.timestamps
    end
  end
end
