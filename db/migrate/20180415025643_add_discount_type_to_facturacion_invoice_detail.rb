class AddDiscountTypeToFacturacionInvoiceDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoice_details, :discount_type, :integer
  end
end
