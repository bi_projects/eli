class CreateAcademicoUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_user_roles do |t|
      t.references :academico_academic_users, foreign_key: true
      t.references :academico_academico_roles, foreign_key: true

      t.timestamps
    end
  end
end
