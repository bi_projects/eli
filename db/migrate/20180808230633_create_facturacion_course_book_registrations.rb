class CreateFacturacionCourseBookRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_course_book_registrations do |t|
      t.references :academico_course, foreign_key: true, index: {:name => "index_book_course_registrations_id"}
      t.decimal :book_price, index: true, index: {:name => "index_book_price_id"}
      t.decimal :registration_price, index: true, index: {:name => "index_registration_price_id"}

      t.timestamps
    end
  end
end
