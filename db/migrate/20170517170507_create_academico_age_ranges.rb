class CreateAcademicoAgeRanges < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_age_ranges do |t|
      t.string :name
      t.integer :min_age
      t.integer :max_age
      t.text :description

      t.timestamps
    end
  end
end
