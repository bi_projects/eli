class CreateFacturacionInvoiceDetailBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_invoice_detail_books do |t|
      t.references :facturacion_invoice_detail, foreign_key: true, index: {:name => "index_fac_invoice_on_invoice_detail_id"} 
      t.references :facturacion_book, foreign_key: true, ndex: {:name => "index_fac_bool_on_fac_book_id"} 

      t.timestamps
    end
  end
end
