class CreateAcademicoTimeFrameStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_time_frame_statuses do |t|
      t.string :description

      t.timestamps
    end
  end
end
