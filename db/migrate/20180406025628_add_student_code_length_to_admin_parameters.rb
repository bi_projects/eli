class AddStudentCodeLengthToAdminParameters < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_parameters, :student_code_length, :integer, null: false
  end
end
