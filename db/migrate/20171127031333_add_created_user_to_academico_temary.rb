class AddCreatedUserToAcademicoTemary < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_temaries, :created_by, :integer, null: false
  end
end
