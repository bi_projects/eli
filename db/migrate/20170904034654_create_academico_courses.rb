class CreateAcademicoCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_courses do |t|
      t.references :academico_level_programs, foreign_key: true
      t.references :academico_academic_users, foreign_key: true
      t.references :academico_course_status, foreign_key: true, default: 1
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
