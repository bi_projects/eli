class AddNoteNumberToAdminParameter < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_parameters, :credit_note_number, :integer
    add_column :admin_parameters, :debit_note_number, :integer
  end
end
