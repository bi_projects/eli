class CreateFacturacionReceiptDetailNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_receipt_detail_notes do |t|
      t.references :facturacion_receipt_detail, foreign_key: true, index: {:name => "index_fact_rec_detail_on_fact_rec_detail_note"}
      t.references :facturacion_note, foreign_key: true, index: {:name => "index_fact_note_on_fact_rec_detail_note"}

      t.timestamps
    end
  end
end
