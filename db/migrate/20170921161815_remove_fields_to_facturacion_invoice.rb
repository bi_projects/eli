class RemoveFieldsToFacturacionInvoice < ActiveRecord::Migration[5.0]
  	def up
		remove_column :facturacion_invoices, :invoice_nullified
		remove_column :facturacion_invoices, :invoice_nullified_date
	end

	def down
		add_column :facturacion_invoices, :invoice_nullified, :boolean
		add_column :facturacion_invoices, :invoice_nullified_date, :date
	end
end
