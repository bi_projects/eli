class CreateFacturacionStudentCarriers < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_student_carriers do |t|
      t.references :facturacion_preregistration, foreign_key: true, index: {:name => "index_fact_student_carriers_on_fact_prereg_id"}
      t.references :facturacion_carrier, foreign_key: true, index: {:name => "index_fact_student_carriers_on_fact_carrier_id"}

      t.timestamps
    end
  end
end
