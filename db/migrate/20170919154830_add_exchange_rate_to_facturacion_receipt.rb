class AddExchangeRateToFacturacionReceipt < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_receipts, :exchange_rate, :decimal
  end
end
