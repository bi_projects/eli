class AddPaymentBalanceToFacturacionReceiptDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_receipt_details, :payment_pre_balance, :decimal
    add_column :facturacion_receipt_details, :payment_post_balance, :decimal
  end
end
