class CreateFacturacionBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_books do |t|
      t.string :name
      t.text :description
      t.integer :quantity
      t.integer :min_stock
      t.decimal :price

      t.timestamps
    end
  end
end
