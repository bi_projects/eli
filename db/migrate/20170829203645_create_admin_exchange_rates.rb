class CreateAdminExchangeRates < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_exchange_rates do |t|
      t.date :date
      t.decimal :value

      t.timestamps
    end
  end
end
