class CreateFacturacionStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_stocks do |t|
      t.date :stock_date
      t.references :facturacion_document_type, foreign_key: true
      t.integer :document_id
      t.references :facturacion_warehouse, foreign_key: true
      t.integer :quantity
      t.decimal :unit_cost
      t.boolean :canceled

      t.timestamps
    end
  end
end
