class CreateAdminFacturacionRolePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_facturacion_role_permissions do |t|
      t.references :admin_resource, foreign_key: true
      t.references :facturacion_facturacion_role, foreign_key: true, index: {:name => "index_fact_role_on_fact_role_permission"}
      t.references :action, foreign_key: true

      t.timestamps
    end
  end
end
