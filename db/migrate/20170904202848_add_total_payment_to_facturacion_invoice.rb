class AddTotalPaymentToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :total_payment, :decimal
  end
end
