class AddNameToAdminAdminUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_admin_users, :name, :string
    add_column :admin_admin_users, :avatar, :string
  end
end
