class CreateAcademicoSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_sessions do |t|
      t.references :academico_temary, foreign_key: true
      t.string :topic

      t.timestamps
    end
  end
end
