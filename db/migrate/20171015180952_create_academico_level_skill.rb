class CreateAcademicoLevelSkill < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_level_skills do |t|
      t.references :academico_staffs, foreign_key: true
      t.references :academico_level_programs, foreign_key: true
      t.timestamps
    end
  end
end
