class AddTotalInvoiceToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :total_invoice, :decimal
  end
end
