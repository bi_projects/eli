class CreateAdminAcademicUserPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_academic_user_positions do |t|
      t.references :academico_academic_user, foreign_key: true, index: {:name => "index_acad_user_on_admin_position_id"} 
      t.references :admin_position, foreign_key: true, index: {:name => "index_admin_position_on_acad_user_id"}
      t.timestamps
    end
  end
end
