class CreateAdminAcademicoRolePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_academico_role_permissions do |t|
      t.references :admin_resource, foreign_key: true
      t.references :academico_academico_role, foreign_key: true, index: {name: 'idx_academico_role_permissions_on_academico_academico_roles_id'}
      t.references :action, foreign_key: true
      t.timestamps
    end
  end
end
