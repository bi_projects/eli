class AddAdminBranchOfficeToFacturacionWarehouse < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_warehouses, :admin_branch_office, foreign_key: true
  end
end
