class CreateAcademicoCourseEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_course_evaluations do |t|
      t.references :academico_courses, foreign_key: true

      t.timestamps
    end
  end
end
