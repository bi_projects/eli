class ChangeColumnReferenceFacturacionPreregistrationStatus < ActiveRecord::Migration[5.0]
  def change
    change_column :facturacion_preregistrations, :facturacion_preregistration_statuses_id, :integer, default: 1, null: false
  end
end
