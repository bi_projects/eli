class AddScaleToAcademicoEvaluationParameter < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_evaluation_parameters, :scale, :integer, null: false, default: 0
  end
end
