class RemoveProgramFromAcademicoEvaluationScheme < ActiveRecord::Migration[5.0]
  def change
    remove_column :academico_evaluation_schemes, :academico_programs_id
  end
end
