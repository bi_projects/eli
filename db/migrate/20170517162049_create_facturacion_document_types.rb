class CreateFacturacionDocumentTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_document_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
