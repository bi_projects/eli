class AddAcademicoTimeFrameToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_courses, :academico_time_frame, foreign_key: true
  end
end
