class AddStatusToAcademicoTimeFrame < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_time_frames, :academico_time_frame_status, foreign_key: true, default: 1, null: false
  end
end
