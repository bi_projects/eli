class AddBalanceFieldsToFacturacionReceipt < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_receipts, :total_receipt, :decimal
    add_column :facturacion_receipts, :total_payment, :decimal
    add_column :facturacion_receipts, :total_change, :decimal
    add_column :facturacion_receipts, :nullified, :boolean
    add_column :facturacion_receipts, :nullified_date, :date
  end
end
