class CreateAcademicoCourseModalities < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_course_modalities do |t|
      t.string :description

      t.timestamps
    end
  end
end
