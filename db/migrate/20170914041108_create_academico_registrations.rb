class CreateAcademicoRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_registrations do |t|
      t.references :academico_courses, foreign_key: true
      t.references :academico_students, foreign_key: true
      t.references :academico_registration_statuses, foreign_key: true, index: {:name => "index_academico_registration_statuses_id"}

      t.timestamps
    end
  end
end
