class CreateFacturacionWarehouses < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_warehouses do |t|
      t.string :name
      t.text :description
      t.text :address
      t.references :facturacion_cost_center, foreign_key: true
      t.boolean :principal

      t.timestamps
    end
  end
end
