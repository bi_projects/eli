class AddScenarioToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_stocks, :scenario, :string
  end
end
