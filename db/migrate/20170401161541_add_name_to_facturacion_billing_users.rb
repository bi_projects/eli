class AddNameToFacturacionBillingUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_billing_users, :name, :string
    add_column :facturacion_billing_users, :avatar, :string
  end
end
