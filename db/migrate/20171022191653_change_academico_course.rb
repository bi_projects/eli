class ChangeAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    remove_column :academico_courses, :academico_academic_users_id
    add_column :academico_courses, :academico_staff_id, :integer
    add_foreign_key :academico_courses, :academico_staffs
  end
end
