class CreateAcademicoLevelProgramBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_level_program_books do |t|
      t.references :academico_level, foreign_key: true
      t.references :academico_program, foreign_key: true
      t.references :facturacion_book, foreign_key: true

      t.timestamps
    end
  end
end
