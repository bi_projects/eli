class AddTotalPaymentToFacturacionInvoicePaymentDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoice_payment_details, :total_payment, :decimal
  end
end
