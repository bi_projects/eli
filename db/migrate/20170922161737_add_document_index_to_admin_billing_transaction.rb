class AddDocumentIndexToAdminBillingTransaction < ActiveRecord::Migration[5.0]
  def change
    add_index :admin_billing_transactions, :document_id
  end
end
