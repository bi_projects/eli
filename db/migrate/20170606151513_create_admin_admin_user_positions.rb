class CreateAdminAdminUserPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_admin_user_positions do |t|
      t.references :admin_admin_user, foreign_key: true
      t.references :admin_position, foreign_key: true

      t.timestamps
    end
  end
end
