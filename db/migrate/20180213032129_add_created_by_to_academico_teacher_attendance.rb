class AddCreatedByToAcademicoTeacherAttendance < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_teacher_attendances, :created_by, :integer
    add_foreign_key :academico_teacher_attendances, :academico_staffs, column: :created_by, foreign_key: true, index: { name: 'idx_teacher_attendance_created_by'}
  end
end
