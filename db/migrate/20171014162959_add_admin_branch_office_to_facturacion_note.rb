class AddAdminBranchOfficeToFacturacionNote < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_notes, :admin_branch_office, foreign_key: true
  end
end
