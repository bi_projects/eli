class ChangeInvoiceReferenceToStudent < ActiveRecord::Migration[5.0]
  def change
    remove_column :facturacion_invoices, :facturacion_preregistration_id
    add_reference :facturacion_invoices, :academico_students, foreign_key: true
  end
end
