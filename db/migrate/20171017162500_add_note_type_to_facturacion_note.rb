class AddNoteTypeToFacturacionNote < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_notes, :note_type, :integer
    add_index :facturacion_notes, :note_type
  end
end
