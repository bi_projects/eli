class CreateFacturacionInvoiceDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_invoice_details do |t|
      t.references :facturacion_payment_detail_concept, foreign_key: true, index: {:name => "index_fac_concept_on_pay_detail_concept_id"} 
      t.text :description
      t.decimal :amount
      t.decimal :discount
      t.decimal :sub_total

      t.timestamps
    end
  end
end
