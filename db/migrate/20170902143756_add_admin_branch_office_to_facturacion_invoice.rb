class AddAdminBranchOfficeToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoices, :admin_branch_office, foreign_key: true
  end
end
