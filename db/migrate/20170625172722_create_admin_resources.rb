class CreateAdminResources < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_resources do |t|
      t.string :description
      t.string :model
      t.string :module
      t.references :resource_type, foreign_key: true, null: true
    end
  end
end
