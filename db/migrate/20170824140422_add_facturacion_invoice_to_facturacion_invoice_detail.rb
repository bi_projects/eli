class AddFacturacionInvoiceToFacturacionInvoiceDetail < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoice_details, :facturacion_invoice, foreign_key: true
  end
end
