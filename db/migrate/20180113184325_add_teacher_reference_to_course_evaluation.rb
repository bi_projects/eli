class AddTeacherReferenceToCourseEvaluation < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_course_evaluations, :academico_staffs, foreign_key: true
  end
end
