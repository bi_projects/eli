class CreateFacturacionReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_receipts do |t|
      t.integer :receipt_number
      t.date :receipt_date
      t.boolean :canceled
      t.boolean :printed
      t.text :comments
      t.references :admin_branch_office, foreign_key: true
      t.references :facturacion_preregistration, foreign_key: true
      t.date :canceled_date

      t.timestamps
    end
  end
end
