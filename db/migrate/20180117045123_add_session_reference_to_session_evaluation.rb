class AddSessionReferenceToSessionEvaluation < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_session_evaluations, :academico_sessions, foreign_key: true
  end
end
