class AddAcademicoStudentToFacturacionReceipt < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_receipts, :academico_student, foreign_key: true
    remove_column :facturacion_receipts, :facturacion_preregistration_id
  end
end
