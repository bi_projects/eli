class DropPaymentColumnsToFacturacionInvoice < ActiveRecord::Migration[5.0]
	def up
		remove_column :facturacion_invoices, :retention_amount_cordoba
		remove_column :facturacion_invoices, :cash_amount_cordoba
		remove_column :facturacion_invoices, :cash_amount_usd
		remove_column :facturacion_invoices, :credit_card_amount_cordoba
		remove_column :facturacion_invoices, :credit_card_amount_usd
		remove_column :facturacion_invoices, :ck_reference_cordoba
		remove_column :facturacion_invoices, :ck_amount_cordoba
		remove_column :facturacion_invoices, :ck_reference_usd
		remove_column :facturacion_invoices, :ck_amount_usd
		remove_column :facturacion_invoices, :bank_reference_usd
		remove_column :facturacion_invoices, :bank_amount_usd
		remove_column :facturacion_invoices, :telepago_reference_usd
		remove_column :facturacion_invoices, :telepago_amount_usd
		remove_column :facturacion_invoices, :change_amount_cordoba
		remove_column :facturacion_invoices, :total_invoice_amount_cordoba
	end

	def down
		add_column :facturacion_invoices, :retention_amount_cordoba, :decimal
		add_column :facturacion_invoices, :cash_amount_cordoba, :decimal
		add_column :facturacion_invoices, :cash_amount_usd, :decimal
		add_column :facturacion_invoices, :credit_card_amount_cordoba, :decimal
		add_column :facturacion_invoices, :credit_card_amount_usd, :decimal
		add_column :facturacion_invoices, :ck_reference_cordoba, :decimal
		add_column :facturacion_invoices, :ck_amount_cordoba, :decimal
		add_column :facturacion_invoices, :ck_reference_usd, :decimal
		add_column :facturacion_invoices, :ck_amount_usd, :decimal
		add_column :facturacion_invoices, :bank_reference_usd, :decimal
		add_column :facturacion_invoices, :bank_amount_usd, :decimal
		add_column :facturacion_invoices, :telepago_reference_usd, :decimal
		add_column :facturacion_invoices, :telepago_amount_usd, :decimal
		add_column :facturacion_invoices, :change_amount_cordoba, :decimal
		add_column :facturacion_invoices, :total_invoice_amount_cordoba, :decimal
	end
end
