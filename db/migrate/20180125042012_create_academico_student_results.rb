class CreateAcademicoStudentResults < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_student_results do |t|
      t.references :academico_students, foreign_key: true
      t.references :academico_course_results, foreign_key: true
      t.integer :final_grade

      t.timestamps
    end
  end
end
