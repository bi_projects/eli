class CreateAcademicoEvaluationMethods < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_evaluation_methods do |t|
      t.references :academico_evaluation_scheme, foreign_key: true, index: {:name => "index_academico_eval_scheme_id"}
      t.references :academico_evaluation_parameter, foreign_key: true, index: {:name => "index_academico_eval_parameter_id"}
      t.decimal :percentage

      t.timestamps
    end
  end
end
