class AddSchedulleToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_courses, :academico_course_schedulles, foreign_key: true
  end
end
