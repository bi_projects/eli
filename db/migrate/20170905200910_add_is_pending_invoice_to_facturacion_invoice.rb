class AddIsPendingInvoiceToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :is_pending_invoice, :boolean
  end
end
