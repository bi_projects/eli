class CreateFacturacionAccountingAccountCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_accounting_account_categories do |t|
      t.string :description

      t.timestamps
    end
  end
end
