class CreateFacturacionStudentTutors < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_student_tutors do |t|
      t.references :facturacion_preregistration, foreign_key: true, index: {:name => "index_fact_student_tutors_on_fact_prereg_id"}
      t.references :facturacion_tutor, foreign_key: true, index: {:name => "index_fact_student_tutors_on_fact_tutor_id"}

      t.timestamps
    end
  end
end
