class CreateFacturacionInvoiceDetailStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_invoice_detail_statuses do |t|
      t.string :description

      t.timestamps
    end
  end
end
