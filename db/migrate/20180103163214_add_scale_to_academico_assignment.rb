class AddScaleToAcademicoAssignment < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_session_assignments, :scale, :integer, null: false, default: 0
  end
end
