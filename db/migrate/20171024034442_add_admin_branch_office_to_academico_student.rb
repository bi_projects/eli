class AddAdminBranchOfficeToAcademicoStudent < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_students, :admin_branch_office_id, :integer
    add_foreign_key :academico_students, :admin_branch_offices
  end
end
