class RemoveFieldsToFacturacionReceipt < ActiveRecord::Migration[5.0]
  	def up
		remove_column :facturacion_receipts, :canceled
		remove_column :facturacion_receipts, :canceled_date
	end

	def down
		add_column :facturacion_receipts, :canceled, :boolean
		add_column :facturacion_receipts, :canceled_date, :date
	end
end
