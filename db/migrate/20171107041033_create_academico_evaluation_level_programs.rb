class CreateAcademicoEvaluationLevelPrograms < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_evaluation_level_programs do |t|
      t.references :academico_evaluation_schemes, foreign_key: true, index: {:name => "index_academico_evaluation_id"}
      t.references :academico_level_programs, foreign_key: true, index: {:name => "index_academico_level_program_id"}

      t.timestamps
    end
  end
end
