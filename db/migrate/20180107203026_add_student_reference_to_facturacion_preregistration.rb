class AddStudentReferenceToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_students, :facturacion_preregistration_id, :integer
    add_foreign_key :academico_students, :facturacion_preregistrations, column: :facturacion_preregistration_id
  end
end
