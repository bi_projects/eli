class AddAddressToAcademicoStudent < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_students, :address, :string
  end
end
