class AddLevelsAndProgramsToFacturacionBooks < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_books, :academico_level, foreign_key: true
    add_reference :facturacion_books, :academico_program, foreign_key: true
  end
end
