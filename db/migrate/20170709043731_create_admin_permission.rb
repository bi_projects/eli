class CreateAdminPermission < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_permissions do |t|
      t.references :action, foreign_key: true
      t.references :admin_resource, foreign_key: true
    end
  end
end
