class AddDescriptionToAcademicoEvaluationScheme < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_evaluation_schemes, :description, :string
    add_index :academico_evaluation_schemes, :description, unique: true
  end
end
