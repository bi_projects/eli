class AddAcademicoLevelToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_preregistrations, :academico_level, foreign_key: true
  end
end
