class AddLevelInfoToAcademicoStudent < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_students, :current_level_id, :integer
    add_column :academico_students, :level_up_id, :integer
    add_foreign_key :academico_students, :academico_levels, column: :current_level_id, foreign_key: true, index: { name: 'idx_student_current_level'}
    add_foreign_key :academico_students, :academico_levels, column: :level_up_id, foreign_key: true, index: { name: 'idx_student_level_up'}
  end
end
