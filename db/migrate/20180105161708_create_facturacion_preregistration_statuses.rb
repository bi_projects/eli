class CreateFacturacionPreregistrationStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_preregistration_statuses do |t|
      t.string :description

      t.timestamps
    end
  end
end
