class CreateFacturacionPaymentDetailConceptDetailPrices < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_payment_detail_concept_detail_prices do |t|
      t.references :facturacion_payment_detail_concept, foreign_key: true, index: { name: :pdc_detail_prices_on_payment_detail_concepts_id }
      t.references :academico_level, foreign_key: true, index: { name: :payment_detail_concept_detail_prices_on_levels_id }
      t.references :academico_program, foreign_key: true, index: { name: :payment_detail_concept_detail_prices_on_programs_id }
      t.decimal :amount
    end
  end
end
