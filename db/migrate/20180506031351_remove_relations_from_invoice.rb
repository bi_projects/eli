class RemoveRelationsFromInvoice < ActiveRecord::Migration[5.0]
  def change
    remove_column :facturacion_invoices, :academico_program_id
    remove_column :facturacion_invoices, :academico_level_id
    remove_column :facturacion_invoices, :course_schedulle_id
    remove_column :facturacion_invoices, :academico_course_modalities_id
  end
end
