class AddRoomToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_courses, :academico_room, foreign_key: true
  end
end
