class CreateFacturacionInvoicePaymentDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_invoice_payment_details do |t|
      t.decimal :retention_amount_cordoba
      t.decimal :cash_amount_cordoba
      t.decimal :cash_amount_usd
      t.decimal :credit_card_amount_cordoba
      t.decimal :credit_card_amount_usd
      t.string :ck_reference_cordoba
      t.decimal :ck_amount_cordoba
      t.string :ck_reference_usd
      t.decimal :ck_amount_usd
      t.string :bank_reference_usd
      t.decimal :bank_amount_usd
      t.string :telepago_reference_usd
      t.decimal :telepago_amount_usd
      t.decimal :change_amount_cordoba
      t.decimal :total_invoice_amount_cordoba
      t.references :facturacion_invoice, foreign_key: true, index: {:name => "index_fac_pay_detail_on_fac_id"} 

      t.timestamps
    end
  end
end
