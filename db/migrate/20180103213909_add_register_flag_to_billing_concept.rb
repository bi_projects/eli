class AddRegisterFlagToBillingConcept < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_payment_detail_concepts, :is_registration, :boolean, null: true
  end
end
