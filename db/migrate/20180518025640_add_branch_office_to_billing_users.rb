class AddBranchOfficeToBillingUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_billing_users, :admin_branch_office, foreign_key: true
  end
end
