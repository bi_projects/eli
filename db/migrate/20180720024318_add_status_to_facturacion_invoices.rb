class AddStatusToFacturacionInvoices < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoices, :facturacion_invoice_statuses, foreign_key: true
  end
end
