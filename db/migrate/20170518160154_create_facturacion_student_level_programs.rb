class CreateFacturacionStudentLevelPrograms < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_student_level_programs do |t|
      t.references :facturacion_preregistration, foreign_key: true, index: {:name => "index_fact_level_program_on_fact_prereg_id"}
      t.references :academico_level, foreign_key: true, index: {:name => "index_acad_level_program_on_acad_level_id"}
      t.references :academico_program, foreign_key: true, index: {:name => "index_acad_level_program_on_acad_program_id"}

      t.timestamps
    end
  end
end
