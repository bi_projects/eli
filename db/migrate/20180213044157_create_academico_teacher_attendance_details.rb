class CreateAcademicoTeacherAttendanceDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_teacher_attendance_details do |t|
      t.references :academico_teacher_attendances, foreign_key: true, index: { name: 'idx_teacher_attendance_detail'}
      t.boolean :marked

      t.timestamps
    end
  end
end
