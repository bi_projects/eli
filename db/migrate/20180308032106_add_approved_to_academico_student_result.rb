class AddApprovedToAcademicoStudentResult < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_student_results, :approved, :boolean
  end
end
