class ChangeFacturacionTurnoReference < ActiveRecord::Migration[5.0]
  def change
    remove_column :facturacion_invoices, :turno
    add_column :facturacion_invoices, :course_schedulle_id, :integer
    add_foreign_key :facturacion_invoices, :academico_course_schedulles, column: :course_schedulle_id
  end
end
