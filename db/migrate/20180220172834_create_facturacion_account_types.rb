class CreateFacturacionAccountTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_account_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
