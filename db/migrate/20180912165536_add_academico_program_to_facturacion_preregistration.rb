class AddAcademicoProgramToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_preregistrations, :academico_program, foreign_key: true
  end
end
