class AddDescriptionToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_stocks, :description, :text
  end
end
