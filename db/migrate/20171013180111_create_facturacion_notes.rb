class CreateFacturacionNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_notes do |t|
      t.integer :note_number
      t.date :note_date
      t.references :facturacion_preregistration, foreign_key: true
      t.text :description
      t.boolean :inatec
      t.decimal :total_note

      t.timestamps
    end
  end
end
