class CreateDailyProcess < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_daily_processes do |t|
      t.references :facturacion_billing_user, foreign_key: true, index: { name: :facturacion_daily_processes_on_facturacion_billing_user_id }
      t.datetime :daily_process_date, null: false
    end
  end
end
