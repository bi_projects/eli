class AddInvoiceDetailToNoteDetail < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_note_details, :facturacion_invoice_details, foreign_key: true, index: { name: :note_invoice_detail_ref }
  end
end
