class AddNameToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_courses, :name, :string
  end
end
