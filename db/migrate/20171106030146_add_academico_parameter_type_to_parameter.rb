class AddAcademicoParameterTypeToParameter < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_evaluation_parameters, :academico_evaluation_parameter_type_id, :integer
    add_foreign_key :academico_evaluation_parameters, :academico_evaluation_parameter_types
  end
end
