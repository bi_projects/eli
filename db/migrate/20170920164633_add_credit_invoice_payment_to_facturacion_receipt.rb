class AddCreditInvoicePaymentToFacturacionReceipt < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_receipts, :credit_invoice_payment, :boolean
  end
end
