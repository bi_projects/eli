class AddAcademicoSessionsToAcademicoTeacherAttendanceDetails < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_teacher_attendance_details, :academico_sessions, foreign_key: true, index: {name: :idx_session_teacher_attendance_det}
  end
end
