class AddTotalChangeToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :total_change, :decimal
  end
end
