class CreateAcademicoSessionEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_session_evaluations do |t|
      t.references :academico_course_evaluations, foreign_key: true, index: { name: 'idx_course_evaluation_reference'}

      t.timestamps
    end
  end
end
