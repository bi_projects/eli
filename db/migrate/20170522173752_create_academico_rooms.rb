class CreateAcademicoRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_rooms do |t|
      t.string :name
      t.integer :size
      t.text :description

      t.timestamps
    end
  end
end
