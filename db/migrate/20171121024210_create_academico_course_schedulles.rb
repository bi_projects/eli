class CreateAcademicoCourseSchedulles < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_course_schedulles do |t|
      t.time :begins, null: false
      t.time :ends, null: false
      t.boolean :disabled, null: false, default: false

      t.timestamps
    end
  end
end
