class AddNullifyFieldsToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :invoice_nullified, :boolean
    add_column :facturacion_invoices, :invoice_nullified_date, :date
    add_column :facturacion_invoices, :invoice_canceled_date, :date
  end
end
