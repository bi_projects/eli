class ChangeReferenceStudentScoreToAssignment < ActiveRecord::Migration[5.0]
  def change
    remove_column :academico_student_scores, :academico_evaluation_parameters_id
    add_reference :academico_student_scores, :academico_session_assignments, foreign_key: true, index: { name: 'idx_assignment_reference'}
  end
end
