class CreateAcademicoStaff < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_staffs do |t|
      t.references :academico_staffs, foreign_key: true
      t.references :academico_staff_statuses, foreign_key: true
      t.references :academico_academic_users, foreign_key: true
    end
  end
end
