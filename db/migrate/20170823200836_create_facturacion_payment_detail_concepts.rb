class CreateFacturacionPaymentDetailConcepts < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_payment_detail_concepts do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.boolean :is_book

      t.timestamps
    end
  end
end
