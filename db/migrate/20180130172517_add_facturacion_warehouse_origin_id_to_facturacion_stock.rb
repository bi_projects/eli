class AddFacturacionWarehouseOriginIdToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_stocks, :facturacion_warehouse_origin_id, :integer
  end
end
