class ChangeYearTypeToAcademicoTimeFrame < ActiveRecord::Migration[5.0]
  def change
    change_column :academico_time_frames, :year, :string
  end
end
