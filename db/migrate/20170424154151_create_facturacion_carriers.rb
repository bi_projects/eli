class CreateFacturacionCarriers < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_carriers do |t|
      t.string :name
      t.string :contact_name
      t.string :phone
      t.text :address
      t.text :description

      t.timestamps
    end
  end
end
