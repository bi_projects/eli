class CreateAcademicoEvaluationParameterTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_evaluation_parameter_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
