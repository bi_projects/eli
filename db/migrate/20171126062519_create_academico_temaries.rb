class CreateAcademicoTemaries < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_temaries do |t|
      t.string :name
      t.references :academico_courses, foreign_key: true

      t.timestamps
    end
  end
end
