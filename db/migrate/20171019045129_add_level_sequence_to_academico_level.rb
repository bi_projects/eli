class AddLevelSequenceToAcademicoLevel < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_levels, :level_sequence, :int, null: false, default: 0
  end
end
