class AddStatusToAcademicoPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_preregistrations, :academico_preregistration_statuses, foreign_key: true, default: 1, index: {name: 'idx_academico_preregistration_statuses'}
  end
end
