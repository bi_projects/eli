class CreateAcademicoAcademicoRoles < ActiveRecord::Migration
  def change
    create_table(:academico_academico_roles) do |t|
      t.string :name
      t.timestamps
    end

    add_index(:academico_academico_roles, :name, name: :academico_roles_name)
  end
end
