class AddFacturacionBookToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_stocks, :facturacion_book, foreign_key: true
  end
end
