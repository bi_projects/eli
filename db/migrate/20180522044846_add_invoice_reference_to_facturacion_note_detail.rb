class AddInvoiceReferenceToFacturacionNoteDetail < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_note_details, :facturacion_invoice, foreign_key: true
  end
end
