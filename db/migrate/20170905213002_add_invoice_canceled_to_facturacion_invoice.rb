class AddInvoiceCanceledToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :invoice_canceled, :boolean
  end
end
