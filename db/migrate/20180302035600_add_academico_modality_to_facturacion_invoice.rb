class AddAcademicoModalityToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoices, :academico_course_modalities, foreign_key: true
  end
end
