class AddStatusToFacturacionReceipts < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_receipts, :facturacion_receipt_statuses, foreign_key: true
  end
end
