class AddIsCashToFacturacionInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_invoices, :is_cash, :boolean
  end
end
