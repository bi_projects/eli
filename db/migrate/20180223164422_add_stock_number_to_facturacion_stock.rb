class AddStockNumberToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_stocks, :stock_number, :int
  end
end
