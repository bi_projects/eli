class RemoveFacturacionWarehouseOriginIdToFacturacionStock < ActiveRecord::Migration[5.0]
  def change
    remove_column :facturacion_stocks, :facturacion_warehouse_origin_id
  end
end
