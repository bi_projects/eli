class CreateFacturacionDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_discounts do |t|
      t.string :name
      t.text :description
      t.decimal :discount_amount

      t.timestamps
    end
  end
end
