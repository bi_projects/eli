class AddDateIndexToAdminExchangeRate < ActiveRecord::Migration[5.0]
	def change
		add_index :admin_exchange_rates, :date
	end
end
