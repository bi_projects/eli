class AddNotePrefixToAdminParameter < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_parameters, :credit_note_prefix, :integer
    add_column :admin_parameters, :debit_note_prefix, :integer
  end
end
