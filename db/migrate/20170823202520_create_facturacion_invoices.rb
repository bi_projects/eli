class CreateFacturacionInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_invoices do |t|
      t.integer :invoice_number
      t.date :invoice_date
      t.references :facturacion_preregistration, foreign_key: true
      t.string :curso
      t.references :academico_program, foreign_key: true
      t.references :academico_level, foreign_key: true
      t.string :turno
      t.text :invoice_concept
      t.decimal :retention_amount_cordoba
      t.decimal :cash_amount_cordoba
      t.decimal :cash_amount_usd
      t.decimal :credit_card_amount_cordoba
      t.decimal :credit_card_amount_usd
      t.string :ck_reference_cordoba
      t.decimal :ck_amount_cordoba
      t.string :ck_reference_usd
      t.decimal :ck_amount_usd
      t.string :bank_reference_usd
      t.decimal :bank_amount_usd
      t.string :telepago_reference_usd
      t.decimal :telepago_amount_usd
      t.decimal :change_amount_cordoba
      t.decimal :total_invoice_amount_cordoba
      t.decimal :exchange_rate
      t.boolean :printed

      t.timestamps
    end
  end
end
