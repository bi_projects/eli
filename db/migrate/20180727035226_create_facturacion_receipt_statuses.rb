class CreateFacturacionReceiptStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_receipt_statuses do |t|
      t.string :description

      t.timestamps
    end
  end
end
