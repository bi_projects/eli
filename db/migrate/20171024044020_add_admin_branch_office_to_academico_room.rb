class AddAdminBranchOfficeToAcademicoRoom < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_rooms, :admin_branch_office_id, :integer
    add_foreign_key :academico_rooms, :admin_branch_offices
  end
end
