class AddFacturacionDocumentTypeToFacturacionNote < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_notes, :facturacion_document_type, foreign_key: true
  end
end
