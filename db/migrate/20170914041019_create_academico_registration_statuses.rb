class CreateAcademicoRegistrationStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_registration_statuses do |t|
      t.string :description

      t.timestamps
    end
  end
end
