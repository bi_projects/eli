class AddCostToFacturacionBook < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_books, :unit_cost, :decimal, null: false, default: 0
  end
end
