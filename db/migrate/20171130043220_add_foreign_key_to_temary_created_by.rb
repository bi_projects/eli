class AddForeignKeyToTemaryCreatedBy < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :academico_temaries, :academico_academic_users, column: :created_by
  end
end
