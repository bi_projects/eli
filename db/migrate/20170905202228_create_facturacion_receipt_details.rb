class CreateFacturacionReceiptDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_receipt_details do |t|
      t.references :facturacion_receipt, foreign_key: true
      t.references :facturacion_invoice, foreign_key: true
      t.boolean :is_credit_note
      t.integer :credit_note_id
      t.decimal :payment_amount

      t.timestamps
    end
  end
end
