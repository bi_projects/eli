class CreateFacturacionStudentDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_student_discounts do |t|
      t.references :facturacion_preregistration, foreign_key: true, index: {:name => "index_fact_student_discounts_on_fact_prereg_id"}
      t.references :facturacion_discount, foreign_key: true, index: {:name => "index_fact_student_discounts_on_fact_discount_id"}

      t.timestamps
    end
  end
end
