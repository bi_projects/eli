class RemoveTotalInvoiceAmountCordobaToFacturacionInvoicePaymentDetail < ActiveRecord::Migration[5.0]
  def up
		remove_column :facturacion_invoice_payment_details, :total_invoice_amount_cordoba
	end

	def down
		add_column :facturacion_invoice_payment_details, :total_invoice_amount_cordoba, :decimal
	end
end