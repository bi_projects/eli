class CreateAcademicoStudentScores < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_student_scores do |t|
      t.references :academico_student_evaluations, foreign_key: true, index: { name: 'idx_student_evaluations_reference'}
      t.references :academico_evaluation_parameters, foreign_key: true, index: { name: 'idx_evaluation_parameters_reference'}
      t.integer :score

      t.timestamps
    end
  end
end
