class CreateAcademicoStudentEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_student_evaluations do |t|
      t.references :academico_session_evaluations, foreign_key: true, index: { name: 'idx_session_evaluations_reference'}
      t.references :academico_students, foreign_key: true, index: { name: 'idx_academico_students_reference'}

      t.timestamps
    end
  end
end
