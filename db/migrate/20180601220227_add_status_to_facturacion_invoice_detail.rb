class AddStatusToFacturacionInvoiceDetail < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_invoice_details, :facturacion_invoice_detail_status, foreign_key: true, default: 1, index: { name: :invoice_detail_status_ref }
  end
end
