class CreateFacturacionTransferRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_transfer_requests do |t|
      t.references :facturacion_document_type, foreign_key: true, index: {:name => "index_transfer_on_facturacion_document_type_id"}
      t.date :transfer_date
      t.references :facturacion_warehouse, foreign_key: true, index: {:name => "index_transfer_on_facturacion_document_warehouse_id"}
      t.integer :facturacion_warehouse_origin_id
     
      t.timestamps
    end
  end
end
