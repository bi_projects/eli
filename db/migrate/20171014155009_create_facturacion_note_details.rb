class CreateFacturacionNoteDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_note_details do |t|
      t.decimal :amount
      t.text :description
      t.references :facturacion_note, foreign_key: true

      t.timestamps
    end
  end
end
