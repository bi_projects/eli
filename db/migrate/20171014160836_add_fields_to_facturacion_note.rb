class AddFieldsToFacturacionNote < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_notes, :printed, :boolean
    add_column :facturacion_notes, :canceled, :boolean
  end
end
