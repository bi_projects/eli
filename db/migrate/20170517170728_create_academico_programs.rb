class CreateAcademicoPrograms < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_programs do |t|
      t.string :name
      t.references :academico_age_range, foreign_key: true
      t.decimal :price
      t.text :description

      t.timestamps
    end
  end
end
