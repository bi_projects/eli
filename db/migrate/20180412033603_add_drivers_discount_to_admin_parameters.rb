class AddDriversDiscountToAdminParameters < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_parameters, :drivers_discount, :decimal
  end
end
