class AddAcademicoStudentToFacturacionNote < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_notes, :academico_student, foreign_key: true
    remove_column :facturacion_notes, :facturacion_preregistration_id
  end
end
