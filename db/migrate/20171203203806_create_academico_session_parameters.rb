class CreateAcademicoSessionParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_session_parameters do |t|
      t.references :academico_evaluation_parameter, foreign_key: true, index: {:name => "index_for_academico_evaluation_parameter"}
      t.references :academico_session, foreign_key: true, index: {:name => "index_for_academico_session"}

      t.timestamps
    end
  end
end
