class CreateAcademicoTeacherAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_teacher_attendances do |t|
      t.references :academico_courses, foreign_key: true
      t.references :academico_staffs, foreign_key: true

      t.timestamps
    end
  end
end
