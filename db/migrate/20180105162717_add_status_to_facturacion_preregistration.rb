class AddStatusToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_reference :facturacion_preregistrations, :facturacion_preregistration_statuses, foreign_key: true, index: {name: 'idx_facturacion_preregistration_statuses'}
  end
end
