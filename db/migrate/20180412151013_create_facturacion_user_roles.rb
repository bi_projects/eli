class CreateFacturacionUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_user_roles do |t|
      t.references :facturacion_billing_user, foreign_key: true
      t.references :facturacion_facturacion_role, foreign_key: true

      t.timestamps
    end
  end
end
