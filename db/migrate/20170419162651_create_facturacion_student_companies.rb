class CreateFacturacionStudentCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_student_companies do |t|
      t.references :facturacion_preregistration, foreign_key: true,  index: {:name => "index_fact_student_companies_on_fact_prereg_id"}
      t.references :facturacion_company, foreign_key: true,  index: {:name => "index_fact_student_companies_on_fact_company_id"}

      t.timestamps
    end
  end
end
