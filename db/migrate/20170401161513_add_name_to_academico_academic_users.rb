class AddNameToAcademicoAcademicUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :academico_academic_users, :name, :string
    add_column :academico_academic_users, :avatar, :string
  end
end
