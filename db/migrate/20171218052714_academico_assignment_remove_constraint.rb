class AcademicoAssignmentRemoveConstraint < ActiveRecord::Migration[5.0]
  def change
    remove_reference :academico_session_assignments, :academico_session_parameters
    add_reference :academico_session_assignments, :academico_session_parameters, index: { name: :index_for_academico_session_assignment }, foreign_key: { on_delete: :cascade }
  end
end
