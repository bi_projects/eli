class CreateFacturacionTransferRequestStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_transfer_request_stocks do |t|
      t.references :facturacion_transfer_request, foreign_key: true, index: {:name => "index_transfer_req_stock_on_facturacion_trans_req"}
      t.references :facturacion_stock, foreign_key: true, index: {:name => "index_transfer_req_stock_on_facturacion_stock"}

      t.timestamps
    end
  end
end
