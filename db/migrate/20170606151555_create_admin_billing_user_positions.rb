class CreateAdminBillingUserPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_billing_user_positions do |t|
      t.references :facturacion_billing_user, foreign_key: true, index: {:name => "index_fact_user_on_admin_position_id"} 
      t.references :admin_position, foreign_key: true, index: {:name => "index_admin_position_on_fact_user_id"} 

      t.timestamps
    end
  end
end
