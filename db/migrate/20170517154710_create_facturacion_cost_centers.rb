class CreateFacturacionCostCenters < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_cost_centers do |t|
      t.string :code
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
