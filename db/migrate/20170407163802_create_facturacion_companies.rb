class CreateFacturacionCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :facturacion_companies do |t|
      t.string :name
      t.string :contact_name
      t.string :phone
      t.string :email
      t.text :address
      t.text :company_description
      t.references :facturacion_company_type, foreign_key: true

      t.timestamps
    end
  end
end
