class CreateAcademicoTimeFrames < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_time_frames do |t|
      t.numeric :year
      t.string :number
      t.string :name
      t.references :admin_branch_office, foreign_key: true
      t.date :start_date
      t.date :end_date
      t.references :academico_course_modality, foreign_key: true
      t.references :academico_course_schedulle, foreign_key: true

      t.timestamps
    end
  end
end
