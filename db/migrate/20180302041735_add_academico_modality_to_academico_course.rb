class AddAcademicoModalityToAcademicoCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :academico_courses, :academico_course_modalities, foreign_key: true
  end
end
