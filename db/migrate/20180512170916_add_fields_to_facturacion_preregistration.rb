class AddFieldsToFacturacionPreregistration < ActiveRecord::Migration[5.0]
  def change
    add_column :facturacion_preregistrations, :address, :string
    add_column :facturacion_preregistrations, :phone, :string
    add_column :facturacion_preregistrations, :email, :string
  end
end
