class CreateAcademicoStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_students do |t|
      t.string :first_name
      t.string :last_name
      t.datetime :birth_date
      t.string :student_code
      t.string :email
      t.string :phone
      t.boolean :active, default: false
      t.timestamps
    end
  end
end
