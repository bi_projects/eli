class CreateAcademicoPreregistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :academico_preregistrations do |t|
      t.references :academico_student, foreign_key: true
      t.references :academico_level, foreign_key: true
      t.references :academico_program, foreign_key: true

      t.timestamps
    end
  end
end
