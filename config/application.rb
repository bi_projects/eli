require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Eli
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    I18n.config.available_locales = [:en,:es,"es-MX"]
    config.time_zone = "Central America"
    config.i18n.default_locale = :es
	config.enable_dependency_loading = true
	config.autoload_paths << Rails.root.join('lib')
  end
end
