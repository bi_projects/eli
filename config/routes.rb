Rails.application.routes.draw do

  resources :actions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  namespace :admin do
    devise_for :admin_users, class_name: 'Admin::AdminUser',
                             controllers: { registrations: 'admin/admin_users/registrations',
                                            sessions: 'admin/admin_users/sessions',
                                            passwords: 'admin/admin_users/passwords' },
                             path_names: { sign_in: 'login', sign_out: 'logout',
                                           password: 'secret', confirmation: 'verification',
                                           unlock: 'unblock', registration: 'register',
                                           sign_up: 'sign_up' }

    root to: '/admin/dashboard#index'
    resources :positions

    resources :academic_users do
      member do
        get 'reset_password_form'
        patch 'reset_password'
      end
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :billing_users do
      member do
        get 'reset_password_form'
        patch 'reset_password'
      end
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :roles do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :facturacion_roles do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :academico_roles do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :academico_role_permissions do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :exchange_rates do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :branch_offices do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :parameters do

    end

    resources :billing_transactions do

    end

  end

  namespace :facturacion do
    resources :course_book_registrations do
      collection do
        put :bulk_prices
      end
    end
    resources :daily_processes do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :companies do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :company_types do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :preregistrations do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :carriers do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :discounts do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :tutors do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :stocks do 
      collection do
        get :multi_transfer
        post :create_transfer
      end
    end

    resources :document_types

    resources :warehouses do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :cost_centers do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :books do
      collection do
        delete 'destroy_multiple'
      end
    end

    get 'invoices/search_students', to: 'invoices#search_students'
    get 'invoices/pick_discount', to: 'invoices#read_discount'
    get 'invoices/student_courses', to: 'invoices#student_courses'
    resources :invoices do
      member do
        put :void_document
        get :student
      end
      collection do
        get :detail_values, defaults: {format: 'js'}
        get :remain_values
      end
    end

    resources :receipts do
      member do
        put :void_document
      end
      collection do
        get :student_credit_invoices
      end
    end
    get 'notes/student_invoices/:student_id', to: 'notes#student_invoices'
    get 'notes/invoice_concepts/:invoice_id', to: 'notes#invoice_concepts'

    resources :payment_detail_concepts do
      collection do
        delete "destroy_multiple"
      end
    end

    resources :notes do
    end
    resources :debit_notes do
    end

    resources :accounting_accounts do
      collection do
        delete 'destroy_multiple'
      end
    end

    get 'accounting_accounts/:parent_account/suggest_next_account_number', to: 'accounting_accounts#suggest_next_account_number'
    get 'parameters', to: 'application#parameters'

    devise_for :billing_users, class_name: 'Facturacion::BillingUser',
                               controllers: { registrations: 'facturacion/billing_users/registrations',
                                              sessions: 'facturacion/billing_users/sessions',
                                              passwords: 'facturacion/billing_users/passwords' },
                               path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret',
                                             confirmation: 'verification', unlock: 'unblock',
                                             registration: 'register', sign_up: 'sign_up' }

    root to: '/facturacion/dashboard#index'

  end

  namespace :academico do
    devise_for :academic_users, class_name: 'Academico::AcademicUser',
                                controllers: { registrations: 'academico/academic_users/registrations',
                                               sessions: 'academico/academic_users/sessions',
                                               passwords: 'academico/academic_users/passwords' },
                                path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret',
                                              confirmation: 'verification', unlock: 'unblock',
                                              registration: 'register', sign_up: 'sign_up' }

    root to: '/academico/dashboard#index'

    resources :levels do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :programs do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :age_ranges do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :students do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :rooms do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :courses do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :staffs do
      collection do
        delete 'destroy_multiple'
      end
    end
    get 'level_program/:level_program_id/staff', to: 'staffs#staff_by_level_skill'
    resources :evaluation_schemes do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :evaluation_parameters do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :evaluation_parameter_types do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :course_schedulles do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :temaries do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :course_evaluations do
      collection do
        delete 'destroy_multiple'
      end
    end
    resources :course_results do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :teacher_attendances do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :course_modalities do
      collection do
        delete 'destroy_multiple'
      end
    end

    resources :time_frames do
      collection do
        delete 'destroy_multiple'
      end
    end

    get 'course/:course_id/evaluation_parameters', to: 'courses#evaluation_parameters_by_course'
    get 'courses/:time_frame_id/time_frame_info', to: 'courses#time_frame_related_info'
    get 'courses/:office_id/lookup_classrooms', to: 'courses#lookup_classrooms'
    # this ugly non restfull path was needed this way
    get 'temaries/:course_id/session', to: 'temaries#session_parameters'
    get 'students/:preregistration_id/preregistration', to: 'students#facturacion_preregistration'
    get 'student_reports/index'
    get 'student_reports_xlsx_file', to: 'student_reports#xlsx_file'
    get 'course_student_reports/index'
    get 'course_student_reports_xlsx_file', to: 'course_student_reports#xlsx_file'
    get 'course_student_reports_pdf_file', to: 'course_student_reports#pdf_file'
    get 'course_quota_reports/index'
    get 'course_quota_reports_xlsx_file', to: 'course_quota_reports#xlsx_file'
    get 'student_ages_reports/index'
  end
end
