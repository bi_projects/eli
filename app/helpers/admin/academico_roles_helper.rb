module Admin::AcademicoRolesHelper
  def actions_for_resource(role_permissions, resource)
    role_permissions.select {|permission| permission.admin_resource_id == resource.id}
    .sort_by {|permission| permission.action_id}
  end
end
