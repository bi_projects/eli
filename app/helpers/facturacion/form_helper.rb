module Facturacion::FormHelper
	def setup_preregistration(facturacion_preregistration)
		if facturacion_preregistration.new_record?
			facturacion_preregistration.facturacion_student_company ||= Facturacion::StudentCompany.new
			facturacion_preregistration.facturacion_student_carrier ||= Facturacion::StudentCarrier.new
			facturacion_preregistration.facturacion_student_tutor ||= Facturacion::StudentTutor.new
			facturacion_preregistration.facturacion_student_discounts ||= Facturacion::StudentDiscount.new
			facturacion_preregistration.facturacion_student_level_program ||= Facturacion::StudentLevelProgram.new
		else
			facturacion_preregistration.facturacion_student_company || facturacion_preregistration.build_facturacion_student_company
			facturacion_preregistration.facturacion_student_tutor || facturacion_preregistration.build_facturacion_student_tutor
			facturacion_preregistration.facturacion_student_discounts || facturacion_preregistration.build_facturacion_student_discounts
			facturacion_preregistration.facturacion_student_level_program || facturacion_preregistration.build_facturacion_student_level_program
		end
		facturacion_preregistration
	end
end