module ApplicationHelper
	def document_related_link(admin_billing_transaction)
		#1;"Factura"
		#2;"Recibo"
		#3;"Nota Crédito"
		#4;"Nota Débito"
		#5;"Comprobante"

		case admin_billing_transaction.facturacion_document_type_id
		when 1
		  return link_to admin_billing_transaction.document_number.to_s, facturacion_invoice_path(admin_billing_transaction.document_id)
		when 2
		  return link_to admin_billing_transaction.document_number.to_s, facturacion_receipt_path(admin_billing_transaction.document_id)
		when 3
		  puts 'Nota Crédito'
		when 4
		  puts 'Nota Débito'
		when 5
		  puts 'Comprobante'
		else
			nil
		end

		admin_billing_transaction.document_number
	end
	def bulk_action_popup label="", id="ModalFilter"
		content_tag :button, :type=>"button", :class=>"btn btn-info navbar-left mar-right-10", 
		:"data-toggle"=>"modal", :"data-target"=>"##{id}" do
			label
		end

	end
	def filter_button_tag(label = "")
		content_container = ActiveSupport::SafeBuffer.new
		content_tag :button, :type=>"button", :class=>"btn btn-info navbar-right", 
		:"data-toggle"=>"modal", :"data-target"=>"#ModalFilter" do
			content_container << content_tag(:span, :class => "icon_svg inline") do
				content_tag :svg, :class=> "svg-icon", :viewBox => "0 0 20 20" do
					content_tag :path, :fill => "none", :d => "M15.808,14.066H6.516v-1.162H5.354v1.162H4.193c-0.321,0-0.581,0.26-0.581,0.58s0.26,0.58,0.581,0.58h1.162
						v1.162h1.162v-1.162h9.292c0.32,0,0.58-0.26,0.58-0.58S16.128,14.066,15.808,14.066z M15.808,9.419h-1.742V8.258h-1.162v1.161
						h-8.71c-0.321,0-0.581,0.26-0.581,0.581c0,0.321,0.26,0.581,0.581,0.581h8.71v1.161h1.162v-1.161h1.742
						c0.32,0,0.58-0.26,0.58-0.581C16.388,9.679,16.128,9.419,15.808,9.419z M17.55,0.708H2.451c-0.962,0-1.742,0.78-1.742,1.742v15.1
						c0,0.961,0.78,1.74,1.742,1.74H17.55c0.962,0,1.742-0.779,1.742-1.74v-15.1C19.292,1.488,18.512,0.708,17.55,0.708z M18.13,17.551
						c0,0.32-0.26,0.58-0.58,0.58H2.451c-0.321,0-0.581-0.26-0.581-0.58v-15.1c0-0.321,0.26-0.581,0.581-0.581H17.55
						c0.32,0,0.58,0.26,0.58,0.581V17.551z M15.808,4.774H9.419V3.612H8.258v1.162H4.193c-0.321,0-0.581,0.26-0.581,0.581
						s0.26,0.581,0.581,0.581h4.065v1.162h1.161V5.935h6.388c0.32,0,0.58-0.26,0.58-0.581S16.128,4.774,15.808,4.774z" do
	                end
				end
			end
			content_container << label
		end
	end
	def remove_child_button(name, class_name="")
    content_tag(:a,"#{name}".html_safe,
		:class => "btn btn-primary remove_child " + class_name)
	end

    # function made to implement an Add+ item to build a master detail form
    def new_fields_template(f,association,options={})
		options[:object] ||= f.object.class.reflect_on_association(association).klass.new
		#two level nested form
		if options[:is_two_level]
			options[:object].send(options[:nested_builder_method]).present?
		end
		options[:partial] ||= association.to_s.singularize+"_fields"
		options[:template] ||= association.to_s+"_fields" 
		options[:f] ||= :f

		render_proc = Proc.new { |b| render(:partial=>options[:partial],:locals =>{:f => b, :parent_local => options[:parent_local]}) }

		use_custom_association_name = options[:association_name].present?

		input_id_placeholder = "new_#{association}"

		# build an array with parameters for fields_for but exclude proc because can't be destructure
		fields_for_parameters = [use_custom_association_name ? options[:association_name] : association,
			options[:object],
			use_custom_association_name ? {:index => input_id_placeholder} : {:child_index => input_id_placeholder}] # for some reason whether index or child_index must be used

		tmpl = content_tag(:div,:class =>"#{options[:template]} detail_fields") do
			tmpl = if options[:association_name].present?
				fields_for(*fields_for_parameters, &render_proc)
			else
				f.fields_for(*fields_for_parameters, &render_proc)
			end
		end

		tmpl = tmpl.gsub /(?<!\n)\n(?!\n)/, ''
		template_identifier = association.to_s.singularize
		# accepts this 2 options to extend the template in deep nested fields
		# to provide unique js identifiers for variable snippets for at least 3 levels of deep i.e. var association_0_1_fields
		template_identifier += "_" + options[:template_index1].to_s if options[:template_index1].present?
		template_identifier += "_" + options[:template_index2].to_s if options[:template_index2].present?
		template_identifier += "_fields"
		# placing a id to script tag to be able to run the scripts in ajax callbacks with eval


		puts "----------------++++++++++++++++++++++ #{options[:template]}"
		return "<script class=\"script_with_identifier\" id=\"#{template_identifier}\"> var #{template_identifier} = '#{tmpl.to_s}'</script>".html_safe
	end

	def build_relation(f,association)
		f.object.class.reflect_on_association(association).klass.new
	end
	def add_child_button(name, association,target)
	    content_tag(:a,"#{name}".html_safe,
			:class => "btn btn-success add_child",
			:"data-association" => association,
			:target => target)
	end

	def footer_menu
		items = [left_link, middle_left_link, middle_right_link, right_link]
		content_tag :div, :class => "sidebar-footer hidden-small" do
			items.collect { |item| concat item}
		end
	end
	def left_link
		link_to(content_tag(:span, nil, :class=>"glyphicon", :"aria-hidden"=>"true"),
			nil, :"data-toggle"=>"", :"data-placement"=>"", :title=>'')
	end
	def middle_left_link
		left_link
	end
	def middle_right_link
		left_link
	end
	def right_link
		link_to(content_tag(:span, nil, :class=>"glyphicon glyphicon-off", :"aria-hidden"=>"true"),
			end_session_path, :class=>"link", :"data-toggle"=>"tooltip", :"data-placement"=>"top", :title=>'Salir', :method => :delete )
	end

	def top_nav_menu
		content_tag :div, :class => "top_nav" do
			content_tag :div, :class => "nav_menu" do
				content_tag :nav, :role => "navigation" do
					bar_link = content_tag :div, :class=>"nav toggle" do
						content_tag :a, :id => "menu_toggle" do
							content_tag(:i,nil,:class=>"fa fa-bars")
						end
					end
					menu_container = content_tag :ul, :class=> "nav navbar-nav navbar-right" do
						concat user_menu
						concat alerts_menu
					end
					concat bar_link
					concat menu_container
				end
			end
		end
	end

	def user_menu
		content_tag :li do
			content_container = ActiveSupport::SafeBuffer.new
			internal_content_container = ActiveSupport::SafeBuffer.new
			content_container << link_to("#", :class=>"user-profile dropdown-toggle", :"data-toggle"=>"dropdown", :"aria-expanded"=>"false") do
				concat image_tag(current_user.image_source("avatar"), "aria-hidden"=>"false")
				concat " " + current_user.name.to_s + " "
				concat content_tag(:span, nil, :class=>"fa fa-angle-down")
			end

			content_container << content_tag(:ul, :class=>"dropdown-menu dropdown-usermenu pull-right") do
				internal_content_container << content_tag(:li) do
					concat link_to("Perfil", edit_eli_user_path)
				end
				#internal_content_container << content_tag(:li) do
				#	link_to("Settings", "#")
				#end
				#internal_content_container << content_tag(:li) do
				#	link_to("Help", "#")
				#end
				internal_content_container << content_tag(:li) do
					link_to(content_tag(:i, nil, :class=>"fa fa-sign-out pull-right") + " Salir",
						end_session_path, :method => :delete)
				end
				internal_content_container
			end
			content_container
		end
	end

	def alerts_menu
		content_container = ActiveSupport::SafeBuffer.new
		internal_content_container = ActiveSupport::SafeBuffer.new
		sub_internal_content_container = ActiveSupport::SafeBuffer.new
		content_tag(:li, :role, :class=>"dropdown") do
			content_container << link_to("#", :class=>"dropdown-toggle info-number", :"data-toggle"=>"dropdown", :"aria-expanded"=>"false") do
				concat content_tag(:i, nil, :class=>"fa fa-envelope-o")
				concat content_tag(:span, 6, :class=>"badge bg-green")
			end
			content_container << content_tag(:ul, :class=>"dropdown-menu list-unstyled msg_list", :role=>"menu") do
				internal_content_container << content_tag(:li) do
					content_tag :a do
						content_tag(:span, :class=>"image") do
							sub_internal_content_container << content_tag(:img, nil,
								:src=>"/assets/img.jpg", :alt=>"Profile Image")
							sub_internal_content_container << content_tag(:span) do
								concat content_tag(:span, "Gerald Morales")
								concat content_tag(:span, "3 mins ago", :class=>"time")
							end
							sub_internal_content_container << content_tag(:span,
								"Film festivals used to be do-or-die moments for movie makers. They were where...", :class=>"message")
							sub_internal_content_container
						end
					end
				end
				internal_content_container << content_tag(:li) do
					content_tag(:div, :class=>"text-center") do
						link_to "#" do
							concat content_tag(:strong, "See All Alerts ")
							concat content_tag(:i, nil, :class=> "fa fa-angle-right")
						end
					end
				end
				internal_content_container
			end
			content_container
		end
	end
end
