module Academico::ApplicationHelper
  def check_permission(action, model, field_value = nil)
    class_name = model.is_a?(String) ? model : model.class.name
    model_obj = model.is_a?(String) ? model.constantize : model
    return case class_name
    when 'Academico::CourseEvaluation'
      can?(action.to_sym, model_obj, academico_staffs_id: current_user.academico_staff.id)
    when 'Academico::CourseResult'
      can?(action.to_sym, model_obj, academico_staffs_id: current_user.academico_staff.id)
    when 'Academico::Temary'
      can?(action.to_sym, model_obj, created_by: current_user.id)
    when 'Academico::TeacherAttendance'
      can?(action.to_sym, model_obj, created_by: current_user.academico_staff.id)
    else
      can?(action.to_sym, model_obj)
    end
  end

  def bootstrap_class_for flash_type
    case flash_type
      when 'success'
        "alert-success"
      when 'error'
        "alert-danger"
      when 'alert'
        "alert-warning"
      when 'notice'
        "alert-info"
      else
        flash_type.to_s
    end
  end

end
