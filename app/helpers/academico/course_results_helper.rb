module Academico::CourseResultsHelper
  def load_parameter_colors(parameters)
    Academico::EvaluationParameter::parameter_color_code(parameters)
  end

  def get_color(parameter)
    Academico::EvaluationParameter.get_my_color(parameter)
  end
end
