module Academico::StaffsHelper
  def skills_for_level(skills, level)
    skills_filtered = skills.select {|skill| skill.academico_level_program.academico_level_id == level.id}
    .sort_by {|skill| skill.academico_level_program.academico_program_id}
  end

  def skills_for_program(skills, program)
    skills.select {|skill| skill.academico_level_program.academico_program_id == program.id}
    .sort_by {|skill| skill.academico_level_program.academico_level_id}
  end
end
