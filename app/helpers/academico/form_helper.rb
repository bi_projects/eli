module Academico::FormHelper
	def setup_level(academico_level)
		if academico_level.new_record?
			if academico_level.academico_level_program_books.blank?
				academico_level.academico_level_program_books.build
			else
				academico_level.academico_level_program_books
			end
		else
			academico_level.academico_level_program_books || academico_level.build_academico_level_program_book
		end
		academico_level
	end
end