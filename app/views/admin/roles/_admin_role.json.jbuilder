json.extract! admin_role, :id, :name, :description, :created_at, :updated_at
json.url admin_role_url(admin_role, format: :json)
