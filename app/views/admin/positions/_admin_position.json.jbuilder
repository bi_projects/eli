json.extract! admin_position, :id, :name, :description, :created_at, :updated_at
json.url admin_position_url(admin_position, format: :json)
