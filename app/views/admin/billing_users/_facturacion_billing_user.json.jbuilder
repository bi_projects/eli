json.extract! facturacion_billing_user, :id, :created_at, :updated_at
json.url facturacion_billing_user_url(facturacion_billing_user, format: :json)
