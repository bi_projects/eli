json.extract! admin_academic_user, :id, :created_at, :updated_at
json.url admin_academic_user_url(admin_academic_user, format: :json)
