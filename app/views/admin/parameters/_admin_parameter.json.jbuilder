json.extract! admin_parameter, :id, :invoice_prefix, :invoice_number, :receipt_prefix, :receipt_number, :created_at, :updated_at
json.url admin_parameter_url(admin_parameter, format: :json)
