json.extract! admin_branch_office, :id, :name, :description, :created_at, :updated_at
json.url admin_branch_office_url(admin_branch_office, format: :json)
