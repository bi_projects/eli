json.extract! admin_exchange_rate, :id, :date, :value, :created_at, :updated_at
json.url admin_exchange_rate_url(admin_exchange_rate, format: :json)
