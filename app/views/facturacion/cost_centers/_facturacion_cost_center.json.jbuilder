json.extract! facturacion_cost_center, :id, :code, :name, :description, :created_at, :updated_at
json.url facturacion_cost_center_url(facturacion_cost_center, format: :json)
