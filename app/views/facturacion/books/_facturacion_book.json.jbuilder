json.extract! facturacion_book, :id, :name, :description, :quantity, :min_stock, :price, :created_at, :updated_at
json.url facturacion_book_url(facturacion_book, format: :json)
