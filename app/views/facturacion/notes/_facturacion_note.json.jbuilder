json.extract! facturacion_note, :id, :note_number, :note_date, :facturacion_preregistration_id, :description, :inatec, :total_note, :created_at, :updated_at
json.url facturacion_note_url(facturacion_note, format: :json)
