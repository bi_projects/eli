json.extract! facturacion_accounting_account, :id, :name, :account_number, :parent_account, :is_disabled, :facturacion_account_to_records_id, :facturacion_account_types_id, :facturacion_accounting_account_categories_id, :created_at, :updated_at
json.url facturacion_accounting_account_url(facturacion_accounting_account, format: :json)
