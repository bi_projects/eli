json.extract! facturacion_preregistration, :id, :first_name, :last_name, :birth_date, :created_at, :updated_at
json.url facturacion_preregistration_url(facturacion_preregistration, format: :json)
