json.extract! facturacion_payment_detail_concept, :id, :name, :description, :price, :is_book, :created_at, :updated_at
json.url facturacion_payment_detail_concept_url(facturacion_payment_detail_concept, format: :json)
