json.extract! facturacion_stock, :id, :stock_date, :facturacion_document_type_id, :document_id, :facturacion_warehouse_id, :quantity, :unit_cost, :canceled, :created_at, :updated_at
json.url facturacion_stock_url(facturacion_stock, format: :json)
