json.extract! facturacion_course_book_registration, :id, :academico_course_id, :book_price, :registration_price, :created_at, :updated_at
json.url facturacion_course_book_registration_url(facturacion_course_book_registration, format: :json)
