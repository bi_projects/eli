json.extract! facturacion_tutor, :id, :first_name, :last_name, :phone, :email, :created_at, :updated_at
json.url facturacion_tutor_url(facturacion_tutor, format: :json)
