json.extract! facturacion_carrier, :id, :name, :contact_name, :phone, :string, :address, :description, :created_at, :updated_at
json.url facturacion_carrier_url(facturacion_carrier, format: :json)
