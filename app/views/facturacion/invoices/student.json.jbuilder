json.academico_student([@academico_student].map do |student|
  student.as_json.merge({ academico_course: (student.academico_course && student.academico_course.as_json.merge({
    program_id: student.academico_course.academico_program.id,
    level_id: student.academico_course.academico_level.id })
  ) || {}})
  student.as_json.merge({
    programs_seed: [@academico_student.facturacion_preregistration.academico_program],
    levels_seed: @levels
  })
end.first)