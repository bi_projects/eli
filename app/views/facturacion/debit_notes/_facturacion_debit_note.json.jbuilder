json.extract! facturacion_debit_note, :id, :created_at, :updated_at
json.url facturacion_debit_note_url(facturacion_debit_note, format: :json)
