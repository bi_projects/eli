json.extract! facturacion_warehouse, :id, :name, :description, :address, :facturacion_cost_center_id, :principal, :created_at, :updated_at
json.url facturacion_warehouse_url(facturacion_warehouse, format: :json)
