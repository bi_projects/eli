json.extract! facturacion_receipt, :id, :receipt_number, :receipt_date, :canceled, :printed, :comments, :admin_branch_office_id, :facturacion_preregistration_id, :canceled_date, :created_at, :updated_at
json.url facturacion_receipt_url(facturacion_receipt, format: :json)
