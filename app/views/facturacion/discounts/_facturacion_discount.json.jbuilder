json.extract! facturacion_discount, :id, :name, :description, :discount_amount, :created_at, :updated_at
json.url facturacion_discount_url(facturacion_discount, format: :json)
