json.extract! facturacion_company, :id, :name, :contact_name, :phone, :email, :company_description, :facturacion_company_type_id, :created_at, :updated_at
json.url facturacion_company_url(facturacion_company, format: :json)
