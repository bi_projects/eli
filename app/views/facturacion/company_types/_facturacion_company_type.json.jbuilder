json.extract! facturacion_company_type, :id, :name, :description, :created_at, :updated_at
json.url facturacion_company_type_url(facturacion_company_type, format: :json)
