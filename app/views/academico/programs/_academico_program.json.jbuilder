json.extract! academico_program, :id, :name, :academico_age_range_id, :price, :description, :created_at, :updated_at
json.url academico_program_url(academico_program, format: :json)
