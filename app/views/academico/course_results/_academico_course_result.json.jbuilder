json.extract! academico_course_result, :id, :academico_courses_id, :created_at, :updated_at
json.url academico_course_result_url(academico_course_result, format: :json)
