json.extract! academico_level, :id, :name, :description, :created_at, :updated_at
json.url academico_level_url(academico_level, format: :json)
