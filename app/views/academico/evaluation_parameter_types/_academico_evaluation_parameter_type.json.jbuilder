json.extract! academico_evaluation_parameter_type, :id, :name, :created_at, :updated_at
json.url academico_evaluation_parameter_type_url(academico_evaluation_parameter_type, format: :json)
