json.extract! academico_course_evaluation, :id, :academico_courses_id, :created_at, :updated_at
json.url academico_course_evaluation_url(academico_course_evaluation, format: :json)
