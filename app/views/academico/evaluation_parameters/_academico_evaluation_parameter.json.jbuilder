json.extract! academico_evaluation_parameter, :id, :name, :created_at, :updated_at
json.url academico_evaluation_parameter_url(academico_evaluation_parameter, format: :json)
