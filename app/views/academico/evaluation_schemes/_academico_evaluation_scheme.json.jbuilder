json.extract! academico_evaluation_scheme, :id, :academico_programs_id, :created_at, :updated_at
json.url academico_evaluation_scheme_url(academico_evaluation_scheme, format: :json)
