json.extract! academico_course_modality, :id, :description, :created_at, :updated_at
json.url academico_course_modality_url(academico_course_modality, format: :json)
