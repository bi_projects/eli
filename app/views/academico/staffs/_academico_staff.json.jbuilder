json.extract! academico_staff, :id, :created_at, :updated_at
json.url academico_staff_url(academico_staff, format: :json)
