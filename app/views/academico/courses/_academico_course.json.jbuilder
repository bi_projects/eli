json.extract! academico_course, :id, :academico_programs_id, :academico_levels_id, :admin_academic_user_positions_id, :academico_course_status_id, :start_date, :end_date, :created_at, :updated_at
json.url academico_course_url(academico_course, format: :json)
