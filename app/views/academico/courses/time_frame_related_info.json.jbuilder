json.academico_time_frame([@time_frame].map do |time_frame|
  time_frame.as_json.merge({ 
    branch_office: time_frame.admin_branch_office,
    course_schedulle: time_frame.academico_course_schedulle.try(:printable_schedulle),
    course_modality: time_frame.academico_course_modality,
   })
end.first)