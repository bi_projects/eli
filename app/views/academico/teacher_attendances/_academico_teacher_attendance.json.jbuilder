json.extract! academico_teacher_attendance, :id, :academico_courses_id, :academico_staffs_id, :created_at, :updated_at
json.url academico_teacher_attendance_url(academico_teacher_attendance, format: :json)
