json.extract! academico_age_range, :id, :name, :min_age, :max_age, :description, :created_at, :updated_at
json.url academico_age_range_url(academico_age_range, format: :json)
