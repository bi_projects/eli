json.extract! academico_course_schedulle, :id, :begins, :ends, :disabled, :created_at, :updated_at
json.url academico_course_schedulle_url(academico_course_schedulle, format: :json)
