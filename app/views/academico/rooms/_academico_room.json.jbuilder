json.extract! academico_room, :id, :name, :size, :description, :created_at, :updated_at
json.url academico_room_url(academico_room, format: :json)
