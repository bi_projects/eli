json.academico_student([@academico_student].map do |student|
  student.as_json.merge({ academico_course: (student.academico_course && student.academico_course.as_json.merge({
    program_id: student.academico_course.academico_program.id,
    level_id: student.academico_course.academico_level.id })
  ) || {}})
end.first)