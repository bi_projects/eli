json.extract! academico_time_frame, :id, :year, :number, :name, :admin_branch_office_id, :start_date, :end_date, :academico_course_modality_id, :academico_course_schedulle_id, :created_at, :updated_at
json.url academico_time_frame_url(academico_time_frame, format: :json)
