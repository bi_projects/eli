json.extract! academico_temary, :id, :name, :academico_courses_id, :created_at, :updated_at
json.url academico_temary_url(academico_temary, format: :json)
