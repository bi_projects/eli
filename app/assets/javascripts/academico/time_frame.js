function generateTimeFrameAlias(fieldValues) {
  var tfYear = fieldValues.yearValue;
  var tfNumber = fieldValues.numberValue;
  var tfOffice = fieldValues.officeValue;
  var tfModality = fieldValues.modalityValue;
  var tfSchedulle = fieldValues.schedulleValue;
  var tfStartDate = fieldValues.startDateValue;
  var tfEndDate = fieldValues.endDateValue;

  if (
        tfYear
        && tfNumber
        && tfOffice
        && tfModality
        && tfSchedulle
        && tfStartDate
        && tfEndDate) {
    return tfYear + "-#" + tfNumber + "-" + tfOffice + "-" + tfModality + "-" + tfSchedulle + "-" + tfStartDate + "-" + tfEndDate;
  } else {
    return "";
  }
}

var nameFieldSelector = '#academico_time_frame_name';
var yearFieldSelector = '#academico_time_frame_year';
var numberFieldSelector = '#academico_time_frame_number';
var officeFieldSelector = '#academico_time_frame_admin_branch_office_id';
var modalityFieldSelector = '#academico_time_frame_academico_course_modality_id';
var schedulleFieldSelector = '#academico_time_frame_academico_course_schedulle_id';
var startDateFieldSelector = '#academico_time_frame_start_date';
var endDateFieldSelector = '#academico_time_frame_end_date';

$(document).ready(function(){

  var nameField = $(nameFieldSelector);
  var yearField = $(yearFieldSelector);
  var numberField = $(numberFieldSelector);
  var officeField = $(officeFieldSelector);
  var modalityField = $(modalityFieldSelector);
  var schedulleField = $(schedulleFieldSelector);
  var startDateField = $(startDateFieldSelector);
  var endDateField = $(endDateFieldSelector);

  function updateTimeFrameName() {
    var fieldEditedFlag = true;
    if (nameField.data('fieldEdited') === false) {
      nameField.data('fieldEdited', true);
      nameField.css('backgroundColor', '#E9C46A');
      nameField.attr('title', 'Para recalcular el valor del campo debe borrar manualmente el valor actual');
    }

    if ( !nameField.val() && (typeof fieldEditedFlag ==='undefined' || fieldEditedFlag)) {
      var yearValue = yearField.val();
      var numberValue = numberField.val();
      var officeValue = officeField.find('option:selected') ? officeField.find('option:selected').text() : '';
      var modalityValue = modalityField.find('option:selected') ? modalityField.find('option:selected').text() : '';
      var schedulleValue = schedulleField.find('option:selected') ? schedulleField.find('option:selected').text() : '';
      var startDateValue = startDateField.val();
      var endDateValue = endDateField.val();

      if (
        yearValue
        && numberValue
        && officeValue
        && modalityValue
        && schedulleValue
        && startDateValue
        && endDateValue) {

        var fieldValues = {
          yearValue: yearValue,
          numberValue: numberValue,
          officeValue: officeValue,
          modalityValue: modalityValue,
          schedulleValue: schedulleValue,
          startDateValue: startDateValue,
          endDateValue: endDateValue
        };
        var nameValue = generateTimeFrameAlias(fieldValues);

        nameField.val(nameValue);
        nameField.data('fieldEdited', false);
        nameField.css('backgroundColor', '#fff');
      }
    }
  }

  $(document).on('change', yearFieldSelector, function() {
    updateTimeFrameName();
  });

  $(document).on('change', numberFieldSelector, function() {
    updateTimeFrameName();
  });

  $(document).on('change', officeFieldSelector, function() {
    updateTimeFrameName();
  });

  $(document).on('change', modalityFieldSelector, function() {
    updateTimeFrameName();
  });

  $(document).on('change', schedulleFieldSelector, function() {
    updateTimeFrameName();
  });

  startDateField.on('apply.daterangepicker', function(ev, picker) {
    updateTimeFrameName();
  });

  endDateField.on('apply.daterangepicker', function(ev, picker) {
    updateTimeFrameName();
  });
});