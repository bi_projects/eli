$(document).ready(function(){
  $(document).on('click', '.add_child', generateAddChild(function() {
    initFieldDatepickers();
  }));

  $('#academico_temary_academico_courses_id').on('select2:selecting', function(e) {

    var changeCourseAllowed = $('#change_course_allowed').val();
    if (changeCourseAllowed === 'no') {
      if (confirm("Para editar el curso debe borrar antes todas las asignaciones actuales y guardar, OJO: el curso no se guarda. ¿Desea continuar?")) {
        var removeChecks = $('.session-parameter input[type="checkbox"]');
        removeChecks.each(function() {
          $(this).iCheck('check');
        });
      }
      e.preventDefault();
    }
  });

  $('#academico_temary_academico_courses_id').change(function() {
    var courseId = $(this).val() || 0;
    loadParametersForCourse(courseId);
  });
});

function loadParametersForCourse(courseId) {
  var $requestsQueue = [];
  $('.parameters-panel').each(function() {
    var currentPanel = $(this);
    // widgetIndex tiene el valor de f.index de form_builder para asegurarnos que los indices continuen con el mismo orden
    var widgetIndex = currentPanel.data('widget-index');
    var remoteMethod = '/academico/temaries/'  + courseId + '/session?current_index=' + widgetIndex;
    var requestPromise = $.get(remoteMethod, function(data) {
      currentPanel.find('.panel-body').html(data);
    });
    $requestsQueue.push(requestPromise);
  });
  // when callback will run when every request had ended
  $.when.apply($, $requestsQueue).then(function() {
    var scriptsToRun = $('.script_with_identifier');
    scriptsToRun.each(function() {
      var currentScript = $(this);
      // the script comes with a variable that we need to be placed in global scope, eval doesn't put it in global scope
      $.globalEval(currentScript.text());
    });
  });
}