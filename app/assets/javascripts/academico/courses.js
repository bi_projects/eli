var roomCapacity = {
  availableSeats: 0,
  roomSize: 0,
  usedSeats: 0
};

$(function() {
  roomCapacity.roomSize = $('#hidden_room_size').val();
  roomCapacity.usedSeats = $('.registration_picker:checked').length;

  $('#academico_course_academico_room_id').change(function() {
    var roomId = $(this).val();
    if (roomId) {
      var serviceUrl = '/academico/rooms/' + roomId;
      refreshRoomCapacity(serviceUrl);
    }
  });

  $('input.registration_picker').on('ifChanged', function() {
    roomCapacity.usedSeats = $('.registration_picker:checked').length;
    roomCapacity.availableSeats = roomCapacity.roomSize - roomCapacity.usedSeats;
    roomCapacity.availableSeats = roomCapacity.availableSeats > 0 ? roomCapacity.availableSeats : 0;
    $("#seats_used").text(roomCapacity.usedSeats);
    $("#seats_available").text(roomCapacity.availableSeats);
  });

  $('#academico_course_academico_level_programs_id').change(function(){
    var levelProgramId = $(this).val() || 0;
    var remoteMethod = '/academico/level_program/' + levelProgramId + '/staff.json';

    $.getJSON(remoteMethod, function(data) {
      if (typeof data !== 'undefined' && data !== null) {
        if (Array.isArray(data )) {
          $('#academico_course_academico_staff_id')
          .find('option')
          .remove()
          .end()
          .append('<option value=""></option>');
          data.forEach(function(item) {
            $('#academico_course_academico_staff_id')
            .append('<option value="' + item.id + '">' + item.display_name  + '</option>')
          })
          init_select2();
        }
      }
    });
  });


  $('#academico_course_academico_time_frame_id').change(function changeTimeFrameHandler() {
    var timeFrameId = $(this).val();

    setReadOnlyTimeFrameFields({});

    if (timeFrameId) {
      var remoteMethod = '/academico/courses/' + timeFrameId + '/time_frame_info.json';

      $.getJSON(remoteMethod, function loadTimeFrameInformation(data) {

        if (data) {
          var academico_time_frame = data.academico_time_frame;
          if (academico_time_frame) {
            var branch_office = academico_time_frame.branch_office && academico_time_frame.branch_office.description;
            var course_modality = academico_time_frame.course_modality && academico_time_frame.course_modality.description;
            var course_schedulle = academico_time_frame.course_schedulle;
            var start_date = academico_time_frame.start_date;
            var end_date = academico_time_frame.end_date;

            var readOnlyFields = {
              branchOffice: branch_office,
              modality: course_modality,
              schedulle: course_schedulle,
              startDate: start_date,
              endDate: end_date,
            };

            setReadOnlyTimeFrameFields(readOnlyFields);

            if (academico_time_frame.branch_office) {
              loadOfficesClassRooms(academico_time_frame.branch_office.id);
            }
          }
        }
      });
    }
  });
});

function refreshRoomCapacity(remoteMethod) {
  if ($(".container.spinner").length > 0) {
    $(".container.spinner").show();
  }
  $.getJSON(remoteMethod, function(data) {
    roomCapacity.roomSize = data.size || 0;
    roomCapacity.availableSeats = roomCapacity.roomSize - roomCapacity.usedSeats;
    roomCapacity.availableSeats = roomCapacity.availableSeats > 0 ? roomCapacity.availableSeats : 0;

    $("#seats").text(roomCapacity.roomSize);
    $("#seats_used").text(roomCapacity.usedSeats);
    $("#seats_available").text(roomCapacity.availableSeats);

    if ($(".container.spinner").length > 0) {
      $(".container.spinner").hide();
    }
  });
}

function setReadOnlyTimeFrameFields(readOnlyFields) {
  $('#read_only_branch_office').val(readOnlyFields.branchOffice || '');
  $('#read_only_modality').val(readOnlyFields.modality || '');
  $('#read_only_schedulle').val(readOnlyFields.schedulle || '');
  $('#read_only_start_date').val(readOnlyFields.startDate || '');
  $('#read_only_end_date').val(readOnlyFields.endDate || '');
}

function loadOfficesClassRooms(officeId) {
  $('#academico_course_academico_room_id').empty().trigger('change');
  if (officeId) {
    var remoteMethod = '/academico/courses/' + officeId + '/lookup_classrooms.json';
    $.getJSON(remoteMethod, function(data) {
      if (data && data.length > 0) {
        $('#academico_course_academico_room_id').append('<option></option>');
        data.forEach(function(room) {
          $('#academico_course_academico_room_id').append('<option value="' + room.id +'">' + room.name +'</option>');
        });
        $('#academico_course_academico_room_id').trigger('change');
      }
    });
  }
}