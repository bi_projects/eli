# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('#academico_student_facturacion_preregistration_id').on 'select2:select', (event) =>
    preregistrationId = event.params.data.id
    dataPromise = $.getJSON "/academico/students/"+preregistrationId+"/preregistration.json", (data) ->
      preregistration = data.facturacion_preregistration
      $('#academico_student_first_name').val(preregistration.first_name)
      $('#academico_student_last_name').val(preregistration.last_name)
      date_parts=preregistration.birth_date.split('-')
      parsedDate=new Date(date_parts[0], date_parts[1]-1, date_parts[2])
      $('#academico_student_birth_date').data('daterangepicker').setStartDate(parsedDate)