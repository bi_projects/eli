# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('#facturacion_accounting_account_parent_account').on 'select2:select', (event) =>
    parentAccountId = event.params.data.id
    dataPromise = $.getJSON "/facturacion/accounting_accounts/"+parentAccountId+"/suggest_next_account_number.json", (data) ->
      account_number = data.account_number
      $('#facturacion_accounting_account_account_number').val(account_number)