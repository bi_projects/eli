var invoiceTotal = 0.00;
var invoiceDetailTotal = 0.00;
var priceTotal = 0.00;
var discountTotal = 0.00;
var changeTotal = 0.00;
var paymentTotal = 1;


function isCashListenerById(dom) {
  isCashElement = document.getElementById(dom);
  paymentContainer = document.getElementById("payment_container");
  if (paymentContainer != null && isCashElement != null) {
    inputs = paymentContainer.getElementsByTagName("input");
    isCashElement.addEventListener("change", function(evt){
        if (isCashElement.checked) {
          paymentContainer.style.display="block";
          cashInputsEnabler(inputs, false);
        } else {
          paymentContainer.style.display="none";
          cashInputsEnabler(inputs, true);
        }
    });
  }
}

function cashInputsEnabler(inputs, disabled){
  Array.prototype.forEach.call(inputs, function(el){
    el.value = "";
    el.disabled = disabled;
  });
}


function updateTotalListenerByClass(dom){
  elements = document.getElementsByClassName(dom);
  prices = document.getElementsByClassName("detail_amount") 
  if (isEmpty(prices)){
    prices = document.getElementsByClassName("payment_detail_amount");
  }
  discounts = document.getElementsByClassName("detail_discount");
  if (elements.length > 0)
    Array.prototype.forEach.call(elements, function(el){
      el.addEventListener('keypress', function(evt){
        decimalValidation(el, evt);
      });
      el.addEventListener('change', function(evt){
        setInvoiceTotal(prices, discounts);
        setInvoiceDetailTotal(dom);
        setOtherPayment(el);
      });
      el.addEventListener('keyup', function(evt){
        setInvoiceTotal(prices, discounts);
        setInvoiceDetailTotal(dom);
        setOtherPayment(el);
      });
    }); 
}

function setOtherPayment(dom) {

  credit_note = $(dom).closest(".facturacion_receipt_details_fields.detail_fields").find(".is_credit_note"); 

  var total_amount = 0;
  is_credit_note_elements = $(".is_credit_note");
  is_credit_note_elements.each(function(){
      amount = $(this).closest(".facturacion_receipt_details_fields.detail_fields").find(".payment_detail_amount").val(); 
      if (isNaN(amount) || isEmpty(amount) || !credit_note.is(":checked")){
        amount = 0;
      }
      total_amount += parseFloat(amount).toFixed(2);
  });
  $(".other_payment").val(parseFloat(total_amount).toFixed(2));
  loadAmounts();

}

function setInvoiceDetailTotal(dom) {
  $("."+dom).each(function(){
    child = $(this).closest("div.child");
    price = child.find(".detail_amount").val();
    discount = child.find(".detail_discount").val();
    if (isNaN(price) || isEmpty(price)){
        price = 0;
    }
    if (isNaN(discount) || isEmpty(discount)){
        discount = 0;
    }
    subtotal = child.find(".subtotal");
    setSubtotal(price, discount, subtotal)
  });
}

function setSubtotal(price, discount, subtotalDom){
  subtotalDom.val(price*(1-discount/100));
}

function setInvoiceTotal(prices, discounts) {
  var total_price = 0.00;
  var total_discount = 0.00;

  Array.prototype.forEach.call(prices, function(innerEl){
    if (innerEl.parentElement.parentElement.getElementsByClassName("removable")[0].value == "false") {
      if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
        innerEl.value = 0;
      }
      total_price += parseFloat(innerEl.value);
    }
  });

  Array.prototype.forEach.call(discounts, function(innerEl){
    if (innerEl.parentElement.parentElement.getElementsByClassName("removable")[0].value == "false") {
      if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
        innerEl.value = 0;
      }
      total_discount += parseFloat(innerEl.value);
    }
  });


  priceTotal = total_price;
  discountTotal = total_discount;
  invoiceTotal = priceTotal*(1-discountTotal/100);
  changeTotal = paymentTotal - invoiceTotal;
  if (isNaN(invoiceTotal)){
    invoiceTotal = 0
  }
  if (isNaN(changeTotal)){
    changeTotal = 0
  }

  totalElement = document.getElementById("total_invoice") || document.getElementById("total_receipt");

  console.log(invoiceTotal, priceTotal, discountTotal);

  if (totalElement != null && document.getElementById("total_change") != null) {
    totalElement.innerHTML = invoiceTotal.toFixed(2);
    document.getElementById("total_change").innerHTML = changeTotal.toFixed(2);
  }
}

function setPaymentTotal(elements, usd_elements) {
  var total = 0.00;
  Array.prototype.forEach.call(usd_elements, function(innerEl){
    if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
      innerEl.value = 0;
    }

    if (document.getElementById("facturacion_receipt_exchange_rate") != null) {
      exchange_rate = document.getElementById("facturacion_receipt_exchange_rate").value;
    } else {
      exchange_rate = document.getElementById("facturacion_receipt_facturacion_invoices_attributes_0_exchange_rate").value;
    }
   
    console.log(">>", exchange_rate);
    if (isNaN(exchange_rate) || isEmpty(exchange_rate)){
      exchange_rate = 0;
    }
    total += parseFloat(innerEl.value)*parseFloat(exchange_rate);
  })
  Array.prototype.forEach.call(elements, function(innerEl){
    if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
      innerEl.value = 0;
    }
    total += parseFloat(innerEl.value);;
  })
  paymentTotal = total;
  changeTotal = paymentTotal - invoiceTotal;
  if (isNaN(paymentTotal)){
    paymentTotal = 0
  }
  if (isNaN(changeTotal)){
    changeTotal = 0
  }
  if (document.getElementById("total_payment") != null && document.getElementById("total_change") != null) {
    document.getElementById("total_payment").innerHTML = paymentTotal.toFixed(2);
    document.getElementById("total_change").innerHTML = changeTotal.toFixed(2);
  }
}

function updatePaymentListenerByClass(dom){
  elements = document.getElementsByClassName(dom);
  payments = document.getElementsByClassName("payment");
  usd_payments = document.getElementsByClassName("payment_usd");
  if (elements.length > 0)
    Array.prototype.forEach.call(elements, function(el){
      el.addEventListener('change', function(evt){
        setPaymentTotal(payments, usd_payments);
      });
      el.addEventListener('keyup', function(evt){
        setPaymentTotal(payments, usd_payments);
      });
    }); 
}

updateTotalListenerByClass("detail_amount");
updateTotalListenerByClass("detail_discount");
updateTotalListenerByClass("payment_detail_amount");
updatePaymentListenerByClass("payment_inputs");
$(document).ready(function(){
  loadAmounts();
  isCashListenerById("facturacion_receipt_facturacion_invoices_attributes_0_is_cash");
  default_invoice_receipt();
  invoices_detail_triger();

  $('.add_child').click(generateAddChild(function() {
      if (isNaN(document.getElementById("facturacion_receipt_academico_students_id").value) || isEmpty(document.getElementById("facturacion_receipt_academico_students_id").value)){
        alert("seleccione el estudiante para cargar el detalle del recibo");
        $(".remove_child").click();
      } else {
        invokeFacturacionListeners();
        invoices_detail_triger();
        isCreditNote();
      }
    })
  );


  $(".book_hidden select").attr( "required","required" )
  $(".book_hidden.hide select").removeAttr( "required" )
  isCreditNote();
  initSelectSearch(function(data) {
    return {
      results: $.map(data, function(item, index) { return { id: item.id, text: (item.student_code || '') + (item.student_code ? " - " : '') + (item.first_name || '') + (item.first_name && item.last_name ? ' ' : '') + (item.last_name || '') } })
    }
  });

  $(document).on('change', '#academico_time_frame', function() {
    searchForGroupCourses();
  });
  $(document).on('change', '#academico_level', function() {
    searchForGroupCourses();
  });
  $(document).on('change', '#academico_program', function() {
    searchForGroupCourses();
  });
});

function loadAmounts(){
  prices = document.getElementsByClassName("detail_amount");
  if (isEmpty(prices)){
    prices = document.getElementsByClassName("payment_detail_amount");
  }
  discounts = document.getElementsByClassName("detail_discount");
  payments = document.getElementsByClassName("payment");
  usd_payments = document.getElementsByClassName("payment_usd");
  setInvoiceTotal(prices, discounts);
  setPaymentTotal(payments, usd_payments);
}

function isCreditNote(){
  is_credit_note_elements = $(".is_credit_note");
  is_credit_note_elements.each(function(){
    $(this).on("ifChanged", function () { 
      $(this).closest(".facturacion_receipt_details_fields.detail_fields").find(".note_hidden select").val('').trigger('change');
      if ($(this).is(":checked")){
        $(this).closest(".facturacion_receipt_details_fields.detail_fields").find(".note_hidden").removeClass("hide"); 
      } else {
        $(this).closest(".facturacion_receipt_details_fields.detail_fields").find(".note_hidden").addClass("hide"); 
      }
      $(this).closest(".facturacion_receipt_details_fields.detail_fields").find(".payment_detail_amount").val("0"); 
      loadAmounts();
      setOtherPayment($(this));
    });
  });
}

function invoices_detail_triger() {
  invoice_elements = $(".invoice_detail_element");
  invoice_elements.each(function(){
    var el = $(this);
    $(this).on("change", function () {
      var child = el.closest("div.child");
      var paymentConceptId = child.find('.payment_detail_concept_id');
      var studentId = $("#facturacion_receipt_academico_students_id").val();
      var courseId = $("#facturacion_receipt_facturacion_invoices_attributes_0_academico_course_id").val();
      var bookId = child.find('.book_id').val();

      if (studentId && courseId && paymentConceptId.val()) {
        getElementAttributes("/facturacion/invoices/detail_values",
                          { element_id: paymentConceptId.val(), student_id: studentId,
                            course_id: courseId, book_id: bookId
                          }, paymentConceptId.attr("id"));
      }
    });
    $("#facturacion_receipt_facturacion_invoices_attributes_0_academico_course_id").on('change', function () {
      invoice_elements.trigger('change');
    })
  });

  invoice_remain_elements = $(".invoice_remain_element");
  invoice_remain_elements.each(function(){
    var el = $(this);
    $(this).on("change", function () {
      getElementAttributes("/facturacion/invoices/remain_values", { element_id: el.val() }, el.attr("id"));
    });
  });
  
  el = $("#facturacion_receipt_academico_students_id");
  el.on("change", function () {
    getElementAttributes("/facturacion/receipts/student_credit_invoices", { element_id: el.val() }, "changedItem");
    var studentId = $(this).val();

    if (studentId) {
      $.getJSON('/facturacion/invoices/' + studentId + '/student.json', function(data) {
        var student = data.academico_student;

        $('#facturacion_receipt_facturacion_invoices_attributes_0_admin_branch_office_id').val(student.admin_branch_office_id  || '').trigger('change');

        $('#academico_level').empty().trigger('change');
        if (student.levels_seed) {
          student.levels_seed.forEach(function iterateLevelsForStudent(level) {
            $('#academico_level').append($('<option>', {
              value: level.id,
              text: level.name})
            );
          });
          $('#academico_level').trigger('change');
        }
        $('#academico_program').empty().trigger('change');
        if (student.programs_seed) {
          student.programs_seed.forEach(function iterateProgramsForStudent(program) {
            $('#academico_program').append($('<option>', {
              value: program.id,
              text: program.name})
            );
          });
          $('#academico_program').trigger('change');
        }
      });
    }

  });
}


function searchForGroupCourses() {
  var academicoTimeFrameId = $('#academico_time_frame').val();
  var academicoLevelId = $('#academico_level').val();
  var academicoProgramId = $('#academico_program').val();

  if (academicoTimeFrameId || academicoLevelId || academicoProgramId) {
    var requestData = {};
    if (academicoTimeFrameId) {
      requestData['time_frame_id'] = academicoTimeFrameId;
    }
    if (academicoLevelId) {
      requestData['level_id'] = academicoLevelId;
    }
    if (academicoProgramId) {
      requestData['program_id'] = academicoProgramId;
    }

    $.getJSON('/facturacion/invoices/student_courses.json', requestData).done(
      function(data) {
        if (data.academico_courses) {
          var courses = data.academico_courses;
          $('#facturacion_receipt_facturacion_invoices_attributes_0_academico_course_id').empty().trigger('change');
          if (courses.length && courses.length > 0 && courses[0].id) {

            courses.forEach(function iterateCoursesForStudent(course) {
              $('#facturacion_receipt_facturacion_invoices_attributes_0_academico_course_id').append($('<option>', {
                value: course.id,
                text: course.name})
              );
            });
            $('#facturacion_receipt_facturacion_invoices_attributes_0_academico_course_id').trigger('change');
          }
        }
      }
    );

  }

}


function default_invoice_receipt() {
  el = $("#facturacion_receipt_academico_students_id");
  if (el != null){
    getElementAttributes("/facturacion/receipts/student_credit_invoices", { element_id: el.val() }, "changedItem");
  }
}
function getElementAttributes(remoteMethod, params, domId) {
  $(".container.spinner").show();
  parameteresObject = ""
  Object.keys(params).forEach(function (key) {
    parameteresObject += key + "=" + params[key] + "&"
  })
  $.getScript(remoteMethod + '?' + parameteresObject + 'domId=' + domId, function () {
    init_select2();
  });
}

function valueSetter(dom, target, value) {
  $("#"+dom).parent().siblings().find(target).val(value);
}
function valueChecker(dom, target, value) {
  $("#"+dom).parent().siblings().find(target).val(value);
}