var invoiceTotal = 0.00;
var invoiceDetailTotal = 0.00;
var priceTotal = 0.00;
var discountTotal = 0.00;
var changeTotal = 0.00;
var paymentTotal = 1;


function isCashListenerById(dom) {
  isCashElement = document.getElementById(dom);
  paymentContainer = document.getElementById("payment_container");
  if (paymentContainer != null) {
    inputs = paymentContainer.getElementsByTagName("input");
    isCashElement.addEventListener("change", function(evt){
        if (isCashElement.checked) {
          paymentContainer.style.display="block";
          cashInputsEnabler(inputs, false);
        } else {
          paymentContainer.style.display="none";
          cashInputsEnabler(inputs, true);
        }
    });
  }
}

function cashInputsEnabler(inputs, disabled){
  Array.prototype.forEach.call(inputs, function(el){
    el.value = "";
    el.disabled = disabled;
  });
}


function updateTotalListenerByClass(dom){
  elements = document.getElementsByClassName(dom);
  prices = document.getElementsByClassName("detail_amount");
  discounts = document.getElementsByClassName("detail_discount");

  $(document).on('change', '.'+dom, function() {
    setInvoiceTotal(prices, discounts);
    setInvoiceDetailTotal(dom);
  });
  $(document).on('keypress', '.'+dom, function(evt) {
    decimalValidation($(this)[0], evt);
  });
  $(document).on('keyup', '.'+dom, function() {
    setInvoiceTotal(prices, discounts);
    setInvoiceDetailTotal(dom);
  });
}

function updateSubTotalDetail(dom){
  prices = document.getElementsByClassName("detail_amount");
  discounts = document.getElementsByClassName("detail_discount");
  setInvoiceTotal(prices, discounts);
  setInvoiceDetailTotal(dom);
}

function setInvoiceDetailTotal(dom) {
  $("."+dom).each(function(){
    child = $(this).closest("div.child");
    price = child.find(".detail_amount").val();
     discount = child.find(".detail_discount").val();
    if (isNaN(price) || isEmpty(price)){
        price = 0;
    }
    if (isNaN(discount) || isEmpty(discount)){
        discount = 0;
    }
    subtotal = child.find(".subtotal");
    setSubtotal(price, discount, subtotal)
  });
}

function setSubtotal(price, discount, subtotalDom){
  subtotalDom.val(price*(1-discount/100));
}

function setInvoiceTotal(prices, discounts) {
  var total_price = 0.00;
  var total_discount = 0.00;
  Array.prototype.forEach.call(prices, function(innerEl){
    if (innerEl.parentElement.parentElement.getElementsByClassName("removable")[0].value == "false") {
      if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
        innerEl.value = 0;
      }
      total_price += parseFloat(innerEl.value);
    }
  });
  Array.prototype.forEach.call(discounts, function(innerEl){
    if (innerEl.parentElement.parentElement.getElementsByClassName("removable")[0].value == "false") {
      if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
        innerEl.value = 0;
      }
      total_discount += parseFloat(innerEl.value);
    }
  });
  priceTotal = total_price;
  discountTotal = total_discount;
  invoiceTotal = priceTotal*(1-discountTotal/100);
  changeTotal = paymentTotal - invoiceTotal;
  if (isNaN(invoiceTotal)){
    invoiceTotal = 0
  }
  if (isNaN(changeTotal)){
    changeTotal = 0
  }
  if (document.getElementById("total_invoice") != null && document.getElementById("total_change") != null) {
    document.getElementById("total_invoice").innerHTML = invoiceTotal.toFixed(2);
    document.getElementById("total_change").innerHTML = changeTotal.toFixed(2);
  }
}

function setPaymentTotal(elements, usd_elements) {
  var total = 0.00;
  Array.prototype.forEach.call(usd_elements, function(innerEl){
    if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
      innerEl.value = 0;
    }
    exchange_rate = document.getElementById("facturacion_invoice_exchange_rate").value;
    console.log(">>", exchange_rate);
    if (isNaN(exchange_rate) || isEmpty(exchange_rate)){
      exchange_rate = 0;
    }
    total += parseFloat(innerEl.value)*parseFloat(exchange_rate);
  })
  Array.prototype.forEach.call(elements, function(innerEl){
    if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
      innerEl.value = 0;
    }
    total += parseFloat(innerEl.value);;
  })
  paymentTotal = total;
  changeTotal = paymentTotal - invoiceTotal;
  if (isNaN(paymentTotal)){
    paymentTotal = 0
  }
  if (isNaN(changeTotal)){
    changeTotal = 0
  }
  if (document.getElementById("total_payment") != null && document.getElementById("total_change") != null) {
    document.getElementById("total_payment").innerHTML = paymentTotal.toFixed(2);
    document.getElementById("total_change").innerHTML = changeTotal.toFixed(2);
  }
}

function updatePaymentListenerByClass(dom){
  elements = document.getElementsByClassName(dom);
  payments = document.getElementsByClassName("payment");
  usd_payments = document.getElementsByClassName("payment_usd");
  if (elements.length > 0)
    Array.prototype.forEach.call(elements, function(el){
      el.addEventListener('change', function(evt){
        setPaymentTotal(payments, usd_payments);
      });
      el.addEventListener('keyup', function(evt){
        setPaymentTotal(payments, usd_payments);
      });
    }); 
}

function loadAmounts(){
  prices = document.getElementsByClassName("detail_amount");
  if (isEmpty(prices)){
    prices = document.getElementsByClassName("payment_detail_amount");
  }
  discounts = document.getElementsByClassName("detail_discount");
  payments = document.getElementsByClassName("payment");
  usd_payments = document.getElementsByClassName("payment_usd");
  setInvoiceTotal(prices, discounts);
  setPaymentTotal(payments, usd_payments);
}


$(document).ready(function(){
  updateTotalListenerByClass("detail_amount");
  updateTotalListenerByClass("detail_discount");
  updatePaymentListenerByClass("payment_inputs");

  prices = document.getElementsByClassName("detail_amount");
  discounts = document.getElementsByClassName("detail_discount");
  payments = document.getElementsByClassName("payment");
  usd_payments = document.getElementsByClassName("payment_usd");
  setInvoiceTotal(prices, discounts);
  setPaymentTotal(payments, usd_payments);
  isCashListenerById("facturacion_invoice_is_cash");
  invoices_detail_triger();
  $('.add_child').click(generateAddChild(function() {
      if (isNaN(document.getElementById("facturacion_invoice_academico_students_id").value) || isEmpty(document.getElementById("facturacion_invoice_academico_students_id").value)){
        alert("seleccione el estudiante para cargar el detalle de factura");
        $(".remove_child").click();
      } else {
        invokeFacturacionListeners();
        invoices_detail_triger();
      }
    })
  );
  paymentContainer = document.getElementById("payment_container");
  if (paymentContainer != null) {
    inputs = paymentContainer.getElementsByTagName("input");
    if (document.getElementById("facturacion_invoice_is_cash").checked) {
      paymentContainer.style.display="block";
    } else {
      paymentContainer.style.display="none";
      cashInputsEnabler(inputs, true);
    }
  }
  $(".book_hidden select").attr( "required","required" )
  $(".book_hidden.hide select").removeAttr( "required" )
  student_change();

  initSelectSearch(function(data) {
    return {
      results: $.map(data, function(item, index) { return { id: item.id, text: (item.student_code || '') + (item.student_code ? " - " : '') + (item.first_name || '') + (item.first_name && item.last_name ? ' ' : '') + (item.last_name || '') } })
    }
  });

  $(document).on('change', '[data-role="select-discount-type"]', function() {
    var readonlyColor = '#333';
    var editableColor = '#fff';
    var student_id = $("#facturacion_invoice_academico_students_id").val();
    var discountTypeSelect = $(this);
    var discountBox = discountTypeSelect.closest('.child').find('.detail_discount');
    var discountType = discountTypeSelect.val();
    if (student_id && discountType) {
      if (discountType !== 'custom') {
        $.getJSON('/facturacion/invoices/pick_discount.json?student_id=' + student_id + '&discount_type='+ discountType, function(data) {
          discountBox.val(data || 0).css('background-color', readonlyColor).attr('readonly', 'readonly');
          updateSubTotalDetail('detail_discount');
        });
      } else {
        discountBox.val(0).css('background-color', editableColor).removeAttr('readonly');
        updateSubTotalDetail('detail_discount');
      }
    }

  });

  $(document).on('change', '#academico_time_frame', function() {
    searchForGroupCourses();
  });
  $(document).on('change', '#academico_level', function() {
    searchForGroupCourses();
  });
  $(document).on('change', '#academico_program', function() {
    searchForGroupCourses();
  });


});

function student_change(){
  student = $("#facturacion_invoice_academico_students_id");
  student.on("change", function () {
    if (isNaN($(this).val()) || isEmpty($(this).val())){
      $(".remove_child").click();
    } else {

      var studentId = $(this).val();
      if (studentId) {
        $.getJSON('/facturacion/invoices/' + studentId + '/student.json', function(data) {
          var student = data.academico_student;

          $('#facturacion_invoice_admin_branch_office_id').val(student.admin_branch_office_id  || '').trigger('change');

          $('#academico_level').empty().trigger('change');
          if (student.levels_seed) {
            student.levels_seed.forEach(function iterateLevelsForStudent(level) {
              $('#academico_level').append($('<option>', {
                value: level.id,
                text: level.name})
              );
            });
            $('#academico_level').trigger('change');
          }
          $('#academico_program').empty().trigger('change');
          if (student.programs_seed) {
            student.programs_seed.forEach(function iterateProgramsForStudent(program) {
              $('#academico_program').append($('<option>', {
                value: program.id,
                text: program.name})
              );
            });
            $('#academico_program').trigger('change');
          } else {
            alert("El estudiante no tiene un programa asociado, seleccionar un programa en el formulario de preregistro")
          }
        });
      }

    }
  });
}

function searchForGroupCourses() {
  var academicoTimeFrameId = $('#academico_time_frame').val();
  var academicoLevelId = $('#academico_level').val();
  var academicoProgramId = $('#academico_program').val();

  if (academicoTimeFrameId || academicoLevelId || academicoProgramId) {
    var requestData = {};
    if (academicoTimeFrameId) {
      requestData['time_frame_id'] = academicoTimeFrameId;
    }
    if (academicoLevelId) {
      requestData['level_id'] = academicoLevelId;
    }
    if (academicoProgramId) {
      requestData['program_id'] = academicoProgramId;
    }

    $.getJSON('/facturacion/invoices/student_courses.json', requestData).done(
      function(data) {
        if (data.academico_courses) {
          var courses = data.academico_courses;
          $('#facturacion_invoice_academico_course_id').empty().trigger('change');
          if (courses.length && courses.length > 0 && courses[0].id) {

            courses.forEach(function iterateCoursesForStudent(course) {
              $('#facturacion_invoice_academico_course_id').append($('<option>', {
                value: course.id,
                text: course.name})
              );
            });
            $('#facturacion_invoice_academico_course_id').trigger('change');
          }
        }
      }
    );

  }

}

function invoices_detail_triger() {
  invoice_elements = $(".invoice_detail_element");
  invoice_elements.each(function(){
    var el = $(this);
    el.on("change", function () {
      var child = el.closest("div.child");
      var paymentConceptId = child.find('.payment_detail_concept_id');
      var courseId = $('#facturacion_invoice_academico_course_id').val();
      var studentId = $("#facturacion_invoice_academico_students_id").val();
      var bookId = child.find('.book_id').val();

      if (courseId && studentId && paymentConceptId.val()) {
        getElementAttributes("/facturacion/invoices/detail_values",
                          { element_id: paymentConceptId.val(), student_id: studentId,
                            course_id: courseId, book_id: bookId
                          }, paymentConceptId.attr("id"));
      }
    });
  });
  $("#facturacion_invoice_academico_course_id").on('change', function () {
    invoice_elements.trigger('change');
  })
}

function getElementAttributes(remoteMethod, params, domId) {
  $(".container.spinner").show();
  parameteresObject = ""
  Object.keys(params).forEach(function(key) {
    parameteresObject += key + "=" + params[key] + "&"
  })
  $.getScript(remoteMethod+'?'+ parameteresObject +'domId=' + domId, function() {
    init_select2();
  });
}

function valueSetter(dom, target, value) {
  $("#"+dom).parent().siblings().find(target).val(value);
}