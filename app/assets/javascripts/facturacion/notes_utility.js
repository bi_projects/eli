var noteTotal = 0.00;

function updateTotalListenerByClass(dom){
  elements = document.getElementsByClassName(dom);
  prices = document.getElementsByClassName("detail_amount");
  if (elements.length > 0)
    Array.prototype.forEach.call(elements, function(el){
      el.addEventListener('keypress', function(evt){
        decimalValidation(el, evt);
      });
      el.addEventListener('change', function(evt){
        setnoteTotal(prices);
      });
      el.addEventListener('keyup', function(evt){
        setnoteTotal(prices);
      });
    }); 
}

function setnoteTotal(prices) {
  var total_price = 0.00;
  Array.prototype.forEach.call(prices, function(innerEl){
    if (isNaN(innerEl.value) || isEmpty(innerEl.value)){
      innerEl.value = 0;
    }
    total_price += parseFloat(innerEl.value);;
  });
 
  noteTotal = total_price;
  if (isNaN(noteTotal)){
    noteTotal = 0
  }
  if (document.getElementById("total_note") != null) {
    document.getElementById("total_note").innerHTML = noteTotal.toFixed(2);
    console.log(noteTotal);
  }
}


updateTotalListenerByClass("detail_amount");
$(document).ready(function(){
  prices = document.getElementsByClassName("detail_amount");
  setnoteTotal(prices);
  $('.add_child').click(generateAddChild(
    function() {
      invokeFacturacionListeners();
    })
  );
  $(document).on('change','.associate_invoice', function() {
    $(this).closest('.clearfix.child').find('[data-role="invoice_reference"]').toggleClass('note_of_invoice');
  });
  el = $("#facturacion_note_academico_student_id");
  el.on("change", function () {
    var studentId = $(this).val();
    $.getJSON('/facturacion/notes/student_invoices/' + studentId + '.json', function(data) {
      if (data.facturacion_invoices) {
        var invoices = data.facturacion_invoices;
        $('.student_invoice').empty();
        if (invoices.length && invoices.length > 0) {
          $('.student_invoice').each(function(i, invoiceSelect) {
            $(invoiceSelect).append($('<option></option>'));
            $.each(invoices, function(j, invoice) {
              $(invoiceSelect).append(
                $('<option></option>')
                .attr('value', invoice.id)
                .text(invoice.invoice_number)
              );
            });
            $(invoiceSelect).trigger('change');
          });
        }
      }
    });
  });

  $(document).on('change', '.student_invoice', function() {
    var invoiceId = $(this).val();
    if (invoiceId) {
      $.getJSON('/facturacion/notes/invoice_concepts/' + invoiceId + '.json', function(data) {
        if (data.invoice_concepts) {
          var invoice_concepts = data.invoice_concepts;
          $('.concept_select').empty();
          if (invoice_concepts.length && invoice_concepts.length > 0) {

            $('.concept_select').each(function(i, conceptSelect) {
              $(conceptSelect).append($('<option></option>'));
              $.each(invoice_concepts, function(j, concept) {
                $(conceptSelect).append(
                  $('<option></option>')
                  .attr('value', concept.id)
                  .text(concept.book_name)
                );
              });
              $(conceptSelect).trigger('change');
            });

          }
        }
      });
    }
    
  });
});