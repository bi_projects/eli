// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery_ujs

//= require_tree .
//= require gentelella
//= require gentelella-custom
//= require cocoon

//= stub webRes/plugin
//= stub webRes/scripts
//= stub webRes/jquery.min
//= stub jquery
//= stub dropzone-amd-module
//= stub dropzone
//= stub facturacion/invoices_utility
//= stub facturacion/receipts_utility
//= stub facturacion/notes_utility
//= stub facturacion/stocks_utility
//= stub academico/courses
//= stub academico/evaluation_schemes
//= stub academico/temaries
//= stub academico/time_frame
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap

/*TURBO LINKS WERE REMOVED require turbolinks*/
var start_date = moment().subtract(4, 'years');
var birth_date = moment();
var document_date = moment();
// use it for add_child click handler
// receives callback to add custom code specific to page
function generateAddChild(callback) {
	return function addChildHandler() {
		var association = $(this).attr('data-association');
		var target = $(this).attr('target');
		var regexp = new RegExp('new_' + association, 'g');
		var new_id = new Date().getTime();
		var Dest = (target == '') ? $(this).parent() : $('#'+target);
		Dest.append(window[target+'_fields'].replace(regexp, new_id));
		//inicializar selects y checkboxes plugins en caso de que existan
		if (typeof callback !== 'undefined' && $.isFunction(callback)) {
			callback();
		}
		init_select2();
		flatConverter();
    switchConverter();
		return false;
	}
}

function invokeFacturacionListeners() {
	updateTotalListenerByClass("detail_amount");
	updateTotalListenerByClass("detail_discount");
	updateTotalListenerByClass("payment_detail_amount");
}

function removeHandler(removeButton) {
	$(removeButton).closest('.child').find('.removable')[0].value = 1;
	$(removeButton).closest('.child').hide().find(".form-control").each(function(index, element) {
		element.removeAttribute("required");
	});
	return false;
}

function initFieldDatepickers() {
	$('.field_datepicker').each(function(index, elem) {
		var minDate = moment().subtract(6, 'years');
		var maxDate = moment().add(10, 'month');

		$(elem).daterangepicker({
			singleDatePicker: true,
			calender_style: "picker_2",
			showDropdowns: true,
			opens: 'right',
			locale: {
				format: 'DD/MM/YYYY',
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Desde',
				toLabel: 'Hasta',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
			$(elem).val(start.format('DD-MM-YYYY'));
		});
	});
}

$(document).ready(function(){
	$(".children:first-child .remove_child").remove();
	// global remove children details
	$(document).delegate('.remove_child','click', function() {
		removeHandler(this);
		loadAmounts();
	});

	$('.datepicker').each(function(index, elem) {
		var isSchedulle = $(elem).hasClass('schedulle');
		var startDate = isSchedulle ? moment() : start_date;
		var minDate = isSchedulle ? moment().subtract(6, 'years') : moment().subtract(60, 'years');
		var maxDate = isSchedulle ? moment().add(6, 'month') : moment().subtract(4, 'years');

		$(elem).daterangepicker({
			autoUpdateInput: $(elem).hasClass('is-filter') ? false : true,
			singleDatePicker: true,
			calender_style: "picker_2",
			minDate: minDate,
			startDate: startDate,
			maxDate: maxDate,
			showDropdowns: true,
			opens: 'right',
			locale: {
				format: 'DD/MM/YYYY',
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Desde',
				toLabel: 'Hasta',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
			if ($(elem).hasClass('is-filter')) {
				$(elem).val(start.format('DD-MM-YYYY'));
			}
		});
	});

	$('.document_datepicker').daterangepicker({
		autoUpdateInput: true,
		singleDatePicker: true,
		calender_style: "picker_2",
		startDate: document_date,
		maxDate: moment(),
		showDropdowns: true,
		opens: 'right',
		locale: {
			format: 'DD/MM/YYYY',
			applyLabel: 'Aceptar',
			cancelLabel: 'Cancelar',
			fromLabel: 'Desde',
			toLabel: 'Hasta',
			customRangeLabel: 'Custom',
			daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			firstDay: 1
		}
	}, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});

	$('.datepicker.birth_date').daterangepicker({
		autoUpdateInput: true,
		singleDatePicker: true,
		calender_style: "picker_2",
		minDate: moment().subtract(60, 'years'),
		startDate: birth_date,
		maxDate: moment(),
		showDropdowns: true,
		opens: 'right',
		locale: {
			format: 'DD/MM/YYYY',
			applyLabel: 'Aceptar',
			cancelLabel: 'Cancelar',
			fromLabel: 'Desde',
			toLabel: 'Hasta',
			customRangeLabel: 'Custom',
			daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			firstDay: 1
		}
	}, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	});

	initFieldDatepickers();

	// Select the submit buttons of forms with data-confirm attribute
	var submit_buttons = $("form[data-confirm] a#delete_records.submit-form");
	// On click of one of these submit buttons
	submit_buttons.on('click', function (e) {

		// Prevent the form to be submitted
		e.preventDefault();

		var button = $(this); // Get the button
		var form = button.closest('form'); // Get the related form
		var msg = form.data('confirm'); // Get the confirm message

		if(confirm(msg)) form.submit(); // If the user confirm, submit the form
	});

	var selectColChecks = $('.select_col');
	if (selectColChecks.length > 0) {
		$(document).on('ifChecked', '.select_col', function() {
			selectAllColumn(true, this);
		});
		$(document).on('ifUnchecked', '.select_col', function() {
			selectAllColumn(false, this);
		});
	}

	var selectRowChecks = $('.select_row');
	if (selectRowChecks.length > 0) {
		$(document).on('ifChecked', '.select_row', function() {
			selectAllRow(true, this);
		});
		$(document).on('ifUnchecked', '.select_row', function() {
			selectAllRow(false, this);
		});
	}
	keyPressListener("document_numeric_input");

	$('#eli-datatable').DataTable({paging: false, order: [[1, 'asc']]});
});

function selectAllRow(checked, source) {
	var $headCheckbox = $(source);
	var currentRow = $headCheckbox.closest('tr');
	var rowIndex = -1;
	if (currentRow.length > 0) {
		rowIndex = currentRow.index();
		var currentTable = currentRow.closest('table');
		if (currentTable.length > 0) {
			// we use rowIndex + 1 since jQuery's index() is 1 based but css selector nth-child is zero based
			var sameRowChecks = currentTable.find('tr:nth-child(' + (rowIndex + 1) +')');
			if (sameRowChecks.length > 0) {
				if (checked) {
					sameRowChecks.find('input[type="checkbox"]').iCheck('check');
				} else {
					sameRowChecks.find('input[type="checkbox"]').iCheck('uncheck');
				}
			}
		}
	}
}
function selectAllColumn(checked, source) {
	var $headCheckbox = $(source);
	var currentTH = $headCheckbox.closest('th');
	var colIndex = -1;
	if (currentTH.length > 0) {
		colIndex = currentTH.index();
		var currentTable = currentTH.closest('table');
		if (currentTable.length > 0) {
			// we use colIndex + 1 since jQuery's index() is 1 based but css selector nth-child is zero based
			var sameColumnChecks = currentTable.find('tr td:nth-child(' + (colIndex + 1) +')');
			if (sameColumnChecks.length > 0) {
				if (checked) {
					sameColumnChecks.find('input[type="checkbox"]').iCheck('check');
				} else {
					sameColumnChecks.find('input[type="checkbox"]').iCheck('uncheck');
				}
			}
		}
	}
}
var elements;
function keyPressListener(dom) {
	elements = document.getElementsByClassName(dom);
	if (elements.length > 0)
		Array.prototype.forEach.call(elements, function(el){
			el.addEventListener('keypress', function(evt){
				decimalValidation(el, evt);
			});
		});
};

function decimalValidation(el, evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	var number = el.value.split('.');
	if(charCode == 8) {
		return true;
	}
	if(el.value.length<1 && charCode == 48 || el.value.length<1 && charCode == 46 || charCode == 13){
		evt.preventDefault();
		return false;
	}

	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
		evt.preventDefault();
		return false;
	}
    if(number.length>1 && charCode == 46){
    	evt.preventDefault();
    	return false;
    }
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
    	evt.preventDefault();
    	return false;
    }
    return true;
}

function getSelectionStart(o) {
	return o.selectionStart;
}

function cleanHasOneOption(param) {
	if (param !== '') {
		$('#destroy_academic_user_position').attr('disabled', 'disabled');
	} else {
		$('#destroy_academic_user_position').removeAttr('disabled');
	}
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function flatConverter() {
	$('input.flat').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});
}

function switchConverter() {
  // apply switch only once to elements that don't have it yet
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch:not([data-switchery])'));
  elems.forEach(function (html) {
    var switchery = new Switchery(html, {
      color: '#26B99A'
    });
  });
}

function getElementBySiblings(dom, target){
  return $("#"+dom).parent().siblings().find(target);
}

// set select2 with a filter to search in a custom url via ajax
// processCallback is a function to transform the ajax result to a something compatible with select2
// url is a data property set in the select
function initSelectSearch(processCallback) {

	$(".select2_search").select2({
    ajax: {
      url: function(params) {
        return $(this).data('endpoint');
      },
      processResults: processCallback
    }
  });
}