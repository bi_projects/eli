module FacturacionUtility
  extend ActiveSupport::Concern

  included do
    before_create :set_billing_user

    def self.exists_daily_process?(date)
      Facturacion::DailyProcess.where(facturacion_billing_user_id: Facturacion::BillingUser.current.try(:id))
                               .where('DATE(daily_process_date) = ?', date.end_of_day.in_time_zone).any?
    end
  end

  private

  def set_billing_user
    self.facturacion_billing_user_id = Facturacion::BillingUser.current.try(:id)
  end
end
