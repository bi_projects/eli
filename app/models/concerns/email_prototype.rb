module EmailPrototype
  extend ActiveSupport::Concern

  included do
    validates_format_of :email,
      :email => true,
      :with => /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i,
      :unless => Proc.new {|c| c.email.blank?},
      :uniqueness => { :case_sensitive => false }
  end

end