module AvoidManageDocumentUtility
	extend ActiveSupport::Concern
	
	private

	def isPrinted?(printed)
		if printed
			raise CustomExceptions::DocumentException, "No es posible hacer gestiones con el registro, el registro ya ha sido impreso."
			#return false
		end
	end

	module ClassMethods
		
	end
end
