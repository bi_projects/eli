module CourseFiltering
  extend ActiveSupport::Concern


  module ClassMethods
    

    def quota_by_course(academico_course_schedulle_id: nil, academico_course_modality_id: nil, branch_office_id: nil)
      courses = Academico::Course.on_going
      courses = courses.joins(academico_level_program: [:academico_level, :academico_program])

      if branch_office_id.present?
        courses = courses.office_eq(branch_office_id)
      end
      if academico_course_schedulle_id.present?
        courses = courses.schedulle_eq(academico_course_schedulle_id)
      end
      if academico_course_modality_id.present?
        courses = courses.modality_eq(academico_course_modality_id)
      end

      courses = courses.order('academico_programs.name, academico_levels.name')

      courses_by_program = {}
      courses.each do |course_row|
        if courses_by_program[course_row.academico_program.name].blank?
          courses_by_program[course_row.academico_program.name] = []
        end
        courses_by_program[course_row.academico_program.name] << {
          level: course_row.academico_level.name,
          taken_seats: course_row.taken_seats,
          seats: course_row.seats,
          idle_seats: course_row.idle_seats
        }
      end
      courses_by_program
    end


    # returns array of courses
    def filter_courses(level_id=nil, program_id=nil, modality_id=nil, schedulle_id=nil, status_alias=Academico::Course::ACTIVE_COURSE)
      if status_alias == Academico::Course::ACTIVE_COURSE
        courses = Academico::Course::current
      else
        courses = Academico::Course::status_eq(Academico::CourseStatus::FINISHED)
      end

      courses = courses::level_eq(level_id) if level_id.present?
      courses = courses.program_eq(program_id) if courses.present? && program_id.present?
      courses = courses.modality_eq(modality_id) if courses.present? && modality_id.present?
      courses = courses.schedulle_eq(schedulle_id) if courses.present? && schedulle_id.present?
      if courses.present?
        return courses.sort_by {|course| course.idle_seats}
      else
        return []
      end
    end


    def courses_from_teacher(staff_id, is_new)
      courses = Academico::Course.where("academico_staff_id = ? or second_academico_staff_id = ?", staff_id, staff_id)
      if is_new
        courses = courses.where("NOT EXISTS(SELECT id FROM academico_course_evaluations WHERE academico_courses_id=academico_courses.id AND academico_staffs_id = ?) AND academico_course_status_id = ?", staff_id, Academico::CourseStatus::ON_GOING)
      end
      courses
    end

    def filter_groups_of_courses(time_frame_id = nil, level_id = nil, program_id = nil)
      courses = Academico::Course.current
      courses = courses.time_frame_eq(time_frame_id) if time_frame_id.present?
      courses = courses.level_eq(level_id) if level_id.present?
      courses = courses.program_eq(program_id) if program_id.present?

      if courses.present?
        return courses.sort_by {|course| course.idle_seats}
      else
        return []
      end
    end

  end

end