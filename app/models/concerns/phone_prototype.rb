module PhonePrototype
  extend ActiveSupport::Concern

  included do
    validates_format_of :phone,
      :with => /\A([\s\-\d]{0,16})\z/,
      :unless => Proc.new {|c| c.phone.blank?}
  end

end