module FormatDate
  extend ActiveSupport::Concern

  def formatted_date(field, opt_value = nil)
    if !self.new_record? && self.send(field).present?
      self.send(field).strftime("%d/%m/%Y")
    else
      opt_value.present? ? opt_value.strftime("%d/%m/%Y") : Date.today.strftime("%d/%m/%Y")
    end
  end

end