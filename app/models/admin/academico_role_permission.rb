class Admin::AcademicoRolePermission < ApplicationRecord
  belongs_to :admin_resource, class_name: 'Admin::Resource'
  belongs_to :academico_role, :inverse_of => :academico_role_permissions, class_name: 'Academico::AcademicoRole', foreign_key: :academico_academico_role_id
  belongs_to :action
  has_many :academico_user_roles, through: :academico_role

  accepts_nested_attributes_for :academico_role
end
