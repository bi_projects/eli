class Admin::AdminUser < ApplicationRecord
	mount_uploader :avatar, AvatarUploader

	has_one :admin_admin_user_position, :class_name => "Admin::AdminUserPosition",
		foreign_key: "admin_admin_user_id", :inverse_of => :admin_admin_user

	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable
	include UserModelUtility
end
