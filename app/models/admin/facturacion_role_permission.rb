class Admin::FacturacionRolePermission < ApplicationRecord
  belongs_to :admin_resource, class_name: 'Admin::Resource'
  belongs_to :facturacion_facturacion_role, :inverse_of => :facturacion_role_permissions, class_name: 'Facturacion::FacturacionRole', foreign_key: :facturacion_facturacion_role_id
  belongs_to :action
  has_many :facturacion_user_roles, through: :facturacion_facturacion_role

  accepts_nested_attributes_for :facturacion_facturacion_role
end
