class Admin::AdminUserPosition < ApplicationRecord
  belongs_to :admin_admin_user, :class_name => "Admin::AdminUser"
  belongs_to :admin_position, :class_name => "Admin::Position"
end
