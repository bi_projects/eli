class Admin::ExchangeRate < ApplicationRecord
	#File can be found on http://www.bcn.gob.ni/estadisticas/mercados_cambiarios/tipo_cambio/cordoba_dolar/
	attr_accessor :bcn_file

	def self.current_exchange_rate
		exchange_rate = self.find_by_date(Date.today.strftime("%d/%m/%Y"))
		exchange_rate.present? ? exchange_rate.value.round(4) : 0.00
	end

end
