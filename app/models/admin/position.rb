class Admin::Position < ApplicationRecord
	before_destroy :check_for_admin_user, :check_for_billing_user, :check_for_academic_user

  TEACHER = 1
  COORDINATOR = 2
  SUPERVISOR = 3
	has_many :admin_admin_user_positions, :class_name => "Admin::AdminUserPosition",
		foreign_key: "admin_position_id", :inverse_of => :admin_position
	has_many :admin_billing_user_positions, :class_name => "Admin::BillingUserPosition",
		foreign_key: "admin_position_id", :inverse_of => :admin_position
	has_many :admin_academic_user_positions, :class_name => "Admin::AcademicUserPosition",
		foreign_key: "admin_position_id", :inverse_of => :admin_position

	include AvoidDestroyReferencesUtility

	def display_name
		name
	end

	scope :academico_positions, -> () { where('admin_module_id = ?', Admin::Module::ACADEMIC)}

	private
	def check_for_admin_user
		check_for_relation(admin_admin_user_positions, "Usuario Administativo")
	end
	def check_for_billing_user
		check_for_relation(admin_billing_user_positions, "Usuario Facturación")
	end
	def check_for_academic_user
		check_for_relation(admin_academic_user_positions, "Usuario Académico")
	end
end
