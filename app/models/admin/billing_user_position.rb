class Admin::BillingUserPosition < ApplicationRecord
  belongs_to :facturacion_billing_user, :class_name => "Facturacion::BillingUser"
  belongs_to :admin_position, :class_name => "Admin::Position"
end
