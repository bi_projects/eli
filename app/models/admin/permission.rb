class Admin::Permission < ApplicationRecord
  belongs_to :admin_resource, inverse_of: :admin_permissions, class_name: "Admin::Resource"
  belongs_to :action
end
