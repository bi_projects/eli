class Admin::Resource < ApplicationRecord
  belongs_to :resource_type, inverse_of: :admin_resources
  has_many :admin_permissions, inverse_of: :admin_resource, foreign_key: :admin_resource_id, class_name: "Admin::Permission"
end
