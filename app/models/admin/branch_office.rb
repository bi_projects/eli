class Admin::BranchOffice < ApplicationRecord
	before_destroy :check_for_invoice

	has_many :facturacion_invoices, :class_name => "Facturacion::Invoice",
    	foreign_key: "admin_branch_office_id", :inverse_of => :admin_branch_office

	has_many :facturacion_receipts, :class_name => "Facturacion::Receipt",
    	foreign_key: "admin_branch_office_id", :inverse_of => :admin_branch_office

	has_many :academico_students, :class_name => "Academico::Student",
			foreign_key: "admin_branch_office_id", :inverse_of => :admin_branch_office

	has_many :academico_rooms, :class_name => "Academico::Room",
			foreign_key: "admin_branch_office_id", :inverse_of => :admin_branch_office

	has_many :facturacion_warehouses, :class_name => "Facturacion::Warehouse",
			foreign_key: "admin_branch_office_id", :inverse_of => :admin_branch_office

	has_many :facturacion_billing_users, class_name: 'Facturacion::BillingUser'

  validates_presence_of :name, :description

  include AvoidDestroyReferencesUtility

  def display_name
		name
	end

	private
	def check_for_invoice
		check_for_relation(facturacion_invoices, "Facturas")
	end
end
