class Admin::Parameter < ApplicationRecord
	before_create :avoid_multiple_records
	before_destroy :avoid_destroy_record
	validates_presence_of :invoice_prefix, :invoice_number, :receipt_prefix, :receipt_number,
						:credit_note_prefix, :credit_note_number, :debit_note_prefix, :debit_note_number, :student_prefix
	validates :student_code_length, numericality: true
	validates :drivers_discount, numericality: true

	def self.read_student_prefix
		self.last.try(:student_prefix) || 'ELI'
	end

	def self.read_student_code_length
		self.last.try(:student_code_length) || 5
	end

	private

	def avoid_multiple_records
		if Admin::Parameter.all.present?
			raise CustomExceptions::AdminException, "Solo es permitido un registro para la coleccion de parámetros"
		end
	end

	def avoid_destroy_record
		raise CustomExceptions::AdminException, "El registro ingresado no puede ser eliminado, es requerido para la funcionalidad correcta del sistema"
	end

end
