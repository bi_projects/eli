class Admin::AcademicUserPosition < ApplicationRecord
  belongs_to :academico_academic_user, :class_name => "Academico::AcademicUser"
  belongs_to :admin_position, :class_name => "Admin::Position"
end
