class Academico::CourseStatus < ApplicationRecord
  has_many :academico_courses, :class_name => "Academico::Course",
		foreign_key: "academico_course_status_id", :inverse_of => :academico_course_status
  REGISTRATION = 1
  ON_GOING = 2
  FINISHED = 3
end
