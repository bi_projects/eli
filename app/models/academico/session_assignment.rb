class Academico::SessionAssignment < ApplicationRecord
  belongs_to :academico_session_parameter, class_name: 'Academico::SessionParameter', foreign_key: :academico_session_parameters_id
  has_many :academico_student_scores, class_name: 'Academico::StudentScore', foreign_key: :academico_session_assignments_id,
    inverse_of: :academico_session_assignment
end
