class Academico::Staff < ApplicationRecord
  belongs_to :academico_staff_status, class_name: 'Academico::StaffStatus',
    foreign_key: 'academico_staff_statuses_id', inverse_of: :academico_staffs
  belongs_to :academico_academic_user, class_name: 'Academico::AcademicUser',
    foreign_key: 'academico_academic_users_id', inverse_of: :academico_staff
  belongs_to :academico_staff, class_name: 'Academico::Staff',
    foreign_key: 'academico_staffs_id'
  has_many :academico_level_skills, class_name: 'Academico::LevelSkill',
    foreign_key: 'academico_staffs_id', inverse_of: :academico_staff
  has_many :academico_courses, class_name: 'Academico::Course',
    foreign_key: 'academico_staff_id', inverse_of: :academico_staff
  has_many :academico_course_evaluations, class_name: 'Academico::CourseEvaluation',
    foreign_key: 'academico_staffs_id', inverse_of: :academico_staff
  has_many :academico_course_results, class_name: 'Academico::CourseResult',
    foreign_key: 'academico_staffs_id', inverse_of: :coordinator
  has_many :academico_teacher_attendances, inverse_of: :academico_staff,
    class_name: 'Academico::TeacherAttendance', foreign_key: :academico_staffs_id
  has_many :academico_teacher_attendances, inverse_of: :coordinator,
    class_name: 'Academico::TeacherAttendance', foreign_key: :created_by
  scope :staff_by_positions, -> (position_groups) { joins(academico_academic_user: :admin_academic_user_position)
	.where('admin_academic_user_positions.admin_position_id IN (?)', position_groups)
  }
  scope :staff_by_skill, -> (level_program_id) {
    joins(:academico_level_skills)
    .where('academico_level_skills.academico_level_programs_id = ?', level_program_id)
  }
  scope :staff_rotation, -> () {
    select('academico_staffs.*, count(academico_courses.id) courses')
    .joins('LEFT JOIN academico_courses ON academico_courses.academico_staff_id = academico_staffs.id AND academico_courses.academico_course_status_id = 2')
    .group(:id)
    .order('courses asc')
  }
  scope :staff_by_status, -> (status_id) { where('academico_staff_statuses_id = ?', status_id) }
  accepts_nested_attributes_for :academico_level_skills, allow_destroy: true, reject_if: :all_blank
  def display_name
    self.academico_academic_user.name + ' - ' + self.academico_academic_user.display_user_position
  end
end
