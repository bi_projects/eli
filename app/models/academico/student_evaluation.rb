class Academico::StudentEvaluation < ApplicationRecord
  belongs_to :academico_session_evaluation, class_name: 'Academico::SessionEvaluation',
    foreign_key: "academico_session_evaluations_id", inverse_of: :academico_student_evaluations
  belongs_to :academico_student, class_name: 'Academico::Student',
    foreign_key: "academico_students_id", inverse_of: :academico_student_evaluations
  has_many :academico_student_scores, class_name: 'Academico::StudentScore',
    foreign_key: "academico_student_evaluations_id", inverse_of: :academico_student_evaluation, dependent: :delete_all
  accepts_nested_attributes_for :academico_student_scores,
    reject_if: proc { |attributes| attributes['score'].blank? }
  # Used to guarantee order of elements according to session.all_session_assignments
  def all_scores
    academico_student_scores.sort_by { |score| score.academico_session_assignments_id}
  end
end
