class Academico::EvaluationParameterType < ApplicationRecord
  has_many :academico_evaluation_parameters, :class_name => "Academico::EvaluationParameter",
    :inverse_of => "academico_evaluation_parameter", foreign_key: "academico_evaluation_parameter_type_id"

  # ---formula field is used to calculate grades---
  # average means all the parameters of this type must be averaged
  # average_then_sum means that if there are other parameters with this in a session they must be averaged and the value added to the calculated value of parameters in other sessions
  # sum_then_average is like average_then_sum but parameters alike are first added in the same session and the result is averaged with othere sessions
  # sum is used in like final exams the score is added straight to final score
  enum formula: {average_then_sum: 1, sum_then_average: 2, average_only: 3, sum_only: 4}
end
