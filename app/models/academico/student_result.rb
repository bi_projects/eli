class Academico::StudentResult < ApplicationRecord
  after_save :inactivate_user_after_result
  belongs_to :academico_student, class_name: 'Academico::Student',
    foreign_key: :academico_students_id, inverse_of: :academico_student_results
  belongs_to :academico_course_result, class_name: 'Academico::CourseResult',
    foreign_key: :academico_course_results_id, inverse_of: :academico_student_results


  # get every evaluation of the student during the course with a teacher
  def pull_evaluations(staff_id)
    self.academico_student.academico_student_evaluations.select do |student_evaluation|
      course_evaluation = student_evaluation.academico_session_evaluation.academico_course_evaluation
      course_evaluation.academico_courses_id == self.academico_course_result.academico_course.id && course_evaluation.academico_staffs_id == staff_id
    end
  end


  # get all the sessions given the related temary
  def pull_sessions(staff_id)
    sessions = pull_temary(staff_id).academico_sessions.try(:to_a)
    sessions || []
  end


  # get all the scores of the student during a course with a teacher
  def pull_student_scores(staff_id)
    evaluations = pull_evaluations(staff_id)
    pull_scores_from_evaluations(evaluations)
  end


  def pull_scores_from_evaluations(evaluations)
    student_scores = evaluations.map(&:academico_student_scores).flatten
    return nil if student_scores.empty?
    student_scores
  end


  def pull_temary(staff_id)
    academico_course_result.academico_course.temary_record(staff_id)
  end


  def scores_groups(by_staff_id)

    scores = pull_student_scores(by_staff_id)

    score_groups_map = {}
    scores.each do |score_record|
      session_parameter = score_record.academico_session_parameter
      if score_groups_map[session_parameter.academico_session.topic].blank?
        score_groups_map[session_parameter.academico_session.topic] = { }
      end

      if score_groups_map[session_parameter.academico_session.topic][session_parameter.academico_evaluation_parameter.name].blank?
        score_groups_map[session_parameter.academico_session.topic][session_parameter.academico_evaluation_parameter.name] = { score: score_record.score, goal: score_record.max_possible_score }
      end
    end
    score_groups_map
  end


  # creates a map with all the average scores for every evaluation parameter in the course
  def parameter_avg(staff_id)
    temary = pull_temary(staff_id)
    return nil if temary.blank?
    student_scores = pull_student_scores(staff_id)
    return nil if student_scores.blank?
    evaluation_methods = temary.fetch_evaluation_scheme.academico_evaluation_methods

    calculate_parameter_avgs(student_scores, temary, evaluation_methods)
  end


  def calculate_parameter_avgs(student_scores, temary, evaluation_methods)
    score_groups, assignment_groups, parameter_weights = Hash.new, Hash.new, Hash.new

    student_scores.each do |student_score|
      evaluation_parameter = student_score.academico_session_parameter.academico_evaluation_parameter
      parameter = evaluation_parameter.name

      if score_groups[parameter].blank?
        score_groups[parameter] = []
        assignment_groups[parameter] = temary.assignments_by_parameter(evaluation_parameter.id)
        evaluation_method = evaluation_methods.find {|em| em.academico_evaluation_parameter.id == evaluation_parameter.id}
        parameter_weights[parameter] = evaluation_method.percentage
      end
      max_scale = student_score.max_possible_score

      # scores are calculated at a parameter base, assignment_score/assignment_max_grade
      score_groups[parameter] << (max_scale.present? ? student_score.score.to_f/max_scale : 0)
    end
    # the average of parameter is multiplied by the weight of that parameter
    Hash[score_groups.map do |parameter, scores|
      [parameter, assignment_groups[parameter].length > 0 ?
        (scores.reduce(0, :+)/assignment_groups[parameter].length * parameter_weights[parameter]).round(1)
         : 0]
      end
    ]
  end


  # get the evaluations for the 1..2 teachers assigned to the course
  def teachers_evaluations
    if @evaluations.blank?
      course = academico_course_result.academico_course
      @evaluations = []
      parameters_avgs = parameter_avg(course.academico_staff_id)
      if parameters_avgs.present?
        scores_groups = scores_groups(course.academico_staff.id)
        @evaluations << {staff: course.academico_staff, avgs: parameters_avgs, scores_groups: scores_groups}
      end

      parameters_avgs = parameter_avg(course.second_academico_staff_id)
      if parameters_avgs.present?
        scores_groups = scores_groups(course.second_academico_staff.id)
        @evaluations << {staff: course.second_academico_staff, avgs: parameters_avgs, scores_groups: scores_groups}
      end
    end
    @evaluations
  end


  # get the teachers results in a map
  def grade_by_teachers
    Hash[teachers_evaluations.map {|teacher_evaluation| [teacher_evaluation[:staff].id, teacher_evaluation[:avgs].values.reduce(0, :+).round]}]
  end


  # get the final score of the student after consolidating both teachers results
  def final_score
    grades = grade_by_teachers.values
    grades.length > 0 ? grades.reduce(0, :+) / grades.length : 0
  end


  def inactivate_user_after_result
    self.academico_student.active = false
    if self.approved
      self.academico_student.update_level_up = self.academico_student.try(:current_level).try(:next_level_in_progression)
    else
      self.academico_student.update_level_up = self.academico_student.try(:current_level)
    end
    self.academico_student.save!
  end

end