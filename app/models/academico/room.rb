class Academico::Room < ApplicationRecord
  has_many :academico_courses, :class_name => "Academico::Course",
    :inverse_of => :academico_room, foreign_key: "academico_room_id"
  belongs_to :admin_branch_office, inverse_of: :academico_rooms,
    class_name: 'Admin::BranchOffice', foreign_key: :admin_branch_office_id
  validates :name, uniqueness: true
  validates :size, numericality: true

  scope :rooms_by_office, -> (office_id) {
    where('admin_branch_office_id = ?', office_id)
  }

  def branch_name
    admin_branch_office.present? ? admin_branch_office.name : ""
  end

end
