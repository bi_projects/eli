class Academico::CourseEvaluation < ApplicationRecord
  belongs_to :academico_course, :class_name => "Academico::Course",
    :inverse_of => :academico_course_evaluations, foreign_key: "academico_courses_id"
  belongs_to :academico_staff, class_name: "Academico::Staff",
    foreign_key: "academico_staffs_id", inverse_of: :academico_course_evaluations
  has_many :academico_session_evaluations, class_name: "Academico::SessionEvaluation",
    foreign_key: "academico_course_evaluations_id", inverse_of: :academico_course_evaluation, dependent: :destroy
  accepts_nested_attributes_for :academico_session_evaluations,
   allow_destroy: true,
   reject_if: :reject_session_if_no_students_records
  def reject_session_if_no_students_records(attributes)
    attributes[:academico_student_evaluations_attributes].count == 0
  end
end
