class Academico::SessionParameter < ApplicationRecord
  belongs_to :academico_evaluation_parameter, class_name: 'Academico::EvaluationParameter', foreign_key: :academico_evaluation_parameter_id
  belongs_to :academico_session, inverse_of: :academico_session_parameters,
    class_name: 'Academico::Session', foreign_key: :academico_session_id
  has_many :academico_session_assignments, inverse_of: :academico_session_parameter,
    class_name: 'Academico::SessionAssignment', foreign_key: :academico_session_parameters_id, dependent: :destroy
  has_many :academico_student_scores, through: :academico_session_assignments, class_name: 'Academico::StudentScore'
  def parameter_name
    self.academico_evaluation_parameter.name if self.academico_evaluation_parameter.present?
  end
  accepts_nested_attributes_for :academico_session_assignments,
    allow_destroy: true,
    reject_if: :all_blank

  def has_no_assignments
    is_there_any = self.academico_session_assignments.any?
    !is_there_any
  end
end
