class Academico::AcademicoRole < ApplicationRecord
  has_many :academico_user_roles, class_name: 'Academico::UserRole', foreign_key: :academico_academico_roles_id

  has_many :academico_role_permissions, inverse_of: :academico_role, class_name: "Admin::AcademicoRolePermission", foreign_key: 'academico_academico_role_id'
  accepts_nested_attributes_for :academico_role_permissions, allow_destroy: true
end
