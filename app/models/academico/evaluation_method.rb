class Academico::EvaluationMethod < ApplicationRecord
  belongs_to :academico_evaluation_scheme, :class_name => "Academico::EvaluationScheme",
  :inverse_of => :academico_evaluation_methods, foreign_key: "academico_evaluation_scheme_id"
  belongs_to :academico_evaluation_parameter, :class_name => "Academico::EvaluationParameter",
  :inverse_of => :academico_evaluation_methods, foreign_key: "academico_evaluation_parameter_id"
  validates_presence_of :percentage
  validates :percentage, numericality: true
end
