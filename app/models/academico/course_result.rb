class Academico::CourseResult < ApplicationRecord
  belongs_to :academico_course, :class_name => "Academico::Course",
    :inverse_of => :academico_course_result, foreign_key: "academico_courses_id"
  has_many :academico_student_results, class_name: 'Academico::StudentResult',
    foreign_key: :academico_course_results_id, inverse_of: :academico_course_result, dependent: :destroy
  belongs_to :coordinator, :class_name => "Academico::Staff",
    :inverse_of => :academico_course_results, foreign_key: "academico_staffs_id"
  accepts_nested_attributes_for :academico_student_results,
   allow_destroy: true,
   reject_if: :all_blank

  validate :course_not_repeated

  scope :course_results_by_course, -> (course_id) {
    where('academico_courses_id = ?', course_id)
  }

  private

  def course_not_repeated
    courses = Academico::CourseResult.course_results_by_course(self.academico_courses_id)
    courses = courses.where('id <> ?', self.id) if self.persisted?
    errors.add(:base, 'Ya existen resultados guardados para este curso') if courses.present?
  end
end
