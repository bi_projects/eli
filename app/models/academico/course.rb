# This class corresponds to a course group i.e. Level 1 of Adults
# is named course because it existed before time_frame was even conceived

class Academico::Course < ApplicationRecord
  include Filterable
  include AvoidDestroyReferencesUtility
  include FormatDate
  include CourseFiltering

  ACTIVE_COURSE = 'active'
  INACTIVE_COURSE = 'inactive'
  before_destroy :check_for_registrations
  after_save :update_teacher_attendance, :update_course_evaluations, :create_course_price_record

  belongs_to :academico_time_frame, :class_name => "Academico::TimeFrame",
    :inverse_of => :academico_courses, foreign_key: "academico_time_frame_id"
  belongs_to :academico_level_program, :class_name => "Academico::LevelProgram",
    :inverse_of => :academico_courses, foreign_key: "academico_level_programs_id"
  belongs_to :academico_staff, :class_name => "Academico::Staff",
    :inverse_of => :academico_courses, foreign_key: "academico_staff_id"
  belongs_to :second_academico_staff, :class_name => "Academico::Staff",
    :inverse_of => :academico_courses, foreign_key: "second_academico_staff_id"
  belongs_to :academico_course_status, :class_name => "Academico::CourseStatus",
    :inverse_of => :academico_courses, foreign_key: "academico_course_status_id"
  belongs_to :academico_room, :class_name => "Academico::Room",
    :inverse_of => :academico_courses, foreign_key: "academico_room_id"
  has_many :academico_registrations, :class_name => "Academico::Registration",
    :inverse_of => :academico_course, foreign_key: "academico_courses_id"
  belongs_to :created_by_staff, :class_name => "Academico::Staff",
    foreign_key: "created_by"
  belongs_to :updated_by_staff, :class_name => "Academico::Staff",
    foreign_key: "updated_by"
  has_many :academico_temaries, inverse_of: :academico_course,
   class_name: 'Academico::Temary', foreign_key: :academico_courses_id
  has_many :academico_teacher_attendances, inverse_of: :academico_course,
    class_name: 'Academico::TeacherAttendance', foreign_key: :academico_courses_id
  has_many :academico_course_evaluations, inverse_of: :academico_course,
   class_name: 'Academico::CourseEvaluation', foreign_key: :academico_courses_id
  has_one :academico_course_result, inverse_of: :academico_course,
   class_name: 'Academico::CourseResult', foreign_key: :academico_courses_id

  delegate :academico_program, to: :academico_level_program, allow_nil: true
  delegate :academico_level, to: :academico_level_program, allow_nil: true

  validate :validate_has_associated_records, if: :persisted?


  belongs_to :facturacion_book, :class_name => "Facturacion::Book",
    foreign_key: "facturacion_book_id"

  def validate_has_associated_records

    if self.academico_level_programs_id_changed? ||
      self.academico_time_frame_id_changed?
        errors.add(:base, 'No se puede editar o borrar el curso porque existen alumnos matriculados') if has_registrations_records?

        errors.add(:base, 'No se puede editar o borrar el curso porque hay un temario asociado') if has_temary_records?

        errors.add(:base, 'No se puede editar o borrar el curso porque hay asistencias registradas') if has_teacher_attendances_records?

        errors.add(:base, 'No se puede editar o borrar el curso porque se han guardado evaluaciones') if has_course_evaluations_records?

        errors.add(:base, 'No se puede editar o borrar el curso porque se han guardado resultados') if has_course_result_record?
    end
  end

  def has_registrations_records?
    self.academico_registrations.present?
  end

  def has_temary_records?
    self.academico_temaries.present?
  end

  def has_teacher_attendances_records?
    self.academico_teacher_attendances.present?
  end

  def has_course_evaluations_records?
    self.academico_course_evaluations.present?
  end

  def has_course_result_record?
    self.academico_course_result.present?
  end

  def staff
    academico_staff.display_name if academico_staff.present?
  end

  def second_staff
    second_academico_staff.display_name if second_academico_staff.present?
  end

  scope :status_eq, -> (status) { where('academico_course_status_id = ?', status)}
  scope :on_going, -> { self.status_eq(Academico::CourseStatus::ON_GOING) }
  scope :current, -> { where('academico_course_status_id = ? OR academico_course_status_id = ?', Academico::CourseStatus::REGISTRATION, Academico::CourseStatus::ON_GOING) }
  scope :level_eq, -> (level) { joins(:academico_level_program).where('academico_level_programs.academico_level_id = ?', level)}
  scope :program_eq, -> (program) { joins(:academico_level_program).where('academico_level_programs.academico_program_id = ?', program)}
  scope :start_date_begins, -> (start_date) { joins(:academico_time_frame).where('academico_time_frames.start_date >= ?', start_date)}
  scope :start_date_ends, -> (start_date) { joins(:academico_time_frame).where('academico_time_frames.start_date <= ?', start_date)}
  scope :end_date_begins, -> (end_date) { joins(:academico_time_frame).where('academico_time_frames.end_date >= ?', end_date)}
  scope :end_date_ends, -> (end_date) { joins(:academico_time_frame).where('academico_time_frames.end_date <= ?', end_date)}
  scope :schedulle_eq, -> (course_schedulle) { joins(:academico_time_frame).where('academico_time_frames.academico_course_schedulle_id = ?', course_schedulle)}
  scope :staff_eq, -> (staff) { where('academico_staff_id = ? OR second_academico_staff_id = ?', staff, staff)}
  scope :modality_eq, -> (modality) { joins(:academico_time_frame).where('academico_time_frames.academico_course_modality_id = ?', modality)}
  scope :office_eq, -> (office) { joins(:academico_room).where('public.academico_rooms.admin_branch_office_id = ?', office)}
  scope :time_frame_eq, -> (time_frame) { where('academico_time_frame_id = ?', time_frame)}

  def registereds
    self.academico_registrations.find_all { |course| course.academico_registration_statuses_id == Academico::CourseStatus::ON_GOING }
  end


  accepts_nested_attributes_for :academico_registrations,
   allow_destroy: true,
   reject_if:   :all_blank

  validates_presence_of :academico_level_programs_id

  validate :quota_academico_course

  validate :room_is_available

  validate :validate_uniqueness

  def courses_by_room_and_schedulle
    courses = Academico::Course
      .joins(:academico_time_frame)
      .where('academico_course_status_id <> ?', Academico::CourseStatus::FINISHED)
      .where('academico_room_id = ?', self.academico_room_id)

    if self.academico_time_frame.present?
      this_time_frame = self.academico_time_frame
      courses = courses.where('academico_time_frames.academico_course_schedulle_id = ?', this_time_frame.academico_course_schedulle_id)
      courses = courses.where('academico_time_frames.end_date > ?', this_time_frame.start_date)
      courses = courses.where('academico_time_frames.academico_course_modality_id = ?', this_time_frame.academico_course_modality_id)
    end

    courses = courses.where('academico_courses.id <> ?', self.id) if self.persisted?
    courses
  end

  def released
    Academico::Student.students_by_next_level(academico_level.id)
  end

  def booked
    candidates = Academico::Student.joins(facturacion_invoices: :academico_course)
    .where('facturacion_invoices.academico_course_id = ? AND EXISTS(SELECT * FROM facturacion_invoice_details d WHERE d.facturacion_payment_detail_concept_id=2 AND d.facturacion_invoice_id=facturacion_invoices.id)', self.id)
  end

  def candidate_students
    candidates = released + booked
    candidates.uniq.each do |candidate|
      if self.id
        # look for already saved registrations to merge with the candidates
        registration = Academico::Registration.where("academico_students_id = ? AND academico_courses_id = ?", candidate.id, self.id)
        if !registration.present?
          self.academico_registrations.build(academico_courses_id: self.id, academico_students_id: candidate.id, academico_registration_statuses_id: 1).academico_registration_status = Academico::RegistrationStatus.find(1)
        end
      end
    end
  end

  def fulfilled_seats

  end
  def check_for_registrations
    check_for_relation(academico_registrations, "Matriculas")
  end

  def description_of_the_course
    self.name.present? ?
    self.name :
    ["#{self.academico_level_program.group_name}",
    self.course_schedulle,
    "#{self.formatted_date('course_start_date')} - #{self.formatted_date('course_start_date')}"].join(' > ')
  end

  def course_office
    academico_time_frame.present? ? academico_time_frame.try(:admin_branch_office).try(:display_name) : ''
  end

  def course_schedulle
    academico_time_frame.present? ? academico_time_frame.try(:academico_course_schedulle).try(:printable_schedulle) : ''
  end

  def course_modality
    academico_time_frame.present? ? academico_time_frame.try(:academico_course_modality).try(:description) : ''
  end

  def course_start_date
    academico_time_frame.try(:start_date)
  end

  def course_end_date
    academico_time_frame.try(:end_date)
  end

  def course_room
    academico_room.present? ? academico_room.name : ''
  end

  def student_registrations
    academico_registrations.map(&:academico_student).flatten
  end

  def update_teacher_attendance
    if self.academico_staff_id.present?
      academico_teacher_attendance = teacher_attendance_record(self.academico_staff_id)
      if academico_teacher_attendance.blank?
        academico_teacher_attendance = self.academico_teacher_attendances.build(academico_courses_id: self.id, academico_staffs_id: self.academico_staff_id, created_by: self.created_by_changed? ? self.created_by : self.updated_by)
      end
      academico_teacher_attendance.save
    end

    if self.second_academico_staff_id.present?
      academico_teacher_attendance = teacher_attendance_record(self.second_academico_staff_id)
      if academico_teacher_attendance.blank?
        academico_teacher_attendance = self.academico_teacher_attendances.build(academico_courses_id: self.id, academico_staffs_id: self.second_academico_staff_id, created_by: self.created_by_changed? ? self.created_by : self.updated_by)
      end
      academico_teacher_attendance.save
    end
  end

  def update_course_evaluations
    if self.academico_staff_id.present?
      academico_course_evaluation = course_evaluation_record(self.academico_staff_id)
      if academico_course_evaluation.blank?
        academico_course_evaluation = self.academico_course_evaluations.build(academico_courses_id: self.id, academico_staffs_id: self.academico_staff_id)
      end
      academico_course_evaluation.save
    end

    if self.second_academico_staff_id.present?
      academico_course_evaluation = course_evaluation_record(self.second_academico_staff_id)
      if academico_course_evaluation.blank?
        academico_course_evaluation = self.academico_course_evaluations.build(academico_courses_id: self.id, academico_staffs_id: self.second_academico_staff_id)
      end
      academico_course_evaluation.save
    end
  end

  def teacher_attendance_record(staff_id)
    self.academico_teacher_attendances.find {|attendance| attendance.academico_staffs_id == staff_id}
  end

  def course_evaluation_record(staff_id)
    self.academico_course_evaluations.find {|evaluation| evaluation.academico_staffs_id == staff_id}
  end

  def temary_record(staff_id)
    academico_temaries.find {|temary| temary.created_by_user.academico_staff.id == staff_id}
  end

  def is_finished?
    self.persisted? && self.academico_course_status_id == Academico::CourseStatus::FINISHED
  end

  def idle_seats
    seats - taken_seats
  end

  def seats
    try(:academico_room).try(:size) || 0
  end

  def taken_seats
    try(:registereds).try(:size) || 0
  end

  private


  def quota_academico_course
    errors.add(:base, 'No se puede exceder el cupo del curso') if self.academico_room.present? && self.academico_registrations.size > self.academico_room.size
  end

  def room_is_available
    errors.add(:base, 'El salón ya está asignado a otro curso en ese horario') if self.courses_by_room_and_schedulle.size > 0
  end

  def validate_uniqueness
    courses = Academico::Course.where("academico_time_frame_id = ? AND academico_level_programs_id = ?",
      self.academico_time_frame_id,
      self.academico_level_programs_id)

    courses = courses.where('academico_courses.id <> ?', self.id) if self.persisted?
    errors.add(:base, 'Ya existe un grupo guardado en este curso') if courses.any?
  end

  def create_course_price_record 
    Facturacion::CourseBookRegistration.find_or_create_by!(:academico_course_id => self.id)
  end
end
