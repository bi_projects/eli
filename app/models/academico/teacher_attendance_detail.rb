class Academico::TeacherAttendanceDetail < ApplicationRecord
  include FormatDate
  belongs_to :academico_teacher_attendance, :class_name => "Academico::TeacherAttendance",
    :inverse_of => :academico_teacher_attendance_details, foreign_key: "academico_teacher_attendances_id"
  belongs_to :academico_session, :class_name => "Academico::Session",
    :inverse_of => :academico_teacher_attendance_detail, foreign_key: "academico_sessions_id"
end
