class Academico::SessionEvaluation < ApplicationRecord
  belongs_to :academico_course_evaluation, class_name: "Academico::CourseEvaluation",
    foreign_key: "academico_course_evaluations_id", inverse_of: :academico_session_evaluations
  belongs_to :academico_session, class_name: "Academico::Session",
    foreign_key: "academico_sessions_id", inverse_of: :academico_session_evaluation
  has_many :academico_student_evaluations, class_name: 'Academico::StudentEvaluation',
    foreign_key: "academico_session_evaluations_id", inverse_of: :academico_session_evaluation, dependent: :destroy
  accepts_nested_attributes_for :academico_student_evaluations,
   allow_destroy: true,
   reject_if: :all_student_scores_blank?

  def all_student_scores_blank?(attributes)
    attributes[:academico_student_scores_attributes].values.all? {|parameter_score| parameter_score[:score].blank?}
  end
end