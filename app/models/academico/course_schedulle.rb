class Academico::CourseSchedulle < ApplicationRecord
  has_many :academico_courses, :class_name => "Academico::Course",
    :inverse_of => :academico_course_schedulle, foreign_key: "academico_course_schedulles_id"
  def status
    self.disabled ? "Deshabilitado" : "Habilitado"
  end
  def printable_schedulle
    "#{self.begins.strftime('%H:%M %p')} - #{self.ends.strftime('%H:%M %p')}"
  end

  scope :disableds, -> { where('disabled = true')}
  scope :enableds, -> { where('disabled = false')}
end
