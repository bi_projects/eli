class Academico::EvaluationParameter < ApplicationRecord
  has_many :academico_evaluation_methods, :class_name => "Academico::EvaluationMethod",
    :inverse_of => "academico_evaluation_parameter", foreign_key: "academico_evaluation_parameter_id"
  belongs_to :academico_evaluation_parameter_type, :class_name => "Academico::EvaluationParameterType",
    :inverse_of => :academico_evaluation_parameters, foreign_key: "academico_evaluation_parameter_type_id"
  has_and_belongs_to_many :academico_sessions, join_table: :academico_session_parameters,
    class_name: 'Academico::SessionParameter', foreign_key: :academico_evaluation_parameter_id, association_foreign_key: :academico_session_id, dependent: :destroy

  scope :parameters_in_course_scheme, -> (course_id) {
    joins(academico_evaluation_methods: {academico_evaluation_scheme: {academico_evaluation_level_programs: {academico_level_program: :academico_courses}}})
    .where('academico_courses.id = ?', course_id)
  }
  # generates a map of colors for a set of parameter names
  # and keeps track of them in a class variable for future use in GUI
  def self.parameter_color_code(parameters)
    return @@palette if defined? @@palette
    # our palette of colors has 10 so we can have that as max
    palette = %w(#e1f7d5 #ffbdbd #c9c9ff #ffffff #f1cbff #373854 #493267 #9e379f #e86af0 #7bb3ff)
    @@palette = Hash[parameters.each_with_index.map {|parameter, i| [parameter, palette[i]]}]
    @@palette
  end

  # gets access to the value for a parameter in the color map
  def self.get_my_color(color)
    @@palette[color]
  end
end
