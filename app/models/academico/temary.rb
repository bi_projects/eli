class Academico::Temary < ApplicationRecord
  belongs_to :academico_course, inverse_of: :academico_temaries,
   class_name: 'Academico::Course', foreign_key: :academico_courses_id
  belongs_to :created_by_user, inverse_of: :academico_temaries,
   class_name: 'Academico::AcademicUser', foreign_key: :created_by
  has_many :academico_sessions, inverse_of: :academico_temary,
   class_name: 'Academico::Session', foreign_key: :academico_temary_id, dependent: :destroy
  accepts_nested_attributes_for :academico_sessions,
    allow_destroy: true, 
    reject_if: :all_blank

  def has_any_session_with_assignments?
      assignments_count = self.academico_sessions.map(&:session_assignments_count).reduce(0, :+)
      assignments_count > 0
  end

  def fetch_evaluation_scheme
    self.academico_course.try(:academico_level_program).try(:academico_evaluation_level_programs).try(:first).try(:academico_evaluation_scheme) 
  end

  def assignments_by_parameter(parameter_id)
    session_parameters = academico_sessions.map(&:academico_session_parameters).flatten.select {|p| p.academico_evaluation_parameter_id == parameter_id}
    [] if session_parameters.blank?
    session_parameters.map(&:academico_session_assignments).flatten
  end

  # get all the evaluation parameters saved in temary
  def pull_evaluation_parameters_types
    academico_sessions.map {|session| session.academico_evaluation_parameters}.flatten.uniq
  end
end
