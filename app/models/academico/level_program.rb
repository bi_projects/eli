class Academico::LevelProgram < ApplicationRecord
  include AvoidDestroyReferencesUtility
  belongs_to :academico_level, :class_name => "Academico::Level",
		foreign_key: "academico_level_id", :inverse_of => :academico_level_programs
  belongs_to :academico_program, :class_name => "Academico::Program",
		foreign_key: "academico_program_id", :inverse_of => :academico_level_programs
  has_many :academico_courses, :class_name => "Academico::Course",
		foreign_key: "academico_level_programs_id", :inverse_of => :academico_level_program
  has_many :academico_level_skills, :class_name => "Academico::LevelSkill",
		foreign_key: "academico_level_programs_id", :inverse_of => :academico_level_program
  has_many :academico_evaluation_level_programs, :class_name => "Academico::EvaluationLevelProgram",
    :inverse_of => "academico_level_program", foreign_key: "academico_level_programs_id"

  def group_name
    "#{academico_level.name} #{academico_program.name}"
  end

  def check_for_level_skill
    check_for_relation(academico_level_skills, "Habilidades x Nivel")
  end

  def check_for_courses
    check_for_relation(academico_courses, "Curso")
  end

  def level_name
    academico_level.display_name
  end

  def program_name
    academico_program.display_name
  end
end
