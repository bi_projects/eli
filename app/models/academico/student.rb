class Academico::Student < ApplicationRecord
  include PersonalData
  include Filterable
  include EmailPrototype

  DEFAULT_DISCOUNT = 'custom'
  before_save :before_change_level
  has_one :academico_preregistration, inverse_of: :academico_student,
   class_name: 'Academico::Preregistration', foreign_key: :academico_student_id, dependent: :destroy
  belongs_to :facturacion_preregistration, inverse_of: :academico_student,
   class_name: 'Facturacion::Preregistration', foreign_key: :facturacion_preregistration_id
  belongs_to :current_level, class_name: 'Academico::Level', foreign_key: :current_level_id
  belongs_to :level_up, class_name: 'Academico::Level', foreign_key: :level_up_id
  belongs_to :academico_course, class_name: 'Academico::Course', foreign_key: :academico_course_id
  has_many :academico_registrations, inverse_of: :academico_student,
   class_name: 'Academico::Registration', foreign_key: :academico_students_id, dependent: :destroy
  has_many :facturacion_invoices, inverse_of: :academico_student,
   class_name: 'Facturacion::Invoice', foreign_key: :academico_students_id
  has_many :facturacion_receipts, inverse_of: :academico_student,
   class_name: 'Facturacion::Receipt', foreign_key: :academico_students_id
  belongs_to :admin_branch_office, inverse_of: :academico_students,
  class_name: 'Admin::BranchOffice', foreign_key: :admin_branch_office_id
  has_many :academico_student_evaluations, class_name: 'Academico::StudentEvaluation',
    foreign_key: "academico_students_id", inverse_of: :academico_student
  has_many :academico_student_results, class_name: 'Academico::StudentResult',
    foreign_key: "academico_students_id", inverse_of: :academico_student_results
  scope :first_name_like, -> (first_name) { where('first_name ilike ?', "%#{first_name}%")}
	scope :last_name_like, -> (last_name) { where('last_name ilike ?', "%#{last_name}%")}
  scope :active_student, -> () { where('active = true')}
  scope :inactive, -> () { where('active = false')}
  scope :by_paid_level, -> (level = nil, schedulle = nil, modality = nil) do
    students = joins(facturacion_invoices: {facturacion_invoice_details: :facturacion_payment_detail_concept}).where("facturacion_payment_detail_concepts.is_registration = 'true'")
    students = students.where("facturacion_invoices.academico_level_id = ?  AND level_up_id = ?", level, level) if level.present?
    students = students.where("facturacion_invoices.course_schedulle_id = ?", schedulle) if schedulle.present?
    students = students.where("facturacion_invoices.academico_course_modalities_id = ?", modality) if modality.present?
    students.distinct
  end

  after_save :update_preregistration

  scope :search_student, -> (search_key) { where('student_code = ? OR first_name ilike ? OR last_name ilike ?', search_key, "%#{search_key}%", "%#{search_key}%") }
  accepts_nested_attributes_for :academico_preregistration, allow_destroy: true

  def branch_name
    admin_branch_office.present? ? admin_branch_office.name : ""
  end

  def self.create_student_from_facturacion_preregistration(preregistration)
    code_prefix = Admin::Parameter.read_student_prefix
    max_id = (Academico::Student.maximum(:id) || 0) + 1
    code_length = Admin::Parameter.read_student_code_length
    code_sufix = max_id.to_s.rjust(code_length, '0')
    student_code = "#{code_prefix}-#{code_sufix}"
    Academico::Student.create(first_name: preregistration.first_name, last_name: preregistration.last_name, birth_date: preregistration.birth_date, facturacion_preregistration_id: preregistration.id, level_up_id: preregistration.academico_level_id, student_code: student_code, phone: preregistration.phone, address: preregistration.address, email: preregistration.email)
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.students_summary(start_date: nil, end_date: nil, branch_office_id: nil, course_schedulle_id: nil)
    students = joins(academico_registrations: {academico_course: [:academico_time_frame, :academico_room, academico_level_program: [:academico_program, :academico_level]]})
    if branch_office_id.present?
      students = students.where('academico_rooms.admin_branch_office_id = ?', branch_office_id)
    end
    if course_schedulle_id.present?
      students = students.where('academico_time_frames.academico_course_schedulle_id = ?', course_schedulle_id)
    end
    if start_date.present?
      students = students.where('academico_time_frames.start_date >= ?', Date.strptime(start_date, '%d/%m/%Y'))
    end
    if end_date.present?
      students = students.where('academico_time_frames.end_date <= ?', Date.strptime(end_date, '%d/%m/%Y'))
    end
    students_results = students.select('academico_programs.id AS program_id, academico_programs.name AS program, academico_levels.name AS level, academico_levels.level_sequence AS level_sequence, COUNT(*) AS amount')
    .group('academico_programs.id, academico_programs.name, academico_levels.name, academico_levels.level_sequence')
    .order('academico_programs.id, academico_levels.level_sequence')

    students_by_program = {}
    students_results.each do |student_row|
      if students_by_program[student_row.program].blank?
        students_by_program[student_row.program] = []
      end
      students_by_program[student_row.program] << {level: student_row.level, amount: student_row.amount}
    end
    students_by_program
  end

  def self.students_by_course(start_date: nil, end_date: nil, branch_office_id: nil, course_schedulle_id: nil, level_id: nil, program_id: nil, staff_id: nil)
    students = joins(academico_registrations: {
      academico_course: [:academico_time_frame, :academico_course_schedulle,
        academico_level_program: [:academico_program, :academico_level],
        academico_room: :admin_branch_office]
        }
      )
    .joins('LEFT JOIN academico_staffs ON academico_staffs.id = academico_courses.academico_staff_id '\
      ' LEFT JOIN academico_academic_users ON academico_academic_users.id = academico_staffs.academico_academic_users_id '\
      ' LEFT JOIN academico_staffs second_academico_staffs_academico_courses ON second_academico_staffs_academico_courses.id = academico_courses.second_academico_staff_id '\
      ' LEFT JOIN academico_academic_users academico_academic_users_academico_staffs ON academico_academic_users_academico_staffs.id = second_academico_staffs_academico_courses.academico_academic_users_id')

    if branch_office_id.present?
      students = students.where('academico_rooms.admin_branch_office_id = ?', branch_office_id)
    end
    if course_schedulle_id.present?
      students = students.where('academico_time_frames.academico_course_schedulle_id = ?', course_schedulle_id)
    end
    if start_date.present?
      students = students.where('academico_time_frames.start_date >= ?', Date.strptime(start_date, '%d/%m/%Y'))
    end
    if end_date.present?
      students = students.where('academico_time_frames.end_date <= ?', Date.strptime(end_date, '%d/%m/%Y'))
    end
    if level_id.present?
      students = students.where('academico_levels.id = ?', level_id)
    end
    if program_id.present?
      students = students.where('academico_programs.id = ?', program_id)
    end
    if staff_id.present?
      students = students.where('academico_courses.academico_staff_id = ? or academico_courses.second_academico_staff_id = ?', staff_id, staff_id)
    end

    students_results = students.select('student_code, first_name, last_name, birth_date,'\
      'academico_levels.name AS level, academico_programs.name AS program,'\
      'to_char(academico_course_schedulles.begins, \'HH24:MI:SS\') AS schedulle_begins,'\
      'to_char(academico_course_schedulles.ends, \'HH24:MI:SS\') AS schedulle_ends,'\
      'academico_academic_users.name AS first_teacher,'\
      'academico_academic_users_academico_staffs.name AS second_teacher,'\
      'academico_rooms.name AS room, admin_branch_offices.name AS branch_office')
    .order('level, first_name, last_name')

    students_by_program = {}
    students_results.each do |student_row|
      if students_by_program[student_row.program].blank?
        students_by_program[student_row.program] = []
      end
      students_by_program[student_row.program] << {
        student_code: student_row.student_code,
        full_name: "#{student_row.first_name} #{student_row.last_name}",
        birth_date: student_row.birth_date,
        level: student_row.level,
        schedulle: "#{student_row.schedulle_begins} - #{student_row.schedulle_ends}",
        first_teacher: student_row.first_teacher,
        second_teacher: student_row.second_teacher,
        room: student_row.room,
        branch_office: student_row.branch_office
      }
    end
    students_by_program
  end

  def self.students_ages(academico_course_schedulle_id: nil, academico_course_modality_id: nil, branch_office_id: nil)
    students = Academico::Student.active_student.joins(academico_registrations: {
      academico_course: [:academico_time_frame, :academico_course_schedulle, academico_level_program: [:academico_program, :academico_level],
      academico_room: :admin_branch_office] })

    if branch_office_id.present?
      students = students.where('academico_rooms.admin_branch_office_id = ?', branch_office_id)
    end
    if academico_course_schedulle_id.present?
      students = students.where('academico_time_frames.academico_course_schedulle_id = ?', academico_course_schedulle_id)
    end
    if academico_course_modality_id.present?
      students = students.where('academico_time_frames.academico_course_modality_id = ?', academico_course_modality_id)
    end

    students = students.select('academico_programs.name AS program, academico_levels.name AS level, AVG(EXTRACT(YEAR FROM AGE(birth_date))) AS age_')
    .group('academico_programs.name, academico_levels.name')
    .order('program, level')

    students_ages = {}
    students.each do |student_row|
      if students_ages[student_row.program].blank?
        students_ages[student_row.program] = []
      end
      students_ages[student_row.program] << {
        level: student_row.level,
        age: student_row.age_
      }
    end
    students_ages
  end

  def inquiry_level
    level = Academico::Level.find(level_sequence: 1)
    if self.active
      level ||= self.current_level
    else
      level ||= self.level_up
    end
    level
  end

  def my_default_discount
    discount_type = student_default_discount || DEFAULT_DISCOUNT
    my_discount(discount_type)
  end

  def my_discount(_discount_type = DEFAULT_DISCOUNT)
    if _discount_type.present?
      discount_type = _discount_type
    else
      discount_type = student_default_discount || DEFAULT_DISCOUNT
    end
    discount = 0
    if discount_type == 'preregistration'
      discount = self.try(:facturacion_preregistration).try(:total_discounts)
    end

    if discount_type == 'driver'
      discount = Admin::Parameter.first.try(:drivers_discount)
    end
    discount
  end

  def valid_discount_types
    discounts = []

    preregistration = self.facturacion_preregistration

    if preregistration.present?
      if preregistration.facturacion_carrier.present? && Admin::Parameter.first.try(:drivers_discount).present?
        discounts << [Facturacion::InvoiceDetail::DRIVER, 'driver']
      end
      if preregistration.facturacion_discounts.present?
        discounts << [Facturacion::InvoiceDetail::DISCOUNT, 'preregistration']
      end
    end

    discounts << [Facturacion::InvoiceDetail::CUSTOM, 'custom']
    discounts
  end

  def student_default_discount
    valid_discount_types.first[1]
  end

  def student_levels
    levels = []
    if self.active
      if self.current_level.present?
        levels << self.current_level
      else
        levels << self.academico_course.academico_level if self.academico_course.try(:id).present?
      end
    else
      if self.level_up.present?
        levels << self.level_up
      else
        next_level = self.academico_course.try(:academico_level).try(:next_level_in_progression) if self.academico_course.try(:id).present?
        levels << next_level.next_level_in_progression if next_level.present?
      end
    end
    return levels
  end

  def student_programs
    Academico::AgeRange.programs_list_by_age_range(self.age)
  end

  def relevant_courses
    if self.active
      return self.academico_course.id.present? ? [self.academico_course] : Academico::Course.filter_courses(self.current_level_id)
    else
      current_course = self.academico_course
      if current_course.blank?
        return Academico::Course.filter_courses(self.level_up_id)
      else
        courses = Academico::Course::filter_courses(
          self.level_up_id,
          current_course.try(:academico_level_program).try(:academico_program_id),
          current_course.try(:academico_time_frame).try(:academico_course_modality_id),
          current_course.try(:academico_time_frame).try(:academico_course_schedulle_id)
        )
        return courses
      end
    end
  end

  def update_level_up=(level_up)
    # use update_attribute to update level_up in order to skip student's before_change_level callback
    update_attribute('level_up', level_up)
  end

  def before_change_level
    if level_up_id_changed?
      registration = Academico::Registration.find_by(academico_students_id: self.id, academico_courses_id: self.academico_course_id)
      registration.delete if registration.present?
      self.active = false
    end
  end

  def self.students_by_next_level(level_id)
    Academico::Student.where("active = false AND level_up_id = ?", level_id)
  end

  def update_preregistration
    if self.facturacion_preregistration
      self.facturacion_preregistration.update_attributes(first_name: self.first_name, 
        last_name: self.last_name, 
        birth_date: self.birth_date, 
        phone: self.phone, 
        address: self.address, 
        email: self.email,
        academico_level_id: current_level.id)
      facturacion_preregistration.save!
    end
  end
end
