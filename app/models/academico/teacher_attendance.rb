class Academico::TeacherAttendance < ApplicationRecord
  belongs_to :academico_course, :class_name => "Academico::Course",
    :inverse_of => :academico_teacher_attendances, foreign_key: "academico_courses_id"
  belongs_to :academico_staff, :class_name => "Academico::Staff",
    :inverse_of => :academico_teacher_attendances, foreign_key: "academico_staffs_id"
  belongs_to :coordinator, :class_name => "Academico::Staff",
    :inverse_of => :academico_teacher_attendances, foreign_key: "created_by"
  has_many :academico_teacher_attendance_details, :class_name => "Academico::TeacherAttendanceDetail",
    :inverse_of => :academico_teacher_attendance, foreign_key: "academico_teacher_attendances_id"
  accepts_nested_attributes_for :academico_teacher_attendance_details,
    allow_destroy: true,
    reject_if:   :all_blank
end
