class Academico::EvaluationLevelProgram < ApplicationRecord
  belongs_to :academico_evaluation_scheme, :class_name => "Academico::EvaluationScheme",
    :inverse_of => :academico_evaluation_level_programs, foreign_key: "academico_evaluation_schemes_id"
  belongs_to :academico_level_program, :class_name => "Academico::LevelProgram",
    :inverse_of => :academico_evaluation_level_programs, foreign_key: "academico_level_programs_id"
end
