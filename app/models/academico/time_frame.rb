class Academico::TimeFrame < ApplicationRecord
  include FormatDate
  belongs_to :admin_branch_office, class_name: 'Admin::BranchOffice', foreign_key: :admin_branch_office_id
  belongs_to :academico_course_modality, class_name: 'Academico::CourseModality', foreign_key: :academico_course_modality_id
  belongs_to :academico_course_schedulle, class_name: 'Academico::CourseSchedulle', foreign_key: :academico_course_schedulle_id
  belongs_to :academico_time_frame_status, class_name: 'Academico::TimeFrameStatus', foreign_key: :academico_time_frame_status_id
  has_many :academico_courses, class_name: 'Academico::Course', foreign_key: :academico_time_frame_id

  validate :is_unique_record
  validates_numericality_of :number
  validate :validate_end_date_bigger_than_start_date

  scope :active_times_frames, -> () { where("academico_time_frame_status_id = ?", Academico::TimeFrameStatus::STATUS_ACTIVE) }

  def display_name
    %Q(
    #{self.year.to_s}-
    ##{self.number}-
    #{self.admin_branch_office.try(:display_name)}-
    #{self.academico_course_modality.try(:description)}-
    #{self.academico_course_schedulle.try(:printable_schedulle)}-
    #{self.start_date.try(:strftime, "%d/%m/%y")}-
    #{self.end_date.try(:strftime, "%d/%m/%y")}
    )
  end

  private

  def is_unique_record
    equal_time_frames = Academico::TimeFrame
    .where("year = ? AND admin_branch_office_id = ?", self.year, self.admin_branch_office_id)
    .where("academico_course_modality_id = ?", self.academico_course_modality_id)
    .where("academico_course_schedulle_id = ?", self.academico_course_schedulle_id)
    .where("start_date = ? AND end_date = ?", self.start_date, self.end_date)
    equal_time_frames = equal_time_frames.where("id <> ?", self.id) if self.persisted?

    if equal_time_frames.present?
      errors.add(:base, 'Ya existe un curso con los datos ingresados')
    end
  end

  def validate_end_date_bigger_than_start_date
    errors.add(:base, 'Fecha de inicio debe ser menor o igual que fecha de fin') unless start_date <= end_date
  end
end
