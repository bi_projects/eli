class Academico::StaffStatus < ApplicationRecord
  has_many :academico_staffs, class_name: 'Academico::Staff',
    foreign_key: "academico_staff_statuses_id", inverse_of: :academico_staff_status
  ACTIVE = 1
  INACTIVE = 2
end
