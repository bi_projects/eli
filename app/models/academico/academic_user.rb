class Academico::AcademicUser < ApplicationRecord
	mount_uploader :avatar, AvatarUploader

	has_one :admin_academic_user_position, class_name: 'Admin::AcademicUserPosition',
		foreign_key: 'academico_academic_user_id', inverse_of: :academico_academic_user, dependent: :destroy
  has_many :academico_user_roles, class_name: 'Academico::UserRole', foreign_key: :academico_academic_users_id
	has_many :academico_courses, :class_name => "Academico::Course",
		foreign_key: "academico_academic_users_id", :inverse_of => :academico_academic_user
	has_one :academico_staff, class_name: 'Academico::Staff',
		foreign_key: 'academico_academic_users_id', inverse_of: :academico_academic_user, dependent: :destroy
	has_many :academico_temaries, :class_name => "Academico::Temary",
		foreign_key: "created_by", :inverse_of => :academico_academic_user
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable
	include UserModelUtility
	accepts_nested_attributes_for :academico_user_roles, allow_destroy: true
	accepts_nested_attributes_for :admin_academic_user_position, allow_destroy: true
	scope :users_in_staff, -> (position_groups) { joins(admin_academic_user_position: :admin_position)
		.where('admin_positions.admin_module_id = ?', Admin::Module::ACADEMIC)
		.where('admin_academic_user_positions.admin_position_id IN (?)', position_groups)
	}
	scope :unasigned_users, -> () {
		where('academico_academic_users.id NOT IN (?)', Academico::Staff.select(:academico_academic_users_id))
	}
	def display_user_position
		self.admin_academic_user_position.admin_position.display_name if self.admin_academic_user_position.present?
	end

	def display_name_and_position
		self.name + ' - ' + self.display_user_position
	end
end
