class Academico::Level < ApplicationRecord
	include AvoidDestroyReferencesUtility
	before_destroy :check_for_level_preregistration, :check_for_preregistration_level_program, :check_for_level_program
	after_create :create_corresponding_level_programs
	has_many :academico_level_program_books, :class_name => "Academico::LevelProgramBook",
		foreign_key: "academico_level_id", :inverse_of => :academico_level, :dependent => :destroy

	has_many :facturacion_student_level_programs, :class_name => "Facturacion::StudentLevelProgram",
		foreign_key: "academico_level_id", :inverse_of => :academico_level

	has_many :facturacion_preregistrations, :class_name => "Facturacion::Preregistration",
		foreign_key: "academico_level_id", :inverse_of => :academico_level

  has_many :academico_level_programs, :class_name => "Academico::LevelProgram",
    foreign_key: "academico_level_id", :inverse_of => :academico_level, dependent: :destroy

	has_many :facturacion_invoices, :class_name => "Facturacion::Invoice",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :academico_level

  has_many :facturacion_book, class_name: 'Facturacion::Book', foreign_key: 'academico_level_id',
             inverse_of: :academico_level

	validates :level_sequence, numericality: true

	accepts_nested_attributes_for :academico_level_program_books,
		:allow_destroy => true,
		:reject_if     => :all_blank

	def display_name
		name
	end

  def next_level_in_progression
    Academico::Level.where('level_sequence > ?', self.level_sequence).order(:level_sequence).first
  end

	private
	def check_for_level_preregistration
		check_for_relation(facturacion_preregistrations, "Preregistro")
	end
	def check_for_preregistration_level_program
		check_for_relation(facturacion_student_level_programs, "Preregistro")
	end
  def check_for_level_program
    check_for_relation(academico_level_programs, "Nivel_Programa")
  end
  def create_corresponding_level_programs
    Academico::Program.all.each do |program|
      Academico::LevelProgram.find_or_create_by!(academico_level_id: self.id, academico_program_id: program.id)
    end
  end
end
