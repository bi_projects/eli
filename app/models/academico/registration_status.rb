class Academico::RegistrationStatus < ApplicationRecord
  PENDING = 1
  FULFILLED = 2
end
