class Academico::AgeRange < ApplicationRecord
	before_destroy :check_for_program

	has_many :academico_programs, :class_name => "Academico::Program",
		foreign_key: "academico_age_range_id", :inverse_of => :academico_age_range

	validates_presence_of :name, :min_age, :max_age, :description
	validates_numericality_of :min_age, :max_age
	validates_uniqueness_of :name

	include AvoidDestroyReferencesUtility

	def display_name
		name
	end

  def self.programs_list_by_age_range(age)
    programs = Academico::Program.joins(:academico_age_range).where("academico_age_ranges.min_age <= ? AND academico_age_ranges.max_age >= ?", age, age)
    program_list = programs.to_a
    if program_list.present? && program_list.any? {|program| program.id == Academico::Program::PROGRAM_TEENS}
      program_list << Academico::Program.find(3)
    end
    return program_list
  end

	private
	def check_for_program
		check_for_relation(academico_programs, "Programa")
  end
end
