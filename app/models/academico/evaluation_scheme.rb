class Academico::EvaluationScheme < ApplicationRecord
  belongs_to :academico_program, :class_name => "Academico::Program",
    :inverse_of => :academico_evaluation_schemes, foreign_key: "academico_programs_id"
  has_many :academico_evaluation_methods, :class_name => "Academico::EvaluationMethod",
    :inverse_of => "academico_evaluation_scheme", foreign_key: "academico_evaluation_scheme_id", dependent: :destroy
  has_many :academico_evaluation_level_programs, :class_name => "Academico::EvaluationLevelProgram",
    :inverse_of => "academico_evaluation_scheme", foreign_key: "academico_evaluation_schemes_id", dependent: :destroy
  accepts_nested_attributes_for :academico_evaluation_methods, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :academico_evaluation_level_programs, allow_destroy: true, reject_if: :all_blank

  validate :check_total_percentage
  validate :check_repeated_parameters

  def check_total_percentage
    errors.add(:base, 'La suma de los porcentajes debe ser igual a 100') if self.total_percentage != 100
  end

  def check_repeated_parameters
    errors.add(:base, 'No pueden guardarse parametros de evaluación repetidos') if detect_repeated_parameters.present?
  end

  def total_percentage
    self.academico_evaluation_methods.reject(&:marked_for_destruction?).map(&:percentage).reduce(0, :+)
  end

  def detect_repeated_parameters
    evaluation_parameters = self.academico_evaluation_methods.reject(&:marked_for_destruction?).map {|evaluation_method| evaluation_method.academico_evaluation_parameter}.flatten
    evaluation_parameters.detect {|parameters| evaluation_parameters.count(parameters) > 1}
  end
end
