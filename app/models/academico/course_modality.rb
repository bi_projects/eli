class Academico::CourseModality < ApplicationRecord
  has_many :facturacion_invoices, :class_name => "Facturacion::Invoice",
    foreign_key: "academico_course_modalities_id", :inverse_of => :academico_course_modality
  has_many :academico_courses, :class_name => "Academico::Course",
    foreign_key: "academico_course_modalities_id", :inverse_of => :academico_course_modality
end
