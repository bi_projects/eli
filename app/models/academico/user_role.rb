class Academico::UserRole < ApplicationRecord
  belongs_to :academic_user, inverse_of: :academico_user_roles, class_name: 'Academico::AcademicUser', foreign_key: :academico_academic_users_id
  belongs_to :academico_role, inverse_of: :academico_user_roles, class_name: 'Academico::AcademicoRole', foreign_key: :academico_academico_roles_id
end
