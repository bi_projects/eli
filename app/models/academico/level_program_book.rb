class Academico::LevelProgramBook < ApplicationRecord
  belongs_to :academico_level, :class_name => "Academico::Level", :inverse_of => :academico_level_program_books
  belongs_to :academico_program, :class_name => "Academico::Program", :inverse_of => :academico_level_program_books
  belongs_to :facturacion_book, :class_name => "Facturacion::Book", :inverse_of => :academico_level_program_books
end
