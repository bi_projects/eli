class Academico::AcademicoAbility < Ability
  def initialize(user)
    user ||= Academico::AcademicUser.new
    permissions = Admin::AcademicoRolePermission.joins(:academico_role, :academico_user_roles).where('academico_user_roles.academico_academic_users_id = ?', user.id)
    permissions.each do |permission|
      if permission.admin_resource.model == 'Academico::CourseEvaluation'
        if ['show', 'edit', 'update', 'destroy'].include?(permission.action.description)
          can permission.action.description.to_sym, Academico::CourseEvaluation, academico_staffs_id: user.academico_staff.id if user.academico_staff.present?
        else
          can permission.action.description.to_sym, Academico::CourseEvaluation
        end
      elsif permission.admin_resource.model == 'Academico::CourseResult'
        if ['show', 'edit', 'update', 'destroy'].include?(permission.action.description)
          can permission.action.description.to_sym, Academico::CourseResult, academico_staffs_id: user.academico_staff.id if user.academico_staff.present?
        else
          can permission.action.description.to_sym, Academico::CourseResult
        end
      elsif permission.admin_resource.model == 'Academico::Temary'
        if ['edit', 'update', 'destroy'].include?(permission.action.description)
          can permission.action.description.to_sym, Academico::Temary, created_by: user.id
        else
          can permission.action.description.to_sym, Academico::Temary
        end
      elsif permission.admin_resource.model == 'Academico::TeacherAttendance'
        if ['show', 'edit', 'update', 'destroy'].include?(permission.action.description)
          can permission.action.description.to_sym, Academico::TeacherAttendance, created_by: user.academico_staff.id if user.academico_staff.present?
        else
          can permission.action.description.to_sym, Academico::TeacherAttendance
        end
      else
        can permission.action.description.to_sym, permission.admin_resource.model.constantize
      end
    end
  end
end
