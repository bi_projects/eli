class Academico::Preregistration < ApplicationRecord
  belongs_to :academico_student, class_name: "Academico::Student",
   :inverse_of => :academico_preregistration, foreign_key: "academico_student_id"
  belongs_to :academico_level
  belongs_to :academico_program
  belongs_to :academico_preregistration_status, class_name: "Academico::PreregistrationStatus",
    foreign_key: "academico_preregistration_statuses_id"
end
