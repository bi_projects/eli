class Academico::Session < ApplicationRecord
  include FormatDate
  belongs_to :academico_temary, inverse_of: :academico_sessions,
    class_name: 'Academico::Temary', foreign_key: :academico_temary_id
  has_many :academico_session_parameters, inverse_of: :academico_session,
    class_name: 'Academico::SessionParameter', foreign_key: :academico_session_id, dependent: :delete_all
  has_many :academico_evaluation_parameters, through: :academico_session_parameters,
    class_name: 'Academico::EvaluationParameter'
  has_one :academico_session_evaluation, class_name: 'Academico::SessionEvaluation',
    foreign_key: 'academico_sessions_id', inverse_of: :academico_session
  has_one :academico_teacher_attendance_detail, class_name: 'Academico::TeacherAttendanceDetail',
    foreign_key: 'academico_sessions_id', inverse_of: :academico_session
  accepts_nested_attributes_for :academico_evaluation_parameters,
    allow_destroy: true,
    reject_if: :all_blank
  accepts_nested_attributes_for :academico_session_parameters,
    allow_destroy: true,
    reject_if: proc {|attributes| attributes[:academico_session_assignments_attributes].blank? }
  validates_presence_of :topic, :schedulle
  def session_assignments_count
    if !self.academico_session_parameters.present?
      return 0
    else
      all_session_assignments.size
    end
  end
  def all_session_assignments
    self.academico_session_parameters.map(&:academico_session_assignments).flatten.sort_by { |assignment| assignment.id }
  end
end
