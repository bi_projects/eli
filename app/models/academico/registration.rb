class Academico::Registration < ApplicationRecord
  belongs_to :academico_course, class_name: "Academico::Course",
    inverse_of: :academico_registrations, foreign_key: "academico_courses_id"
  belongs_to :academico_student, class_name: "Academico::Student",
    :inverse_of => :academico_registrations, foreign_key: "academico_students_id"
  belongs_to :academico_registration_status, class_name: "Academico::RegistrationStatus",
    foreign_key: "academico_registration_statuses_id"
  after_save :activar_estudiante
  before_destroy :desactivar_estudiante

  private

  def activar_estudiante
    begin
      preregistrations = Academico::Preregistration
        .where("academico_student_id = ?", self.academico_students_id)
        .where("academico_preregistration_statuses_id = ?", Academico::PreregistrationStatus::PREREGISTRATION_STATUS_CREATED)

      if preregistrations.present?
        preregistration = preregistrations.first
        preregistration.academico_preregistration_statuses_id = Academico::PreregistrationStatus::PREREGISTRATION_STATUS_REGISTERED
        preregistration.save!
      end
      if self.academico_student.present?
        self.academico_student.active = true
        current_level = academico_course.try(:academico_level_program).try(:academico_level)
        self.academico_student.current_level = current_level
        self.academico_student.academico_course = self.academico_course

        self.academico_student.admin_branch_office_id = self.academico_course
          .try(:academico_time_frame)
          .try(:admin_branch_office)
          .try(:id)

        self.academico_student.save!
      end
    rescue Exception => e
      raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
    end
  end

  def desactivar_estudiante
    begin
      preregistrations = Academico::Preregistration.where("academico_student_id = ? AND academico_preregistration_statuses_id = ?",
          self.academico_students_id, Academico::PreregistrationStatus::PREREGISTRATION_STATUS_REGISTERED)
      if preregistrations.present?
        preregistration = preregistrations.first
        preregistration.academico_preregistration_statuses_id = Academico::PreregistrationStatus::PREREGISTRATION_STATUS_CREATED
        preregistration.save!
      end
      if self.academico_student.present?
        self.academico_student.active = false
        self.academico_student.try(:update_level_up, self.academico_course.try(:academico_level_program).try(:academico_level))
        self.academico_student.save!
      end
    rescue Exception => e
      raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
    end
  end
end
