class Academico::LevelSkill < ApplicationRecord
  belongs_to :academico_staff, class_name: 'Academico::Staff',
    foreign_key: 'academico_staffs_id', inverse_of: :academico_level_skills
  belongs_to :academico_level_program, class_name: 'Academico::LevelProgram',
    foreign_key: 'academico_level_programs_id', :inverse_of => :academico_level_skills
  def group_name
    self.academico_level_program.group_name
  end
end
