class Academico::PreregistrationStatus < ApplicationRecord
  PREREGISTRATION_STATUS_CREATED = 1
  PREREGISTRATION_STATUS_REGISTERED = 2
end
