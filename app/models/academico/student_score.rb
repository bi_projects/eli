class Academico::StudentScore < ApplicationRecord
  belongs_to :academico_student_evaluation, class_name: 'Academico::StudentEvaluation',
    foreign_key: "academico_student_evaluations_id", inverse_of: :academico_student_scores
  belongs_to :academico_session_assignment, class_name: 'Academico::SessionAssignment',
    foreign_key: "academico_session_assignments_id", inverse_of: :academico_student_scores
  delegate :academico_session_parameter, to: :academico_session_assignment, allow_nil: true
  validates_numericality_of :score, less_than_or_equal_to: :max_possible_score
  def max_possible_score
    self.academico_session_assignment.scale
  end
end
