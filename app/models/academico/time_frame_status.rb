class Academico::TimeFrameStatus < ApplicationRecord
  has_many :academico_time_frames, inverse_of: :academico_time_frame_status

  STATUS_ACTIVE = 1
  STATUS_INACTIVE = 2
end
