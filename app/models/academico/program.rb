class Academico::Program < ApplicationRecord
  PROGRAM_CHILDREN = 1
  PROGRAM_TEENS = 2
  PROGRAM_ADULTS = 3
	before_destroy :check_for_level_program_book, :check_for_preregistration_level_program, :check_for_level_program
	after_create :create_corresponding_level_programs
  belongs_to :academico_age_range, :class_name => "Academico::AgeRange", :inverse_of => :academico_programs

  has_many :academico_level_program_books, :class_name => "Academico::LevelProgramBook",
		foreign_key: "academico_program_id", :inverse_of => :academico_program

  has_many :facturacion_student_level_programs, :class_name => "Facturacion::StudentLevelProgram",
		foreign_key: "academico_program_id", :inverse_of => :academico_program

  has_many :academico_level_programs, :class_name => "Academico::LevelProgram",
	  foreign_key: "academico_program_id", :inverse_of => :academico_program, dependent: :destroy

  has_many :facturacion_invoices, :class_name => "Facturacion::Invoice",
    foreign_key: "facturacion_preregistration_id", :inverse_of => :academico_program

  has_many :academico_evaluation_schemes, :class_name => "Academico::EvaluationScheme",
    foreign_key: "academico_programs_id", :inverse_of => :academico_program

  has_many :facturacion_book, class_name: 'Facturacion::Book', foreign_key: 'academico_program_id',
              inverse_of: :academico_program

  validates_presence_of :name, :price, :description
  validates_numericality_of :price
  validates_uniqueness_of :name

  include AvoidDestroyReferencesUtility

  def display_name
    name
  end

  private
  def check_for_level_program_book
    check_for_relation(academico_level_program_books, "Nivel")
  end
  def check_for_preregistration_level_program
    check_for_relation(facturacion_student_level_programs, "Preregistro")
  end
  def check_for_level_program
    check_for_relation(academico_level_programs, "Nivel_Programa")
  end
  def create_corresponding_level_programs
    Academico::Level.all.each do |level|
      Academico::LevelProgram.find_or_create_by!(academico_level_id: level.id, academico_program_id: self.id)
    end
  end
end
