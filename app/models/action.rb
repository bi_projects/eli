class Action < ApplicationRecord

  INDEX = 1
  SHOW = 2
  CREATE = 3
  UPDATE = 4
  NEW = 5
  EDIT = 6
  DESTROY = 7

  def display_name
    self.description
  end
end
