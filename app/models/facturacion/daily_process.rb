class Facturacion::DailyProcess < ApplicationRecord
  include FacturacionUtility

  belongs_to :facturacion_billing_user, class_name: 'Facturacion::BillingUser', foreign_key: 'facturacion_billing_user_id'

  before_validation :set_daily_process_date

  validate :verify_daily_process_date

  def verify_daily_process_date
    if Facturacion::DailyProcess.exists_daily_process?(self.daily_process_date)
      self.errors.add(:base, 'Ya existe cierre diario para la fecha de hoy')
    end
  end

  private

  def set_daily_process_date
    self.daily_process_date = Time.now
  end
end