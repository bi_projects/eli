class Facturacion::Warehouse < ApplicationRecord
	before_destroy :check_for_stock

	belongs_to :facturacion_cost_center, :class_name => "Facturacion::CostCenter", :inverse_of => :facturacion_warehouses
	belongs_to :admin_branch_office, :class_name => "Admin::BranchOffice", :inverse_of => :facturacion_warehouses

	has_many :facturacion_invoice, :through => :admin_branch_office, :class_name => "Facturacion::Invoice"

	has_many :facturacion_stocks, :class_name => "Facturacion::Stock", 
		foreign_key: "facturacion_warehouse_id", :inverse_of => :facturacion_warehouse

	include AvoidDestroyReferencesUtility 
	validates_presence_of :name, :address, :description, :admin_branch_office_id
	validates_uniqueness_of :name, :admin_branch_office_id

	def display_name
		name
	end

	private
	def check_for_stock
		check_for_relation(facturacion_stocks, "Stock")
	end
end
