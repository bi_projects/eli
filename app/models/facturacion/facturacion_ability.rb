class Facturacion::FacturacionAbility < Ability
	def initialize(user)
		user ||= Facturacion::BillingUser.new
		permissions = Admin::FacturacionRolePermission.joins(:facturacion_facturacion_role, :facturacion_user_roles).where('facturacion_user_roles.facturacion_billing_user_id = ?', user.id)
		permissions.each do |permission|
			can permission.action.description.to_sym, permission.admin_resource.model.constantize
			if permission.admin_resource.model == 'Facturacion::CourseBookRegistration'
				can :bulk_prices, Facturacion::CourseBookRegistration
			end
		end
	end
end
