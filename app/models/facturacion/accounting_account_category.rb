class Facturacion::AccountingAccountCategory < ApplicationRecord
  has_many :facturacion_accounting_accounts, inverse_of: :facturacion_accounting_account_category,
   class_name: 'Facturacion::AccountingAccount', foreign_key: :facturacion_accounting_account_categories_id

  ACTIVO = 1
  PASIVO = 2
  CAPITAL = 3
  EGRESO = 4
  INGRESO = 5
end
