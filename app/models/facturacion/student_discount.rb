class Facturacion::StudentDiscount < ApplicationRecord
  belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_student_discounts
  belongs_to :facturacion_discount, :class_name => "Facturacion::Discount", :inverse_of => :facturacion_student_discounts
end
