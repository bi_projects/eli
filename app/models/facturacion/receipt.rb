class Facturacion::Receipt < ApplicationRecord
  include FacturacionUtility
  include AvoidManageDocumentUtility

  before_destroy :check_for_printed_document
  before_update :check_for_printed_document

  before_create :set_receipt_number
  before_save :complete_receipt_info_for_cash_invoice
  before_save :set_receipt_total, :set_total_payment, :receipt_total_change

  after_save :complete_receipt_details_info_for_cash_invoice
  after_save :set_bussiness_actions

  after_initialize :init, unless: :persisted?

  has_many :facturacion_receipt_details, :class_name => "Facturacion::ReceiptDetail",
    :foreign_key => "facturacion_receipt_id", :inverse_of => :facturacion_receipt, :dependent => :destroy

  has_one :facturacion_receipt_payment_detail, :class_name => "Facturacion::ReceiptPaymentDetail",
    :foreign_key => "facturacion_receipt_id", :inverse_of => :facturacion_receipt, :dependent => :destroy

  belongs_to :admin_branch_office, :class_name => "Admin::BranchOffice", :inverse_of => :facturacion_receipts
  #belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_receipts

  # this automatically creates the receipt_details when saving the nested cash invoice
  has_many :facturacion_invoices, :through => :facturacion_receipt_details, :class_name => "Facturacion::Invoice"

  has_many :facturacion_receipt_detail_notes, :through => :facturacion_receipt_details, :class_name => "Facturacion::ReceiptDetailNote",
    :inverse_of => :facturacion_receipts

  has_many :facturacion_notes, :through => :facturacion_receipt_details, :class_name => "Facturacion::Note", inverse_of: :facturacion_receipts

  belongs_to :academico_student, :class_name => "Academico::Student", :inverse_of => :facturacion_receipts, foreign_key: "academico_student_id"

  belongs_to :facturacion_billing_user, class_name: 'Facturacion::BillingUser', inverse_of: :facturacion_receipts, foreign_key: 'facturacion_billing_user_id'


  accepts_nested_attributes_for :facturacion_receipt_details,
    :allow_destroy => true,
    :reject_if     => :all_blank

  accepts_nested_attributes_for :facturacion_receipt_payment_detail,
    :allow_destroy => true,
    :reject_if     => :all_blank

  accepts_nested_attributes_for :facturacion_invoices,
    :allow_destroy => true,
    :reject_if     => :all_blank

  validates_uniqueness_of :receipt_number

  def receipt_date_formatted
    if !self.new_record?
      receipt_date.strftime("%d/%m/%Y")
    else
      receipt_date
    end
  end

  def form_receipt_number
    prefix = Admin::Parameter.count > 0 ? Admin::Parameter.first.receipt_prefix : "RCB"
    "#{prefix}-#{self.receipt_number}"
  end

  def total_detail #CASH TRANSACTIONS && Index
    total = 0
    self.facturacion_receipt_details.each do |detail|
      total += detail.payment_amount || 0
    end
    total
  end

  def total_invoice #CASH TRANSACTIONS && Index
    total = 0
    self.facturacion_invoices.each do |detail|
      total += detail.total_invoice
    end
    total
  end

  def total_invoice_remain #CASH TRANSACTIONS && Index
    total = 0
    self.facturacion_invoices.each do |detail|
      total += detail.invoice_remain
    end
    total
  end

  def total_balance_credit_invoice_detail(invoice) #CREDIT TRANSACTIONS on create / edit
    total_payments = 0
    invoice.facturacion_receipt_details.each do |detail|
      if !detail._destroy
        total_payments += detail.payment_amount || 0
      end
    end
    invoice.total_invoice - total_payments
  end

  def total_payment_credit_invoice_detail #CREDIT TRANSACTIONS on create / edit
    detail = self.facturacion_invoice_payment_detail
    if detail.present? && !detail._destroy
      self.total_payment = detail.set_total_payment(self.exchange_rate || 0)
    end
  end


  def self.total_payment_by_note(credit_note)
    self.joins(:facturacion_receipt_detail_notes).where("facturacion_receipt_detail_notes.facturacion_note_id = ?",credit_note)
  end

  def void_effects
    if credit_invoice_payment?
      try_void_credit_invoice_canceled
    else
      try_void_cash_invoice_counterpart
    end
    void_transactions
  end

  private

  def total_receipt_detail
    total_amount = 0
    self.facturacion_receipt_details.each_with_index do |detail, index|
      if !detail._destroy
        total_amount += detail.payment_amount || 0
      end

      if detail.is_credit_note && detail.facturacion_receipt_detail_note.blank?
        detail.build_facturacion_receipt_detail_note
        raise CustomExceptions::DocumentException, "Seleccione la nota de credito"
      end

    end
    total_amount
  end

  def total_payment_detail
    detail = self.facturacion_receipt_payment_detail
    self.total_payment = BigDecimal.new(0)
    if detail.present? && !detail._destroy
      self.total_payment = detail.set_total_payment(self.exchange_rate || 0)
    end
  end

  def init
    set_receipt_number
    self.receipt_date ||= Date.today
    self.facturacion_document_type_id ||= Facturacion::DocumentType.find_by(:name => "Recibo").id
    self.exchange_rate  ||= Admin::ExchangeRate.current_exchange_rate #will set the default value only if it's nil
  end

  def check_for_printed_document
    isPrinted?(printed)
  end

  def set_receipt_total
    self.total_receipt = total_receipt_detail
  end

  def set_total_payment
    total_payment_detail
  end

  def receipt_total_change
    if self.total_payment < self.total_receipt
      raise CustomExceptions::DocumentException, "El monto a pagar es mayor que la cantidad recibida, verifique los montos y proceda nuevamente"
    else
      self.total_change = self.total_payment - self.total_receipt
    end
  end

  def set_receipt_number
    receipt_number_resource = Facturacion::Receipt.last || Admin::Parameter.first || 0
    next_receipt_number = 1 + (receipt_number_resource.class != Fixnum ? receipt_number_resource.receipt_number : receipt_number_resource)
    self.receipt_number = next_receipt_number
  end

  def credit_invoice_payment?
    self.credit_invoice_payment
  end

  def build_cash_transaction(invoice)
    if invoice.present?
      receiptTransaction = Admin::BillingTransaction.find_by(:document_id => self.id,
      :facturacion_document_type_id => self.facturacion_document_type_id)

      if receiptTransaction.blank?
        transaction = Admin::BillingTransaction.new(
          description: "Cancelación de Factura #{invoice.form_invoice_number} con recibo #{self.form_receipt_number}",
          date: Date.today,
          document_id: self.id,
          facturacion_document_type_id: self.facturacion_document_type_id,
          document_number: self.receipt_number,
          transaction_amount: invoice.total_invoice)
        transaction.save!
      end
    end
  end

  def build_credit_transaction(invoice)
    if invoice.present?
      transaction = Admin::BillingTransaction.new(
      description: "Pago de Factura #{invoice.form_invoice_number} con recibo #{self.form_receipt_number}",
      date: Date.today,
      document_id: self.id,
      facturacion_document_type_id: self.facturacion_document_type_id,
      document_number: self.receipt_number,
      transaction_amount: receipt_detail.payment_amount)

      transaction.save!
    end
  end

  def build_transactions(receipt_detail)
    if receipt_detail.present?
      invoice = receipt_detail.facturacion_invoice

      if invoice.present?
        if invoice.is_cash
          build_cash_transaction(invoice)
        else
          build_cash_transaction(invoice)
        end
      end
    end
  end


  # voids for receipts linked to both credit or cash invoices
  def void_transactions
    receiptTransaction = Admin::BillingTransaction.find_by(:document_id => self.id,
        :facturacion_document_type_id => self.facturacion_document_type_id)

    if receiptTransaction.present?
      transaction = Admin::BillingTransaction.new(
      description: "Anulación de recibo #{self.form_receipt_number}",
      date: Date.today,
      document_id: receiptTransaction.document_id,
      facturacion_document_type_id: receiptTransaction.facturacion_document_type_id,
      document_number: receiptTransaction.document_number,
      transaction_amount: -receiptTransaction.transaction_amount)

      transaction.save!
    end
  end

  def try_void_cash_invoice_counterpart
    if !credit_invoice_payment?
      self.facturacion_receipt_details.each do |receipt_detail|
        invoice = receipt_detail.facturacion_invoice

        if invoice.present? && invoice.is_cash
          destructor = Business::InvoiceDestructor.new(invoice)
          destructor.destroy
        end
      end
    end
  end

  def try_void_credit_invoice_canceled
    if credit_invoice_payment?
      self.facturacion_receipt_details.each do |receipt_detail|
        invoice = receipt_detail.facturacion_invoice
        if invoice.is_pending_invoice == false || invoice.invoice_canceled == true
          invoice.is_pending_invoice = true
          invoice.invoice_canceled = false

          invoice.save!
        end
      end
    end
  end

  def set_bussiness_actions
    begin

      self.facturacion_receipt_details.each do |receipt_detail|

        if credit_invoice_payment?
          invoice = receipt_detail.facturacion_invoice
          # TODO ensure this works
          if total_balance_credit_invoice_detail(invoice) > 0
            invoice.is_pending_invoice = true
            invoice.invoice_canceled = false
          else 
            invoice.is_pending_invoice = false
            invoice.invoice_canceled = true
            invoice.invoice_canceled_date = Date.today
          end

          invoice.save!
        end

        build_transactions(receipt_detail)

      end
    rescue Exception => e
      raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
    end
  end

  def complete_receipt_details_info_for_cash_invoice
    self.facturacion_receipt_details.each do |receipt_detail|
      invoice = receipt_detail.facturacion_invoice

      if invoice.present? && invoice.is_cash
        receipt_detail.payment_amount = invoice.total_invoice
        receipt_detail.payment_pre_balance = invoice.invoice_remain + receipt_detail.payment_amount
        receipt_detail.payment_post_balance = receipt_detail.payment_pre_balance - receipt_detail.payment_amount
        receipt_detail.save
      end
    end
  end

  def complete_receipt_info_for_cash_invoice
    # this should be one iteration only but for sake of safety...
    # the receipt detail is already present via means of the has_many through association
    self.facturacion_receipt_details.each do |receipt_detail|
      invoice = receipt_detail.facturacion_invoice

      if invoice.present? && invoice.is_cash
        self.academico_student_id = invoice.academico_student.id
        #self.comments = "Cancelación de Factura #{invoice.form_invoice_number}"
        self.credit_invoice_payment = false
        self.admin_branch_office_id = invoice.admin_branch_office_id
      end
    end
  end
end
