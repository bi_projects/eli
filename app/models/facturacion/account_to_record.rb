class Facturacion::AccountToRecord < ApplicationRecord
  has_many :facturacion_accounting_accounts, inverse_of: :facturacion_account_to_record,
   class_name: 'Facturacion::AccountingAccount', foreign_key: :facturacion_account_to_records_id
end
