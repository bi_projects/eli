class Facturacion::PaymentDetailConceptDetailPrice < ApplicationRecord
  belongs_to :academico_level, :class_name => "Academico::Level",
		foreign_key: "academico_level_id", :inverse_of => :academico_level_programs
  belongs_to :academico_program, :class_name => "Academico::Program",
		foreign_key: "academico_program_id", :inverse_of => :academico_level_programs
  has_many :facturacion_payment_detail_concept,
           class_name: 'Facturacion::PaymentDetailConcept',
           foreign_key: 'facturacion_payment_detail_concept_id',
           inverse_of: :facturacion_payment_detail_concept_detail_price

  validates_presence_of :academico_level_id, :academico_program_id, :amount
end
