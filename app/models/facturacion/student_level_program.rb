class Facturacion::StudentLevelProgram < ApplicationRecord
  belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_student_level_program
  belongs_to :academico_level, :class_name => "Academico::Level", :inverse_of => :facturacion_student_level_programs
  belongs_to :academico_program, :class_name => "Academico::Program", :inverse_of => :facturacion_student_level_programs
end
