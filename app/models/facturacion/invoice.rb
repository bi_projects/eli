class Facturacion::Invoice < ApplicationRecord
  include FacturacionUtility

	before_destroy :check_for_printed_document
	after_destroy :deregister_student
	before_update :check_for_printed_document

	before_create :set_invoice_number
	before_save :is_credit_validation, :set_invoice_total, :set_total_payment, :invoice_total_change

  after_save :set_bussiness_actions
	after_save :register_student

	after_initialize :init, unless: :persisted?

	has_many :facturacion_receipt_details, :class_name => "Facturacion::ReceiptDetail", 
		:foreign_key => "facturacion_invoice_id", :inverse_of => :facturacion_invoice

	has_many :facturacion_invoice_details, :class_name => "Facturacion::InvoiceDetail",
	foreign_key: "facturacion_invoice_id", :inverse_of => :facturacion_invoice, :dependent => :destroy

	has_many :facturacion_note_details, :class_name => "Facturacion::NoteDetail",
	foreign_key: "facturacion_invoice_id", :inverse_of => :facturacion_invoice

	has_one :facturacion_invoice_payment_detail, :class_name => "Facturacion::InvoicePaymentDetail",
    foreign_key: "facturacion_invoice_id", :inverse_of => :facturacion_invoice, :dependent => :destroy
	
	belongs_to :admin_branch_office, :class_name => "Admin::BranchOffice", :inverse_of => :facturacion_invoices
	belongs_to :academico_student, :class_name => "Academico::Student", :inverse_of => :facturacion_invoices, foreign_key: "academico_students_id"
	belongs_to :academico_course, :class_name => "Academico::Course", foreign_key: "academico_course_id"
	belongs_to :facturacion_document_type, :class_name => "Academico::CourseModality", foreign_key: "academico_course_modalities_id"
  belongs_to :facturacion_invoice_status, :class_name => "Facturacion::InvoiceStatus", foreign_key: "facturacion_invoice_statuses_id"
  delegate :academico_time_frame, to: :academico_course, allow_nil: true

	has_many :facturacion_warehouses, :through => :admin_branch_office, :class_name => "Facturacion::Warehouse"

	has_many :facturacion_receipts, :through => :facturacion_receipt_details, :class_name => "Facturacion::Receipt"


	belongs_to :facturacion_billing_user, class_name: 'Facturacion::BillingUser', inverse_of: :facturacion_invoices, foreign_key: 'facturacion_billing_user_id'

	accepts_nested_attributes_for :facturacion_invoice_details,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_invoice_payment_detail,
		:allow_destroy => true,
		:reject_if     => :all_blank

	scope :student_book_invoices, -> (student_id, status) do
		invoices = joins(facturacion_invoice_details: :facturacion_payment_detail_concept).where("facturacion_payment_detail_concepts.is_book = 'true'")
		if student_id.present?
			invoices = invoices.where('academico_students_id = ?', student_id)
			invoices = invoices.where('facturacion_invoice_details.facturacion_invoice_detail_status_id = ?', status) if status.present?
			return invoices
		else
      # TODO quitar
			return invoices.first(10)
		end
	end

  scope :invoice_concepts, -> (invoice_id, status) {
    details = Facturacion::InvoiceDetail.where("facturacion_invoice_id = ?", invoice_id)
    details = details.where("facturacion_invoice_detail_status_id = ?", status) if status.present?
    details
  }

	include AvoidManageDocumentUtility

	validates_uniqueness_of :invoice_number

	def display_name
		form_invoice_number
	end

	def invoice_date_formatted
		if !self.new_record?
			invoice_date.strftime("%d/%m/%Y")
		else
			invoice_date
		end
	end

	def form_invoice_number
		prefix = Admin::Parameter.count > 0 ? Admin::Parameter.first.invoice_prefix : "FAC"
		"#{prefix}-#{self.invoice_number}"
	end

	def invoice_remain
		payments = 0
		if self.is_cash
			payments += (self.total_payment - self.total_change)
		else
			self.facturacion_receipt_details.each do |receipt_detail|
				payments +=	receipt_detail.payment_amount
			end
		end
		self.total_invoice - payments
	end

	def student_name
		academico_student.present? ? academico_student.full_name : ''
	end

	def pays_registration?
		facturacion_invoice_details.any? {|detail| detail.try(:facturacion_payment_detail_concept).try(:is_registration)}
	end

  def invoice_status
    facturacion_invoice_status.try(:description)
  end

  def is_void?
    facturacion_invoice_status.try(:id) == Facturacion::InvoiceStatus::VOID
  end

  def register_student
    if self.academico_course.present? && self.academico_student.present?
      if pays_registration? &&
        self.facturacion_invoice_statuses_id != Facturacion::InvoiceStatus::VOID
        Academico::Registration.find_or_create_by!(
          academico_courses_id: self.academico_course.id,
          academico_students_id: self.academico_student.id,
          academico_registration_statuses_id: Academico::RegistrationStatus::FULFILLED)
      else
        # if concept is edited and no longer pays registration we must rollback the registration
        deregister_student
      end
    end
  end

  def deregister_student
    if self.academico_course.present? && self.academico_student.present?

      if pays_registration?
        registration = Academico::Registration.find_by(
          academico_courses_id: self.academico_course_id,
          academico_students_id: self.academico_students_id)
        registration.destroy if registration.present?
      end

    end
  end


  def build_transactions
    invoiceTransaction = Admin::BillingTransaction.where(
      document_id: self.id,
      facturacion_document_type_id: self.facturacion_document_type_id)

    if invoiceTransaction.blank?
      description_of_transaction = ''
      if self.is_cash
        description_of_transaction = "Factura #{self.form_invoice_number}"
      else
        description_of_transaction = "Factura Crédito #{self.form_invoice_number}"
      end

      transaction = Admin::BillingTransaction.new(
        description: description_of_transaction,
        date: Date.today,
        document_id: self.id,
        facturacion_document_type_id: self.facturacion_document_type_id,
        document_number: self.invoice_number,
        transaction_amount: self.total_invoice)
      transaction.save!
    end

  end


  def void_transactions
    is_credit = !self.is_cash
    description_of_transaction = ''
    if is_credit
      description_of_transaction = "Anulación Factura Crédito #{self.form_invoice_number}"
    else
      description_of_transaction = "Anulación Factura #{self.form_invoice_number}"
    end

    invoiceTransaction = Admin::BillingTransaction.where(
      document_id: self.id, 
      facturacion_document_type_id: self.facturacion_document_type_id)

    if invoiceTransaction.present?
      transaction = Admin::BillingTransaction.new(
        description: description_of_transaction,
        date: Date.today,
        document_id: self.id,
        facturacion_document_type_id: self.facturacion_document_type_id,
        document_number: self.invoice_number,
        transaction_amount: -self.total_invoice)
      transaction.save!
    end
  end

	private
    
	def total_invoice_detail
		total_amount = 0
		self.facturacion_invoice_details.each_with_index do |detail, index|
			if !detail._destroy
				detail.sub_total = detail.amount * (1 - detail.discount/100)
				puts "Este es el subtotal del registro #{index}: #{detail.amount} + #{detail.discount} = #{detail.sub_total}"
				total_amount += detail.sub_total || 0

				if detail.facturacion_payment_detail_concept.is_book && detail.facturacion_invoice_detail_book.blank?
					detail.build_facturacion_invoice_detail_book
  				raise CustomExceptions::DocumentException, "Seleccione el libro para los conceptos de pago de tipo libro"
  			end

			end
		end
		total_amount
	end

	def total_payment_detail
		detail = self.facturacion_invoice_payment_detail
		if detail.present? && !detail._destroy
			self.total_payment = detail.set_total_payment(self.exchange_rate || 0)
		end
	end

    def init
    	set_invoice_number
    	self.facturacion_document_type_id ||= Facturacion::DocumentType.find_by(:name => "Factura").id
     	self.exchange_rate  ||= Admin::ExchangeRate.current_exchange_rate #will set the default value only if it's nil
      if self.is_cash
        self.invoice_canceled ||= true
        self.invoice_canceled_date ||= Date.today
      end
    end

    def check_for_printed_document
    	isPrinted?(printed)
    end

    def set_invoice_total
    	self.total_invoice = total_invoice_detail
    end

    def set_total_payment
    	total_payment_detail
    end

    def is_credit_validation
    	if !self.is_cash
    		self.facturacion_invoice_payment_detail = nil
    		self.is_pending_invoice = true
    	end
    end

  	def invoice_total_change
  		if self.is_cash
  			raise CustomExceptions::DocumentException, "Ingrese el monto a pagar" if !self.total_payment.present?
  			if self.total_payment < self.total_invoice
  				raise CustomExceptions::DocumentException, "El monto a pagar es mayor que la cantidad recibida, verifique los montos y proceda nuevamente"
  			else
				self.total_change = self.total_payment - self.total_invoice
			end
		else
			if self.new_record?
				self.total_change = 0
			else
				self.total_change = self.total_payment - self.total_invoice if self.total_payment.present?
			end
		end
	end

	def set_invoice_number
		invoice_number_resource = Facturacion::Invoice.last || Admin::Parameter.first || 0
		next_invoice_number = 1 + (invoice_number_resource.class != Fixnum ? invoice_number_resource.invoice_number : invoice_number_resource)
		self.invoice_number = next_invoice_number
	end


	def set_bussiness_actions
		begin
      build_transactions
		rescue Exception => e
			raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
		end
	end


end
