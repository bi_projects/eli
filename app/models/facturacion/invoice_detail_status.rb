class Facturacion::InvoiceDetailStatus < ApplicationRecord
  has_many :facturacion_invoice_details, :class_name => "Facturacion::InvoiceDetail", :inverse_of => :facturacion_invoice_detail_status, foreign_key: :facturacion_invoice_detail_status_id

  INVOICE_DETAIL_ACTIVE = 1
  INVOICE_DETAIL_INACTIVE = 2
end
