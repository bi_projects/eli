class Facturacion::Stock < ApplicationRecord
	belongs_to :facturacion_document_type, :class_name => "Facturacion::DocumentType", :inverse_of => :facturacion_stocks
	belongs_to :facturacion_warehouse, :class_name => "Facturacion::Warehouse", :inverse_of => :facturacion_stocks
	belongs_to :facturacion_book, :class_name => "Facturacion::Book", :inverse_of => :facturacion_stocks

  has_many :facturacion_transfer_request_stocks, :class_name => "Facturacion::TransferRequestStock", 
		:foreign_key => "facturacion_stock_id", :inverse_of => :facturacion_stock, :dependent => :destroy

  has_many :facturacion_transfer_requests, :through => :facturacion_transfer_request_stocks, :class_name => "Facturacion::TransferRequest"

	accepts_nested_attributes_for :facturacion_transfer_request_stocks,
		:reject_if     => :all_blank

	validates_presence_of :stock_date, :facturacion_document_type_id, :facturacion_book_id, :facturacion_warehouse_id, :scenario, :quantity, :unit_cost, :description

	after_save :set_bussiness_actions
	after_destroy :revert_book_quantity
	after_initialize :init, unless: :persisted?

	def self.scenario_map
		[["Carga","Carga"],["Descarga","Descarga"]]
	end

	def cost_average
		items = Facturacion::Stock.where(:facturacion_book_id => self.facturacion_book_id).where("LOWER(scenario) = 'carga'")
		items.sum(:unit_cost) / items.count(:unit_cost)
	end

	private
	def init
    set_stock_number
    self.stock_date ||= Date.today
  end

  def set_stock_number
		stock_number_resource = Facturacion::Stock.last || Facturacion::Stock.first || 0
		next_stock_number = 1 + (stock_number_resource.class != Fixnum ? stock_number_resource.stock_number : stock_number_resource)
		self.stock_number = next_stock_number
	end

	def delete_book_quantity
		begin
			book = Facturacion::Book.find(self.facturacion_book_id)
			book.quantity = self.quantity
			book.save!
		rescue Exception => e
			raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
		end
	end
	def revert_book_quantity
		book = Facturacion::Book.find(self.facturacion_book_id)
		book.quantity -= self.quantity if ("carga".upcase == self.scenario.upcase)
		book.quantity += self.quantity if ("descarga".upcase == self.scenario.upcase)
		book.unit_cost = cost_average
		book.save!
	end
	def set_book_quantity
		#begin
			book = Facturacion::Book.find(self.facturacion_book_id)
			if book.quantity.blank? 
				book.quantity = 0
			end
      # on edit revert back the previous amount and apply the new whether it is charge or discharge
			if self.canceled?
        # if transaction was void invert the sign of the operations
				book.quantity += self.quantity_was if ("carga".upcase == self.scenario_was.upcase)
				book.quantity -= self.quantity if ("carga".upcase == self.scenario.upcase)
				book.quantity -= self.quantity_was if ("descarga".upcase == self.scenario_was.upcase)
				book.quantity += self.quantity if ("descarga".upcase == self.scenario.upcase)
			else
        # regular scenario
				book.quantity -= self.quantity_was if self.scenario_was.present? && ("carga".upcase == self.scenario_was.upcase)
				book.quantity += self.quantity if ("carga".upcase == self.scenario.upcase)
				book.quantity += self.quantity_was if self.scenario_was.present?  && ("descarga".upcase == self.scenario_was.upcase)
				book.quantity -= self.quantity if ("descarga".upcase == self.scenario.upcase)
			end
			book.unit_cost = cost_average

			book.save!
			
		#rescue Exception => e
			#raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
		#end
	end

	def set_bussiness_actions
		
		begin
			set_book_quantity
			transaction = Admin::BillingTransaction.new(
				description: "#{self.scenario} No. #{self.stock_number}",
				date: Date.today,
				document_id: self.id,
				facturacion_document_type_id: self.facturacion_document_type_id,
				document_number: self.stock_number,
				transaction_amount: self.quantity*self.unit_cost)

			transaction.save!

		rescue Exception => e
			raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
		end
	end

	def self.balance_warehouse(warehouse_id, book_id)
		carga = self.where(:facturacion_warehouse_id => warehouse_id, :facturacion_book_id => book_id).where("LOWER(scenario) = 'carga'").sum(:quantity)
		descarga = self.where(:facturacion_warehouse_id => warehouse_id, :facturacion_book_id => book_id).where("LOWER(scenario) = 'descarga'").sum(:quantity)
		carga - descarga
	end
end