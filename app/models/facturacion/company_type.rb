class Facturacion::CompanyType < ApplicationRecord
	before_destroy :check_for_companies
	has_many :facturacion_companies, :class_name => "Facturacion::Company", 
		foreign_key: "facturacion_company_type_id", inverse_of: :facturacion_company_type
	validates_presence_of :name, :description
	validates_uniqueness_of :name
	include AvoidDestroyReferencesUtility #Validacion en concern para relaciones con modelo preregistro, bloquea el destroy del registro
	
	def display_name
		name
	end
	
	private
	def check_for_companies
		check_for_relation(facturacion_companies, "Empresa")
	end
end
