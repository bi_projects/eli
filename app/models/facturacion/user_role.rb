class Facturacion::UserRole < ApplicationRecord
  belongs_to :facturacion_billing_user, inverse_of: :facturacion_user_roles, class_name: 'Facturacion::BillingUser', foreign_key: :facturacion_billing_user_id
  belongs_to :facturacion_facturacion_role, inverse_of: :facturacion_user_roles, class_name: 'Facturacion::FacturacionRole', foreign_key: :facturacion_facturacion_role_id
end
