class Facturacion::DocumentType < ApplicationRecord
	before_destroy :check_for_stock

	has_many :facturacion_stocks, :class_name => "Facturacion::Stock", 
		foreign_key: "facturacion_document_type_id", :inverse_of => :facturacion_document_type
	has_many :facturacion_invoices, :class_name => "Facturacion::Invoice", 
		foreign_key: "facturacion_document_type_id", :inverse_of => :facturacion_document_type
	has_many :facturacion_transfer_requests, :class_name => "Facturacion::TransferRequest", 
		foreign_key: "facturacion_document_type_id", :inverse_of => :facturacion_document_type

	include AvoidDestroyReferencesUtility 

  FACTURA = 1
  RECIBO = 2
	NOTA_CREDITO = 3
	NOTA_DEBITO = 4
  COMPROBANTE = 5
  INVENTARIO = 6

	def display_name
		name
	end

	private
	def check_for_stock
		check_for_relation(facturacion_stocks, "Stock")
	end
end
