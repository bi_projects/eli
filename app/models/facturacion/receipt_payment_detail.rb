class Facturacion::ReceiptPaymentDetail < ApplicationRecord
	belongs_to :facturacion_receipt, :class_name => "Facturacion::Receipt", :inverse_of => :facturacion_receipt_payment_detail

	def set_total_payment(exchange_rate)
	self.total_payment = self.retention_amount_cordoba + 
		self.cash_amount_usd * exchange_rate  + 
		self.cash_amount_cordoba + 
		self.credit_card_amount_usd * exchange_rate  + 
		self.credit_card_amount_cordoba + 
		self.ck_amount_usd * exchange_rate  + 
		self.ck_amount_cordoba + 
		self.bank_amount_usd * exchange_rate  + 
		self.telepago_amount_usd * exchange_rate +
		self.other_payment
	self.total_payment
	end
end
