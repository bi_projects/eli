class Facturacion::StudentTutor < ApplicationRecord
  belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_student_tutor
  belongs_to :facturacion_tutor, :class_name => "Facturacion::Tutor", :inverse_of => :facturacion_student_tutors
end
