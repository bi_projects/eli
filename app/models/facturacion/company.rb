class Facturacion::Company < ApplicationRecord
	before_destroy :check_for_preregistration
	belongs_to :facturacion_company_type, :class_name => "Facturacion::CompanyType", :inverse_of => :facturacion_companies
	has_many :facturacion_student_companies, :class_name => "Facturacion::StudentCompany", 
		foreign_key: "facturacion_company_id", :inverse_of => :facturacion_company
	
	has_many :facturacion_preregistrations, :through => :facturacion_student_companies, :class_name => "Facturacion::Preregistration"
	
	validates_presence_of :name, :contact_name, :phone, :email, :address, :company_description, :facturacion_company_type
	validates_uniqueness_of :name

	include AvoidDestroyReferencesUtility #Validación en concern para relaciones con modelo preregistro, bloquea el destroy del registro
	
	def display_name
		name
	end

	private
	def check_for_preregistration
		check_for_relation(facturacion_preregistrations, "Preregistro")
	end

end
