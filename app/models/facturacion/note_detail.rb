class Facturacion::NoteDetail < ApplicationRecord
  belongs_to :facturacion_note, :class_name => "Facturacion::Note", :inverse_of => :facturacion_note_details
  belongs_to :facturacion_invoice, :class_name => "Facturacion::Invoice", :inverse_of => :facturacion_note_details
  belongs_to :facturacion_invoice_detail, :class_name => "Facturacion::InvoiceDetail", :inverse_of => :facturacion_note_detail

  after_save :inactivate_invoice_detail
  after_destroy :redo_invoice_detail


  # inactive, i.e. book returned
  def inactivate_invoice_detail
    if self.facturacion_invoice_details_id.present?
      facturacion_invoice_detail_ = Facturacion::InvoiceDetail.find(self.facturacion_invoice_details_id)
      facturacion_invoice_detail_.inactivate
    end
  end

  def redo_invoice_detail
    if self.facturacion_invoice_details_id_was.present?
      facturacion_invoice_detail_ = Facturacion::InvoiceDetail.find(self.facturacion_invoice_details_id_was)
      facturacion_invoice_detail_.redo
    end
  end
end
