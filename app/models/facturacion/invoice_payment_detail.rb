class Facturacion::InvoicePaymentDetail < ApplicationRecord
  belongs_to :facturacion_invoice, :class_name => "Facturacion::Invoice", :inverse_of => :facturacion_invoice_payment_detail

  def set_total_payment(exchange_rate)
    self.total_payment = self.retention_amount_cordoba + 
      self.cash_amount_usd * exchange_rate  + 
      self.cash_amount_cordoba + 
      self.credit_card_amount_usd * exchange_rate  + 
      self.credit_card_amount_cordoba + 
      self.ck_amount_usd * exchange_rate  + 
      self.ck_amount_cordoba + 
      self.bank_amount_usd * exchange_rate  + 
      self.telepago_amount_usd * exchange_rate 
    self.total_payment
  end

end
