class Facturacion::FacturacionRole < ApplicationRecord
  has_many :facturacion_user_roles, class_name: 'Facturacion::UserRole', foreign_key: :facturacion_facturacion_role_id

  has_many :facturacion_role_permissions, inverse_of: :facturacion_facturacion_role, class_name: "Admin::FacturacionRolePermission", foreign_key: 'facturacion_facturacion_role_id'
  accepts_nested_attributes_for :facturacion_role_permissions, allow_destroy: true
end
