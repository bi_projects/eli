class Facturacion::StudentCompany < ApplicationRecord
  belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_student_company
  belongs_to :facturacion_company, :class_name => "Facturacion::Company", :inverse_of => :facturacion_student_companies
end
