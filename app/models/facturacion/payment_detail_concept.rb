class Facturacion::PaymentDetailConcept < ApplicationRecord
  has_many :facturacion_invoice_details, :class_name => "Facturacion::InvoiceDetail",
      foreign_key: "facturacion_payment_detail_concept_id", :inverse_of => :facturacion_payment_detail_concept

  has_many :facturacion_payment_detail_concept_detail_prices,
           class_name: 'Facturacion::PaymentDetailConceptDetailPrice',
           foreign_key: 'facturacion_payment_detail_concept_id'

  accepts_nested_attributes_for :facturacion_payment_detail_concept_detail_prices,
                                allow_destroy: true,
                                reject_if: :all_blank

  validate :check_flags

  attr_accessor :total_discount

  def check_flags
    errors.add(:base, 'El concepto puede ser libro o matrícula, pero no ambos') if self.is_book && self.is_registration
  end

  CONCEPT_BOOK=1
  CONCEPT_REGISTRATION=2


  def display_name
    self.name
  end

  def price_for_classification(book, course_id)
    return book.try(:price) || self.price || BigDecimal.new(0) unless self.is_book || self.is_registration

    amount = Facturacion::CourseBookRegistration.reg_book_prices(course_id).try(:book_price) || 0 if self.is_book
    amount = Facturacion::CourseBookRegistration.reg_book_prices(course_id).try(:registration_price) || 0 if self.is_registration

    #amount = self.facturacion_payment_detail_concept_detail_prices
    #             .where(academico_level_id: academico_level_id,
    #                    academico_program_id: academico_program_id)
    #              .first.try(:amount)

    amount || self.price
  end
end
