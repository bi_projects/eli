class Facturacion::Preregistration < ApplicationRecord
	has_one :facturacion_student_company, :class_name => "Facturacion::StudentCompany",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration, :dependent => :destroy
	has_one :facturacion_company, :through => :facturacion_student_company, :class_name => "Facturacion::Company"

	has_one :facturacion_student_carrier, :class_name => "Facturacion::StudentCarrier",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration, :dependent => :destroy, autosave: true
	has_one :facturacion_carrier, :through => :facturacion_student_carrier, :class_name => "Facturacion::Carrier"

	has_many :facturacion_student_discounts, :class_name => "Facturacion::StudentDiscount",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration, :dependent => :destroy
	has_many :facturacion_discounts, :through => :facturacion_student_discounts, :class_name => "Facturacion::Discount"

	has_one :facturacion_student_tutor, :class_name => "Facturacion::StudentTutor",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration, :dependent => :destroy
	has_one :facturacion_tutor, :through => :facturacion_student_tutor, :class_name => "Facturacion::Tutor"

	has_one :facturacion_student_level_program, :class_name => "Facturacion::StudentLevelProgram",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration, :dependent => :destroy
	#has_one :academico_program, :through => :facturacion_student_level_program, :class_name => "Academico::Program"


	belongs_to :academico_program, :class_name => "Academico::Program"


	has_many :facturacion_invoices, :class_name => "Facturacion::Invoice",
		foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration

  	has_many :facturacion_notes, :class_name => "Facturacion::Note",
    	foreign_key: "facturacion_preregistration_id", :inverse_of => :facturacion_preregistration

	belongs_to :academico_level, :class_name => "Academico::Level"

	has_one :academico_student, class_name: "Academico::Student",
   :inverse_of => :facturacion_preregistration, foreign_key: "facturacion_preregistration_id"

	accepts_nested_attributes_for :facturacion_student_company,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_student_carrier,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_student_discounts,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_student_tutor,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_student_level_program,
		:allow_destroy => true,
		:reject_if     => :all_blank
	
	include StudentPreRegistration
	include Filterable
	before_validation :mark_facturacion_student_company_for_destruction, 
		:mark_facturacion_student_carrier_for_destruction, :mark_facturacion_student_discount_for_destruction,
		:mark_facturacion_student_tutor_for_destruction

	after_save :create_also_academico_student
	scope :has_tutor, -> (tutor) { joins(:facturacion_tutor) if tutor.to_i > 0}
	scope :has_company, -> (company) { joins(:facturacion_company) if company.to_i > 0}
	scope :has_carrier, -> (carrier) { joins(:facturacion_carrier) if carrier.to_i > 0}
	scope :has_discount, -> (discount) { joins(:facturacion_discounts).distinct if discount.to_i > 0}
	
	scope :first_name_like, -> (name) { where("first_name ilike ?","%#{name}%")}
	scope :last_name_like, -> (last_name) { where("last_name ilike ?","%#{last_name}%")}
	scope :operator_filter, -> (operator,age) { where("date_part('year',age(birth_date)) #{operator} ?", age)}
	scope :tutor, -> (tutor) { joins(:facturacion_tutor).where("facturacion_tutors.id = ?",tutor)}
	scope :company, -> (company) { joins(:facturacion_company).where("facturacion_companies.id = ?",company)}
	scope :carrier, -> (carrier) { joins(:facturacion_carrier).where("facturacion_carriers.id = ?",carrier)}
	scope :discount_type, -> (discount) { joins(:facturacion_discounts).where("facturacion_discounts.id = ?",discount).distinct}
	scope :level, -> (level) { joins(:academico_level).where("academico_levels.id = ?",level).distinct}
	scope :with_no_students, -> () { where("NOT exists(SELECT id FROM academico_students WHERE facturacion_preregistrations.id=facturacion_preregistration_id)") }
	scope :created, -> () { where('facturacion_preregistration_statuses_id = 1') }


	def display_name
		full_name
	end

	def full_name
		first_name + " " + last_name
	end

	def mark_facturacion_student_company_for_destruction
		if facturacion_student_company.present? && facturacion_student_company.facturacion_company.blank?
			facturacion_student_company.mark_for_destruction
		end
	end
	
	def mark_facturacion_student_carrier_for_destruction
		if facturacion_student_carrier.present? && facturacion_student_carrier.facturacion_carrier.blank?
			facturacion_student_carrier.mark_for_destruction
		end
	end

	def mark_facturacion_student_tutor_for_destruction
		if facturacion_student_tutor.present? && facturacion_student_tutor.facturacion_tutor.blank?
			facturacion_student_tutor.mark_for_destruction
		end
	end

	def mark_facturacion_student_discount_for_destruction
		facturacion_student_discounts.each do |facturacion_student_discount|
			if facturacion_student_discount.facturacion_discount.blank?
				facturacion_student_discount.mark_for_destruction
			end
		end
	end

	def total_discounts
		total = 0
		self.facturacion_discounts.each do |discount|
			total += discount.discount_amount
		end
		total
	end

	def create_also_academico_student
		if self.academico_student
			self.academico_student.update_attributes(first_name: self.first_name, 
				last_name: self.last_name, 
				birth_date: self.birth_date, 
				facturacion_preregistration_id: self.id,
				phone: self.phone, 
				address: self.address, 
				email: self.email)
			academico_student.save
		else
			Academico::Student.create_student_from_facturacion_preregistration self
		end
	end
end
