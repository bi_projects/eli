class Facturacion::Carrier < ApplicationRecord
  include PhonePrototype
	before_destroy :check_for_preregistration
	has_many :facturacion_student_carriers, :class_name => "Facturacion::StudentCarrier", 
		foreign_key: "facturacion_carrier_id", :inverse_of => :facturacion_carrier
	
	has_many :facturacion_preregistrations, :through => :facturacion_student_carriers, :class_name => "Facturacion::Preregistration"
	
	validates_presence_of :name, :contact_name, :phone
	validates_uniqueness_of :name

	include AvoidDestroyReferencesUtility #Validación en concern para relaciones con modelo preregistro, bloquea el destroy del registro
	
	def display_name
		name
	end

	private
	def check_for_preregistration
		check_for_relation(facturacion_preregistrations, "Preregistro")
	end
end
