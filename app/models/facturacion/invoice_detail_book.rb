class Facturacion::InvoiceDetailBook < ApplicationRecord
	belongs_to :facturacion_invoice_detail, :class_name => "Facturacion::InvoiceDetail", :inverse_of => :facturacion_invoice_detail_book
	belongs_to :facturacion_book, :class_name => "Facturacion::Book", :inverse_of => :facturacion_invoice_detail_books
	after_save :decrease_stock
	after_destroy :increase_stock


  def decrease_stock
    stock = Facturacion::Stock.find_or_initialize_by(
      facturacion_document_type_id: self.facturacion_invoice_detail.try(:facturacion_invoice).try(:facturacion_document_type_id),
      document_id: self.facturacion_invoice_detail.try(:facturacion_invoice).try(:id),
      scenario: 'Descarga')
		stock.facturacion_book_id = self.facturacion_book_id
		stock.facturacion_warehouse_id = self.facturacion_invoice_detail.facturacion_invoice.facturacion_warehouses.first.id
		stock.quantity  = 1
		stock.unit_cost = stock.cost_average
		stock.description = "Descarga de Inventario correspondiente a Factura ##{self.facturacion_invoice_detail.facturacion_invoice.invoice_number} detalle ID ##{self.facturacion_invoice_detail.id}"
		stock.save!
	end
  def increase_stock
		stock = Facturacion::Stock.new
		stock.facturacion_document_type_id = self.facturacion_invoice_detail.try(:facturacion_invoice).try(:facturacion_document_type_id)
    stock.document_id = self.facturacion_invoice_detail.try(:facturacion_invoice).try(:id)
		stock.facturacion_book_id = self.facturacion_book_id
		stock.facturacion_warehouse_id = self.facturacion_invoice_detail.facturacion_invoice.facturacion_warehouses.first.id
		stock.scenario = "Carga"
		stock.quantity  = 1
		stock.unit_cost = stock.cost_average
		stock.description = "Carga de Inventario correspondiente a Factura ##{self.facturacion_invoice_detail.facturacion_invoice.invoice_number} detalle ID ##{self.facturacion_invoice_detail.id}"
		stock.save!
	end
end
