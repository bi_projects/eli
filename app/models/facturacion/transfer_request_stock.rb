class Facturacion::TransferRequestStock < ApplicationRecord
  belongs_to :facturacion_transfer_request, :class_name => "Facturacion::TransferRequest", :inverse_of => :facturacion_transfer_request_stocks, :foreign_key => :facturacion_transfer_request_id
  belongs_to :facturacion_stock, :class_name => "Facturacion::Stock", :inverse_of => :facturacion_transfer_request_stocks, :foreign_key => :facturacion_stock_id
end
