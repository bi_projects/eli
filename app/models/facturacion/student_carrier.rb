class Facturacion::StudentCarrier < ApplicationRecord
  belongs_to :facturacion_preregistration, :class_name => "Facturacion::Preregistration", :inverse_of => :facturacion_student_carrier
  belongs_to :facturacion_carrier, :class_name => "Facturacion::Carrier", :inverse_of => :facturacion_student_carriers
end
