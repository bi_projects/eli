class Facturacion::InvoiceDetail < ApplicationRecord
  before_validation :verify_discount_before_save
	has_one :facturacion_invoice_detail_book, :class_name => "Facturacion::InvoiceDetailBook",
    	foreign_key: "facturacion_invoice_detail_id", :inverse_of => :facturacion_invoice_detail, :dependent => :destroy

	belongs_to :facturacion_payment_detail_concept, :class_name => "Facturacion::PaymentDetailConcept", :inverse_of => :facturacion_invoice_details
	belongs_to :facturacion_invoice, :class_name => "Facturacion::Invoice", :inverse_of => :facturacion_invoice_details, foreign_key: :facturacion_invoice_id
  belongs_to :facturacion_invoice_detail_status, :class_name => "Facturacion::InvoiceDetailStatus", :inverse_of => :facturacion_invoice_details, foreign_key: :facturacion_invoice_detail_status_id
  has_one :facturacion_note_detail, :class_name => "Facturacion::NoteDetail",
    foreign_key: "facturacion_invoice_details_id", :inverse_of => :facturacion_invoice_detail

	accepts_nested_attributes_for :facturacion_invoice_detail_book,
		:allow_destroy => true,
		:reject_if     => :all_blank

	validates_presence_of :facturacion_payment_detail_concept_id, :description, :amount, :discount

  enum discount_type: { custom: 0, preregistration: 1, driver: 2 }


  CUSTOM = 'Personalizada'
  DISCOUNT = 'Descuento'
  DRIVER = 'Transportista'

  def self.discounts(discount_type)
    case discount_type
    when 'custom'
      return [CUSTOM, 'custom']
    when 'preregistration'
      return [DISCOUNT, 'preregistration']
    when 'driver'
      return [DRIVER, 'driver']
    end
  end

  def verify_discount_before_save
    if facturacion_payment_detail_concept_id == Facturacion::PaymentDetailConcept::CONCEPT_BOOK
      self.discount = 0
      self.discount_type = 0
    end
  end

  def book_name
    self.facturacion_invoice_detail_book.try(:facturacion_book).try(:display_name)
  end

  def refund_stock
    if self.facturacion_payment_detail_concept.is_book
      self.facturacion_invoice_detail_book.increase_stock
    end
  end

  def rebuild_stock_record
    if self.facturacion_payment_detail_concept.is_book
      self.facturacion_invoice_detail_book.decrease_stock
    end
  end

  # inactive, i.e. book returned
  def inactivate
    if self.persisted?
      self.facturacion_invoice_detail_status_id = Facturacion::InvoiceDetailStatus::INVOICE_DETAIL_INACTIVE
      self.save
      self.refund_stock
    end
  end

  def redo
    if self.persisted?
      self.facturacion_invoice_detail_status_id = Facturacion::InvoiceDetailStatus::INVOICE_DETAIL_ACTIVE
      self.save
      self.rebuild_stock_record
    end
  end
end
