class Facturacion::AccountType < ApplicationRecord
  has_many :facturacion_accounting_accounts, inverse_of: :facturacion_account_type,
   class_name: 'Facturacion::AccountingAccount', foreign_key: :facturacion_account_types_id
end
