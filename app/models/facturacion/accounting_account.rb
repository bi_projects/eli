class Facturacion::AccountingAccount < ApplicationRecord
  include Filterable
  belongs_to :facturacion_account_to_record, class_name: 'Facturacion::AccountToRecord',
    inverse_of: :facturacion_accounting_accounts, foreign_key: :facturacion_account_to_records_id
  belongs_to :facturacion_account_type, class_name: 'Facturacion::AccountType',
    inverse_of: :facturacion_accounting_accounts, foreign_key: :facturacion_account_types_id
  belongs_to :facturacion_accounting_account_category, class_name: 'Facturacion::AccountingAccountCategory',
    inverse_of: :facturacion_accounting_accounts, foreign_key: :facturacion_accounting_account_categories_id
  belongs_to :facturacion_parent_account, class_name: 'Facturacion::AccountingAccount',
    foreign_key: :parent_account

  scope :activo, -> { where(facturacion_accounting_account_categories_id: Facturacion::AccountingAccountCategory::ACTIVO) }
  scope :pasivo, -> { where(facturacion_accounting_account_categories_id: Facturacion::AccountingAccountCategory::PASIVO) }
  scope :capital, -> { where(facturacion_accounting_account_categories_id: Facturacion::AccountingAccountCategory::CAPITAL) }
  scope :egreso, -> { where(facturacion_accounting_account_categories_id: Facturacion::AccountingAccountCategory::EGRESO) }
  scope :ingreso, -> { where(facturacion_accounting_account_categories_id: Facturacion::AccountingAccountCategory::INGRESO) }
  scope :disableds, -> { where(is_disabled: true) }
  scope :enableds, -> { where(is_disabled: false) }

  scope :account_category_eq, -> (category_id) { where('facturacion_accounting_account_categories_id = ?', category_id)}
  scope :parent_account_eq, -> (account_id) { where('parent_account = ?', account_id)}
  scope :account_type_eq, -> (type_id) { where('facturacion_account_types_id = ?', type_id)}
  scope :account_number_like, -> (account_number) { where('account_number ilike ?', "%#{account_number}%")}
  scope :name_like, -> (account_name) { where('name ilike ?', "%#{account_name}%")}
  scope :status_eq, -> (status) { where('is_disabled = ?', status == 'true')}
  default_scope { enableds.order(:account_number) }

  def status
    self.is_disabled ? 'Inactiva' : 'Activa'
  end

  def hijas(cuentas_padre, recursive = false)
    cuentas = CuentaContable.where(cuenta_padre_id: self.cuenta_contable_id)

    return [self] if self.tipo_cuenta_id == TipoCuenta::DETALLE

    if recursive
      cuentas.map do |c|
        if c.tipo_cuenta_id == TipoCuenta::MAYOR
          c.hijas(nil, true)
        else
          c
        end
      end.flatten
    else
      CuentaContable.where(cuenta_padre_id: self.cuenta_contable_id)
                    .where('cuenta_contable_id not in (?)', cuentas_padre).order(:numero_cuenta)
    end
  end

  def display_name
    "#{self.account_number} / #{self.name}"
  end

  def self.suggest_child_number(parent_account_id)
    return "" if parent_account_id.blank?
    parent_account_obj = self.find(parent_account_id)
    account_digits = 4
    remove_pattern = "\-0{#{account_digits},}"
    if parent_account_obj.present?
      account = ""
      int_part = 0
      first_number = nil
      counter = 0
      regex = Regexp.compile(remove_pattern)

      parent_account_number = parent_account_obj.account_number.gsub(regex, "")
      children_accounts = self.where(parent_account: parent_account_id).order("account_number ASC")
      children_accounts.each do |child|
        account = child.account_number.gsub(regex, "")
        position = account.index(parent_account_number)
        if position.nil?
          next
        end

        position += parent_account_number.size
        left_over = account.slice(position, account.size)
        if left_over.size == 0
          next
        end

          # Iterate children and test if there are holes in between to return the next hole if any otherwise the biggest number
        if (matched_expr = /^\-\d+/.match(left_over)).present?
          int_part = matched_expr.to_s.gsub(/^\-/, "").try(:to_i)
          if first_number.nil?
            first_number = int_part
          end
          if first_number + counter < int_part
            return "#{parent_account_number}-#{self.format_number(first_number + counter, account_digits)}"
          end
          counter+=1
        end
      end
      "#{parent_account_number}-#{self.format_number(counter + 1, account_digits)}"
    end
  end

  def self.format_number(number, digits)
    if number.is_a? Integer
      number_as_str = number.to_s
      if number_as_str.size <= digits
        return '0'*(digits - number_as_str.size) + number_as_str
      end
    end
  end
end
