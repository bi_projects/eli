class Facturacion::CostCenter < ApplicationRecord
	before_destroy :check_for_warehouse

	has_many :facturacion_warehouses, :class_name => "Facturacion::Warehouse", 
		foreign_key: "facturacion_cost_center_id", :inverse_of => :facturacion_cost_center

	include AvoidDestroyReferencesUtility 

	validates_presence_of :code, :name, :description
	validates_uniqueness_of :code, :name

	def display_name
		name
	end

	private
	def check_for_warehouse
		check_for_relation(facturacion_warehouses, "Bodega")
	end
end
