class Facturacion::Tutor < ApplicationRecord
  include EmailPrototype
  include PhonePrototype

	before_destroy :check_for_preregistration
	has_many :facturacion_student_tutors, :class_name => "Facturacion::StudentTutor", 
		foreign_key: "facturacion_tutor_id", :inverse_of => :facturacion_tutor
	
	has_many :facturacion_preregistrations, :through => :facturacion_student_tutors, :class_name => "Facturacion::Preregistration"
	
	validates_presence_of :first_name, :last_name, :phone, :email

	include AvoidDestroyReferencesUtility #Validación en concern para relaciones con modelo preregistro, bloquea el destroy del registro
	def display_name
		full_name
	end
	def full_name
		first_name + " " + last_name
	end
	
	private
	def check_for_preregistration
		check_for_relation(facturacion_preregistrations, "Preregistro")
	end
end
