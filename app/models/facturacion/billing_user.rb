class Facturacion::BillingUser < ApplicationRecord
	mount_uploader :avatar, AvatarUploader
	has_many :facturacion_user_roles, class_name: 'Facturacion::UserRole', foreign_key: :facturacion_billing_user_id
	has_one :admin_billing_user_position, :class_name => "Admin::BillingUserPosition",
		foreign_key: "facturacion_billing_user_id", :inverse_of => :facturacion_billing_user
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable
	include UserModelUtility
	accepts_nested_attributes_for :facturacion_user_roles, allow_destroy: true

	belongs_to :admin_branch_office, class_name: 'Admin::BranchOffice'

	def display_name
		email
	end

  def self.current
    Thread.current[:billing_user]
  end

  def self.current=(user)
    Thread.current[:billing_user] = user
  end
end