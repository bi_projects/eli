class Facturacion::TransferRequest < ApplicationRecord
	belongs_to :facturacion_document_type, :class_name => "Facturacion::DocumentType", :inverse_of => :facturacion_transfer_requests

	has_many :facturacion_transfer_request_stocks, :class_name => "Facturacion::TransferRequestStock", 
		:foreign_key => "facturacion_transfer_request_id", :inverse_of => :facturacion_transfer_request, :dependent => :destroy

	has_many :facturacion_stocks, :through => :facturacion_transfer_request_stocks, :class_name => "Facturacion::Stock", :autosave => true

	accepts_nested_attributes_for :facturacion_transfer_request_stocks,
		:allow_destroy => true,
		:reject_if     => :all_blank

	accepts_nested_attributes_for :facturacion_stocks,
    :allow_destroy => true,
		:reject_if     => :all_blank

  before_validation :autocomplete_stock_data
  before_create :validates_source_destination

  def validates_source_destination
    if self.facturacion_warehouse_id == self.facturacion_warehouse_origin_id
      errors.add(:base, 'La bodega de origen y destino no puedan ser la misma')
    end
  end

  def autocomplete_stock_data
    stock_outputs = []
    self.facturacion_stocks.each do |stock|
      autocomplete_stock_input(stock)

      # record to substract from the source store
      stock_outputs << autocomplete_stock_output(stock)
    end
    self.facturacion_stocks << stock_outputs
  end

  def autocomplete_stock_input(stock)
    stock.facturacion_document_type_id = Facturacion::DocumentType::INVENTARIO
    stock.stock_date = self.transfer_date
    stock.document_id = self.id
    stock.canceled = false
    stock.scenario = 'carga'
    stock.description = 'Carga de Inventario'
    stock.facturacion_warehouse_id = self.facturacion_warehouse_id
    stock
  end

  def autocomplete_stock_output(stock)
    stock_balance = Facturacion::Stock.new
    stock_balance.facturacion_document_type_id = Facturacion::DocumentType::INVENTARIO
    stock_balance.facturacion_book_id = stock.facturacion_book_id
    stock_balance.quantity = stock.quantity
    stock_balance.unit_cost = stock.unit_cost
    stock_balance.stock_date = self.transfer_date
    stock_balance.document_id = self.id
    stock_balance.canceled = false
    stock_balance.description = 'Descarga de Inventario'
    stock_balance.scenario = 'descarga'
    # stock_number += 1 asumes "carga" is being initialized from form input and be saved right after
    # this is important since stock_number is set on initialization and we dont want concurrency issues with other record
    stock_balance.stock_number =  stock.stock_number + 1
    stock_balance.facturacion_warehouse_id = self.facturacion_warehouse_origin_id
    stock_balance
  end
end
