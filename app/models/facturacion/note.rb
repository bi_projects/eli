class Facturacion::Note < ApplicationRecord
  include FacturacionUtility
    belongs_to :academico_student, :class_name => "Academico::Student", :inverse_of => :facturacion_receipts, foreign_key: "academico_student_id"
    belongs_to :admin_branch_note, :class_name => "Admin::BranchOffice", :inverse_of => :facturacion_notes
    
    before_save :set_note_total
    
    after_initialize :init, unless: :persisted?

    after_save :set_bussiness_actions

    has_many :facturacion_note_details, :class_name => "Facturacion::NoteDetail",
    	foreign_key: "facturacion_note_id", :inverse_of => :facturacion_note, :dependent => :destroy
    
    accepts_nested_attributes_for :facturacion_note_details,
		:allow_destroy => true,
		:reject_if     => :all_blank

	has_many :facturacion_receipt_detail_notes, :class_name => "Facturacion::ReceiptDetailNote",
    	foreign_key: "facturacion_note_id", :inverse_of => :facturacion_note

    has_many :facturacion_receipt_details, :class_name => "Facturacion::ReceiptDetail",
    	through: :facturacion_receipt_detail_notes, :inverse_of => :facturacion_note

    has_many :facturacion_receipts, :class_name => "Facturacion::Receipt",
       through: :facturacion_receipt_details, :inverse_of => :facturacion_notes

  belongs_to :facturacion_billing_user, class_name: 'Facturacion::BillingUser', inverse_of: :notes,
    foreign_key: 'facturacion_billing_user_id'
	
	#validates_uniqueness_of :note_number
	def display_name
		"#{form_note_number} - #{total_detail - total_note_payment}"
	end

	def total_note_payment
		total_spent = 0
		self.facturacion_receipt_details.each do |detail|
			total_spent += detail.payment_amount
		end
		total_spent
	end

	def note_date_formatted
		if !self.new_record?
			note_date.strftime("%d/%m/%Y")
		else
			note_date
		end
	end

	def form_note_number
		if self.note_type == NOTA_CREDITO
			prefix = Admin::Parameter.count > 0 ? Admin::Parameter.first.credit_note_prefix : "NC"
		else 
			prefix = Admin::Parameter.count > 0 ? Admin::Parameter.first.debit_note_prefix : "ND"
		end
		"#{prefix}-#{self.note_number}"
	end

	def total_detail
		total = 0
		self.facturacion_note_details.each do |detail|
			total += detail.amount || 0
		end
		total
	end

	private

	def total_note_detail
		total_amount = 0
		self.facturacion_note_details.each_with_index do |detail, index|
			if !detail._destroy
				total_amount += detail.amount || 0
			end
		end
		total_amount
	end

	def set_note_total
    	self.total_note = total_note_detail
    end

  def init
    set_note_number
    self.note_date ||= Date.today
  end

  def set_note_number
    self.facturacion_document_type_id, note_number_method =
      if self.note_type == NOTA_CREDITO
        [Facturacion::DocumentType::NOTA_CREDITO, :credit_note_number]
      else
        [Facturacion::DocumentType::NOTA_DEBITO, :debit_note_number]
      end

      note_number_reference = Facturacion::Note.where(:note_type => self.note_type).last.try(:note_number) ||
                              Admin::Parameter.first.try(note_number_method) || 0

      self.note_number = note_number_reference + 1
  end

	def set_bussiness_actions
		begin

				transaction = Admin::BillingTransaction.new(
					description: "#{self.note_type == NOTA_CREDITO ? 'Nota de Crédito' : 'Nota de débito'} No. #{self.form_note_number}",
					date: Date.today,
					document_id: self.id,
					facturacion_document_type_id: self.facturacion_document_type_id,
					document_number: self.note_number,
					transaction_amount: self.total_note)

				transaction.save!

		rescue Exception => e
			raise CustomExceptions::DocumentException, "Ha ocurrido un error al guardar los datos, por favor verifique y realice nuevamente la gestión: #{e.message}"
		end
	end
end
