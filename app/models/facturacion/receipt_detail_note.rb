class Facturacion::ReceiptDetailNote < ApplicationRecord
 	belongs_to :facturacion_receipt_detail, :class_name => "Facturacion::ReceiptDetail", :inverse_of => :facturacion_receipt_detail_note
  	belongs_to :facturacion_note, :class_name => "Facturacion::Note", :inverse_of => :facturacion_receipt_detail_notes 
    has_many :facturacion_receipts, :through => :facturacion_receipt_details, :class_name => "Facturacion::Receipt", :inverse_of => :facturacion_receipt_detail_notes 
end
