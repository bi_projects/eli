class Facturacion::Book < ApplicationRecord
    before_destroy :check_for_level_program_book, :check_for_stock

    has_many :academico_level_program_books, :class_name => "Academico::LevelProgramBook", 
        foreign_key: "facturacion_book_id", :inverse_of => :facturacion_book

    has_many :facturacion_stocks, :class_name => "Facturacion::Stock", 
        foreign_key: "facturacion_book_id", :inverse_of => :facturacion_book

    has_many :facturacion_invoice_detail_books, :class_name => "Facturacion::InvoiceDetailBook", 
        foreign_key: "facturacion_book_id", :inverse_of => :facturacion_book

  belongs_to :academico_level, class_name: 'Academico::Level', foreign_key: "academico_level_id",
             inverse_of: :facturacion_book
  belongs_to :academico_program, class_name: 'Academico::Program', foreign_key: "academico_program_id",
             inverse_of: :facturacion_book

    include AvoidDestroyReferencesUtility 

    validates_presence_of :name, :description, :min_stock, :price
    validates_uniqueness_of :name

    def display_name
        name
    end

  def self.search_book_for_payment_detail_concept(is_book, is_other_item, course_id)

    if is_book
        course = Facturacion::CourseBookRegistration.where(academico_course_id: course_id).first
        course.facturacion_books
        
    elsif is_other_item
        books = Facturacion::CourseBookRegistration.joins(:facturacion_books).pluck(:facturacion_book_id).uniq
        Facturacion::Book.where("id not in (?)", books)
    end
  end

    private
    def check_for_level_program_book
        check_for_relation(academico_level_program_books, "Nivel")
    end

    def check_for_stock
        check_for_relation(facturacion_stocks, "Stock")
    end
end
