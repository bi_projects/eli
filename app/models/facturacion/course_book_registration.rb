class Facturacion::CourseBookRegistration < ApplicationRecord
	include Filterable
	belongs_to :academico_course, class_name: "Academico::Course"
	has_many :academico_levels, class_name: "Academico::Level", :through => :academico_course
	has_many :academico_level_program, class_name: "Academico::LevelProgram", :through => :academico_course
	has_many :facturacion_books, class_name: "Facturacion::Book", :through => :academico_course
	has_many :academico_course_statuses, class_name: "Academico::CourseStatus", :through => :academico_course


	scope :academico_course_eq, -> (course) { where('academico_course_id = ?', course)}
	scope :academico_level_eq, -> (level) { joins(:academico_level_program).where('academico_level_programs.academico_level_id = ?', level)}
	scope :academico_program_eq, -> (program) { joins(:academico_level_program).where('academico_level_programs.academico_program_id = ?', program)}
	scope :facturacion_book_eq, -> (book) { joins(:facturacion_books).where('facturacion_books.id = ?', book)}
	scope :status_eq, -> (status) { joins(:academico_course_statuses).where('academico_course_statuses.id =  ?', status)}



	def self.reg_book_prices course
		self.academico_course_eq(course).first
	end

end