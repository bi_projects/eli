class Facturacion::ReceiptDetail < ApplicationRecord
	has_one :facturacion_receipt_detail_note, :class_name => "Facturacion::ReceiptDetailNote",
    	foreign_key: "facturacion_receipt_detail_id", :inverse_of => :facturacion_receipt_detail, :dependent => :destroy
	belongs_to :facturacion_receipt, :class_name => "Facturacion::Receipt", :inverse_of => :facturacion_receipt_details
	belongs_to :facturacion_invoice, :class_name => "Facturacion::Invoice", :inverse_of => :facturacion_receipt_details

	
	has_one :facturacion_note, :class_name => "Facturacion::Note",
    	through: :facturacion_receipt_detail_note, :inverse_of => :facturacion_receipt_details

	validates :facturacion_invoice_id, presence: true, if: :credit_invoice_payment?
	validates :payment_amount, presence: true, if: :credit_invoice_payment?

 
	accepts_nested_attributes_for :facturacion_receipt_detail_note,
		:allow_destroy => true,
		:reject_if     => :all_blank

	def credit_invoice_payment?
		facturacion_receipt.credit_invoice_payment
	end
end
