class Facturacion::InvoicesController < Facturacion::AuthorizeController 
  include FacturacionUtilityController
  skip_load_and_authorize_resource only: [:search_students, :student, :detail_values, :read_discount, :remain_values, :student_courses, :void_document]
  before_action :set_facturacion_invoice, only: [:show, :void_document]

  layout "facturacion/dashboard_layout"
  # GET /facturacion/invoices
  # GET /facturacion/invoices.json
  def index
    @facturacion_invoices = Facturacion::Invoice.all
  end

  # GET /facturacion/invoices/1
  # GET /facturacion/invoices/1.json
  def show
  end

  # GET /facturacion/invoices/new
  def new
    @facturacion_invoice = Facturacion::Invoice.new
  end


  def detail_values
    authorize! :index, Facturacion::Invoice
    @payment_detail_concept = Facturacion::PaymentDetailConcept.find_by(:id => params[:element_id])
    @student = Academico::Student.find_by(:id => params[:student_id])
    @available_discounts = @student.try(:valid_discount_types) || []
    @course = Academico::Course.find_by(id: params[:course_id])
    @book = Facturacion::Book.find_by(id: params[:book_id])
    is_book = @payment_detail_concept.is_book
    is_other_item = !@payment_detail_concept.is_book && !@payment_detail_concept.is_registration
    @books = Facturacion::Book.search_book_for_payment_detail_concept(
      is_book,
      is_other_item,
      @course.id
    )

    @price = @payment_detail_concept.price_for_classification(@book, @course.id)

    if @payment_detail_concept.present? && @payment_detail_concept.is_registration
      @payment_detail_concept.total_discount = @student.try(:my_default_discount) || 0
    end

    @domId = params[:domId]
    respond_to do |format|
      format.js { minify(render_to_string) }
    end
  end

  def read_discount
    authorize! :index, Facturacion::Invoice
    discount_type = params[:discount_type]
    student_id = params[:student_id]
    discount = Academico::Student.find(student_id).try(:my_discount, discount_type)
    respond_to do |format|
      format.json { render json: discount }
    end
  end

  def remain_values
    authorize! :index, Facturacion::Invoice
    @invoice = Facturacion::Invoice.find_by(:id => params[:element_id])
    @domId = params[:domId]
    respond_to do |format|
      format.js { minify(render_to_string) }
    end
  end



  # POST /facturacion/invoices
  # POST /facturacion/invoices.json
  def create
    @facturacion_invoice = Facturacion::Invoice.new(facturacion_invoice_params)
    #@facturacion_invoice.is_cash = facturacion_invoice_params[:is_cash]
    
    @facturacion_invoice.transaction do
      respond_to do |format|
        begin
          if @facturacion_invoice.save
            format.html { redirect_to @facturacion_invoice, notice: 'Invoice was successfully created.' }
            format.json { render :show, status: :ok, location: @facturacion_invoice }
          else
            format.html { render :new }
            format.json { render json: @facturacion_invoice.errors, status: :unprocessable_entity }
          end
        rescue CustomExceptions::DocumentException => e
          flash[:alert] = e.to_s
          flash.keep(:alert)
          format.html { render :new }
        end
      end
    end
  end


  def student
    authorize! :index, Facturacion::Invoice
    @academico_student = Academico::Student.find(params[:id])
    @levels = []
    @programs = []
    if @academico_student.present?
      @levels = @academico_student.student_levels
      @programs = @academico_student.facturacion_preregistration.academico_program
    end
  end

  def student_courses
    authorize! :index, Facturacion::Invoice
    time_frame_id = params[:time_frame_id]
    level_id = params[:level_id]
    program_id = params[:program_id]

    
      puts "------------------- #{program_id}"
    @courses = Academico::Course.filter_groups_of_courses(time_frame_id, level_id, program_id)
  end


  def void_document
    authorize! :destroy, Facturacion::Invoice
    begin
      destructor = Business::InvoiceDestructor.new(@facturacion_invoice)
      destructor.destroy
      respond_to do |format|
        format.html { redirect_to facturacion_invoices_url, notice: 'Invoice was successfully void.' }
        format.json { head :no_content }
      end
    rescue CustomExceptions::DocumentException => e
      flash[:alert] = e.to_s
      flash.keep(:alert)
      format.html { render :show, alert: e.to_s }
    end
  end

  def search_students
    authorize! :index, Facturacion::Invoice
    student_filter = params[:term]
    if student_filter.present?
      @students = Academico::Student.search_student(student_filter)
    else
      @students = Academico::Student.first(10)
    end
    respond_to do |format|
      format.json { render json: @students }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_invoice
      @facturacion_invoice = Facturacion::Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_invoice_params
      params.require(:facturacion_invoice).permit(:invoice_number, :invoice_date, 
        :academico_students_id, :curso, :academico_course_id, :invoice_concept, :exchange_rate, :printed, :admin_branch_office_id, :is_cash, :academico_course_modalities_id,
        :facturacion_invoice_statuses_id, :facturacion_invoice_payment_detail_attributes => [:retention_amount_cordoba, :cash_amount_cordoba, :cash_amount_usd,
          :credit_card_amount_cordoba, :credit_card_amount_usd, :ck_reference_cordoba, :ck_amount_cordoba, 
          :ck_reference_usd, :ck_amount_usd, :bank_reference_usd, :bank_amount_usd, :telepago_reference_usd, 
          :telepago_amount_usd, :change_amount_cordoba, :total_invoice_amount_cordoba, :_destroy, :id],
        :facturacion_invoice_details_attributes => [:facturacion_payment_detail_concept_id, :description, :_destroy, :id,
          :amount, :discount, :discount_type, :sub_total, :facturacion_invoice_detail_book_attributes => [ :facturacion_book_id, :_destroy, :id, :facturacion_invoice_detail_id]])
    end

    def resource_params
      facturacion_invoice_params
    end
end
