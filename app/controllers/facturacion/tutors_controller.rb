class Facturacion::TutorsController < Facturacion::AuthorizeController 
  before_action :set_facturacion_tutor, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/tutors
  # GET /facturacion/tutors.json
  def index
    @facturacion_tutors = Facturacion::Tutor.all
  end

  # GET /facturacion/tutors/1
  # GET /facturacion/tutors/1.json
  def show
  end

  # GET /facturacion/tutors/new
  def new
    @facturacion_tutor = Facturacion::Tutor.new
  end

  # GET /facturacion/tutors/1/edit
  def edit
  end

  # POST /facturacion/tutors
  # POST /facturacion/tutors.json
  def create
    @facturacion_tutor = Facturacion::Tutor.new(facturacion_tutor_params)

    respond_to do |format|
      if @facturacion_tutor.save
        format.html { redirect_to @facturacion_tutor, notice: 'Tutor was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_tutor }
      else
        format.html { render :new }
        format.json { render json: @facturacion_tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/tutors/1
  # PATCH/PUT /facturacion/tutors/1.json
  def update
    respond_to do |format|
      if @facturacion_tutor.update(facturacion_tutor_params)
        format.html { redirect_to @facturacion_tutor, notice: 'Tutor was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_tutor }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/tutors/1
  # DELETE /facturacion/tutors/1.json
  def destroy
    @facturacion_tutor.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_tutors_url, notice: 'Tutor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_tutor_ids].each do |id|
      @eli_resource = Facturacion::Tutor.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_tutors_path, notice: 'Registros eliminados exitosamente.'  }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_tutor
      @facturacion_tutor = Facturacion::Tutor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_tutor_params
      params.require(:facturacion_tutor).permit(:first_name, :last_name, :phone, :email)
    end

    def resource_params
      facturacion_tutor_params
    end
end
