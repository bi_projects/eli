class Facturacion::WarehousesController < Facturacion::AuthorizeController 
  before_action :set_facturacion_warehouse, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/warehouses
  # GET /facturacion/warehouses.json
  def index
    @facturacion_warehouses = Facturacion::Warehouse.all
  end

  # GET /facturacion/warehouses/1
  # GET /facturacion/warehouses/1.json
  def show
  end

  # GET /facturacion/warehouses/new
  def new
    @facturacion_warehouse = Facturacion::Warehouse.new
  end

  # GET /facturacion/warehouses/1/edit
  def edit
  end

  # POST /facturacion/warehouses
  # POST /facturacion/warehouses.json
  def create
    @facturacion_warehouse = Facturacion::Warehouse.new(facturacion_warehouse_params)

    respond_to do |format|
      if @facturacion_warehouse.save
        format.html { redirect_to @facturacion_warehouse, notice: 'Warehouse was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_warehouse }
      else
        format.html { render :new }
        format.json { render json: @facturacion_warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/warehouses/1
  # PATCH/PUT /facturacion/warehouses/1.json
  def update
    respond_to do |format|
      if @facturacion_warehouse.update(facturacion_warehouse_params)
        format.html { redirect_to @facturacion_warehouse, notice: 'Warehouse was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_warehouse }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/warehouses/1
  # DELETE /facturacion/warehouses/1.json
  def destroy
    @facturacion_warehouse.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_warehouses_url, notice: 'Warehouse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_warehouse_ids].each do |id|
      @eli_resource = Facturacion::Warehouse.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_warehouses_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_warehouse
      @facturacion_warehouse = Facturacion::Warehouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_warehouse_params
      params.require(:facturacion_warehouse).permit(:name, :description, :address, :facturacion_cost_center_id, :principal, :admin_branch_office_id)
    end

    def resource_params
      facturacion_warehouse_params
    end
end
