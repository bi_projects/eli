class Facturacion::PaymentDetailConceptsController < Facturacion::AuthorizeController 
  before_action :set_facturacion_payment_detail_concept, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/payment_detail_concepts
  # GET /facturacion/payment_detail_concepts.json
  def index
    @facturacion_payment_detail_concepts = Facturacion::PaymentDetailConcept.all
  end

  # GET /facturacion/payment_detail_concepts/1
  # GET /facturacion/payment_detail_concepts/1.json
  def show
  end

  # GET /facturacion/payment_detail_concepts/new
  def new
    @facturacion_payment_detail_concept = Facturacion::PaymentDetailConcept.new
  end

  # GET /facturacion/payment_detail_concepts/1/edit
  def edit
  end

  # POST /facturacion/payment_detail_concepts
  # POST /facturacion/payment_detail_concepts.json
  def create
    @facturacion_payment_detail_concept = Facturacion::PaymentDetailConcept.new(facturacion_payment_detail_concept_params)

    respond_to do |format|
      if @facturacion_payment_detail_concept.save
        format.html { redirect_to @facturacion_payment_detail_concept, notice: 'Payment detail concept was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_payment_detail_concept }
      else
        format.html { render :new }
        format.json { render json: @facturacion_payment_detail_concept.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/payment_detail_concepts/1
  # PATCH/PUT /facturacion/payment_detail_concepts/1.json
  def update
    respond_to do |format|
      if @facturacion_payment_detail_concept.update(facturacion_payment_detail_concept_params)
        format.html { redirect_to @facturacion_payment_detail_concept, notice: 'Payment detail concept was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_payment_detail_concept }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_payment_detail_concept.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/payment_detail_concepts/1
  # DELETE /facturacion/payment_detail_concepts/1.json
  def destroy
    @facturacion_payment_detail_concept.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_payment_detail_concepts_url, notice: 'Payment detail concept was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_payment_detail_concept_ids].each do |id|
      @eli_resource = Facturacion::PaymentDetailConcept.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_payment_detail_concepts_path, notice: 'Registros eliminados exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_payment_detail_concept
      @facturacion_payment_detail_concept = Facturacion::PaymentDetailConcept.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_payment_detail_concept_params
      params.require(:facturacion_payment_detail_concept).permit(:name, :description, :price, :is_book, :is_registration,
        facturacion_payment_detail_concept_detail_prices_attributes: [:id, :_destroy, :academico_program_id, :academico_level_id, :amount])
    end

    def resource_params
      facturacion_payment_detail_concept_params
    end
end
