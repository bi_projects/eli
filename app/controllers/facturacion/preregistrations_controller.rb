class Facturacion::PreregistrationsController < Facturacion::AuthorizeController 
	before_action :set_facturacion_preregistration, only: [:show, :edit, :update, :destroy]
	layout "facturacion/dashboard_layout"
	include Facturacion::FormHelper
	# GET /facturacion/preregistrations
	# GET /facturacion/preregistrations.json
	def index
		@facturacion_preregistrations = Facturacion::Preregistration.filter(filtering_params(params)).order(:first_name)
	end

	def filtering_params(params)
		if params[:filter].present?
			operator = params[:filter][:operator]
			if operator.present? and params[:filter][:age].present?
				params[:filter][:operator_filter] = [params[:filter][:operator],params[:filter][:age]]
			end
			params[:filter].slice(:has_tutor,
				:has_company,
				:has_carrier,
				:has_discount,
				:first_name_like,
				:last_name_like,
				:tutor,
				:company,
				:carrier,
				:discount_type,
				:level,
				:operator_filter)
		else
			Array.new
		end

	end

	# GET /facturacion/preregistrations/1
	# GET /facturacion/preregistrations/1.json
	def show
	end

	# GET /facturacion/preregistrations/new
	def new
		@facturacion_preregistration = Facturacion::Preregistration.new
	end

	# GET /facturacion/preregistrations/1/edit
	def edit
	end

	# POST /facturacion/preregistrations
	# POST /facturacion/preregistrations.json
	def create
		@facturacion_preregistration = Facturacion::Preregistration.new(facturacion_preregistration_params.except :facturacion_student_discounts_attributes)

		if facturacion_preregistration_params[:facturacion_student_discounts_attributes]
			@facturacion_preregistration.facturacion_student_discounts.delete_all
			facturacion_preregistration_params[:facturacion_student_discounts_attributes][:facturacion_discount_ids].each do |d|
				@facturacion_preregistration.facturacion_discounts << Facturacion::Discount.find(d)
			end
		end

		respond_to do |format|
			if @facturacion_preregistration.save
				format.html { redirect_to @facturacion_preregistration, notice: 'Preregistro creado exitosamente.' }
				format.json { render :show, status: :created, location: @facturacion_preregistration }
			else
				format.html { render :new }
				format.json { render json: @facturacion_preregistration.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /facturacion/preregistrations/1
	# PATCH/PUT /facturacion/preregistrations/1.json
	def update
		if facturacion_preregistration_params[:facturacion_student_discounts_attributes]
			@facturacion_preregistration.facturacion_student_discounts.delete_all
			facturacion_preregistration_params[:facturacion_student_discounts_attributes][:facturacion_discount_ids].each do |d|
				@facturacion_preregistration.facturacion_discounts << Facturacion::Discount.find(d)
			end
		else
			@facturacion_preregistration.facturacion_student_discounts.delete_all
		end

		respond_to do |format|
			if @facturacion_preregistration.update(facturacion_preregistration_params.except :facturacion_student_discounts_attributes)
				format.html { redirect_to @facturacion_preregistration, notice: 'Preregistro actualizado exitosamente.' }
				format.json { render :show, status: :ok, location: @facturacion_preregistration }
			else
				format.html { render :edit }
				format.json { render json: @facturacion_preregistration.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /facturacion/preregistrations/1
	# DELETE /facturacion/preregistrations/1.json
	def destroy
		@facturacion_preregistration.destroy
		respond_to do |format|
			format.html { redirect_to facturacion_preregistrations_url, notice: 'Preregistro eliminado exitosamente.' }
			format.json { head :no_content }
		end
	end

	def destroy_multiple
		#if Documento.where("facturacion_preregistration_id in (?)",params[:facturacion_preregistration_ids]).blank?
			params[:facturacion_preregistration_ids].each do |id|
				@eli_resource = Facturacion::Preregistration.find(id)
				@eli_resource.destroy
			end
			respond_to do |format|
				format.html { redirect_to facturacion_preregistrations_path, notice: 'Registros eliminados exitosamente.' }
				format.json { head :no_content }
			end
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_facturacion_preregistration
		@facturacion_preregistration = Facturacion::Preregistration.find(params[:id])
		@selected_discount = @facturacion_preregistration.facturacion_student_discounts.map{|b| b.facturacion_discount_id.to_s}
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def facturacion_preregistration_params
		params.require(:facturacion_preregistration).permit(:id, :first_name, :last_name, :birth_date, :academico_level_id,
			:email, :phone, :address, :note, :academico_program_id,
			:facturacion_student_company_attributes => [:facturacion_company_id, :id],
			:facturacion_student_carrier_attributes => [:facturacion_carrier_id, :facturacion_preregistration_id, :id],
			:facturacion_student_tutor_attributes => [:facturacion_tutor_id, :id],
			:facturacion_student_level_program_attributes => [:academico_level_id, :academico_program_id, :id],
			:facturacion_discount_ids => [])
	end

  def resource_params
    facturacion_preregistration_params
  end
end
