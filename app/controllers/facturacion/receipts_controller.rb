class Facturacion::ReceiptsController < Facturacion::AuthorizeController
  include FacturacionUtilityController

  before_action :set_facturacion_receipt, only: [:show, :void_document]
  skip_load_and_authorize_resource only: [:student_credit_invoices, :void_document]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/receipts
  # GET /facturacion/receipts.json
  def index
    @facturacion_receipts = Facturacion::Receipt.all
  end

  # GET /facturacion/receipts/1
  # GET /facturacion/receipts/1.json
  def show
  end

  # GET /facturacion/receipts/new
  def new
    credit_invoice_payment = ActiveModel::Type::Boolean.new.cast(params[:credit_invoice])
    @facturacion_receipt = Facturacion::Receipt.new
    if credit_invoice_payment
      @facturacion_receipt.credit_invoice_payment = true
    end
  end


  def student_credit_invoices
    puts "params >> #{@facturacion_receipt}"
    @element = params[:element_id].to_i
    @domId = params[:domId]
    respond_to do |format|
      format.js { minify(render_to_string) }
    end
  end


  # POST /facturacion/receipts
  # POST /facturacion/receipts.json
  def create
    @facturacion_receipt = Facturacion::Receipt.new(facturacion_receipt_params)
    respond_to do |format|
      begin
        if @facturacion_receipt.save
          format.html { redirect_to @facturacion_receipt, notice: 'Receipt was successfully created.' }
          format.json { render :show, status: :ok, location: @facturacion_receipt }
        else
          format.html { render :new }
          format.json { render json: @facturacion_receipt.errors, status: :unprocessable_entity }
        end
      rescue CustomExceptions::DocumentException => e
        flash[:alert] = e.to_s
        flash.keep(:alert)
        format.html { render :new }
      end
    end
  end

  def void_document
    authorize! :destroy, Facturacion::Receipt
    begin
      destructor = Business::ReceiptDestructor.new(@facturacion_receipt)
      destructor.destroy
      respond_to do |format|
        format.html { redirect_to facturacion_receipts_url, notice: 'Receipt was successfully void.' }
        format.json { head :no_content }
      end
    rescue CustomExceptions::DocumentException => e
      flash[:alert] = e.to_s
      flash.keep(:alert)
      format.html { render :show, alert: e.to_s }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_receipt
      @facturacion_receipt = Facturacion::Receipt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_receipt_params
      params.require(:facturacion_receipt).permit(:receipt_number, :receipt_date, :canceled, :printed, 
        :comments, :admin_branch_office_id, :academico_student_id, :canceled_date, :credit_invoice_payment,
        :facturacion_receipt_details_attributes => [:facturacion_invoice_id, :is_credit_note, :credit_note_id, :payment_amount, 
          :payment_pre_balance, :payment_post_balance, :id, :_destroy, :facturacion_receipt_detail_note_attributes => [ :facturacion_note_id, :_destroy, :id]],
        :facturacion_receipt_payment_detail_attributes => [:retention_amount_cordoba, :cash_amount_cordoba, :cash_amount_usd, 
          :credit_card_amount_cordoba, :credit_card_amount_usd, :ck_reference_cordoba, :ck_amount_cordoba, 
          :ck_reference_usd, :ck_amount_usd, :bank_reference_usd, :bank_amount_usd, :telepago_reference_usd, 
          :telepago_amount_usd, :change_amount_cordoba, :total_invoice_amount_cordoba, :other_payment, :_destroy, :id],
        :facturacion_invoices_attributes => [:_destroy, :id, :invoice_number, :invoice_date, 
          :academico_students_id, :academico_course_id, :invoice_concept, :exchange_rate, :printed, :admin_branch_office_id, :is_cash,
          :facturacion_invoice_payment_detail_attributes => [:retention_amount_cordoba, :cash_amount_cordoba, :cash_amount_usd, 
            :credit_card_amount_cordoba, :credit_card_amount_usd, :ck_reference_cordoba, :ck_amount_cordoba, 
            :ck_reference_usd, :ck_amount_usd, :bank_reference_usd, :bank_amount_usd, :telepago_reference_usd, 
            :telepago_amount_usd, :change_amount_cordoba, :total_invoice_amount_cordoba, :_destroy, :id],
          :facturacion_invoice_details_attributes => [:facturacion_payment_detail_concept_id, :description, :_destroy, :id,
            :amount, :discount, :sub_total, :facturacion_invoice_detail_book_attributes => [ :facturacion_book_id, :_destroy, :id]]])
    end

    def resource_params
      facturacion_receipt_params
    end
end
