class Facturacion::DailyProcessesController < Facturacion::AuthorizeController
  layout "facturacion/dashboard_layout"

  def index
    @daily_processes = Facturacion::DailyProcess.all
  end

  %i[new show update edit].each do |method|
    define_method(method) do
      redirect_to facturacion_daily_processes_url
    end
  end

  def create
    @daily_process = Facturacion::DailyProcess.new

    if @daily_process.save
      redirect_to facturacion_daily_processes_url, notice: 'Registro almacenado exitosamente.'
    else
      redirect_to facturacion_daily_processes_url, notice: @daily_process.errors.full_messages_for(:base).join("\n")
    end
  end

  def destroy
    @daily_processes = Facturacion::DailyProcess.find(params[:id])
    @daily_processes.destroy
    redirect_to facturacion_daily_processes_url, notice: 'Registro eliminado exitosamente.'
  end

  def destroy_multiple
    params[:daily_process_ids].each do |id|
      @eli_resource = Facturacion::DailyProcess.find(id)
      @eli_resource.destroy
    end
    redirect_to facturacion_daily_processes_url, notice: 'Registros eliminados exitosamente.'
  end
end