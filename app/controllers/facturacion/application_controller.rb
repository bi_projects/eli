class Facturacion::ApplicationController < ApplicationController
	protect_from_forgery with: :exception
	before_action :set_page_title, :authenticate_facturacion_billing_user!, :set_current_user
	helper_method :end_session_path, :current_user, :session_root_path, :edit_eli_user_path
	include Facturacion::ApplicationHelper

  # access to parameters api in billing module
  def parameters
    @parameter = Admin::Parameter.first
    respond_to do |format|
      format.json { render json: @parameter }
    end
  end

	def controller?(*controller)
		controller.include?(params[:controller])
	end

	def action?(*action)
		action.include?(params[:action])
	end

	private

	def set_page_title
		@page_title = "Billing Management"
	end

  def set_current_user
    Facturacion::BillingUser.current = current_facturacion_billing_user
  end

	def current_user
		current_facturacion_billing_user
	end

	def end_session_path
		destroy_facturacion_billing_user_session_path
	end

	def session_root_path
		facturacion_root_path
	end

	def edit_eli_user_path
		edit_facturacion_billing_user_registration_path
	end
end
