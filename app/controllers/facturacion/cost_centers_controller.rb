class Facturacion::CostCentersController < Facturacion::AuthorizeController 
  before_action :set_facturacion_cost_center, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"

  # GET /facturacion/cost_centers
  # GET /facturacion/cost_centers.json
  def index
    @facturacion_cost_centers = Facturacion::CostCenter.all
  end

  # GET /facturacion/cost_centers/1
  # GET /facturacion/cost_centers/1.json
  def show
  end

  # GET /facturacion/cost_centers/new
  def new
    @facturacion_cost_center = Facturacion::CostCenter.new
  end

  # GET /facturacion/cost_centers/1/edit
  def edit
  end

  # POST /facturacion/cost_centers
  # POST /facturacion/cost_centers.json
  def create
    @facturacion_cost_center = Facturacion::CostCenter.new(facturacion_cost_center_params)

    respond_to do |format|
      if @facturacion_cost_center.save
        format.html { redirect_to @facturacion_cost_center, notice: 'Cost center was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_cost_center }
      else
        format.html { render :new }
        format.json { render json: @facturacion_cost_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/cost_centers/1
  # PATCH/PUT /facturacion/cost_centers/1.json
  def update
    respond_to do |format|
      if @facturacion_cost_center.update(facturacion_cost_center_params)
        format.html { redirect_to @facturacion_cost_center, notice: 'Cost center was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_cost_center }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_cost_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/cost_centers/1
  # DELETE /facturacion/cost_centers/1.json
  def destroy
    @facturacion_cost_center.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_cost_centers_url, notice: 'Cost center was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_cost_center_ids].each do |id|
      @eli_resource = Facturacion::CostCenter.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_cost_centers_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_cost_center
      @facturacion_cost_center = Facturacion::CostCenter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_cost_center_params
      params.require(:facturacion_cost_center).permit(:code, :name, :description)
    end

    def resource_params
      facturacion_cost_center_params
    end
end
