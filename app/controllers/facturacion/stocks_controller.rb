class Facturacion::StocksController < Facturacion::AuthorizeController 
  before_action :set_facturacion_stock, only: [:show, :edit, :update, :destroy]
  skip_load_and_authorize_resource only: [:multi_transfer, :create_transfer]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/stocks
  # GET /facturacion/stocks.json
  def index
    @facturacion_stocks = Facturacion::Stock.all.order("ID DESC")
  end

  # GET /facturacion/stocks/1
  # GET /facturacion/stocks/1.json
  def show
  end

  # GET /facturacion/stocks/new
  def new
    @facturacion_stock = Facturacion::Stock.new
  end

  # GET /facturacion/stocks/1/edit
  def edit
  end

  def multi_transfer
    authorize! :new, Facturacion::Stock
    @facturacion_transfer_request = Facturacion::TransferRequest.new
  end

  # POST /facturacion/stocks
  # POST /facturacion/stocks.json
  def create
    if facturacion_stock_params.present?
      @facturacion_stock = Facturacion::Stock.new(facturacion_stock_params)

      respond_to do |format|
        if @facturacion_stock.save
          format.html { redirect_to @facturacion_stock, notice: 'Stock was successfully created.' }
          format.json { render :show, status: :created, location: @facturacion_stock }
        else
          format.html { render :new }
          format.json { render json: @facturacion_stock.errors, status: :unprocessable_entity }
        end
      end
    else
      @facturacion_transfer_request = Facturacion::TransferRequest.new(facturacion_transfer_request_params)

      respond_to do |format|
        if @facturacion_transfer_request.save
          format.html { redirect_to facturacion_stocks_path, notice: 'Stocks was successfully created.' }
          format.json { render :show, status: :created, location: @facturacion_transfer_request }
        else
          format.html { render :new }
          format.json { render json: @facturacion_transfer_request.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def create_transfer
    @facturacion_transfer_request = Facturacion::TransferRequest.new(facturacion_transfer_request_params)

    respond_to do |format|
      if @facturacion_transfer_request.save
        format.html { redirect_to facturacion_stocks_path, notice: 'Stocks was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_transfer_request }
      else
        format.html { render :multi_transfer }
        format.json { render json: @facturacion_transfer_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/stocks/1
  # PATCH/PUT /facturacion/stocks/1.json
  def update
    respond_to do |format|
      if @facturacion_stock.update(facturacion_stock_params)
        format.html { redirect_to @facturacion_stock, notice: 'Stock was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_stock }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_stock.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/stocks/1
  # DELETE /facturacion/stocks/1.json
  def destroy
    @facturacion_stock.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_stocks_url, notice: 'Stock was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_stock
      @facturacion_stock = Facturacion::Stock.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_stock_params
      params.require(:facturacion_stock).permit(:stock_date, 
        :facturacion_document_type_id, :facturacion_book_id, :document_id, 
        :facturacion_warehouse_id, :facturacion_warehouse_origin_id, :quantity, 
        :unit_cost, :canceled, :scenario, :description)
    end

    def facturacion_transfer_request_params
      params.require(:facturacion_transfer_request).permit(
        :facturacion_document_type_id, :transfer_date,
        :facturacion_warehouse_id, :facturacion_warehouse_origin_id,
        :facturacion_stocks_attributes => [:stock_date, :facturacion_document_type_id, 
          :facturacion_warehouse_id, :document_id, :facturacion_book_id,
          :quantity, :unit_cost, :canceled, :_destroy, :id])
    end

    def resource_params
      if params.keys.include?('facturacion_stock')
        return facturacion_stock_params
      else
        return facturacion_transfer_request_params
      end
    end
end
