class Facturacion::BooksController < Facturacion::AuthorizeController 
  before_action :set_facturacion_book, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/books
  # GET /facturacion/books.json
  def index
    @facturacion_books = Facturacion::Book.all
  end

  # GET /facturacion/books/1
  # GET /facturacion/books/1.json
  def show
  end

  # GET /facturacion/books/new
  def new
    @facturacion_book = Facturacion::Book.new
  end

  # GET /facturacion/books/1/edit
  def edit
  end

  # POST /facturacion/books
  # POST /facturacion/books.json
  def create
    @facturacion_book = Facturacion::Book.new(facturacion_book_params)

    respond_to do |format|
      if @facturacion_book.save
        format.html { redirect_to @facturacion_book, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_book }
      else
        format.html { render :new }
        format.json { render json: @facturacion_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/books/1
  # PATCH/PUT /facturacion/books/1.json
  def update
    respond_to do |format|
      if @facturacion_book.update(facturacion_book_params)
        format.html { redirect_to @facturacion_book, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_book }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/books/1
  # DELETE /facturacion/books/1.json
  def destroy
    @facturacion_book.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_books_url, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_book_ids].each do |id|
      @eli_resource = Facturacion::Book.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_books_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_book
      @facturacion_book = Facturacion::Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_book_params
      params.require(:facturacion_book)
            .permit(:name, :description, :quantity, :min_stock, :price, :academico_level_id, :academico_program_id)
    end

    def resource_params
      facturacion_book_params
    end
end
