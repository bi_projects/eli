class Facturacion::DiscountsController < Facturacion::AuthorizeController 
  before_action :set_facturacion_discount, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"

  # GET /facturacion/discounts
  # GET /facturacion/discounts.json
  def index
    @facturacion_discounts = Facturacion::Discount.all
  end

  # GET /facturacion/discounts/1
  # GET /facturacion/discounts/1.json
  def show
  end

  # GET /facturacion/discounts/new
  def new
    @facturacion_discount = Facturacion::Discount.new
  end

  # GET /facturacion/discounts/1/edit
  def edit
  end

  # POST /facturacion/discounts
  # POST /facturacion/discounts.json
  def create
    @facturacion_discount = Facturacion::Discount.new(facturacion_discount_params)

    respond_to do |format|
      if @facturacion_discount.save
        format.html { redirect_to @facturacion_discount, notice: 'Discount was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_discount }
      else
        format.html { render :new }
        format.json { render json: @facturacion_discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/discounts/1
  # PATCH/PUT /facturacion/discounts/1.json
  def update
    respond_to do |format|
      if @facturacion_discount.update(facturacion_discount_params)
        format.html { redirect_to @facturacion_discount, notice: 'Discount was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_discount }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/discounts/1
  # DELETE /facturacion/discounts/1.json
  def destroy
    @facturacion_discount.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_discounts_url, notice: 'Discount was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_discount_ids].each do |id|
      @eli_resource = Facturacion::Discount.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_carriers_path, notice: 'Registros eliminados exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_discount
      @facturacion_discount = Facturacion::Discount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_discount_params
      params.require(:facturacion_discount).permit(:name, :description, :discount_amount)
    end

    def resource_params
      facturacion_discount_params
    end
end
