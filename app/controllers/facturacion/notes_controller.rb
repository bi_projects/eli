class Facturacion::NotesController < Facturacion::AuthorizeController
  include FacturacionUtilityController

  before_action :set_facturacion_note, only: [:show, :edit, :update, :destroy]
  skip_load_and_authorize_resource only: [:student_invoices, :invoice_concepts]
  layout "facturacion/dashboard_layout"

  # GET /facturacion/notes
  # GET /facturacion/notes.json
  def index
    @facturacion_notes = Facturacion::Note.where(:note_type => NOTA_CREDITO)
  end

  # GET /facturacion/notes/1
  # GET /facturacion/notes/1.json
  def show
  end

  # GET /facturacion/notes/new
  def new
    @facturacion_note = Facturacion::Note.new(:note_type => NOTA_CREDITO)
  end

  # GET /facturacion/notes/1/edit
  def edit
  end

  # POST /facturacion/notes
  # POST /facturacion/notes.json
  def create
    @facturacion_note = Facturacion::Note.new(facturacion_note_params)
    @facturacion_note.note_type = NOTA_CREDITO
    respond_to do |format|
      if @facturacion_note.save
        format.html { redirect_to @facturacion_note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_note }
      else
        format.html { render :new }
        format.json { render json: @facturacion_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/notes/1
  # PATCH/PUT /facturacion/notes/1.json
  def update
    respond_to do |format|
      if @facturacion_note.update(facturacion_note_params)
        format.html { redirect_to @facturacion_note, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_note }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/notes/1
  # DELETE /facturacion/notes/1.json
  def destroy
    @facturacion_note.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def student_invoices
    authorize! :index, Facturacion::Note
    @invoices = Facturacion::Invoice.student_book_invoices(params[:student_id], nil)
  end

  def invoice_concepts
    authorize! :index, Facturacion::Note
    @details = Facturacion::Invoice.invoice_concepts(params[:invoice_id], nil)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_note
      @facturacion_note = Facturacion::Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_note_params
      params.require(:facturacion_note).permit(:note_number, :note_date, :admin_branch_office_id,
        :academico_student_id, :description, :inatec, :total_note,
        :facturacion_note_details_attributes => [:amount, :description, :facturacion_invoice_id, :facturacion_invoice_details_id, :_destroy, :id])
    end

    def resource_params
      facturacion_note_params
    end

    def model_name
      'Note'
    end
end
