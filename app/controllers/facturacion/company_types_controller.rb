class Facturacion::CompanyTypesController < Facturacion::AuthorizeController 
	before_action :set_facturacion_company_type, only: [:show, :edit, :update, :destroy]
	layout "facturacion/dashboard_layout"

	# GET /facturacion/company_types
	# GET /facturacion/company_types.json
	def index
		@facturacion_company_types = Facturacion::CompanyType.all
	end

	# GET /facturacion/company_types/1
	# GET /facturacion/company_types/1.json
	def show
	end

	# GET /facturacion/company_types/new
	def new
		@facturacion_company_type = Facturacion::CompanyType.new
	end

	# GET /facturacion/company_types/1/edit
	def edit
	end

	# POST /facturacion/company_types
	# POST /facturacion/company_types.json
	def create
		@facturacion_company_type = Facturacion::CompanyType.new(facturacion_company_type_params)

		respond_to do |format|
			if @facturacion_company_type.save
				format.html { redirect_to @facturacion_company_type, notice: 'Company type was successfully created.' }
				format.json { render :show, status: :created, location: @facturacion_company_type }
			else
				format.html { render :new }
				format.json { render json: @facturacion_company_type.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /facturacion/company_types/1
	# PATCH/PUT /facturacion/company_types/1.json
	def update
		respond_to do |format|
			if @facturacion_company_type.update(facturacion_company_type_params)
				format.html { redirect_to @facturacion_company_type, notice: 'Company type was successfully updated.' }
				format.json { render :show, status: :ok, location: @facturacion_company_type }
			else
				format.html { render :edit }
				format.json { render json: @facturacion_company_type.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /facturacion/company_types/1
	# DELETE /facturacion/company_types/1.json
	def destroy
		@facturacion_company_type.destroy
		respond_to do |format|
			format.html { redirect_to facturacion_company_types_url, notice: 'Company type was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	def destroy_multiple
		params[:facturacion_company_type_ids].each do |id|
			@eli_resource = Facturacion::CompanyType.find(id)
			@eli_resource.destroy
		end
		respond_to do |format|
			format.html { redirect_to facturacion_company_types_path, notice: 'Registros eliminados exitosamente.'  }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_facturacion_company_type
			@facturacion_company_type = Facturacion::CompanyType.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def facturacion_company_type_params
			params.require(:facturacion_company_type).permit(:name, :description)
		end

    def resource_params
      facturacion_company_type_params
    end
	end
