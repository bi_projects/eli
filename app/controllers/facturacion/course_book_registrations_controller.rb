class Facturacion::CourseBookRegistrationsController < Facturacion::AuthorizeController 
  before_action :set_facturacion_course_book_registration, only: [:show, :edit, :update, :destroy]

  # GET /facturacion/course_book_registrations
  # GET /facturacion/course_book_registrations.json
  def index
    @facturacion_course_book_registrations = filtering_params(params).paginate(:page => params[:page])
  end

  def filtering_params(params)
    if params[:filter].present?
      Facturacion::CourseBookRegistration.filter(params[:filter].slice(:academico_course_eq, :academico_level_eq, :academico_program_eq, :facturacion_book_eq, :status_eq))
    else
      Facturacion::CourseBookRegistration.all
    end
  end

  # GET /facturacion/course_book_registrations/1
  # GET /facturacion/course_book_registrations/1.json
  def show
  end

  # GET /facturacion/course_book_registrations/1/edit
  def edit
  end

  # PATCH/PUT /facturacion/course_book_registrations/1
  # PATCH/PUT /facturacion/course_book_registrations/1.json
  def update
    respond_to do |format|
      if @facturacion_course_book_registration.update(facturacion_course_book_registration_params)
        format.html { redirect_to @facturacion_course_book_registration, notice: 'Course book registration was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_course_book_registration }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_course_book_registration.errors, status: :unprocessable_entity }
      end
    end
  end

  def bulk_prices
    params[:facturacion_course_book_registration_ids].each do |id|
      @record = Facturacion::CourseBookRegistration.find(id)
      @record.book_price = params[:book_price][:new_book_price]
      @record.registration_price = params[:registration_price][:new_registration_price]
      @record.save!
    end
    respond_to do |format|
      format.html { redirect_to facturacion_course_book_registrations_path, notice: 'Registros Actualizados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_course_book_registration
      @facturacion_course_book_registration = Facturacion::CourseBookRegistration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_course_book_registration_params
      params.require(:facturacion_course_book_registration).permit(:academico_course_id, :book_price, :registration_price)
    end
end
