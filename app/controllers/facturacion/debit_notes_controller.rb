class Facturacion::DebitNotesController < Facturacion::AuthorizeController 
  skip_load_and_authorize_resource
  before_action :custom_autorize_resource
  before_action :set_facturacion_debit_note, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"

  # GET /facturacion/debit_notes
  # GET /facturacion/debit_notes.json
  def index
    @facturacion_debit_notes = Facturacion::Note.where(:note_type => NOTA_DEBITO)
  end

  # GET /facturacion/debit_notes/1
  # GET /facturacion/debit_notes/1.json
  def show
    @facturacion_note = @facturacion_debit_note
  end

  # GET /facturacion/debit_notes/new
  def new
    @facturacion_debit_note = Facturacion::Note.new(:note_type => NOTA_DEBITO)
  end

  # GET /facturacion/debit_notes/1/edit
  def edit
  end

  # POST /facturacion/debit_notes
  # POST /facturacion/debit_notes.json
  def create
    @facturacion_debit_note = Facturacion::Note.new(facturacion_debit_note_params)
    @facturacion_debit_note.note_type = NOTA_DEBITO
    respond_to do |format|
      if @facturacion_debit_note.save
        format.html { redirect_to @facturacion_debit_note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_debit_note }
      else
        format.html { render :new }
        format.json { render json: @facturacion_debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/debit_notes/1
  # PATCH/PUT /facturacion/debit_notes/1.json
  def update
    respond_to do |format|
      if @facturacion_debit_note.update(facturacion_debit_note_params)
        format.html { redirect_to @facturacion_debit_note, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_debit_note }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/debit_notes/1
  # DELETE /facturacion/debit_notes/1.json
  def destroy
    @facturacion_debit_note.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_debit_note
      @facturacion_debit_note = Facturacion::Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_debit_note_params
      params.require(:facturacion_note).permit(:note_number, :note_date, :admin_branch_office_id,
        :facturacion_preregistration_id, :description, :inatec, :total_note,
        :facturacion_note_details_attributes => [:amount, :description, :_destroy, :id])
    end

    def resource_params
      facturacion_debit_note_params
    end

    def custom_autorize_resource
      authorize! params[:action].to_sym, Facturacion::Note
    end
end