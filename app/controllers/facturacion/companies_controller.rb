class Facturacion::CompaniesController < Facturacion::AuthorizeController 
	before_action :set_facturacion_company, only: [:show, :edit, :update, :destroy]
	layout "facturacion/dashboard_layout"

	# GET /facturacion/companies
	# GET /facturacion/companies.json
	def index
		@facturacion_companies = Facturacion::Company.all
	end

	# GET /facturacion/companies/1
	# GET /facturacion/companies/1.json
	def show
	end

	# GET /facturacion/companies/new
	def new
		@facturacion_company = Facturacion::Company.new
	end

	# GET /facturacion/companies/1/edit
	def edit
	end

	# POST /facturacion/companies
	# POST /facturacion/companies.json
	def create
		@facturacion_company = Facturacion::Company.new(facturacion_company_params)

		respond_to do |format|
			if @facturacion_company.save
				format.html { redirect_to @facturacion_company, notice: 'Company was successfully created.' }
				format.json { render :show, status: :created, location: @facturacion_company }
			else
				format.html { render :new }
				format.json { render json: @facturacion_company.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /facturacion/companies/1
	# PATCH/PUT /facturacion/companies/1.json
	def update
		respond_to do |format|
			if @facturacion_company.update(facturacion_company_params)
				format.html { redirect_to @facturacion_company, notice: 'Company was successfully updated.' }
				format.json { render :show, status: :ok, location: @facturacion_company }
			else
				format.html { render :edit }
				format.json { render json: @facturacion_company.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /facturacion/companies/1
	# DELETE /facturacion/companies/1.json
	def destroy
		@facturacion_company.destroy
		respond_to do |format|
			format.html { redirect_to facturacion_companies_url, notice: 'Company was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	def destroy_multiple
		params[:facturacion_company_ids].each do |id|
			@eli_resource = Facturacion::Company.find(id)
			@eli_resource.destroy
		end
		respond_to do |format|
			format.html { redirect_to facturacion_companies_path, notice: 'Registros eliminados exitosamente.'  }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_facturacion_company
			@facturacion_company = Facturacion::Company.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def facturacion_company_params
			params.require(:facturacion_company).permit(:name, :contact_name, :phone, :email, :address, :company_description, :facturacion_company_type_id)
		end

    def resource_params
      facturacion_company_params
    end
end
