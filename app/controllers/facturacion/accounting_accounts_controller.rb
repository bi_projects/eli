class Facturacion::AccountingAccountsController < Facturacion::AuthorizeController 
  before_action :set_facturacion_accounting_account, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"

  # GET /facturacion/accounting_accounts
  # GET /facturacion/accounting_accounts.json
  def index
    @facturacion_accounting_accounts = filtering_params(params).paginate(:page => params[:page])
  end

  def filtering_params(params)
    if params[:filter].present?
      Facturacion::AccountingAccount.filter(params[:filter].slice(:account_category_eq, :parent_account_eq, :account_number_like, :name_like, :status_eq))
    else
      Facturacion::AccountingAccount.all
    end
  end

  # GET /facturacion/accounting_accounts/1
  # GET /facturacion/accounting_accounts/1.json
  def show
  end

  # GET /facturacion/accounting_accounts/new
  def new
    @facturacion_accounting_account = Facturacion::AccountingAccount.new
  end

  # GET /facturacion/accounting_accounts/1/edit
  def edit
  end

  # POST /facturacion/accounting_accounts
  # POST /facturacion/accounting_accounts.json
  def create
    @facturacion_accounting_account = Facturacion::AccountingAccount.new(facturacion_accounting_account_params)

    respond_to do |format|
      if @facturacion_accounting_account.save
        format.html { redirect_to @facturacion_accounting_account, notice: 'Accounting account was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_accounting_account }
      else
        format.html { render :new }
        format.json { render json: @facturacion_accounting_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/accounting_accounts/1
  # PATCH/PUT /facturacion/accounting_accounts/1.json
  def update
    respond_to do |format|
      if @facturacion_accounting_account.update(facturacion_accounting_account_params)
        format.html { redirect_to @facturacion_accounting_account, notice: 'Accounting account was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_accounting_account }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_accounting_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/accounting_accounts/1
  # DELETE /facturacion/accounting_accounts/1.json
  def destroy
    @facturacion_accounting_account.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_accounting_accounts_url, notice: 'Accounting account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def suggest_next_account_number
    render :json => { account_number: Facturacion::AccountingAccount.suggest_child_number(params[:parent_account]) }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_accounting_account
      @facturacion_accounting_account = Facturacion::AccountingAccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_accounting_account_params
      params.require(:facturacion_accounting_account).permit(:name, :account_number, :parent_account, :is_disabled, :facturacion_account_to_records_id, :facturacion_account_types_id, :facturacion_accounting_account_categories_id)
    end

    def resource_params
      facturacion_accounting_account_params
    end
end
