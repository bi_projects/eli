class Facturacion::CarriersController < Facturacion::AuthorizeController 
  before_action :set_facturacion_carrier, only: [:show, :edit, :update, :destroy]
  layout "facturacion/dashboard_layout"
  # GET /facturacion/carriers
  # GET /facturacion/carriers.json
  def index
    @facturacion_carriers = Facturacion::Carrier.all
  end

  # GET /facturacion/carriers/1
  # GET /facturacion/carriers/1.json
  def show
  end

  # GET /facturacion/carriers/new
  def new
    @facturacion_carrier = Facturacion::Carrier.new
  end

  # GET /facturacion/carriers/1/edit
  def edit
  end

  # POST /facturacion/carriers
  # POST /facturacion/carriers.json
  def create
    @facturacion_carrier = Facturacion::Carrier.new(facturacion_carrier_params)

    respond_to do |format|
      if @facturacion_carrier.save
        format.html { redirect_to @facturacion_carrier, notice: 'Carrier was successfully created.' }
        format.json { render :show, status: :created, location: @facturacion_carrier }
      else
        format.html { render :new }
        format.json { render json: @facturacion_carrier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/carriers/1
  # PATCH/PUT /facturacion/carriers/1.json
  def update
    respond_to do |format|
      if @facturacion_carrier.update(facturacion_carrier_params)
        format.html { redirect_to @facturacion_carrier, notice: 'Carrier was successfully updated.' }
        format.json { render :show, status: :ok, location: @facturacion_carrier }
      else
        format.html { render :edit }
        format.json { render json: @facturacion_carrier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/carriers/1
  # DELETE /facturacion/carriers/1.json
  def destroy
    @facturacion_carrier.destroy
    respond_to do |format|
      format.html { redirect_to facturacion_carriers_url, notice: 'Carrier was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:facturacion_carrier_ids].each do |id|
      @eli_resource = Facturacion::Carrier.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to facturacion_carriers_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_carrier
      @facturacion_carrier = Facturacion::Carrier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_carrier_params
      params.require(:facturacion_carrier).permit(:name, :contact_name, :phone, :address, :description)
    end

    def resource_params
      facturacion_carrier_params
    end
end
