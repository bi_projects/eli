class Admin::FacturacionRolesController < Admin::ApplicationController
  before_action :set_admin_facturacion_role, only: [:show, :edit, :update, :destroy]
  layout "admin/dashboard_layout"

  # GET /facturacion/facturacion_facturacion_roles
  # GET /facturacion/facturacion_facturacion_roles.json
  def index
    @admin_facturacion_roles = Facturacion::FacturacionRole.all
  end

  # GET /facturacion/facturacion_facturacion_roles/1
  # GET /facturacion/facturacion_facturacion_roles/1.json
  def show
  end

  # GET /facturacion/facturacion_facturacion_roles/new
  def new
    @admin_facturacion_role = Facturacion::FacturacionRole.new
    @resources = Admin::Resource.where('module' => 'Facturacion')
    @actions = Action.all
    @facturacion_permissions = Admin::Permission.joins(:admin_resource).where('admin_resources.module' => 'Facturacion')
    # set default permissions so that inputs are set for every possible permission
    @facturacion_permissions.each do |permission|
      @admin_facturacion_role.facturacion_role_permissions.build(admin_resource_id: permission.admin_resource.id, action_id: permission.action.id)
    end
  end

  # GET /facturacion/facturacion_facturacion_roles/1/edit
  def edit
    @admin_facturacion_role = Facturacion::FacturacionRole.find(params[:id])
    @resources = Admin::Resource.where('module' => 'Facturacion')
    @actions = Action.all
    @facturacion_permissions = Admin::Permission.joins(:admin_resource).where('admin_resources.module' => 'Facturacion')
    @facturacion_permissions.each do |permission|
      found = @admin_facturacion_role.facturacion_role_permissions.select {|p| p.action_id == permission.action_id && p.admin_resource_id == permission.admin_resource_id}
      if found.empty?
        @admin_facturacion_role.facturacion_role_permissions.build(admin_resource_id: permission.admin_resource.id, action_id: permission.action.id)
      end
    end
  end

  # POST /facturacion/facturacion_facturacion_roles
  # POST /facturacion/facturacion_facturacion_roles.json
  def create
    @admin_facturacion_role = Facturacion::FacturacionRole.new(facturacion_facturacion_role_params)

    respond_to do |format|
      if @admin_facturacion_role.save
        format.html { redirect_to admin_facturacion_role_path(@admin_facturacion_role.id), notice: 'Facturacion role was successfully created.' }
        format.json { render :show, status: :created, location: @admin_facturacion_role }
      else
        format.html { render :new }
        format.json { render json: @admin_facturacion_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/facturacion_facturacion_roles/1
  # PATCH/PUT /facturacion/facturacion_facturacion_roles/1.json
  def update
    respond_to do |format|
      if @admin_facturacion_role.update(facturacion_facturacion_role_params)
        format.html { redirect_to admin_facturacion_role_path(@admin_facturacion_role.id), notice: 'Facturacion role was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_facturacion_role }
      else
        format.html { render :edit }
        format.json { render json: @admin_facturacion_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturacion/facturacion_facturacion_roles/1
  # DELETE /facturacion/facturacion_facturacion_roles/1.json
  def destroy
    @admin_facturacion_role.destroy
    respond_to do |format|
      format.html { redirect_to admin_facturacion_roles_url, notice: 'Facturacion role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_facturacion_role
      @admin_facturacion_role = Facturacion::FacturacionRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_facturacion_role_params
      params.require(:facturacion_facturacion_role).permit(:name, facturacion_role_permissions_attributes: [:id, :admin_resource_id, :facturacion_facturacion_role_id, :action_id, :_destroy])
    end
end
