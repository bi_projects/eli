class Admin::ExchangeRatesController < Admin::ApplicationController
  before_action :set_admin_exchange_rate, only: [:show, :edit, :update, :destroy]

  layout "admin/dashboard_layout"
  # GET /admin/exchange_rates
  # GET /admin/exchange_rates.json
  def index
    @admin_exchange_rate_uploader = Admin::ExchangeRate.new
    @admin_exchange_rates = Admin::ExchangeRate.all
  end

  # GET /admin/exchange_rates/1
  # GET /admin/exchange_rates/1.json
  def show
    redirect_to admin_exchange_rates_url
  end

  # GET /admin/exchange_rates/new
  def new
    #@admin_exchange_rate = Admin::ExchangeRate.new
    redirect_to admin_exchange_rates_url
  end

  # GET /admin/exchange_rates/1/edit
  def edit
    redirect_to admin_exchange_rates_url
  end

  # POST /admin/exchange_rates
  # POST /admin/exchange_rates.json
  def create
      xlsx_hash = Hash.new
      file_data = params[:file]
      directory = Rails.root + "public/uploads/xlsx/"
      
      xlsx_hash.store(:file_data, file_data)
      xlsx_hash.store(:destination_path, File.join(directory, "#{DateTime.now.to_i}.xlsx"))
      xlsx_builder = Xlsx::XlsxBuilder.new(xlsx_hash)
      begin
        xlsx_builder.bcn_data_storage
        respond_to do |format|
          format.json { render json: {:responseText => "Registros cargados exitosamente"}, status: :ok}
        end
        #redirect_to admin_exchange_rates_url, notice: 'Exchange rate was successfully created.'
      rescue CustomExceptions::InvalidFileException, CustomExceptions::InvalidDateFormatException => e
        respond_to do |format|
          format.json { render json: {:responseText => e.message}, status: :unprocessable_entity }
        end
        #redirect_to admin_exchange_rates_url, notice: e.message
      end
  end

  # PATCH/PUT /admin/exchange_rates/1
  # PATCH/PUT /admin/exchange_rates/1.json
  def update
    respond_to do |format|
      if @admin_exchange_rate.update(admin_exchange_rate_params)
        format.html { redirect_to @admin_exchange_rate, notice: 'Exchange rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_exchange_rate }
      else
        format.html { render :edit }
        format.json { render json: @admin_exchange_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/exchange_rates/1
  # DELETE /admin/exchange_rates/1.json
  def destroy
    @admin_exchange_rate.destroy
    respond_to do |format|
      format.html { redirect_to admin_exchange_rates_url, notice: 'Exchange rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:admin_exchange_rate_ids].each do |id|
      @eli_resource = Admin::ExchangeRate.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to admin_exchange_rates_url, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_exchange_rate
      @admin_exchange_rate = Admin::ExchangeRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_exchange_rate_params
      params.require(:admin_exchange_rate).permit(:bcn_file)
    end
end
