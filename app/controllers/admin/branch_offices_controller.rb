class Admin::BranchOfficesController < Admin::ApplicationController
  before_action :set_admin_branch_office, only: [:show, :edit, :update, :destroy]

  layout "admin/dashboard_layout"
  # GET /admin/branch_offices
  # GET /admin/branch_offices.json
  def index
    @admin_branch_offices = Admin::BranchOffice.all
  end

  # GET /admin/branch_offices/1
  # GET /admin/branch_offices/1.json
  def show
  end

  # GET /admin/branch_offices/new
  def new
    @admin_branch_office = Admin::BranchOffice.new
  end

  # GET /admin/branch_offices/1/edit
  def edit
  end

  # POST /admin/branch_offices
  # POST /admin/branch_offices.json
  def create
    @admin_branch_office = Admin::BranchOffice.new(admin_branch_office_params)

    respond_to do |format|
      if @admin_branch_office.save
        format.html { redirect_to @admin_branch_office, notice: 'Branch office was successfully created.' }
        format.json { render :show, status: :created, location: @admin_branch_office }
      else
        format.html { render :new }
        format.json { render json: @admin_branch_office.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/branch_offices/1
  # PATCH/PUT /admin/branch_offices/1.json
  def update
    respond_to do |format|
      if @admin_branch_office.update(admin_branch_office_params)
        format.html { redirect_to @admin_branch_office, notice: 'Branch office was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_branch_office }
      else
        format.html { render :edit }
        format.json { render json: @admin_branch_office.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/branch_offices/1
  # DELETE /admin/branch_offices/1.json
  def destroy
    @admin_branch_office.destroy
    respond_to do |format|
      format.html { redirect_to admin_branch_offices_url, notice: 'Branch office was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:admin_branch_office_ids].each do |id|
      @eli_resource = Admin::BranchOffice.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to admin_branch_offices_url, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_branch_office
      @admin_branch_office = Admin::BranchOffice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_branch_office_params
      params.require(:admin_branch_office).permit(:name, :description)
    end
end
