class Admin::BillingTransactionsController < Admin::ApplicationController
  layout "admin/dashboard_layout"
  # GET /admin/billing_transactions
  # GET /admin/billing_transactions.json
  def index
    @admin_billing_transactions = Admin::BillingTransaction.all
  end

  # GET /admin/billing_transactions/1
  # GET /admin/billing_transactions/1.json
  def show
    redirect_to admin_billing_transactions_url
  end

  # GET /admin/billing_transactions/new
  def new
    redirect_to admin_billing_transactions_url
  end

  # GET /admin/billing_transactions/1/edit
  def edit
    redirect_to admin_billing_transactions_url
  end

  # POST /admin/billing_transactions
  # POST /admin/billing_transactions.json
  def create
    return false
  end

  # PATCH/PUT /admin/billing_transactions/1
  # PATCH/PUT /admin/billing_transactions/1.json
  def update
    return false
  end

  # DELETE /admin/billing_transactions/1
  # DELETE /admin/billing_transactions/1.json
  def destroy
    redirect_to admin_billing_transactions_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_billing_transaction
      #@admin_billing_transaction = Admin::BillingTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_billing_transaction_params
      #params.require(:admin_billing_transaction).permit(:description, :date, :document_id, :facturacion_document_type_id, :document_number, :transaction_amount)
    end
end
