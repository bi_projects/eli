class Admin::BillingUsersController < Admin::ApplicationController
  before_action :set_facturacion_billing_user, only: [:show, :edit, :update, :destroy,
    :reset_password, :reset_password_form]
  layout "admin/dashboard_layout"

  # GET /facturacion/billing_users
  # GET /facturacion/billing_users.json
  def index
    @admin_billing_users = Facturacion::BillingUser.all
  end

  # GET /facturacion/billing_users/1
  # GET /facturacion/billing_users/1.json
  def show
  end

  # GET /facturacion/billing_users/new
  def new
    @admin_billing_user = Facturacion::BillingUser.new
    facturacion_roles_available = Facturacion::FacturacionRole.all
    facturacion_roles_available.each do |role|
      @admin_billing_user.facturacion_user_roles.build(facturacion_facturacion_role_id: role.id).facturacion_facturacion_role = role
      # academico_role = role at the end is just to be able to get role name from view
    end
  end

  # GET /facturacion/billing_users/1/edit
  def edit
    @admin_billing_user = Facturacion::BillingUser.find(params[:id])
    facturacion_roles_available = Facturacion::FacturacionRole.all
    facturacion_roles_available.each do |role|
      found = @admin_billing_user.facturacion_user_roles.select {|r| r.facturacion_facturacion_role_id == role.id}
      if found.empty?
        @admin_billing_user.facturacion_user_roles.build(facturacion_facturacion_role_id: role.id).facturacion_facturacion_role = role
      end
    end
  end

  # POST /facturacion/billing_users
  # POST /facturacion/billing_users.json
  def create
    @admin_billing_user = Facturacion::BillingUser.new(facturacion_billing_user_params)

    respond_to do |format|
      if @admin_billing_user.save
        format.html { redirect_to admin_billing_user_path(@admin_billing_user), notice: 'Billing user was successfully created.' }
        format.json { render :show, status: :created, location: @admin_billing_user }
      else
        format.html { render :new }
        format.json { render json: @admin_billing_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturacion/billing_users/1
  # PATCH/PUT /facturacion/billing_users/1.json
  def update
    respond_to do |format|
      if @admin_billing_user.update(facturacion_billing_user_params)
        format.html { redirect_to admin_billing_user_path(@admin_billing_user), notice: 'Billing user was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_billing_user }
      else
        format.html { render :edit }
        format.json { render json: @admin_billing_user.errors, status: :unprocessable_entity }
      end
    end
  end

  def reset_password_form
  end

  def reset_password
    respond_to do |format|
      new_password = facturacion_billing_user_params[:new_password]
      password_confirmation = facturacion_billing_user_params[:password_confirmation]
      if @admin_billing_user.reset_password(new_password, password_confirmation)
        format.html { redirect_to admin_billing_users_url, notice: 'Password actualizado.'}
      else
        format.html { render :reset_password_form }
      end
    end
  end

  # DELETE /facturacion/billing_users/1
  # DELETE /facturacion/billing_users/1.json
  def destroy
    @admin_billing_user.destroy
    respond_to do |format|
      format.html { redirect_to admin_billing_users_url, notice: 'Billing user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:admin_billing_user_ids].each do |id|
      @eli_resource = Facturacion::BillingUser.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to _url, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facturacion_billing_user
      @admin_billing_user = Facturacion::BillingUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facturacion_billing_user_params
      params.fetch(:facturacion_billing_user, {})
      params.require(:facturacion_billing_user).permit(:name, :email, :password, :new_password,  :password_confirmation,
        facturacion_user_roles_attributes: [:id, :facturacion_billing_user_id, :facturacion_facturacion_role_id, :_destroy])
    end
end
