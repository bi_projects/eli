class Admin::ResourcesController < ApplicationController
  before_action :set_admin_resource, only: [:show, :edit, :update, :destroy]

  # GET /admin/resources
  # GET /admin/resources.json
  def index
    @admin_resources = Resource.all
  end

  # GET /admin/resources/1
  # GET /admin/resources/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_resource
      @admin_resource = Admin::Resource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_resource_params
      params.require(:admin_resource).permit(:resource_id, :description, :model, :resource_type_id, :resource_types_id)
    end
end
