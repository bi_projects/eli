class Admin::AcademicoRolesController < Admin::ApplicationController
  before_action :set_admin_academico_role, only: [:show, :edit, :update, :destroy]
  layout "admin/dashboard_layout"

  # GET /academico/academico_roles
  # GET /academico/academico_roles.json
  def index
    @admin_academico_roles = Academico::AcademicoRole.all
  end

  # GET /academico/academico_roles/1
  # GET /academico/academico_roles/1.json
  def show
  end

  # GET /academico/academico_roles/new
  def new
    @admin_academico_role = Academico::AcademicoRole.new
    @resources = Admin::Resource.where('module' => 'Academico')
    @actions = Action.all
    @academico_permissions = Admin::Permission.joins(:admin_resource).where('admin_resources.module' => 'Academico')
    # set default permissions so that inputs are set for every possible permission
    @academico_permissions.each do |permission|
      @admin_academico_role.academico_role_permissions.build(admin_resource_id: permission.admin_resource.id, action_id: permission.action.id)
    end
  end

  # GET /academico/academico_roles/1/edit
  def edit
    @admin_academico_role = Academico::AcademicoRole.find(params[:id])
    @resources = Admin::Resource.where('module' => 'Academico')
    @actions = Action.all
    @academico_permissions = Admin::Permission.joins(:admin_resource).where('admin_resources.module' => 'Academico')
    @academico_permissions.each do |permission|
      found = @admin_academico_role.academico_role_permissions.select {|p| p.action_id == permission.action_id && p.admin_resource_id == permission.admin_resource_id}
      if found.empty?
        @admin_academico_role.academico_role_permissions.build(admin_resource_id: permission.admin_resource.id, action_id: permission.action.id)
      end
    end
  end

  # POST /academico/academico_roles
  # POST /academico/academico_roles.json
  def create
    @admin_academico_role = Academico::AcademicoRole.new(academico_academico_role_params)

    respond_to do |format|
      if @admin_academico_role.save
        format.html { redirect_to admin_academico_role_path(@admin_academico_role.id), notice: 'Academico role was successfully created.' }
        format.json { render :show, status: :created, location: @admin_academico_role }
      else
        format.html { render :new }
        format.json { render json: @admin_academico_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/academico_roles/1
  # PATCH/PUT /academico/academico_roles/1.json
  def update
    respond_to do |format|
      if @admin_academico_role.update(academico_academico_role_params)
        format.html { redirect_to admin_academico_role_path(@admin_academico_role.id), notice: 'Academico role was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_academico_role }
      else
        format.html { render :edit }
        format.json { render json: @admin_academico_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/academico_roles/1
  # DELETE /academico/academico_roles/1.json
  def destroy
    @admin_academico_role.destroy
    respond_to do |format|
      format.html { redirect_to admin_academico_roles_url, notice: 'Academico role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_academico_role
      @admin_academico_role = Academico::AcademicoRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_academico_role_params
      params.require(:academico_academico_role).permit(:name, academico_role_permissions_attributes: [:id, :admin_resource_id, :academico_academico_role_id, :action_id, :_destroy])
    end
end
