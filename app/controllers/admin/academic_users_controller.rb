class Admin::AcademicUsersController < Admin::ApplicationController
  before_action :set_admin_academic_user, only: [:show, :edit, :update, :destroy,
    :reset_password, :reset_password_form]
  layout "admin/dashboard_layout"

  # GET /academico/academic_users
  # GET /academico/academic_users.json
  def index
    @admin_academicusers = Academico::AcademicUser.all
  end

  # GET /academico/academic_users/1
  # PATCH/PUT /academico/academic_users/1
  # GET /academico/academic_users/1.json
  def show
  end

  # GET /academico/academic_users/new
  def new
    @admin_academicuser = Academico::AcademicUser.new
    academico_roles_available = Academico::AcademicoRole.all
    academico_roles_available.each do |role|
      @admin_academicuser.academico_user_roles.build(academico_academico_roles_id: role.id).academico_role = role
      # academico_role = role at the end is just to be able to get role name from view
    end
    @admin_academicuser.build_admin_academic_user_position
    @admin_positions = Admin::Position.where(admin_module_id: 2)
  end

  # GET /academico/academic_users/1/edit
  def edit
    @admin_academicuser = Academico::AcademicUser.find(params[:id])
    academico_roles_available = Academico::AcademicoRole.all
    academico_roles_available.each do |role|
      found = @admin_academicuser.academico_user_roles.select {|r| r.academico_academico_roles_id == role.id}
      if found.empty?
        @admin_academicuser.academico_user_roles.build(academico_academico_roles_id: role.id).academico_role = role
      end
    end
    if @admin_academicuser.admin_academic_user_position.nil?
      @admin_academicuser.build_admin_academic_user_position
    end
    @admin_positions = Admin::Position.where(admin_module_id: 2)
  end

  # POST /academico/academic_users
  # POST /academico/academic_users.json
  def create
    @admin_academicuser = Academico::AcademicUser.new(admin_academic_user_params)
    respond_to do |format|
      if @admin_academicuser.save
        format.html { redirect_to admin_academic_user_path(@admin_academicuser.id), notice: 'Academic user was successfully created.' }
        format.json { render :show, status: :created, location: @admin_academicuser }
      else
        format.html { render :new }
        format.json { render json: @admin_academicuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/academic_users/1.json
  def update
    respond_to do |format|
      @admin_positions = Admin::Position.where(admin_module_id: 2)
      if @admin_academicuser.update(admin_academic_user_params)
        format.html { redirect_to admin_academic_user_path(@admin_academicuser.id), notice: 'Academic user was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_academicuser }
      else
        format.html { render :edit }
        format.json { render json: @admin_academicuser.errors, status: :unprocessable_entity }
      end
    end
  end

  def reset_password_form
  end

  def reset_password
    respond_to do |format|
      new_password = admin_academic_user_params[:new_password]
      password_confirmation = admin_academic_user_params[:password_confirmation]
      if @admin_academicuser.reset_password(new_password, password_confirmation)
        format.html { redirect_to admin_academic_users_url, notice: 'Password actualizado.'}
      else
        format.html { render :reset_password_form }
      end
    end
  end

  # DELETE /academico/academic_users/1
  # DELETE /academico/academic_users/1.json
  def destroy
    @admin_academicuser.destroy
    respond_to do |format|
      format.html { redirect_to admin_academic_users_url, notice: 'Academic user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:admin_academic_user_ids].each do |id|
      @eli_resource = Academico::AcademicUser.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to admin_academic_users_url, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_academic_user
      @admin_academicuser = Academico::AcademicUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_academic_user_params
      params.require(:academico_academic_user).permit(:name, :email, :password, :new_password, :password_confirmation,
        academico_user_roles_attributes: [:id, :academico_academic_users_id, :academico_academico_roles_id, :_destroy],
        admin_academic_user_position_attributes: [:id, :academico_academic_user_id, :admin_position_id, :_destroy])
    end
end
