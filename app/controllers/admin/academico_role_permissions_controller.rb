class Admin::AcademicoRolePermissionsController < ApplicationController
  before_action :set_academico_role_permission, only: [:show, :edit, :update, :destroy]

  # GET /admin/academico_role_permissions
  # GET /admin/academico_role_permissions.json
  def index
    @academico_role_permissions = Admin::AcademicoRolePermission.all
  end

  # GET /admin/academico_role_permissions/1
  # GET /admin/academico_role_permissions/1.json
  def show
  end

  # GET /admin/academico_role_permissions/new
  def new
    @academico_role_permission = Admin::AcademicoRolePermission.new
  end

  # GET /admin/academico_role_permissions/1/edit
  def edit
  end

  # POST /admin/academico_role_permissions
  # POST /admin/academico_role_permissions.json
  def create
    @academico_role_permission = Admin::AcademicoRolePermission.new(academico_role_permission_params)

    respond_to do |format|
      if @academico_role_permission.save
        format.html { redirect_to @academico_role_permission, notice: 'Resource role was successfully created.' }
        format.json { render :show, status: :created, location: @academico_role_permission }
      else
        format.html { render :new }
        format.json { render json: @academico_role_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/academico_role_permissions/1
  # PATCH/PUT /admin/academico_role_permissions/1.json
  def update
    respond_to do |format|
      if @academico_role_permission.update(academico_role_permission_params)
        format.html { redirect_to @academico_role_permission, notice: 'Resource role was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_role_permission }
      else
        format.html { render :edit }
        format.json { render json: @academico_role_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/academico_role_permissions/1
  # DELETE /admin/academico_role_permissions/1.json
  def destroy
    @academico_role_permission.destroy
    respond_to do |format|
      format.html { redirect_to academico_role_permissions_url, notice: 'Resource role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_role_permission
      @academico_role_permission = Admin::AcademicoRolePermission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_role_permission_params
      params.require(:academico_role_permission).permit(:role_id, :resource_id, :action_id, :admin_resources_id, :academico_academico_roles_id, :actions_id)
    end
end
