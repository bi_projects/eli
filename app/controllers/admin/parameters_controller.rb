class Admin::ParametersController < Admin::ApplicationController
  before_action :set_admin_parameter, only: [:show, :edit, :update, :destroy]
  layout "admin/dashboard_layout"
  # GET /admin/parameters
  # GET /admin/parameters.json
  def index
    @admin_parameters = Admin::Parameter.all
  end

  # GET /admin/parameters/1
  # GET /admin/parameters/1.json
  def show
    redirect_to admin_parameters_url
  end

  # GET /admin/parameters/new
  def new
    if Admin::Parameter.all.present?
      redirect_to admin_parameters_url
    else
      @admin_parameter = Admin::Parameter.new
    end
  end

  # GET /admin/parameters/1/edit
  def edit
  end

  # POST /admin/parameters
  # POST /admin/parameters.json
  def create
    @admin_parameter = Admin::Parameter.new(admin_parameter_params)
    begin

      respond_to do |format|
        if @admin_parameter.save
          format.html { redirect_to @admin_parameter, notice: 'Parameter was successfully created.' }
          format.json { render :show, status: :created, location: @admin_parameter }
        else
          format.html { render :new }
          format.json { render json: @admin_parameter.errors, status: :unprocessable_entity }
        end
      end
    rescue CustomExceptions::AdminException => e
      respond_to do |format|
        format.html { redirect_to admin_parameters_url, :alert => e.message }
      end
    end
  end

  # PATCH/PUT /admin/parameters/1
  # PATCH/PUT /admin/parameters/1.json
  def update
    respond_to do |format|
      if @admin_parameter.update(admin_parameter_params)
        format.html { redirect_to @admin_parameter, notice: 'Parameter was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_parameter }
      else
        format.html { render :edit }
        format.json { render json: @admin_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/parameters/1
  # DELETE /admin/parameters/1.json
  def destroy
    begin
      @admin_parameter.destroy
      respond_to do |format|
        format.html { redirect_to admin_parameters_url, notice: 'Parameter was successfully destroyed.' }
        format.json { head :no_content }
      end
    rescue CustomExceptions::AdminException => e
      respond_to do |format|
        format.html { redirect_to admin_parameters_url, :alert=> e.message }
      end
    end 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_parameter
      @admin_parameter = Admin::Parameter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_parameter_params
      params.require(:admin_parameter).permit(:invoice_prefix, :invoice_number, :receipt_prefix, :receipt_number,
        :credit_note_prefix, :credit_note_number, :debit_note_prefix, :debit_note_number, :student_prefix, :student_code_length, :drivers_discount)
    end
end
