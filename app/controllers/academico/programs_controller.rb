class Academico::ProgramsController < Academico::AuthorizeController
  before_action :set_academico_program, only: [:show, :edit, :update, :destroy]
  layout "academico/dashboard_layout"
  # GET /academico/programs
  # GET /academico/programs.json
  def index
    @academico_programs = Academico::Program.all
  end

  # GET /academico/programs/1
  # GET /academico/programs/1.json
  def show
  end

  # GET /academico/programs/new
  def new
    @academico_program = Academico::Program.new
  end

  # GET /academico/programs/1/edit
  def edit
  end

  # POST /academico/programs
  # POST /academico/programs.json
  def create
    @academico_program = Academico::Program.new(academico_program_params)

    respond_to do |format|
      if @academico_program.save
        format.html { redirect_to @academico_program, notice: 'Program was successfully created.' }
        format.json { render :show, status: :created, location: @academico_program }
      else
        format.html { render :new }
        format.json { render json: @academico_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/programs/1
  # PATCH/PUT /academico/programs/1.json
  def update
    respond_to do |format|
      if @academico_program.update(academico_program_params)
        format.html { redirect_to @academico_program, notice: 'Program was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_program }
      else
        format.html { render :edit }
        format.json { render json: @academico_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/programs/1
  # DELETE /academico/programs/1.json
  def destroy
    @academico_program.destroy
    respond_to do |format|
      format.html { redirect_to academico_programs_url, notice: 'Program was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_program_ids].each do |id|
      @academico_program = Academico::Program.find(id)
      @academico_program.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_programs_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_program
      @academico_program = Academico::Program.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_program_params
      params.require(:academico_program).permit(:name, :academico_age_range_id, :price, :description)
    end

    def resource_params
      academico_program_params
    end
end
