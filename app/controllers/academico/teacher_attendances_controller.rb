class Academico::TeacherAttendancesController < Academico::AuthorizeController
  include CurrentSession
  before_action :set_academico_teacher_attendance, only: [:show, :edit, :update, :destroy]

  # GET /academico/teacher_attendances
  # GET /academico/teacher_attendances.json
  def index
    @academico_teacher_attendances = Academico::TeacherAttendance.all
  end

  # GET /academico/teacher_attendances/1
  # GET /academico/teacher_attendances/1.json
  def show
  end

  # GET /academico/teacher_attendances/new
  def new
    @academico_teacher_attendance = Academico::TeacherAttendance.new
  end

  # GET /academico/teacher_attendances/1/edit
  def edit
    temary = @academico_teacher_attendance.try(:academico_course).try(:temary_record, @academico_teacher_attendance.academico_staffs_id)
    if temary.present?
      @temary = temary
      @temary.academico_sessions.each do |session|
        academico_teacher_attendance_detail = @academico_teacher_attendance.academico_teacher_attendance_details.find {|detail| detail.academico_sessions_id == session.id}
        if academico_teacher_attendance_detail.blank?
          @academico_teacher_attendance.academico_teacher_attendance_details.build(academico_teacher_attendances_id: @academico_teacher_attendance.id, marked: false, schedulle: session.schedulle, academico_sessions_id: session.id)
        end
      end
    end
  end

  # POST /academico/teacher_attendances
  # POST /academico/teacher_attendances.json
  def create
    @academico_teacher_attendance = Academico::TeacherAttendance.new(academico_teacher_attendance_params)
    @academico_teacher_attendance.created_by = get_staff_id_for_user

    respond_to do |format|
      if @academico_teacher_attendance.save
        format.html { redirect_to edit_academico_teacher_attendance_path(@academico_teacher_attendance), notice: 'Teacher attendance was successfully created.' }
        format.json { render :show, status: :created, location: @academico_teacher_attendance }
      else
        format.html { render :new }
        format.json { render json: @academico_teacher_attendance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/teacher_attendances/1
  # PATCH/PUT /academico/teacher_attendances/1.json
  def update
    respond_to do |format|
      @academico_teacher_attendance.created_by = get_staff_id_for_user
      if @academico_teacher_attendance.update(academico_teacher_attendance_params)
        format.html { redirect_to @academico_teacher_attendance, notice: 'Teacher attendance was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_teacher_attendance }
      else
        format.html { render :edit }
        format.json { render json: @academico_teacher_attendance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/teacher_attendances/1
  # DELETE /academico/teacher_attendances/1.json
  def destroy
    @academico_teacher_attendance.destroy
    respond_to do |format|
      format.html { redirect_to academico_teacher_attendances_url, notice: 'Teacher attendance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_teacher_attendance
      @academico_teacher_attendance = Academico::TeacherAttendance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_teacher_attendance_params
      params.require(:academico_teacher_attendance).permit(:academico_courses_id, :academico_staffs_id,
      academico_teacher_attendance_details_attributes: [:id, :academico_teacher_attendances_id, :marked, :schedulle, :academico_sessions_id])
    end

    def resource_params
      academico_teacher_attendance_params
    end
end
