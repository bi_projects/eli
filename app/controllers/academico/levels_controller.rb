class Academico::LevelsController < Academico::AuthorizeController
  before_action :set_academico_level, only: [:show, :edit, :update, :destroy]
  include Academico::FormHelper
  # GET /academico/levels
  # GET /academico/levels.json
  def index
    @academico_levels = Academico::Level.all.order(:level_sequence)
  end

  # GET /academico/levels/1
  # GET /academico/levels/1.json
  def show
  end

  # GET /academico/levels/new
  def new
    @academico_level = Academico::Level.new
  end

  # GET /academico/levels/1/edit
  def edit
  end

  # POST /academico/levels
  # POST /academico/levels.json
  def create
    @academico_level = Academico::Level.new(academico_level_params)

    respond_to do |format|
      if @academico_level.save
        format.html { redirect_to @academico_level, notice: 'Level was successfully created.' }
        format.json { render :show, status: :created, location: @academico_level }
      else
        format.html { render :new }
        format.json { render json: @academico_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/levels/1
  # PATCH/PUT /academico/levels/1.json
  def update
    respond_to do |format|
      if @academico_level.update(academico_level_params)
        format.html { redirect_to @academico_level, notice: 'Level was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_level }
      else
        format.html { render :edit }
        format.json { render json: @academico_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/levels/1
  # DELETE /academico/levels/1.json
  def destroy
    @academico_level.destroy
    respond_to do |format|
      format.html { redirect_to academico_levels_url, notice: 'Level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_level_ids].each do |id|
      @academico_level = Academico::Level.find(id)
      @academico_level.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_levels_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_level
      @academico_level = Academico::Level.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_level_params
      params.require(:academico_level).permit(:name, :description, :level_sequence,
        :academico_level_program_books_attributes => [:id, :academico_program_id, :facturacion_book_id, :_destroy])
    end

    def resource_params
      academico_level_params
    end
end
