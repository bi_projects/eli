class Academico::EvaluationParameterTypesController < Academico::AuthorizeController
  before_action :set_academico_evaluation_parameter_type, only: [:show, :edit, :update, :destroy]

  # GET /academico/evaluation_parameter_types
  # GET /academico/evaluation_parameter_types.json
  def index
    @academico_evaluation_parameter_types = Academico::EvaluationParameterType.all
  end

  # GET /academico/evaluation_parameter_types/1
  # GET /academico/evaluation_parameter_types/1.json
  def show
  end

  # GET /academico/evaluation_parameter_types/new
  def new
    @academico_evaluation_parameter_type = Academico::EvaluationParameterType.new
  end

  # GET /academico/evaluation_parameter_types/1/edit
  def edit
  end

  # POST /academico/evaluation_parameter_types
  # POST /academico/evaluation_parameter_types.json
  def create
    @academico_evaluation_parameter_type = Academico::EvaluationParameterType.new(academico_evaluation_parameter_type_params)

    respond_to do |format|
      if @academico_evaluation_parameter_type.save
        format.html { redirect_to @academico_evaluation_parameter_type, notice: 'Evaluation parameter type was successfully created.' }
        format.json { render :show, status: :created, location: @academico_evaluation_parameter_type }
      else
        format.html { render :new }
        format.json { render json: @academico_evaluation_parameter_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/evaluation_parameter_types/1
  # PATCH/PUT /academico/evaluation_parameter_types/1.json
  def update
    respond_to do |format|
      if @academico_evaluation_parameter_type.update(academico_evaluation_parameter_type_params)
        format.html { redirect_to @academico_evaluation_parameter_type, notice: 'Evaluation parameter type was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_evaluation_parameter_type }
      else
        format.html { render :edit }
        format.json { render json: @academico_evaluation_parameter_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/evaluation_parameter_types/1
  # DELETE /academico/evaluation_parameter_types/1.json
  def destroy
    @academico_evaluation_parameter_type.destroy
    respond_to do |format|
      format.html { redirect_to academico_evaluation_parameter_types_url, notice: 'Evaluation parameter type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_evaluation_parameter_type_ids].each do |id|
      @academico_evaluation_parameter_type = Academico::EvaluationParameterType.find(id)
      @academico_evaluation_parameter_type.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_evaluation_parameter_types_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_evaluation_parameter_type
      @academico_evaluation_parameter_type = Academico::EvaluationParameterType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_evaluation_parameter_type_params
      params.require(:academico_evaluation_parameter_type).permit(:name, :formula)
    end

    def resource_params
      academico_evaluation_parameter_type_params
    end
end
