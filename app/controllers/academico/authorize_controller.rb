class Academico::AuthorizeController < Academico::ApplicationController
	load_and_authorize_resource
	layout "academico/dashboard_layout"

	  rescue_from CanCan::AccessDenied do |exception|
      respond_to do |format|
        format.json { head :forbidden, content_type: 'text/html' }
        format.html { redirect_to academico_root_path, notice: exception.message }
        format.js   { head :forbidden, content_type: 'text/html' }
      end
    end

    private

    def current_ability
  		@current_ability ||= Academico::AcademicoAbility.new(current_user)
  	end

    # implement resource_params in every children controller to avoid cancan prevent from saving models
end
