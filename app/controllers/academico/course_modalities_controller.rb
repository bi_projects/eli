class Academico::CourseModalitiesController < Academico::AuthorizeController
  before_action :set_academico_course_modality, only: [:show, :edit, :update, :destroy]

  # GET /academico/course_modalities
  # GET /academico/course_modalities.json
  def index
    @academico_course_modalities = Academico::CourseModality.all
  end

  # GET /academico/course_modalities/1
  # GET /academico/course_modalities/1.json
  def show
  end

  # GET /academico/course_modalities/new
  def new
    @academico_course_modality = Academico::CourseModality.new
  end

  # GET /academico/course_modalities/1/edit
  def edit
  end

  # POST /academico/course_modalities
  # POST /academico/course_modalities.json
  def create
    @academico_course_modality = Academico::CourseModality.new(academico_course_modality_params)

    respond_to do |format|
      if @academico_course_modality.save
        format.html { redirect_to @academico_course_modality, notice: 'Course modality was successfully created.' }
        format.json { render :show, status: :created, location: @academico_course_modality }
      else
        format.html { render :new }
        format.json { render json: @academico_course_modality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/course_modalities/1
  # PATCH/PUT /academico/course_modalities/1.json
  def update
    respond_to do |format|
      if @academico_course_modality.update(academico_course_modality_params)
        format.html { redirect_to @academico_course_modality, notice: 'Course modality was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_course_modality }
      else
        format.html { render :edit }
        format.json { render json: @academico_course_modality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/course_modalities/1
  # DELETE /academico/course_modalities/1.json
  def destroy
    @academico_course_modality.destroy
    respond_to do |format|
      format.html { redirect_to academico_course_modalities_url, notice: 'Course modality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_course_modality_ids].each do |id|
      @academico_course_modality = Academico::CourseModality.find(id)
      @academico_course_modality.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_course_modalities_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_course_modality
      @academico_course_modality = Academico::CourseModality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_course_modality_params
      params.require(:academico_course_modality).permit(:description)
    end

    def resource_params
      academico_course_modality_params
    end
end
