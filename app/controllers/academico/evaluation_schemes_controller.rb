class Academico::EvaluationSchemesController < Academico::AuthorizeController
  before_action :set_academico_evaluation_scheme, only: [:show, :edit, :update, :destroy]

  # GET /academico/evaluation_schemes
  # GET /academico/evaluation_schemes.json
  def index
    @academico_evaluation_schemes = Academico::EvaluationScheme.all.paginate(:page => params[:page])
  end

  # GET /academico/evaluation_schemes/1
  # GET /academico/evaluation_schemes/1.json
  def show
  end

  # GET /academico/evaluation_schemes/new
  def new
    @academico_evaluation_scheme = Academico::EvaluationScheme.new
    load_new
    load_evaluation_scheme_data(@academico_evaluation_scheme)
  end

  # GET /academico/evaluation_schemes/1/edit
  def edit
    @academico_evaluation_scheme = Academico::EvaluationScheme.find(params[:id])
    load_edit
    load_evaluation_scheme_data(@academico_evaluation_scheme)
  end

  # POST /academico/evaluation_schemes
  # POST /academico/evaluation_schemes.json
  def create
    @academico_evaluation_scheme = Academico::EvaluationScheme.new(academico_evaluation_scheme_params)

    respond_to do |format|
      if @academico_evaluation_scheme.save
        format.html { redirect_to @academico_evaluation_scheme, notice: 'Evaluation scheme was successfully created.' }
        format.json { render :show, status: :created, location: @academico_evaluation_scheme }
      else
        load_new
        load_evaluation_scheme_data(@academico_evaluation_scheme)
        format.html { render :new, object: @academico_evaluation_scheme }
        format.json { render json: @academico_evaluation_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/evaluation_schemes/1
  # PATCH/PUT /academico/evaluation_schemes/1.json
  def update
    respond_to do |format|
      if @academico_evaluation_scheme.update(academico_evaluation_scheme_params)
        format.html { redirect_to @academico_evaluation_scheme, notice: 'Evaluation scheme was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_evaluation_scheme }
      else
        load_edit
        load_evaluation_scheme_data(@academico_evaluation_scheme)
        format.html { render :edit, object: @academico_evaluation_scheme }
        format.json { render json: @academico_evaluation_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/evaluation_schemes/1
  # DELETE /academico/evaluation_schemes/1.json
  def destroy
    @academico_evaluation_scheme.destroy
    respond_to do |format|
      format.html { redirect_to academico_evaluation_schemes_url, notice: 'Evaluation scheme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_evaluation_scheme_ids].each do |id|
      @academico_evaluation_scheme = Academico::EvaluationScheme.find(id)
      @academico_evaluation_scheme.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_evaluation_schemes_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_evaluation_scheme
      @academico_evaluation_scheme = Academico::EvaluationScheme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_evaluation_scheme_params
      params.require(:academico_evaluation_scheme).permit(:description,
        academico_evaluation_methods_attributes: [:id, :academico_evaluation_scheme_id, :academico_evaluation_parameter_id, :percentage, :_destroy],
        academico_evaluation_level_programs_attributes: [:id, :academico_evaluation_schemes_id, :academico_level_programs_id, :_destroy])
    end

    def resource_params
      academico_evaluation_scheme_params
    end

    def load_new
      @levels = Academico::Level.all.order(:level_sequence)
      @programs = Academico::Program.all.order(:id)
    end

    def load_edit
      @levels = Academico::Level.all.order(:level_sequence)
      @programs = Academico::Program.all.order(:id)
    end

    def load_evaluation_scheme_data(evaluation_scheme)

      Academico::LevelProgram.all.each do |level_program|

        if evaluation_scheme.persisted?
          found = evaluation_scheme.academico_evaluation_level_programs.select {|s| s.academico_evaluation_schemes_id == evaluation_scheme.id && s.academico_level_programs_id == level_program.id}
          if found.empty?
            evaluation_scheme.academico_evaluation_level_programs.build(academico_level_programs_id: level_program.id).academico_level_program = level_program
          end
        else
          evaluation_scheme.academico_evaluation_level_programs.build(academico_level_programs_id: level_program.id).academico_level_program = level_program
        end

      end
    end

end
