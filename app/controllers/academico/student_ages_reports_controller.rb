class Academico::StudentAgesReportsController < Academico::AuthorizeController
  include Reports
  before_action :scoped_collection, only: [:index, :xlsx_file]
  skip_load_and_authorize_resource only: [:index, :xlsx_file]

  def scoped_collection
    if params[:search].blank?
      @show_results = false
    else
      @show_results = true
      params[:search] ||= {}
      branch_office_id = params.try(:[], :search).try(:[], :branch_office_id)
      academico_course_schedulle_id = params.try(:[], :search).try(:[], :academico_course_schedulle_id)
      academico_course_modality_id = params.try(:[], :search).try(:[], :academico_course_modality_id)

      @students = Academico::Student.students_ages(
        academico_course_schedulle_id: academico_course_schedulle_id,
        academico_course_modality_id: academico_course_modality_id,
        branch_office_id: branch_office_id
      )

      if branch_office_id.present?
        @branch_office = Admin::BranchOffice.find(branch_office_id)
      end
      if academico_course_schedulle_id.present?
        @schedulle = Academico::CourseSchedulle.find(academico_course_schedulle_id)
      end
      if academico_course_modality_id.present?
        @modality = Academico::CourseModality.find(academico_course_modality_id)
      end
    end
  end

  def index
    authorize! :index, Academico::StudentAgesReportsController
  end
end
