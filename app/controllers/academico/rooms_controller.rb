class Academico::RoomsController < Academico::AuthorizeController
  before_action :set_academico_room, only: [:show, :edit, :update, :destroy]
  skip_authorize_resource :only => :lookup_capacity

  # GET /academico/rooms
  # GET /academico/rooms.json
  def index
    @academico_rooms = filtering_params(params).paginate(:page => params[:page])
  end

  def filtering_params(params)
    if params[:filter].present?
      Academico::Room.filter(params[:filter].slice(:name))
    else
      Academico::Room.all
    end
  end

  # GET /academico/rooms/1
  # GET /academico/rooms/1.json
  def show
  end

  # GET /academico/rooms/new
  def new
    @academico_room = Academico::Room.new
    @offices = Admin::BranchOffice.all
  end

  # GET /academico/rooms/1/edit
  def edit
    @offices = Admin::BranchOffice.all
  end

  # POST /academico/rooms
  # POST /academico/rooms.json
  def create
    @academico_room = Academico::Room.new(academico_room_params)

    respond_to do |format|
      if @academico_room.save
        format.html { redirect_to @academico_room, notice: 'Room was successfully created.' }
        format.json { render :show, status: :created, location: @academico_room }
      else
        format.html { render :new }
        format.json { render json: @academico_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/rooms/1
  # PATCH/PUT /academico/rooms/1.json
  def update
    respond_to do |format|
      if @academico_room.update(academico_room_params)
        format.html { redirect_to @academico_room, notice: 'Room was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_room }
      else
        format.html { render :edit }
        format.json { render json: @academico_room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/rooms/1
  # DELETE /academico/rooms/1.json
  def destroy
    @academico_room.destroy
    respond_to do |format|
      format.html { redirect_to academico_rooms_url, notice: 'Room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_room_ids].each do |id|
      @eli_resource = Academico::Room.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_rooms_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_room
      @academico_room = Academico::Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_room_params
      params.require(:academico_room).permit(:name, :size, :description, :admin_branch_office_id)
    end

    def resource_params
      academico_room_params
    end
end
