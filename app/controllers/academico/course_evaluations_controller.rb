class Academico::CourseEvaluationsController < Academico::AuthorizeController
  include CurrentSession
  before_action :set_academico_course_evaluation, only: [:show, :edit, :update, :destroy]

  # GET /academico/course_evaluations
  # GET /academico/course_evaluations.json
  def index
    @academico_course_evaluations = Academico::CourseEvaluation.all.paginate(:page => params[:page])
  end

  # GET /academico/course_evaluations/1
  # GET /academico/course_evaluations/1.json
  def show
  end

  # GET /academico/course_evaluations/new
  def new
    @academico_course_evaluation = Academico::CourseEvaluation.new
    fetch_courses true

  end

  # GET /academico/course_evaluations/1/edit
  def edit
    fetch_courses false
    # Load sessions from temary(schedulle)
    temary = @academico_course_evaluation.academico_course.temary_record(@academico_course_evaluation.academico_staffs_id)
    if temary.present?
      temary.academico_sessions.each do |session|
        academico_session_evaluation = @academico_course_evaluation.academico_session_evaluations.select {|p| p.academico_course_evaluations_id == @academico_course_evaluation.id && p.academico_sessions_id == session.id}
        if academico_session_evaluation.empty?
          academico_session_evaluation = @academico_course_evaluation.academico_session_evaluations.build(academico_course_evaluations_id: @academico_course_evaluation.id, academico_sessions_id: session.id)
        else
          academico_session_evaluation = academico_session_evaluation.first
        end
        # Load students records from course - second level
        @academico_course_evaluation.academico_course.student_registrations.each do |academico_student|
          academico_student_evaluation = academico_session_evaluation.academico_student_evaluations.select {|s| s.academico_session_evaluations_id == academico_session_evaluation.id && s.academico_students_id == academico_student.id}
          if academico_student_evaluation.empty?
            academico_student_evaluation = academico_session_evaluation.academico_student_evaluations.build(academico_students_id: academico_student.id, academico_session_evaluations_id: academico_session_evaluation.id)
          else
            academico_student_evaluation = academico_student_evaluation.first
          end

          # Load assignments to be scored from temary(schedulle) - third level
          assignments = academico_session_evaluation.academico_session.all_session_assignments
          assignments.each do |session_assignment|
            student_score = academico_student_evaluation.academico_student_scores.select {|e| e.academico_student_evaluations_id == academico_student_evaluation.id && e.academico_session_assignments_id == session_assignment.id}

            if student_score.empty?
              academico_student_evaluation.academico_student_scores.build(academico_student_evaluations_id: academico_student_evaluation.id, academico_session_assignments_id: session_assignment.id)
            end
          end
        end
      end
    end
  end

  # POST /academico/course_evaluations
  # POST /academico/course_evaluations.json
  def create
    @academico_course_evaluation = Academico::CourseEvaluation.new(academico_course_evaluation_params)
    @academico_course_evaluation.academico_staffs_id = get_staff_id_for_user

    respond_to do |format|
      if @academico_course_evaluation.save
        format.html { redirect_to edit_academico_course_evaluation_path(@academico_course_evaluation) }
        format.json { render :show, status: :created, location: @academico_course_evaluation }
      else
        format.html { render :new }
        format.json { render json: @academico_course_evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/course_evaluations/1
  # PATCH/PUT /academico/course_evaluations/1.json
  def update
    respond_to do |format|
      @academico_course_evaluation.academico_staffs_id = get_staff_id_for_user
      if @academico_course_evaluation.update(academico_course_evaluation_params)
        format.html { redirect_to @academico_course_evaluation, notice: 'Course evaluation was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_course_evaluation }
      else
        format.html { render :edit }
        format.json { render json: @academico_course_evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/course_evaluations/1
  # DELETE /academico/course_evaluations/1.json
  def destroy
    @academico_course_evaluation.destroy
    respond_to do |format|
      format.html { redirect_to academico_course_evaluations_url, notice: 'Course evaluation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_course_evaluation
      @academico_course_evaluation = Academico::CourseEvaluation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_course_evaluation_params
      params.require(:academico_course_evaluation).permit(:academico_courses_id, academico_session_evaluations_attributes: [:id, :academico_course_evaluations_id, :academico_sessions_id, :_destroy,
        academico_student_evaluations_attributes: [:id, :academico_session_evaluations_id, :academico_students_id, :_destroy,
          academico_student_scores_attributes: [:id, :academico_student_evaluations_id, :academico_session_assignments_id, :score]]])
    end

    def resource_params
      academico_course_evaluation_params
    end

    def fetch_courses(is_new)
      teacher_id = get_staff_id_for_user
      @courses = Academico::Course.courses_from_teacher teacher_id, is_new
      @courses
    end
end
