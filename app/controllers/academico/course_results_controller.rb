class Academico::CourseResultsController < Academico::AuthorizeController
  before_action :set_academico_course_result, only: [:show, :edit, :update, :destroy]

  # GET /academico/course_results
  # GET /academico/course_results.json
  def index
    @academico_course_results = Academico::CourseResult.all.paginate(:page => params[:page])
  end

  # GET /academico/course_results/1
  # GET /academico/course_results/1.json
  def show
    @academico_course_result.academico_student_results.each do |student_result|
      student_result.teachers_evaluations
    end
  end

  # GET /academico/course_results/new
  def new
    @academico_course_result = Academico::CourseResult.new
    @academico_course_result.academico_staffs_id = current_user.academico_staff.id
    @academico_course_result.academico_courses_id = params[:course_id] if params[:course_id].present?
    load_active_courses
  end

  # GET /academico/course_results/1/edit
  def edit
    load_active_courses
    @academico_course_result.academico_course.student_registrations.each do |student|
      student_result = @academico_course_result.academico_student_results.select {|r| r.academico_students_id == student.id}
      if student_result.empty?
        student_result = @academico_course_result.academico_student_results.build(academico_course_results_id: @academico_course_result.id, academico_students_id: student.id)
      else
        student_result = student_result.first
      end
      student_result.teachers_evaluations
    end
  end

  # POST /academico/course_results
  # POST /academico/course_results.json
  def create
    @academico_course_result = Academico::CourseResult.new(academico_course_result_params)

    respond_to do |format|
      if @academico_course_result.save
        format.html { redirect_to edit_academico_course_result_path(@academico_course_result)}
        format.json { render :show, status: :created, location: @academico_course_result }
      else
        load_active_courses
        format.html { render :new }
        format.json { render json: @academico_course_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/course_results/1
  # PATCH/PUT /academico/course_results/1.json
  def update
    respond_to do |format|
      if @academico_course_result.update(academico_course_result_params)
        format.html { redirect_to @academico_course_result, notice: 'Course result was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_course_result }
      else
        load_active_courses
        format.html { render :edit }
        format.json { render json: @academico_course_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/course_results/1
  # DELETE /academico/course_results/1.json
  def destroy
    @academico_course_result.destroy
    respond_to do |format|
      format.html { redirect_to academico_course_results_url, notice: 'Course result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_course_result_ids].each do |id|
      @academico_course_result = Academico::CourseResult.find(id)
      @academico_course_result.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_course_results_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_course_result
      @academico_course_result = Academico::CourseResult.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_course_result_params
      params.require(:academico_course_result).permit(:academico_courses_id, :academico_staffs_id, academico_student_results_attributes: [:id, :academico_students_id, :academico_course_results_id, :final_grade, :approved])
    end

    def resource_params
      academico_course_result_params
    end

    def load_active_courses
      @courses = Academico::Course.status_eq(Academico::CourseStatus::FINISHED)
    end
end
