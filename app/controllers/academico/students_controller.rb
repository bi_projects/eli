class Academico::StudentsController < Academico::AuthorizeController
  before_action :set_academico_student, only: [:show, :edit, :update, :destroy]
  skip_load_and_authorize_resource only: :facturacion_preregistration
  include Academico::FormHelper
  # GET /academico/students
  # GET /academico/students.json
  def index
    @academico_students = filtering_params(params).paginate(:page => params[:page])
  end

  def filtering_params(params)
    if params[:filter].present?
      Academico::Student.filter(params[:filter].slice(:first_name_like, :last_name_like))
    else
      Academico::Student.all
    end
  end

  # GET /academico/students/1
  # GET /academico/students/1.json
  def show
    @academico_registrations = @academico_student.academico_registrations
                                                 .includes(academico_course: [:academico_level_program])
                                                 .order('academico_courses.start_date DESC')
  end

  # GET /academico/students/new
  def new
    @academico_student = Academico::Student.new
    @programs = Academico::Program.all
    @levels = Academico::Level.all
    @academico_student.build_academico_preregistration
    @offices = Admin::BranchOffice.all
    redirect_to academico_students_url
  end

  # GET /academico/students/1/edit
  def edit
    @academico_student = Academico::Student.find(params[:id])
    @programs = Academico::Program.all
    @levels = Academico::Level.all
    if @academico_student.academico_preregistration.nil?
      @academico_student.build_academico_preregistration
    end
    @offices = Admin::BranchOffice.all
  end

  # POST /academico/students
  # POST /academico/students.json
  def create
    @academico_student = Academico::Student.new(academico_student_params)

    respond_to do |format|
      if @academico_student.save
        format.html { redirect_to @academico_student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @academico_student }
      else
        format.html { render :new }
        format.json { render json: @academico_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/students/1
  # PATCH/PUT /academico/students/1.json
  def update
    respond_to do |format|
      if @academico_student.update(academico_student_params)
        format.html { redirect_to @academico_student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_student }
      else
        format.html { render :edit }
        format.json { render json: @academico_student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/students/1
  # DELETE /academico/students/1.json
  def destroy
    @academico_student.destroy
    respond_to do |format|
      format.html { redirect_to academico_students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_student_ids].each do |id|
      @eli_resource = Academico::Student.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_students_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  def facturacion_preregistration
    @facturacion_preregistration = Facturacion::Preregistration.find(params[:preregistration_id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_student
      @academico_student = Academico::Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_student_params
      params.fetch(:academico_student, {})
      params.require(:academico_student).permit(:first_name, :last_name, :birth_date, :email, :phone, :address, :admin_branch_office_id, :student_code, :facturacion_preregistration_id, :level_up_id)
    end

    def resource_params
      academico_student_params
    end
end
