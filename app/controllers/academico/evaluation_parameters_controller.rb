class Academico::EvaluationParametersController < Academico::AuthorizeController
  before_action :set_academico_evaluation_parameter, only: [:show, :edit, :update, :destroy]

  # GET /academico/evaluation_parameters
  # GET /academico/evaluation_parameters.json
  def index
    @academico_evaluation_parameters = Academico::EvaluationParameter.all
  end

  # GET /academico/evaluation_parameters/1
  # GET /academico/evaluation_parameters/1.json
  def show
  end

  # GET /academico/evaluation_parameters/new
  def new
    @academico_evaluation_parameter = Academico::EvaluationParameter.new
  end

  # GET /academico/evaluation_parameters/1/edit
  def edit
  end

  # POST /academico/evaluation_parameters
  # POST /academico/evaluation_parameters.json
  def create
    @academico_evaluation_parameter = Academico::EvaluationParameter.new(academico_evaluation_parameter_params)

    respond_to do |format|
      if @academico_evaluation_parameter.save
        format.html { redirect_to @academico_evaluation_parameter, notice: 'Evaluation parameter was successfully created.' }
        format.json { render :show, status: :created, location: @academico_evaluation_parameter }
      else
        format.html { render :new }
        format.json { render json: @academico_evaluation_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/evaluation_parameters/1
  # PATCH/PUT /academico/evaluation_parameters/1.json
  def update
    respond_to do |format|
      if @academico_evaluation_parameter.update(academico_evaluation_parameter_params)
        format.html { redirect_to @academico_evaluation_parameter, notice: 'Evaluation parameter was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_evaluation_parameter }
      else
        format.html { render :edit }
        format.json { render json: @academico_evaluation_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/evaluation_parameters/1
  # DELETE /academico/evaluation_parameters/1.json
  def destroy
    @academico_evaluation_parameter.destroy
    respond_to do |format|
      format.html { redirect_to academico_evaluation_parameters_url, notice: 'Evaluation parameter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_evaluation_parameter_ids].each do |id|
      @academico_evaluation_parameter = Academico::EvaluationParameter.find(id)
      @academico_evaluation_parameter.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_evaluation_parameters_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_evaluation_parameter
      @academico_evaluation_parameter = Academico::EvaluationParameter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_evaluation_parameter_params
      params.require(:academico_evaluation_parameter).permit(:name, :academico_evaluation_parameter_type_id)
    end

    def resource_params
      academico_evaluation_parameter_params
    end
end
