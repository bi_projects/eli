class Academico::ApplicationController < ApplicationController
	protect_from_forgery with: :exception
	before_action :set_page_title, :authenticate_academico_academic_user!
	helper_method :end_session_path, :current_user, :session_root_path, :edit_eli_user_path
	include Academico::ApplicationHelper

	def controller?(*controller)
		controller.include?(params[:controller])
	end

	def action?(*action)
		action.include?(params[:action])
	end

	private

	def set_page_title
		@page_title = "Academic Management"
	end

	def current_user
		current_academico_academic_user
	end

	def end_session_path
		destroy_academico_academic_user_session_path
	end

	def session_root_path
		academico_root_path
	end

	def edit_eli_user_path
		edit_academico_academic_user_registration_path
	end
end
