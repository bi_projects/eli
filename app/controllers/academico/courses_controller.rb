class Academico::CoursesController < Academico::AuthorizeController
  include CurrentSession
  include Academico::CoursesHelper
  skip_load_and_authorize_resource only: [:evaluation_parameters_by_course, :time_frame_related_info, :lookup_classrooms]
  before_action :set_academico_course, only: [:show, :edit, :update, :destroy]

  # GET /academico/courses
  # GET /academico/courses.json
  def index
    @academico_courses = filtering_params(params).paginate(:page => params[:page])
  end

  def filtering_params(params)
    if params[:filter].present?
      Academico::Course.filter(params[:filter].slice(:status_eq, :level_eq, :program_eq, :start_date_begins, :start_date_ends, :end_date_begins, :end_date_ends, :schedulle_eq, :staff_eq))
    else
      Academico::Course.all
    end
  end

  # GET /academico/courses/1
  # GET /academico/courses/1.json
  def show
  end

  # GET /academico/courses/new
  def new
    @academico_course = Academico::Course.new
    load_new
  end

  # GET /academico/courses/1/edit
  def edit
    @academico_course = Academico::Course.find(params[:id])
    load_edit
  end

  # POST /academico/courses
  # POST /academico/courses.json
  def create
    @academico_course = Academico::Course.new(academico_course_params)
    @academico_course.created_by = get_staff_id_for_user
      respond_to do |format|
        begin
          if @academico_course.save
            format.html { redirect_to @academico_course, notice: 'Course was successfully created.' }
            format.json { render :show, status: :created, location: @academico_course }
          else
            load_new
            format.html { render :new, object: @academico_course }
            format.json { render json: @academico_course.errors, status: :unprocessable_entity }
          end
        rescue Exception => e
          flash[:alert] = e.to_s
          flash.keep(:alert)
          load_new
          format.html { render :new, object: @academico_course }
          format.json { render json: @academico_course.errors, status: :unprocessable_entity }
        end
      end
  end

  # PATCH/PUT /academico/courses/1
  # PATCH/PUT /academico/courses/1.json
  def update
    respond_to do |format|
      begin
        if @academico_course.update(academico_course_params.merge({updated_by: get_staff_id_for_user}))
          format.html { redirect_to @academico_course, notice: 'Course was successfully updated.' }
          format.json { render :show, status: :ok, location: @academico_course }
        else
          load_edit
          format.html { render :edit, object: @academico_course }
          format.json { render json: @academico_course.errors, status: :unprocessable_entity }
        end
      rescue Exception => e
        flash[:alert] = e.to_s
        flash.keep(:alert)
        load_edit
        format.html { render :edit, object: @academico_course }
        format.json { render json: @academico_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/courses/1
  # DELETE /academico/courses/1.json
  def destroy
    @academico_course.destroy
    respond_to do |format|
      format.html { redirect_to academico_courses_url, notice: 'Course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_course_ids].each do |id|
      @academico_course = Academico::Course.find(id)
      @academico_course.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_courses_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  def evaluation_parameters_by_course
    authorize! :index, Academico::Course
    @course_parameters = Academico::EvaluationParameter
    .parameters_in_course_scheme(params[:course_id])
    respond_to do |format|
      format.json { render json: @course_parameters }
    end
  end

  def time_frame_related_info
    authorize! :index, Academico::Course
    @time_frame = Academico::TimeFrame.find(params[:time_frame_id])
  end

  def lookup_classrooms
    authorize! :edit, Academico::Course
    rooms = Academico::Room.rooms_by_office(params[:office_id])
    respond_to do |format|
      format.json { render json: rooms }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_course
      @academico_course = Academico::Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_course_params
      params.require(:academico_course).permit(:academico_level_programs_id, :academico_staff_id, :second_academico_staff_id, :name,
      :academico_course_status_id, :academico_room_id, :created_by, :updated_by, :academico_time_frame_id, :facturacion_book_id,
      academico_registrations_attributes: [:id, :academico_courses_id, :academico_students_id, :academico_registration_statuses_id, :_destroy])
    end

    def resource_params
      academico_course_params
    end

    def load_new
      @potential_teachers = Academico::Staff
        .staff_rotation
        .staff_by_status(Academico::StaffStatus::ACTIVE)
    end

    def load_edit
      fetch_candidates(@academico_course)
      @potential_teachers = Academico::Staff
      .staff_rotation
      .staff_by_skill(@academico_course.academico_level_programs_id)
    end
end
