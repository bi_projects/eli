class Academico::AcademicUsers::RegistrationsController < Devise::RegistrationsController
    before_action :set_page_title, :authenticate_facturacion_billing_user!
    helper_method :end_session_path, :current_user, :session_root_path, :edit_eli_user_path
    include Academico::ApplicationHelper
    layout "academico/dashboard_layout"

    # GET /resource/sign_up
    def new
        redirect_to academico_root_path
    end

    # POST /resource
    # def create
    #   super
    # end

    # GET /resource/edit
    # def edit
    #   super
    # end

    # PUT /resource
    def update
       super
    end

    # DELETE /resource
    # def destroy
    #   super
    # end

    # GET /resource/cancel
    # Forces the session data which is usually expired after sign
    # in to be expired now. This is useful if the user wants to
    # cancel oauth signing in/up in the middle of the process,
    # removing all OAuth session data.
    # def cancel
    #   super
    # end

    def controller?(*controller)
        controller.include?(params[:controller])
    end

    def action?(*action)
        action.include?(params[:action])
    end

    private

    def set_page_title
        @page_title = "Academic Management"
    end

    def current_user
        current_academico_academic_user
    end

    def end_session_path
        destroy_academico_academic_user_session_path
    end

    def session_root_path
        academico_root_path
    end

    def edit_eli_user_path
        edit_academico_academic_user_registration_path
    end

    protected

    def after_update_path_for(resource)
        edit_academico_academic_user_registration_path(resource)
    end

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_sign_up_params
    #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
    # end

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_account_update_params
    #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
    # end

    # The path used after sign up.
    # def after_sign_up_path_for(resource)
    #   super(resource)
    # end

    # The path used after sign up for inactive accounts.
    # def after_inactive_sign_up_path_for(resource)
    #   super(resource)
    # end
end
