class Academico::StaffsController < Academico::AuthorizeController
  skip_load_and_authorize_resource only: :staff_by_level_skill
  before_action :set_academico_staff, only: [:show, :edit, :update, :destroy]
  def index
    #authorize! :index, 'Academico::AcademicUser'
    @academico_staffs = Academico::Staff.all
  end

  # GET /academico/staffs/1
  # GET /academico/staffs/1.json
  def show
  end

  # GET /academico/staffs/new
  def new
    @academico_staff = Academico::Staff.new

    load_form_data
    @academico_staff_users = @academico_staff_users.unasigned_users
    Academico::LevelProgram.all.each do |level_program|
      @academico_staff.academico_level_skills.build(academico_staffs_id: @academico_staff.id, academico_level_programs_id: level_program.id).academico_level_program = level_program
    end
  end

  # GET /academico/staffs/1/edit
  def edit
    @academico_staff = Academico::Staff.find(params[:id])
    @academico_staff_users = Academico::AcademicUser.users_in_staff([1, 2, 3])

    load_form_data
    Academico::LevelProgram.all.each do |level_program|
      found = @academico_staff.academico_level_skills.select {|s| s.academico_staffs_id == @academico_staff.id && s.academico_level_programs_id == level_program.id}
      if found.empty?
        @academico_staff.academico_level_skills.build(academico_staffs_id: @academico_staff.id, academico_level_programs_id: level_program.id).academico_level_program = level_program
      end
    end
  end

  # POST /academico/staffs
  # POST /academico/staffs.json
  def create
    @academico_staff = Academico::Staff.new(academico_staff_params)

    respond_to do |format|
      if @academico_staff.save
        format.html { redirect_to @academico_staff, notice: 'Staff was successfully created.' }
        format.json { render :show, status: :created, location: @academico_staff }
      else
        load_form_data
        @academico_staff_users = @academico_staff_users.unasigned_users
        format.html { render :new }
        format.json { render json: @academico_staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/staffs/1
  # PATCH/PUT /academico/staffs/1.json
  def update
    respond_to do |format|
      if @academico_staff.update(academico_staff_params)
        format.html { redirect_to @academico_staff, notice: 'Staff was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_staff }
      else
        load_form_data
        format.html { render :edit }
        format.json { render json: @academico_staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/staffs/1
  # DELETE /academico/staffs/1.json
  def destroy
    @academico_staff.destroy
    respond_to do |format|
      format.html { redirect_to academico_staffs_url, notice: 'Staff was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_staff_ids].each do |id|
      @eli_resource = Academico::Staff.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_staffs_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  def staff_by_level_skill
    staff_members = Academico::Staff
    .staff_rotation
    .staff_by_skill(params[:level_program_id])
    @staff_candidates = staff_members.map {|staff| staff.serializable_hash.merge(:display_name => staff.display_name) }
    respond_to do |format|
      format.json { render json: @staff_candidates }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_staff
      @academico_staff = Academico::Staff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_staff_params
      params.require(:academico_staff).permit(:academico_staff_statuses_id, :academico_academic_users_id, :academico_staffs_id, academico_level_skills_attributes: [:id, :academico_staffs_id, :academico_level_programs_id, :_destroy])
    end

    def resource_params
      academico_staff_params
    end

    def load_statuses
      @staff_statuses = Academico::StaffStatus.all
    end

    def load_staff_users
      @academico_staff_users = Academico::AcademicUser.users_in_staff(
        [Admin::Position::TEACHER, Admin::Position::COORDINATOR, Admin::Position::SUPERVISOR]
      )
    end

    def load_liables
      @staff_liables = Academico::Staff.staff_by_positions(
        [Admin::Position::COORDINATOR, Admin::Position::SUPERVISOR]
      )
    end

    def load_levels
      @levels = Academico::Level.all.order(:level_sequence)
    end

    def load_programs
      @programs = Academico::Program.all.order(:id)
    end

    def load_form_data
      load_statuses
      load_staff_users
      load_liables
      load_levels
      load_programs
    end
end
