class Academico::AgeRangesController < Academico::AuthorizeController
  before_action :set_academico_age_range, only: [:show, :edit, :update, :destroy]
  layout "academico/dashboard_layout"
  # GET /academico/age_ranges
  # GET /academico/age_ranges.json
  def index
    @academico_age_ranges = Academico::AgeRange.all
  end

  # GET /academico/age_ranges/1
  # GET /academico/age_ranges/1.json
  def show
  end

  # GET /academico/age_ranges/new
  def new
    @academico_age_range = Academico::AgeRange.new
  end

  # GET /academico/age_ranges/1/edit
  def edit
  end

  # POST /academico/age_ranges
  # POST /academico/age_ranges.json
  def create
    @academico_age_range = Academico::AgeRange.new(academico_age_range_params)

    respond_to do |format|
      if @academico_age_range.save
        format.html { redirect_to @academico_age_range, notice: 'Age range was successfully created.' }
        format.json { render :show, status: :created, location: @academico_age_range }
      else
        format.html { render :new }
        format.json { render json: @academico_age_range.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/age_ranges/1
  # PATCH/PUT /academico/age_ranges/1.json
  def update
    respond_to do |format|
      if @academico_age_range.update(academico_age_range_params)
        format.html { redirect_to @academico_age_range, notice: 'Age range was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_age_range }
      else
        format.html { render :edit }
        format.json { render json: @academico_age_range.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/age_ranges/1
  # DELETE /academico/age_ranges/1.json
  def destroy
    @academico_age_range.destroy
    respond_to do |format|
      format.html { redirect_to academico_age_ranges_url, notice: 'Age range was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_age_range_ids].each do |id|
      @eli_resource = Academico::AgeRange.find(id)
      @eli_resource.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_age_ranges_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_age_range
      @academico_age_range = Academico::AgeRange.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_age_range_params
      params.require(:academico_age_range).permit(:name, :min_age, :max_age, :description)
    end

    def resource_params
      academico_age_range_params
    end
end
