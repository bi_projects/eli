class Academico::CourseStudentReportsController < Academico::AuthorizeController
  include Reports
  before_action :scoped_collection, only: [:index, :xlsx_file, :pdf_file]
  skip_load_and_authorize_resource only: [:index, :xlsx_file, :pdf_file]

  def scoped_collection
    if params[:search].blank?
      @show_results = false
    else
      @show_results = true
      params[:search] ||= {}
      date_start = params[:search][:date_gteq]
      date_end = params[:search][:date_lteq]
      branch_office_id = params[:search][:branch_office_id]
      course_schedulle_id = params[:search][:course_schedulle_id]
      level_id = params[:search][:level_id]
      program_id = params[:search][:program_id]
      staff_id = params[:search][:staff_id]

      @students = Academico::Student.students_by_course(
        start_date: date_start,
        end_date: date_end,
        branch_office_id: branch_office_id,
        course_schedulle_id: course_schedulle_id,
        level_id: level_id,
        program_id: program_id,
        staff_id: staff_id)

      @date_start = date_start
      @date_end = date_end
      if params.try(:[], :search).try(:[], :branch_office_id).present?
        @branch_office = Admin::BranchOffice.find(params[:search][:branch_office_id])
      end
      if params.try(:[], :search).try(:[], :course_schedulle_id).present?
        @schedulle = Academico::CourseSchedulle.find(params[:search][:course_schedulle_id])
      end
      if params.try(:[], :search).try(:[], :level_id).present?
        @level = Academico::Level.find(params[:search][:level_id])
      end
      if params.try(:[], :search).try(:[], :program_id).present?
        @program = Academico::Program.find(params[:search][:program_id])
      end
      if params.try(:[], :search).try(:[], :staff_id).present?
        @staff = Academico::Staff.find(params[:search][:staff_id])
      end
    end
  end

  def index
    authorize! :index, Academico::CourseStudentReportsController
  end

  def xlsx_file
    authorize! :index, Academico::StudentReportsController
    respond_to do |format|
      format.xlsx do
        render(
          xlsx: 'index',
          filename: "students_by_course_#{Date.today}.xlsx" 
        )
      end
    end
  end

  def pdf_file
    authorize! :index, Academico::StudentReportsController
    pdf_settings = configure_pdf 'academico/course_student_reports/index', @students, 'Landscape'
    report_data = render_to_string(pdf_settings)
    send_data(report_data, filename: "students_by_course_#{Date.today}.pdf", type: "application/pdf")
  end
end
