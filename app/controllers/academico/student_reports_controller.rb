class Academico::StudentReportsController < Academico::AuthorizeController
  include Reports
  before_action :scoped_collection, only: [:index, :xlsx_file]
  skip_load_and_authorize_resource only: [:index, :xlsx_file]

  def scoped_collection
    if params[:search].blank?
      @show_results = false
    else
      @show_results = true
      params[:search] ||= {}
      date_start = params[:search][:date_gteq]
      date_end = params[:search][:date_lteq]
      branch_office_id = params[:search][:branch_office_id]
      course_schedulle_id = params[:search][:course_schedulle_id]

      @students = Academico::Student.students_summary(
        start_date: date_start,
        end_date: date_end,
        branch_office_id: branch_office_id,
        course_schedulle_id: course_schedulle_id)

      @date_start = date_start
      @date_end = date_end
      if params.try(:[], :search).try(:[], :branch_office_id).present?
        @branch_office = Admin::BranchOffice.find(params[:search][:branch_office_id])
      end
      if params.try(:[], :search).try(:[], :course_schedulle_id).present?
          @schedulle = Academico::CourseSchedulle.find(params[:search][:course_schedulle_id])
      end
    end
  end

  def index
    authorize! :index, Academico::StudentReportsController
  end

  def xlsx_file
    authorize! :index, Academico::StudentReportsController
    respond_to do |format|
      format.xlsx do
        render(
          xlsx: 'index',
          filename: "students_summary_#{Date.today}.xlsx" 
        )
      end
    end
  end
end
