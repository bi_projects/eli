class Academico::TemariesController < Academico::AuthorizeController
  before_action :set_academico_temary, only: [:show, :edit, :update, :destroy]
  skip_load_and_authorize_resource only: :session_parameters

  # GET /academico/temaries
  # GET /academico/temaries.json
  def index
    @academico_temaries = Academico::Temary.all.paginate(:page => params[:page])
  end

  # GET /academico/temaries/1
  # GET /academico/temaries/1.json
  def show
  end

  # GET /academico/temaries/new
  def new
    @academico_temary = Academico::Temary.new
    @courses = Academico::Course.all
    @course_parameters = []
    @academico_temary.academico_course = Academico::Course.find(params[:course_id]) if params[:course_id].present?
    #precarga de sessiones de clase
    (1..9).step() do |index|
      @academico_temary.academico_sessions.build
    end
    redirect_to academico_temaries_url
  end

  # GET /academico/temaries/1/edit
  def edit
    @courses = Academico::Course.all
    #

    evaluation_parameters = @academico_temary.academico_course.academico_level_program.academico_evaluation_level_programs.first.academico_evaluation_scheme.academico_evaluation_methods.pluck(:academico_evaluation_parameter_id)
    @academico_temary.academico_sessions.each do |session|
      evaluation_parameters.each do |parameter|
        found = session.academico_session_parameters.select {|p| p.academico_evaluation_parameter_id == parameter}
        if found.empty?
          session.academico_session_parameters.build(academico_evaluation_parameter_id: parameter, academico_session_id: session.id)
        end
      end
    end
  end

  # POST /academico/temaries
  # POST /academico/temaries.json
  def create
    @academico_temary = Academico::Temary.new(academico_temary_params)
    @academico_temary.created_by_user = current_user

    respond_to do |format|
      if @academico_temary.save
        format.html { redirect_to @academico_temary, notice: 'Temary was successfully created.' }
        format.json { render :show, status: :created, location: @academico_temary }
      else
        format.html { render :new }
        format.json { render json: @academico_temary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/temaries/1
  # PATCH/PUT /academico/temaries/1.json
  def update
    respond_to do |format|
      if @academico_temary.update(academico_temary_params)
        format.html { redirect_to @academico_temary, notice: 'Temary was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_temary }
      else
        format.html { render :edit }
        format.json { render json: @academico_temary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/temaries/1
  # DELETE /academico/temaries/1.json
  def destroy
    @academico_temary.destroy
    respond_to do |format|
      format.html { redirect_to academico_temaries_url, notice: 'Temary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_temary_ids].each do |id|
      @academico_temary = Academico::Temary.find(id)
      @academico_temary.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_temaries_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  def session_parameters
    current_index =
     params[:current_index]
    course_parameters = Academico::EvaluationParameter
    .parameters_in_course_scheme(params[:course_id])
    academico_session = Academico::Session.new()
    course_parameters.each do |parameter|
      academico_session.academico_session_parameters.build(academico_evaluation_parameter_id: parameter.id)
    end
    respond_to do |format|
      template_content = render_to_string(partial: 'academico_session_parameters', formats: [:html], layout: false, locals: {academico_session: academico_session, first_index: current_index })
      format.html { render :html => template_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_temary
      @academico_temary = Academico::Temary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_temary_params
      params.require(:academico_temary).permit(:name, :academico_courses_id,
        academico_sessions_attributes: [:id, :academico_temary_id, :topic, :schedulle, {academico_evaluation_parameter_ids: []}, :_destroy,
          academico_session_parameters_attributes: [:id, :academico_evaluation_parameter_id, :academico_session_id, :_destroy,
            academico_session_assignments_attributes: [:id, :academico_session_parameters_id, :description, :scale, :_destroy]
            ]
          ])
    end

    def resource_params
      academico_temary_params
    end
end
