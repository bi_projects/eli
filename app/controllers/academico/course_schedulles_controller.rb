class Academico::CourseSchedullesController < Academico::AuthorizeController
  before_action :set_academico_course_schedulle, only: [:show, :edit, :update, :destroy]

  # GET /academico/course_schedulles
  # GET /academico/course_schedulles.json
  def index
    @academico_course_schedulles = Academico::CourseSchedulle.all
  end

  # GET /academico/course_schedulles/1
  # GET /academico/course_schedulles/1.json
  def show
  end

  # GET /academico/course_schedulles/new
  def new
    @academico_course_schedulle = Academico::CourseSchedulle.new
  end

  # GET /academico/course_schedulles/1/edit
  def edit
  end

  # POST /academico/course_schedulles
  # POST /academico/course_schedulles.json
  def create
    @academico_course_schedulle = Academico::CourseSchedulle.new(academico_course_schedulle_params)

    respond_to do |format|
      if @academico_course_schedulle.save
        format.html { redirect_to @academico_course_schedulle, notice: 'Course schedulle was successfully created.' }
        format.json { render :show, status: :created, location: @academico_course_schedulle }
      else
        format.html { render :new }
        format.json { render json: @academico_course_schedulle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/course_schedulles/1
  # PATCH/PUT /academico/course_schedulles/1.json
  def update
    respond_to do |format|
      if @academico_course_schedulle.update(academico_course_schedulle_params)
        format.html { redirect_to @academico_course_schedulle, notice: 'Course schedulle was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_course_schedulle }
      else
        format.html { render :edit }
        format.json { render json: @academico_course_schedulle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/course_schedulles/1
  # DELETE /academico/course_schedulles/1.json
  def destroy
    @academico_course_schedulle.destroy
    respond_to do |format|
      format.html { redirect_to academico_course_schedulles_url, notice: 'Course schedulle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_course_schedulle_ids].each do |id|
      @academico_course_schedulle = Academico::CourseSchedulle.find(id)
      @academico_course_schedulle.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_course_schedulles_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_course_schedulle
      @academico_course_schedulle = Academico::CourseSchedulle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_course_schedulle_params
      params.require(:academico_course_schedulle).permit(:begins, :ends, :disabled)
    end

    def resource_params
      academico_course_schedulle_params
    end
end
