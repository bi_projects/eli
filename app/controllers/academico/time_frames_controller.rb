class Academico::TimeFramesController < Academico::AuthorizeController
  before_action :set_academico_time_frame, only: [:show, :edit, :update, :destroy]

  # GET /academico/time_frames
  # GET /academico/time_frames.json
  def index
    @academico_time_frames = Academico::TimeFrame.all.paginate(:page => params[:page])
  end

  # GET /academico/time_frames/1
  # GET /academico/time_frames/1.json
  def show
  end

  # GET /academico/time_frames/new
  def new
    @academico_time_frame = Academico::TimeFrame.new
  end

  # GET /academico/time_frames/1/edit
  def edit
  end

  # POST /academico/time_frames
  # POST /academico/time_frames.json
  def create
    @academico_time_frame = Academico::TimeFrame.new(academico_time_frame_params)

    respond_to do |format|
      if @academico_time_frame.save
        format.html { redirect_to @academico_time_frame, notice: 'Time frame was successfully created.' }
        format.json { render :show, status: :created, location: @academico_time_frame }
      else
        format.html { render :new }
        format.json { render json: @academico_time_frame.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academico/time_frames/1
  # PATCH/PUT /academico/time_frames/1.json
  def update
    respond_to do |format|
      if @academico_time_frame.update(academico_time_frame_params)
        format.html { redirect_to @academico_time_frame, notice: 'Time frame was successfully updated.' }
        format.json { render :show, status: :ok, location: @academico_time_frame }
      else
        format.html { render :edit }
        format.json { render json: @academico_time_frame.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academico/time_frames/1
  # DELETE /academico/time_frames/1.json
  def destroy
    @academico_time_frame.destroy
    respond_to do |format|
      format.html { redirect_to academico_time_frames_url, notice: 'Time frame was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def destroy_multiple
    params[:academico_time_frame_ids].each do |id|
      @academico_time_frame = Academico::TimeFrame.find(id)
      @academico_time_frame.destroy
    end
    respond_to do |format|
      format.html { redirect_to academico_time_frames_path, notice: 'Registros eliminados exitosamente.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academico_time_frame
      @academico_time_frame = Academico::TimeFrame.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academico_time_frame_params
      params.require(:academico_time_frame).permit(:year, :number, :name, :admin_branch_office_id, :start_date, :end_date, :academico_course_modality_id, :academico_course_schedulle_id, :academico_time_frame_status_id)
    end

    def resource_params
      academico_time_frame_params
    end
end
