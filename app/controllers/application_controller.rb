class ApplicationController < ActionController::Base
	before_action :configure_account_update_params, if: :devise_controller?
	protect_from_forgery with: :exception
	skip_before_action :verify_authenticity_token, if: :devise_controller?

	rescue_from ActiveRecord::InvalidForeignKey, CustomExceptions::RecordAssociationException do |ex|
		respond_to do |format|
			eli_resource = controller_path.classify.gsub('::', '').underscore
			eli_resource_ref = instance_variable_get("@#{eli_resource}")
			format.html { redirect_to({controller: controller_name, action: "index"}, notice: eli_resource_ref.errors.messages[:base].first || ex.to_s)}
			format.json { head :no_content }
		end
	end

	def configure_account_update_params
		devise_parameter_sanitizer.permit(:account_update, keys: [:name, :avatar])
	end

	private
	def minify(content)
		Uglifier.new.compile(content)
	end
end
