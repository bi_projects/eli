module FacturacionUtilityController
  extend ActiveSupport::Concern

  included do
    before_action :validate_daily_process, only: %i[new edit update create destroy]
  end

  private

  def validate_daily_process
    if Facturacion::DailyProcess.exists_daily_process?(get_date)
      redirect_to Rails.application.routes.recognize_path(['facturacion/', controller_name].join),
                  notice: "El período #{ get_date.to_date.strftime("%d/%m/%Y") } esta cerrado."
    end
  end

  def get_date
    if params[:id].present?
      document = model_class.find_by_id(params[:id])
      document.send([model_name.downcase, '_date'].join.to_sym)
    else
      Date.current
    end
  end

  def model_class
    ['Facturacion::', model_name].join.constantize
  end

  def model_name
    controller_name.singularize.titlecase
  end
end
