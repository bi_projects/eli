module Reports
  extend ActiveSupport::Concern

  def get_date_range(month, year)
    if month.present? && year.present?
      date = Date.new(year.to_i, month.to_i)
      [date.beginning_of_month.to_s, date.end_of_month.to_s]
    end
  end

  def configure_pdf(view, data, orientation = 'Portrait')
    { pdf: view,
      template: view,
      orientation: orientation,
      locals: {
        data: data,
        stylesheet: "pdf.css" },
      encoding: "UTF-8",
      page_size: 'A4',
      footer: {
        left: "Impreso el #{I18n.l(Time.now, format: :long)}",
        right: 'Pag. [page] de [topage]',
        font_size: 7
      }
    }
  end
end