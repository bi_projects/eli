module CurrentSession
  extend ActiveSupport::Concern

  def get_staff_id_for_user
    current_user.academico_staff.id if current_user.academico_staff.present?
  end
end
