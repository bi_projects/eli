class CustomExceptions::InvalidDateFormatException < StandardError
  def http_status
    403
  end

  def initialize(msg=nil)
    @message = msg
  end

  def code
    'Invalid Argument'
  end

  def message
    if @message.present?
      @message
    else
      "El formato de la fecha no es correcto, el formato debe ser dd/mm/yyyy"
    else
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

