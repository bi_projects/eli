class CustomExceptions::InvalidFileException < StandardError
  def http_status
    403
  end

  def initialize(msg=nil)
    @message = msg
  end

  def code
    'Invalid Argument'
  end

  def message
    if @message.present?
      @message
    else
      "Formato de archivo incorrecto, archivo debe ser un XLSX"
    end
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

