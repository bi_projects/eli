class CustomExceptions::DocumentException < StandardError
  def http_status
    403
  end

  def code
    'not_allowed'
  end

  def message
    "Invalid Action On Document"
  end

  def to_hash
    {
      message: message,
      code: code
    }
  end
end

