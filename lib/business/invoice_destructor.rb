require_relative "document_destructor"
module Business
  class InvoiceDestructor < DocumentDestructor

    def initialize(invoice)
      @invoice = invoice
    end

    def destroy
      @invoice.facturacion_invoice_statuses_id = Facturacion::InvoiceStatus::VOID
      @invoice.deregister_student
      @invoice.facturacion_invoice_details.each {|detail| detail.inactivate}
      @invoice.void_transactions
      @invoice.save
    end
  end
end