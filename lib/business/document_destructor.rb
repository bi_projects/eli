module Business
  class DocumentDestructor

    # soft destroy marks record as void only, don't delete anything
    def destroy
    end
  end
end