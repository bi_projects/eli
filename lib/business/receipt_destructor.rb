require_relative "document_destructor"
module Business
  class ReceiptDestructor < DocumentDestructor

    def initialize(receipt)
      @receipt = receipt
    end

    def destroy
      @receipt.facturacion_receipt_statuses_id = Facturacion::ReceiptStatus::VOID
      @receipt.void_effects
      @receipt.save
    end
  end
end