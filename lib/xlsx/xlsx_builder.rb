require 'simple_xlsx_reader'
class Xlsx::XlsxBuilder

  def initialize(data)
    @data = data
  end
  #Name with extension >>>>>>>>>>> File.basename(file)
  #Extension name >>>>>>>>>>>>>>>> File.extname(file)
  #Name without extension >>>>>>>> File.basename(file, extn)
  #PATH >>>>>>>>>>>>>>>>>>>>>>>>>> File.dirname(file)
  def bcn_data_storage
    ActiveRecord::Base.transaction do 
      begin
        file_data = data[:file_data]
        directory = "#{Rails.root}/public/uploads/xlsx"
        Dir::mkdir(directory) if !Dir.exist?(directory)
        if File.extname(file_data.path) != ".xlsx"
          raise CustomExceptions::InvalidFileException
        end
        
        File.open(data[:destination_path], "w+b") do  |f| 
          if file_data.respond_to?(:read)
          f.write(file_data.read)
            puts "file_data.read"
          elsif file_data.respond_to?(:path)
            f.write(File.read(file_data.path))
            puts "File.read(file_data.path)"
          else
            raise CustomExceptions::InvalidFileException
          end
        end

        doc = SimpleXlsxReader.open(data[:destination_path])

        doc.sheets.first.data.each do |exchange_rate|
          date = exchange_rate[0]
          value = exchange_rate[1]
          file_name = File.basename(file_data.path)

          begin
            validDate = date_formatter(date.to_s);
            exchange_instance =  Admin::ExchangeRate.find_by_date(validDate)
            if exchange_instance.present?
              exchange_instance.update(:date => validDate, :value => value, :file_name => file_name)
            else  
              if date.present? && value.present? && file_name.present?
                Admin::ExchangeRate.create(:date => validDate, :value => value, :file_name => file_name)
              end
            end
          rescue ArgumentError
            ActiveRecord::Rollback
            raise CustomExceptions::InvalidDateFormatException
          end 
        end
      rescue ArgumentError
        ActiveRecord::Rollback
        raise CustomExceptions::InvalidDateFormatException, "Existen de fecha formatos incorrectos en el documento, El formato correctos es 'dd/mm/yyyy'" 
      rescue CustomExceptions::InvalidFileException
        ActiveRecord::Rollback
        raise CustomExceptions::InvalidFileException
      rescue Exception => e
        puts e.message
        Logger.new("error", e.message)
        Logger.new("error", e)
        ActiveRecord::Rollback
        raise CustomExceptions::InvalidFileException, "Verifique el archivo de carga, el archivo esta dañado o no es un archivo válido xlsx"
      end
    end
  end
  
  def date_formatter(date)
    Date.parse(date).strftime("%d/%m/%Y")
  end

  private

  def data
    @data
  end

end

