require 'test_helper'

class Facturacion::StocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_stock = facturacion_stocks(:one)
  end

  test "should get index" do
    get facturacion_stocks_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_stock_url
    assert_response :success
  end

  test "should create facturacion_stock" do
    assert_difference('Facturacion::Stock.count') do
      post facturacion_stocks_url, params: { facturacion_stock: { canceled: @facturacion_stock.canceled, document_id: @facturacion_stock.document_id, facturacion_document_type_id: @facturacion_stock.facturacion_document_type_id, facturacion_warehouse_id: @facturacion_stock.facturacion_warehouse_id, quantity: @facturacion_stock.quantity, stock_date: @facturacion_stock.stock_date, unit_cost: @facturacion_stock.unit_cost } }
    end

    assert_redirected_to facturacion_stock_url(Facturacion::Stock.last)
  end

  test "should show facturacion_stock" do
    get facturacion_stock_url(@facturacion_stock)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_stock_url(@facturacion_stock)
    assert_response :success
  end

  test "should update facturacion_stock" do
    patch facturacion_stock_url(@facturacion_stock), params: { facturacion_stock: { canceled: @facturacion_stock.canceled, document_id: @facturacion_stock.document_id, facturacion_document_type_id: @facturacion_stock.facturacion_document_type_id, facturacion_warehouse_id: @facturacion_stock.facturacion_warehouse_id, quantity: @facturacion_stock.quantity, stock_date: @facturacion_stock.stock_date, unit_cost: @facturacion_stock.unit_cost } }
    assert_redirected_to facturacion_stock_url(@facturacion_stock)
  end

  test "should destroy facturacion_stock" do
    assert_difference('Facturacion::Stock.count', -1) do
      delete facturacion_stock_url(@facturacion_stock)
    end

    assert_redirected_to facturacion_stocks_url
  end
end
