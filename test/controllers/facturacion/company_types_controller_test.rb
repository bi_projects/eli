require 'test_helper'

class Facturacion::CompanyTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_company_type = facturacion_company_types(:one)
  end

  test "should get index" do
    get facturacion_company_types_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_company_type_url
    assert_response :success
  end

  test "should create facturacion_company_type" do
    assert_difference('Facturacion::CompanyType.count') do
      post facturacion_company_types_url, params: { facturacion_company_type: { description: @facturacion_company_type.description, name: @facturacion_company_type.name } }
    end

    assert_redirected_to facturacion_company_type_url(Facturacion::CompanyType.last)
  end

  test "should show facturacion_company_type" do
    get facturacion_company_type_url(@facturacion_company_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_company_type_url(@facturacion_company_type)
    assert_response :success
  end

  test "should update facturacion_company_type" do
    patch facturacion_company_type_url(@facturacion_company_type), params: { facturacion_company_type: { description: @facturacion_company_type.description, name: @facturacion_company_type.name } }
    assert_redirected_to facturacion_company_type_url(@facturacion_company_type)
  end

  test "should destroy facturacion_company_type" do
    assert_difference('Facturacion::CompanyType.count', -1) do
      delete facturacion_company_type_url(@facturacion_company_type)
    end

    assert_redirected_to facturacion_company_types_url
  end
end
