require 'test_helper'

class Facturacion::CarriersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_carrier = facturacion_carriers(:one)
  end

  test "should get index" do
    get facturacion_carriers_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_carrier_url
    assert_response :success
  end

  test "should create facturacion_carrier" do
    assert_difference('Facturacion::Carrier.count') do
      post facturacion_carriers_url, params: { facturacion_carrier: { address: @facturacion_carrier.address, contact_name: @facturacion_carrier.contact_name, description: @facturacion_carrier.description, name: @facturacion_carrier.name, phone: @facturacion_carrier.phone, string: @facturacion_carrier.string } }
    end

    assert_redirected_to facturacion_carrier_url(Facturacion::Carrier.last)
  end

  test "should show facturacion_carrier" do
    get facturacion_carrier_url(@facturacion_carrier)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_carrier_url(@facturacion_carrier)
    assert_response :success
  end

  test "should update facturacion_carrier" do
    patch facturacion_carrier_url(@facturacion_carrier), params: { facturacion_carrier: { address: @facturacion_carrier.address, contact_name: @facturacion_carrier.contact_name, description: @facturacion_carrier.description, name: @facturacion_carrier.name, phone: @facturacion_carrier.phone, string: @facturacion_carrier.string } }
    assert_redirected_to facturacion_carrier_url(@facturacion_carrier)
  end

  test "should destroy facturacion_carrier" do
    assert_difference('Facturacion::Carrier.count', -1) do
      delete facturacion_carrier_url(@facturacion_carrier)
    end

    assert_redirected_to facturacion_carriers_url
  end
end
