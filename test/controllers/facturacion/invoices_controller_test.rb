require 'test_helper'

class Facturacion::InvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_invoice = facturacion_invoices(:one)
  end

  test "should get index" do
    get facturacion_invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_invoice_url
    assert_response :success
  end

  test "should create facturacion_invoice" do
    assert_difference('Facturacion::Invoice.count') do
      post facturacion_invoices_url, params: { facturacion_invoice: { academico_level_id: @facturacion_invoice.academico_level_id, academico_program_id: @facturacion_invoice.academico_program_id, bank_amount_usd: @facturacion_invoice.bank_amount_usd, bank_reference_usd: @facturacion_invoice.bank_reference_usd, cash_amount_cordoba: @facturacion_invoice.cash_amount_cordoba, cash_amount_usd: @facturacion_invoice.cash_amount_usd, change_amount_cordoba: @facturacion_invoice.change_amount_cordoba, ck_amount_cordoba: @facturacion_invoice.ck_amount_cordoba, ck_amount_usd: @facturacion_invoice.ck_amount_usd, ck_reference_cordoba: @facturacion_invoice.ck_reference_cordoba, ck_reference_usd: @facturacion_invoice.ck_reference_usd, credit_card_amount_cordoba: @facturacion_invoice.credit_card_amount_cordoba, credit_card_amount_usd: @facturacion_invoice.credit_card_amount_usd, curso: @facturacion_invoice.curso, exchange_rate: @facturacion_invoice.exchange_rate, facturacion_preregistration_id: @facturacion_invoice.facturacion_preregistration_id, invoice_concept: @facturacion_invoice.invoice_concept, invoice_date: @facturacion_invoice.invoice_date, invoice_number: @facturacion_invoice.invoice_number, printed: @facturacion_invoice.printed, retention_amount_cordoba: @facturacion_invoice.retention_amount_cordoba, telepago_amount_usd: @facturacion_invoice.telepago_amount_usd, telepago_reference_usd: @facturacion_invoice.telepago_reference_usd, total_invoice_amount_cordoba: @facturacion_invoice.total_invoice_amount_cordoba, turno: @facturacion_invoice.turno } }
    end

    assert_redirected_to facturacion_invoice_url(Facturacion::Invoice.last)
  end

  test "should show facturacion_invoice" do
    get facturacion_invoice_url(@facturacion_invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_invoice_url(@facturacion_invoice)
    assert_response :success
  end

  test "should update facturacion_invoice" do
    patch facturacion_invoice_url(@facturacion_invoice), params: { facturacion_invoice: { academico_level_id: @facturacion_invoice.academico_level_id, academico_program_id: @facturacion_invoice.academico_program_id, bank_amount_usd: @facturacion_invoice.bank_amount_usd, bank_reference_usd: @facturacion_invoice.bank_reference_usd, cash_amount_cordoba: @facturacion_invoice.cash_amount_cordoba, cash_amount_usd: @facturacion_invoice.cash_amount_usd, change_amount_cordoba: @facturacion_invoice.change_amount_cordoba, ck_amount_cordoba: @facturacion_invoice.ck_amount_cordoba, ck_amount_usd: @facturacion_invoice.ck_amount_usd, ck_reference_cordoba: @facturacion_invoice.ck_reference_cordoba, ck_reference_usd: @facturacion_invoice.ck_reference_usd, credit_card_amount_cordoba: @facturacion_invoice.credit_card_amount_cordoba, credit_card_amount_usd: @facturacion_invoice.credit_card_amount_usd, curso: @facturacion_invoice.curso, exchange_rate: @facturacion_invoice.exchange_rate, facturacion_preregistration_id: @facturacion_invoice.facturacion_preregistration_id, invoice_concept: @facturacion_invoice.invoice_concept, invoice_date: @facturacion_invoice.invoice_date, invoice_number: @facturacion_invoice.invoice_number, printed: @facturacion_invoice.printed, retention_amount_cordoba: @facturacion_invoice.retention_amount_cordoba, telepago_amount_usd: @facturacion_invoice.telepago_amount_usd, telepago_reference_usd: @facturacion_invoice.telepago_reference_usd, total_invoice_amount_cordoba: @facturacion_invoice.total_invoice_amount_cordoba, turno: @facturacion_invoice.turno } }
    assert_redirected_to facturacion_invoice_url(@facturacion_invoice)
  end

  test "should destroy facturacion_invoice" do
    assert_difference('Facturacion::Invoice.count', -1) do
      delete facturacion_invoice_url(@facturacion_invoice)
    end

    assert_redirected_to facturacion_invoices_url
  end
end
