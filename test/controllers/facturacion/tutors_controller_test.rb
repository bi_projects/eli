require 'test_helper'

class Facturacion::TutorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_tutor = facturacion_tutors(:one)
  end

  test "should get index" do
    get facturacion_tutors_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_tutor_url
    assert_response :success
  end

  test "should create facturacion_tutor" do
    assert_difference('Facturacion::Tutor.count') do
      post facturacion_tutors_url, params: { facturacion_tutor: { email: @facturacion_tutor.email, first_name: @facturacion_tutor.first_name, last_name: @facturacion_tutor.last_name, phone: @facturacion_tutor.phone } }
    end

    assert_redirected_to facturacion_tutor_url(Facturacion::Tutor.last)
  end

  test "should show facturacion_tutor" do
    get facturacion_tutor_url(@facturacion_tutor)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_tutor_url(@facturacion_tutor)
    assert_response :success
  end

  test "should update facturacion_tutor" do
    patch facturacion_tutor_url(@facturacion_tutor), params: { facturacion_tutor: { email: @facturacion_tutor.email, first_name: @facturacion_tutor.first_name, last_name: @facturacion_tutor.last_name, phone: @facturacion_tutor.phone } }
    assert_redirected_to facturacion_tutor_url(@facturacion_tutor)
  end

  test "should destroy facturacion_tutor" do
    assert_difference('Facturacion::Tutor.count', -1) do
      delete facturacion_tutor_url(@facturacion_tutor)
    end

    assert_redirected_to facturacion_tutors_url
  end
end
