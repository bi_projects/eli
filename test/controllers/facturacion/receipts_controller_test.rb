require 'test_helper'

class Facturacion::ReceiptsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_receipt = facturacion_receipts(:one)
  end

  test "should get index" do
    get facturacion_receipts_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_receipt_url
    assert_response :success
  end

  test "should create facturacion_receipt" do
    assert_difference('Facturacion::Receipt.count') do
      post facturacion_receipts_url, params: { facturacion_receipt: { admin_branch_office_id: @facturacion_receipt.admin_branch_office_id, canceled: @facturacion_receipt.canceled, canceled_date: @facturacion_receipt.canceled_date, comments: @facturacion_receipt.comments, facturacion_preregistration_id: @facturacion_receipt.facturacion_preregistration_id, printed: @facturacion_receipt.printed, receipt_date: @facturacion_receipt.receipt_date, receipt_number: @facturacion_receipt.receipt_number } }
    end

    assert_redirected_to facturacion_receipt_url(Facturacion::Receipt.last)
  end

  test "should show facturacion_receipt" do
    get facturacion_receipt_url(@facturacion_receipt)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_receipt_url(@facturacion_receipt)
    assert_response :success
  end

  test "should update facturacion_receipt" do
    patch facturacion_receipt_url(@facturacion_receipt), params: { facturacion_receipt: { admin_branch_office_id: @facturacion_receipt.admin_branch_office_id, canceled: @facturacion_receipt.canceled, canceled_date: @facturacion_receipt.canceled_date, comments: @facturacion_receipt.comments, facturacion_preregistration_id: @facturacion_receipt.facturacion_preregistration_id, printed: @facturacion_receipt.printed, receipt_date: @facturacion_receipt.receipt_date, receipt_number: @facturacion_receipt.receipt_number } }
    assert_redirected_to facturacion_receipt_url(@facturacion_receipt)
  end

  test "should destroy facturacion_receipt" do
    assert_difference('Facturacion::Receipt.count', -1) do
      delete facturacion_receipt_url(@facturacion_receipt)
    end

    assert_redirected_to facturacion_receipts_url
  end
end
