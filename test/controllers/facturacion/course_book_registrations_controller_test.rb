require 'test_helper'

class Facturacion::CourseBookRegistrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_course_book_registration = facturacion_course_book_registrations(:one)
  end

  test "should get index" do
    get facturacion_course_book_registrations_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_course_book_registration_url
    assert_response :success
  end

  test "should create facturacion_course_book_registration" do
    assert_difference('Facturacion::CourseBookRegistration.count') do
      post facturacion_course_book_registrations_url, params: { facturacion_course_book_registration: { academico_course_id: @facturacion_course_book_registration.academico_course_id, book_price: @facturacion_course_book_registration.book_price, registration_price: @facturacion_course_book_registration.registration_price } }
    end

    assert_redirected_to facturacion_course_book_registration_url(Facturacion::CourseBookRegistration.last)
  end

  test "should show facturacion_course_book_registration" do
    get facturacion_course_book_registration_url(@facturacion_course_book_registration)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_course_book_registration_url(@facturacion_course_book_registration)
    assert_response :success
  end

  test "should update facturacion_course_book_registration" do
    patch facturacion_course_book_registration_url(@facturacion_course_book_registration), params: { facturacion_course_book_registration: { academico_course_id: @facturacion_course_book_registration.academico_course_id, book_price: @facturacion_course_book_registration.book_price, registration_price: @facturacion_course_book_registration.registration_price } }
    assert_redirected_to facturacion_course_book_registration_url(@facturacion_course_book_registration)
  end

  test "should destroy facturacion_course_book_registration" do
    assert_difference('Facturacion::CourseBookRegistration.count', -1) do
      delete facturacion_course_book_registration_url(@facturacion_course_book_registration)
    end

    assert_redirected_to facturacion_course_book_registrations_url
  end
end
