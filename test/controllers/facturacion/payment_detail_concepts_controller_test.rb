require 'test_helper'

class Facturacion::PaymentDetailConceptsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_payment_detail_concept = facturacion_payment_detail_concepts(:one)
  end

  test "should get index" do
    get facturacion_payment_detail_concepts_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_payment_detail_concept_url
    assert_response :success
  end

  test "should create facturacion_payment_detail_concept" do
    assert_difference('Facturacion::PaymentDetailConcept.count') do
      post facturacion_payment_detail_concepts_url, params: { facturacion_payment_detail_concept: { description: @facturacion_payment_detail_concept.description, is_book: @facturacion_payment_detail_concept.is_book, name: @facturacion_payment_detail_concept.name, price: @facturacion_payment_detail_concept.price } }
    end

    assert_redirected_to facturacion_payment_detail_concept_url(Facturacion::PaymentDetailConcept.last)
  end

  test "should show facturacion_payment_detail_concept" do
    get facturacion_payment_detail_concept_url(@facturacion_payment_detail_concept)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_payment_detail_concept_url(@facturacion_payment_detail_concept)
    assert_response :success
  end

  test "should update facturacion_payment_detail_concept" do
    patch facturacion_payment_detail_concept_url(@facturacion_payment_detail_concept), params: { facturacion_payment_detail_concept: { description: @facturacion_payment_detail_concept.description, is_book: @facturacion_payment_detail_concept.is_book, name: @facturacion_payment_detail_concept.name, price: @facturacion_payment_detail_concept.price } }
    assert_redirected_to facturacion_payment_detail_concept_url(@facturacion_payment_detail_concept)
  end

  test "should destroy facturacion_payment_detail_concept" do
    assert_difference('Facturacion::PaymentDetailConcept.count', -1) do
      delete facturacion_payment_detail_concept_url(@facturacion_payment_detail_concept)
    end

    assert_redirected_to facturacion_payment_detail_concepts_url
  end
end
