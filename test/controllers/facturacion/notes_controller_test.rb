require 'test_helper'

class Facturacion::NotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_note = facturacion_notes(:one)
  end

  test "should get index" do
    get facturacion_notes_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_note_url
    assert_response :success
  end

  test "should create facturacion_note" do
    assert_difference('Facturacion::Note.count') do
      post facturacion_notes_url, params: { facturacion_note: { description: @facturacion_note.description, facturacion_preregistration_id: @facturacion_note.facturacion_preregistration_id, inatec: @facturacion_note.inatec, note_date: @facturacion_note.note_date, note_number: @facturacion_note.note_number, total_note: @facturacion_note.total_note } }
    end

    assert_redirected_to facturacion_note_url(Facturacion::Note.last)
  end

  test "should show facturacion_note" do
    get facturacion_note_url(@facturacion_note)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_note_url(@facturacion_note)
    assert_response :success
  end

  test "should update facturacion_note" do
    patch facturacion_note_url(@facturacion_note), params: { facturacion_note: { description: @facturacion_note.description, facturacion_preregistration_id: @facturacion_note.facturacion_preregistration_id, inatec: @facturacion_note.inatec, note_date: @facturacion_note.note_date, note_number: @facturacion_note.note_number, total_note: @facturacion_note.total_note } }
    assert_redirected_to facturacion_note_url(@facturacion_note)
  end

  test "should destroy facturacion_note" do
    assert_difference('Facturacion::Note.count', -1) do
      delete facturacion_note_url(@facturacion_note)
    end

    assert_redirected_to facturacion_notes_url
  end
end
