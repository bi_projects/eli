require 'test_helper'

class Facturacion::PreregistrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_preregistration = facturacion_preregistrations(:one)
  end

  test "should get index" do
    get facturacion_preregistrations_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_preregistration_url
    assert_response :success
  end

  test "should create facturacion_preregistration" do
    assert_difference('Facturacion::Preregistration.count') do
      post facturacion_preregistrations_url, params: { facturacion_preregistration: { birth_date: @facturacion_preregistration.birth_date, first_name: @facturacion_preregistration.first_name, last_name: @facturacion_preregistration.last_name } }
    end

    assert_redirected_to facturacion_preregistration_url(Facturacion::Preregistration.last)
  end

  test "should show facturacion_preregistration" do
    get facturacion_preregistration_url(@facturacion_preregistration)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_preregistration_url(@facturacion_preregistration)
    assert_response :success
  end

  test "should update facturacion_preregistration" do
    patch facturacion_preregistration_url(@facturacion_preregistration), params: { facturacion_preregistration: { birth_date: @facturacion_preregistration.birth_date, first_name: @facturacion_preregistration.first_name, last_name: @facturacion_preregistration.last_name } }
    assert_redirected_to facturacion_preregistration_url(@facturacion_preregistration)
  end

  test "should destroy facturacion_preregistration" do
    assert_difference('Facturacion::Preregistration.count', -1) do
      delete facturacion_preregistration_url(@facturacion_preregistration)
    end

    assert_redirected_to facturacion_preregistrations_url
  end
end
