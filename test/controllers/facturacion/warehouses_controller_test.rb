require 'test_helper'

class Facturacion::WarehousesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_warehouse = facturacion_warehouses(:one)
  end

  test "should get index" do
    get facturacion_warehouses_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_warehouse_url
    assert_response :success
  end

  test "should create facturacion_warehouse" do
    assert_difference('Facturacion::Warehouse.count') do
      post facturacion_warehouses_url, params: { facturacion_warehouse: { address: @facturacion_warehouse.address, description: @facturacion_warehouse.description, facturacion_cost_center_id: @facturacion_warehouse.facturacion_cost_center_id, name: @facturacion_warehouse.name, principal: @facturacion_warehouse.principal } }
    end

    assert_redirected_to facturacion_warehouse_url(Facturacion::Warehouse.last)
  end

  test "should show facturacion_warehouse" do
    get facturacion_warehouse_url(@facturacion_warehouse)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_warehouse_url(@facturacion_warehouse)
    assert_response :success
  end

  test "should update facturacion_warehouse" do
    patch facturacion_warehouse_url(@facturacion_warehouse), params: { facturacion_warehouse: { address: @facturacion_warehouse.address, description: @facturacion_warehouse.description, facturacion_cost_center_id: @facturacion_warehouse.facturacion_cost_center_id, name: @facturacion_warehouse.name, principal: @facturacion_warehouse.principal } }
    assert_redirected_to facturacion_warehouse_url(@facturacion_warehouse)
  end

  test "should destroy facturacion_warehouse" do
    assert_difference('Facturacion::Warehouse.count', -1) do
      delete facturacion_warehouse_url(@facturacion_warehouse)
    end

    assert_redirected_to facturacion_warehouses_url
  end
end
