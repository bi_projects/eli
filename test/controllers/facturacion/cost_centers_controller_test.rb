require 'test_helper'

class Facturacion::CostCentersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_cost_center = facturacion_cost_centers(:one)
  end

  test "should get index" do
    get facturacion_cost_centers_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_cost_center_url
    assert_response :success
  end

  test "should create facturacion_cost_center" do
    assert_difference('Facturacion::CostCenter.count') do
      post facturacion_cost_centers_url, params: { facturacion_cost_center: { code: @facturacion_cost_center.code, description: @facturacion_cost_center.description, name: @facturacion_cost_center.name } }
    end

    assert_redirected_to facturacion_cost_center_url(Facturacion::CostCenter.last)
  end

  test "should show facturacion_cost_center" do
    get facturacion_cost_center_url(@facturacion_cost_center)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_cost_center_url(@facturacion_cost_center)
    assert_response :success
  end

  test "should update facturacion_cost_center" do
    patch facturacion_cost_center_url(@facturacion_cost_center), params: { facturacion_cost_center: { code: @facturacion_cost_center.code, description: @facturacion_cost_center.description, name: @facturacion_cost_center.name } }
    assert_redirected_to facturacion_cost_center_url(@facturacion_cost_center)
  end

  test "should destroy facturacion_cost_center" do
    assert_difference('Facturacion::CostCenter.count', -1) do
      delete facturacion_cost_center_url(@facturacion_cost_center)
    end

    assert_redirected_to facturacion_cost_centers_url
  end
end
