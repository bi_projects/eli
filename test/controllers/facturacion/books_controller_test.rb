require 'test_helper'

class Facturacion::BooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_book = facturacion_books(:one)
  end

  test "should get index" do
    get facturacion_books_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_book_url
    assert_response :success
  end

  test "should create facturacion_book" do
    assert_difference('Facturacion::Book.count') do
      post facturacion_books_url, params: { facturacion_book: { description: @facturacion_book.description, min_stock: @facturacion_book.min_stock, name: @facturacion_book.name, price: @facturacion_book.price, quantity: @facturacion_book.quantity } }
    end

    assert_redirected_to facturacion_book_url(Facturacion::Book.last)
  end

  test "should show facturacion_book" do
    get facturacion_book_url(@facturacion_book)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_book_url(@facturacion_book)
    assert_response :success
  end

  test "should update facturacion_book" do
    patch facturacion_book_url(@facturacion_book), params: { facturacion_book: { description: @facturacion_book.description, min_stock: @facturacion_book.min_stock, name: @facturacion_book.name, price: @facturacion_book.price, quantity: @facturacion_book.quantity } }
    assert_redirected_to facturacion_book_url(@facturacion_book)
  end

  test "should destroy facturacion_book" do
    assert_difference('Facturacion::Book.count', -1) do
      delete facturacion_book_url(@facturacion_book)
    end

    assert_redirected_to facturacion_books_url
  end
end
