require 'test_helper'

class Facturacion::DiscountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_discount = facturacion_discounts(:one)
  end

  test "should get index" do
    get facturacion_discounts_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_discount_url
    assert_response :success
  end

  test "should create facturacion_discount" do
    assert_difference('Facturacion::Discount.count') do
      post facturacion_discounts_url, params: { facturacion_discount: { description: @facturacion_discount.description, discount_amount: @facturacion_discount.discount_amount, name: @facturacion_discount.name } }
    end

    assert_redirected_to facturacion_discount_url(Facturacion::Discount.last)
  end

  test "should show facturacion_discount" do
    get facturacion_discount_url(@facturacion_discount)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_discount_url(@facturacion_discount)
    assert_response :success
  end

  test "should update facturacion_discount" do
    patch facturacion_discount_url(@facturacion_discount), params: { facturacion_discount: { description: @facturacion_discount.description, discount_amount: @facturacion_discount.discount_amount, name: @facturacion_discount.name } }
    assert_redirected_to facturacion_discount_url(@facturacion_discount)
  end

  test "should destroy facturacion_discount" do
    assert_difference('Facturacion::Discount.count', -1) do
      delete facturacion_discount_url(@facturacion_discount)
    end

    assert_redirected_to facturacion_discounts_url
  end
end
