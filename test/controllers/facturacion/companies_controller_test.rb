require 'test_helper'

class Facturacion::CompaniesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_company = facturacion_companies(:one)
  end

  test "should get index" do
    get facturacion_companies_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_company_url
    assert_response :success
  end

  test "should create facturacion_company" do
    assert_difference('Facturacion::Company.count') do
      post facturacion_companies_url, params: { facturacion_company: { company_description: @facturacion_company.company_description, contact_name: @facturacion_company.contact_name, email: @facturacion_company.email, facturacion_company_type_id: @facturacion_company.facturacion_company_type_id, name: @facturacion_company.name, phone: @facturacion_company.phone } }
    end

    assert_redirected_to facturacion_company_url(Facturacion::Company.last)
  end

  test "should show facturacion_company" do
    get facturacion_company_url(@facturacion_company)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_company_url(@facturacion_company)
    assert_response :success
  end

  test "should update facturacion_company" do
    patch facturacion_company_url(@facturacion_company), params: { facturacion_company: { company_description: @facturacion_company.company_description, contact_name: @facturacion_company.contact_name, email: @facturacion_company.email, facturacion_company_type_id: @facturacion_company.facturacion_company_type_id, name: @facturacion_company.name, phone: @facturacion_company.phone } }
    assert_redirected_to facturacion_company_url(@facturacion_company)
  end

  test "should destroy facturacion_company" do
    assert_difference('Facturacion::Company.count', -1) do
      delete facturacion_company_url(@facturacion_company)
    end

    assert_redirected_to facturacion_companies_url
  end
end
