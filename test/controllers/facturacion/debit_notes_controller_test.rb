require 'test_helper'

class Facturacion::DebitNotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_debit_note = facturacion_debit_notes(:one)
  end

  test "should get index" do
    get facturacion_debit_notes_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_debit_note_url
    assert_response :success
  end

  test "should create facturacion_debit_note" do
    assert_difference('Facturacion::DebitNote.count') do
      post facturacion_debit_notes_url, params: { facturacion_debit_note: {  } }
    end

    assert_redirected_to facturacion_debit_note_url(Facturacion::DebitNote.last)
  end

  test "should show facturacion_debit_note" do
    get facturacion_debit_note_url(@facturacion_debit_note)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_debit_note_url(@facturacion_debit_note)
    assert_response :success
  end

  test "should update facturacion_debit_note" do
    patch facturacion_debit_note_url(@facturacion_debit_note), params: { facturacion_debit_note: {  } }
    assert_redirected_to facturacion_debit_note_url(@facturacion_debit_note)
  end

  test "should destroy facturacion_debit_note" do
    assert_difference('Facturacion::DebitNote.count', -1) do
      delete facturacion_debit_note_url(@facturacion_debit_note)
    end

    assert_redirected_to facturacion_debit_notes_url
  end
end
