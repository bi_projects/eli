require 'test_helper'

class Facturacion::DocumentTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_document_type = facturacion_document_types(:one)
  end

  test "should get index" do
    get facturacion_document_types_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_document_type_url
    assert_response :success
  end

  test "should create facturacion_document_type" do
    assert_difference('Facturacion::DocumentType.count') do
      post facturacion_document_types_url, params: { facturacion_document_type: { name: @facturacion_document_type.name } }
    end

    assert_redirected_to facturacion_document_type_url(Facturacion::DocumentType.last)
  end

  test "should show facturacion_document_type" do
    get facturacion_document_type_url(@facturacion_document_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_document_type_url(@facturacion_document_type)
    assert_response :success
  end

  test "should update facturacion_document_type" do
    patch facturacion_document_type_url(@facturacion_document_type), params: { facturacion_document_type: { name: @facturacion_document_type.name } }
    assert_redirected_to facturacion_document_type_url(@facturacion_document_type)
  end

  test "should destroy facturacion_document_type" do
    assert_difference('Facturacion::DocumentType.count', -1) do
      delete facturacion_document_type_url(@facturacion_document_type)
    end

    assert_redirected_to facturacion_document_types_url
  end
end
