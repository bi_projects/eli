require 'test_helper'

class Academico::LevelProgramBooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_level_program_book = academico_level_program_books(:one)
  end

  test "should get index" do
    get academico_level_program_books_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_level_program_book_url
    assert_response :success
  end

  test "should create academico_level_program_book" do
    assert_difference('Academico::LevelProgramBook.count') do
      post academico_level_program_books_url, params: { academico_level_program_book: { academico_level_id: @academico_level_program_book.academico_level_id, academico_program_id: @academico_level_program_book.academico_program_id, facturacion_book_id: @academico_level_program_book.facturacion_book_id } }
    end

    assert_redirected_to academico_level_program_book_url(Academico::LevelProgramBook.last)
  end

  test "should show academico_level_program_book" do
    get academico_level_program_book_url(@academico_level_program_book)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_level_program_book_url(@academico_level_program_book)
    assert_response :success
  end

  test "should update academico_level_program_book" do
    patch academico_level_program_book_url(@academico_level_program_book), params: { academico_level_program_book: { academico_level_id: @academico_level_program_book.academico_level_id, academico_program_id: @academico_level_program_book.academico_program_id, facturacion_book_id: @academico_level_program_book.facturacion_book_id } }
    assert_redirected_to academico_level_program_book_url(@academico_level_program_book)
  end

  test "should destroy academico_level_program_book" do
    assert_difference('Academico::LevelProgramBook.count', -1) do
      delete academico_level_program_book_url(@academico_level_program_book)
    end

    assert_redirected_to academico_level_program_books_url
  end
end
