require 'test_helper'

class Academico::LevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_level = academico_levels(:one)
  end

  test "should get index" do
    get academico_levels_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_level_url
    assert_response :success
  end

  test "should create academico_level" do
    assert_difference('Academico::Level.count') do
      post academico_levels_url, params: { academico_level: { description: @academico_level.description, name: @academico_level.name } }
    end

    assert_redirected_to academico_level_url(Academico::Level.last)
  end

  test "should show academico_level" do
    get academico_level_url(@academico_level)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_level_url(@academico_level)
    assert_response :success
  end

  test "should update academico_level" do
    patch academico_level_url(@academico_level), params: { academico_level: { description: @academico_level.description, name: @academico_level.name } }
    assert_redirected_to academico_level_url(@academico_level)
  end

  test "should destroy academico_level" do
    assert_difference('Academico::Level.count', -1) do
      delete academico_level_url(@academico_level)
    end

    assert_redirected_to academico_levels_url
  end
end
