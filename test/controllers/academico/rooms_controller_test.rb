require 'test_helper'

class Academico::RoomsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_room = academico_rooms(:one)
  end

  test "should get index" do
    get academico_rooms_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_room_url
    assert_response :success
  end

  test "should create academico_room" do
    assert_difference('Academico::Room.count') do
      post academico_rooms_url, params: { academico_room: { description: @academico_room.description, name: @academico_room.name, size: @academico_room.size } }
    end

    assert_redirected_to academico_room_url(Academico::Room.last)
  end

  test "should show academico_room" do
    get academico_room_url(@academico_room)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_room_url(@academico_room)
    assert_response :success
  end

  test "should update academico_room" do
    patch academico_room_url(@academico_room), params: { academico_room: { description: @academico_room.description, name: @academico_room.name, size: @academico_room.size } }
    assert_redirected_to academico_room_url(@academico_room)
  end

  test "should destroy academico_room" do
    assert_difference('Academico::Room.count', -1) do
      delete academico_room_url(@academico_room)
    end

    assert_redirected_to academico_rooms_url
  end
end
