require 'test_helper'

class Academico::ProgramsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_program = academico_programs(:one)
  end

  test "should get index" do
    get academico_programs_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_program_url
    assert_response :success
  end

  test "should create academico_program" do
    assert_difference('Academico::Program.count') do
      post academico_programs_url, params: { academico_program: { academico_age_range_id: @academico_program.academico_age_range_id, description: @academico_program.description, name: @academico_program.name, price: @academico_program.price } }
    end

    assert_redirected_to academico_program_url(Academico::Program.last)
  end

  test "should show academico_program" do
    get academico_program_url(@academico_program)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_program_url(@academico_program)
    assert_response :success
  end

  test "should update academico_program" do
    patch academico_program_url(@academico_program), params: { academico_program: { academico_age_range_id: @academico_program.academico_age_range_id, description: @academico_program.description, name: @academico_program.name, price: @academico_program.price } }
    assert_redirected_to academico_program_url(@academico_program)
  end

  test "should destroy academico_program" do
    assert_difference('Academico::Program.count', -1) do
      delete academico_program_url(@academico_program)
    end

    assert_redirected_to academico_programs_url
  end
end
