require 'test_helper'

class Academico::AgeRangesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_age_range = academico_age_ranges(:one)
  end

  test "should get index" do
    get academico_age_ranges_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_age_range_url
    assert_response :success
  end

  test "should create academico_age_range" do
    assert_difference('Academico::AgeRange.count') do
      post academico_age_ranges_url, params: { academico_age_range: { description: @academico_age_range.description, max_age: @academico_age_range.max_age, min_age: @academico_age_range.min_age, name: @academico_age_range.name } }
    end

    assert_redirected_to academico_age_range_url(Academico::AgeRange.last)
  end

  test "should show academico_age_range" do
    get academico_age_range_url(@academico_age_range)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_age_range_url(@academico_age_range)
    assert_response :success
  end

  test "should update academico_age_range" do
    patch academico_age_range_url(@academico_age_range), params: { academico_age_range: { description: @academico_age_range.description, max_age: @academico_age_range.max_age, min_age: @academico_age_range.min_age, name: @academico_age_range.name } }
    assert_redirected_to academico_age_range_url(@academico_age_range)
  end

  test "should destroy academico_age_range" do
    assert_difference('Academico::AgeRange.count', -1) do
      delete academico_age_range_url(@academico_age_range)
    end

    assert_redirected_to academico_age_ranges_url
  end
end
