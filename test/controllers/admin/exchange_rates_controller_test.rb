require 'test_helper'

class Admin::ExchangeRatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_exchange_rate = admin_exchange_rates(:one)
  end

  test "should get index" do
    get admin_exchange_rates_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_exchange_rate_url
    assert_response :success
  end

  test "should create admin_exchange_rate" do
    assert_difference('Admin::ExchangeRate.count') do
      post admin_exchange_rates_url, params: { admin_exchange_rate: { date: @admin_exchange_rate.date, value: @admin_exchange_rate.value } }
    end

    assert_redirected_to admin_exchange_rate_url(Admin::ExchangeRate.last)
  end

  test "should show admin_exchange_rate" do
    get admin_exchange_rate_url(@admin_exchange_rate)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_exchange_rate_url(@admin_exchange_rate)
    assert_response :success
  end

  test "should update admin_exchange_rate" do
    patch admin_exchange_rate_url(@admin_exchange_rate), params: { admin_exchange_rate: { date: @admin_exchange_rate.date, value: @admin_exchange_rate.value } }
    assert_redirected_to admin_exchange_rate_url(@admin_exchange_rate)
  end

  test "should destroy admin_exchange_rate" do
    assert_difference('Admin::ExchangeRate.count', -1) do
      delete admin_exchange_rate_url(@admin_exchange_rate)
    end

    assert_redirected_to admin_exchange_rates_url
  end
end
