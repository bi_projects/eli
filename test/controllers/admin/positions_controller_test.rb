require 'test_helper'

class Admin::PositionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_position = admin_positions(:one)
  end

  test "should get index" do
    get admin_positions_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_position_url
    assert_response :success
  end

  test "should create admin_position" do
    assert_difference('Admin::Position.count') do
      post admin_positions_url, params: { admin_position: { description: @admin_position.description, name: @admin_position.name } }
    end

    assert_redirected_to admin_position_url(Admin::Position.last)
  end

  test "should show admin_position" do
    get admin_position_url(@admin_position)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_position_url(@admin_position)
    assert_response :success
  end

  test "should update admin_position" do
    patch admin_position_url(@admin_position), params: { admin_position: { description: @admin_position.description, name: @admin_position.name } }
    assert_redirected_to admin_position_url(@admin_position)
  end

  test "should destroy admin_position" do
    assert_difference('Admin::Position.count', -1) do
      delete admin_position_url(@admin_position)
    end

    assert_redirected_to admin_positions_url
  end
end
