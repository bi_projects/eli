require 'test_helper'

class Admin::BillingTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_billing_transaction = admin_billing_transactions(:one)
  end

  test "should get index" do
    get admin_billing_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_billing_transaction_url
    assert_response :success
  end

  test "should create admin_billing_transaction" do
    assert_difference('Admin::BillingTransaction.count') do
      post admin_billing_transactions_url, params: { admin_billing_transaction: { date: @admin_billing_transaction.date, description: @admin_billing_transaction.description, document_id: @admin_billing_transaction.document_id, document_number: @admin_billing_transaction.document_number, facturacion_document_type_id: @admin_billing_transaction.facturacion_document_type_id, transaction_amount: @admin_billing_transaction.transaction_amount } }
    end

    assert_redirected_to admin_billing_transaction_url(Admin::BillingTransaction.last)
  end

  test "should show admin_billing_transaction" do
    get admin_billing_transaction_url(@admin_billing_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_billing_transaction_url(@admin_billing_transaction)
    assert_response :success
  end

  test "should update admin_billing_transaction" do
    patch admin_billing_transaction_url(@admin_billing_transaction), params: { admin_billing_transaction: { date: @admin_billing_transaction.date, description: @admin_billing_transaction.description, document_id: @admin_billing_transaction.document_id, document_number: @admin_billing_transaction.document_number, facturacion_document_type_id: @admin_billing_transaction.facturacion_document_type_id, transaction_amount: @admin_billing_transaction.transaction_amount } }
    assert_redirected_to admin_billing_transaction_url(@admin_billing_transaction)
  end

  test "should destroy admin_billing_transaction" do
    assert_difference('Admin::BillingTransaction.count', -1) do
      delete admin_billing_transaction_url(@admin_billing_transaction)
    end

    assert_redirected_to admin_billing_transactions_url
  end
end
