require 'test_helper'

class Admin::AcademicUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @academico_academic_user = academico_academic_users(:one)
  end

  test "should get index" do
    get academico_academic_users_url
    assert_response :success
  end

  test "should get new" do
    get new_academico_academic_user_url
    assert_response :success
  end

  test "should create academico_academic_user" do
    assert_difference('Academico::AcademicUser.count') do
      post academico_academic_users_url, params: { academico_academic_user: {  } }
    end

    assert_redirected_to academico_academic_user_url(Academico::AcademicUser.last)
  end

  test "should show academico_academic_user" do
    get academico_academic_user_url(@academico_academic_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_academico_academic_user_url(@academico_academic_user)
    assert_response :success
  end

  test "should update academico_academic_user" do
    patch academico_academic_user_url(@academico_academic_user), params: { academico_academic_user: {  } }
    assert_redirected_to academico_academic_user_url(@academico_academic_user)
  end

  test "should destroy academico_academic_user" do
    assert_difference('Academico::AcademicUser.count', -1) do
      delete academico_academic_user_url(@academico_academic_user)
    end

    assert_redirected_to academico_academic_users_url
  end
end
