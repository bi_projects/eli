require 'test_helper'

class Admin::BillingUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facturacion_billing_user = facturacion_billing_users(:one)
  end

  test "should get index" do
    get facturacion_billing_users_url
    assert_response :success
  end

  test "should get new" do
    get new_facturacion_billing_user_url
    assert_response :success
  end

  test "should create facturacion_billing_user" do
    assert_difference('Facturacion::BillingUser.count') do
      post facturacion_billing_users_url, params: { facturacion_billing_user: {  } }
    end

    assert_redirected_to facturacion_billing_user_url(Facturacion::BillingUser.last)
  end

  test "should show facturacion_billing_user" do
    get facturacion_billing_user_url(@facturacion_billing_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_facturacion_billing_user_url(@facturacion_billing_user)
    assert_response :success
  end

  test "should update facturacion_billing_user" do
    patch facturacion_billing_user_url(@facturacion_billing_user), params: { facturacion_billing_user: {  } }
    assert_redirected_to facturacion_billing_user_url(@facturacion_billing_user)
  end

  test "should destroy facturacion_billing_user" do
    assert_difference('Facturacion::BillingUser.count', -1) do
      delete facturacion_billing_user_url(@facturacion_billing_user)
    end

    assert_redirected_to facturacion_billing_users_url
  end
end
