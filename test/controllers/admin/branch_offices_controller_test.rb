require 'test_helper'

class Admin::BranchOfficesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_branch_office = admin_branch_offices(:one)
  end

  test "should get index" do
    get admin_branch_offices_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_branch_office_url
    assert_response :success
  end

  test "should create admin_branch_office" do
    assert_difference('Admin::BranchOffice.count') do
      post admin_branch_offices_url, params: { admin_branch_office: { description: @admin_branch_office.description, name: @admin_branch_office.name } }
    end

    assert_redirected_to admin_branch_office_url(Admin::BranchOffice.last)
  end

  test "should show admin_branch_office" do
    get admin_branch_office_url(@admin_branch_office)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_branch_office_url(@admin_branch_office)
    assert_response :success
  end

  test "should update admin_branch_office" do
    patch admin_branch_office_url(@admin_branch_office), params: { admin_branch_office: { description: @admin_branch_office.description, name: @admin_branch_office.name } }
    assert_redirected_to admin_branch_office_url(@admin_branch_office)
  end

  test "should destroy admin_branch_office" do
    assert_difference('Admin::BranchOffice.count', -1) do
      delete admin_branch_office_url(@admin_branch_office)
    end

    assert_redirected_to admin_branch_offices_url
  end
end
